#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-08-09

import pyvisa as visa
import csv
import sys
import time
import math
import json
import os
import shutil
import datetime

import Thermotron8200PlusInterface as TEMPERATURE

PERIOD = 10 #minutes
HEADER = ['Time', 'Temperature', 'Frequency', 'NF', 'Gain', 'averageNF', 'averageGain', 'amplitude_response_10MHz', 'amplitude_response_120MHz', 'amplitude_response_500MHz', 'amplitude_response_1000MHz', 'amplitude_response_10MHz_max', 'amplitude_response_120MHz_max', 'amplitude_response_500MHz_max', 'amplitude_response_1000MHz_max', 'min_NF', 'max_NF', 'min_Gain', 'max_Gain']


## Equipment Addresses ##
SA_VISA = 'TCPIP0::10.0.10.184::hislip0::INSTR'
TChamber_VISA = 'TCPIP0::10.0.10.151::8888::SOCKET'
rm = visa.ResourceManager()
SA = rm.open_resource(SA_VISA)
SA.timeout = 10000
TChamber = rm.open_resource(TChamber_VISA)
TChamber.timeout = 10000

def SA_Screenshot(productNumber,time_value):     
    #SA.write(":DISPlay:FSCReen:STATe 1") 
    filename = productNumber + '_' + time_value + '.PNG'   
    #temporary storage location (becasue MXA cannot save to Production folder)
    SA_save_path = "N:\\" 
    #saving a screenshot on the equipment
    SA.write(":MMEMory:STORe:SCReen '" + SA_save_path + filename + "'")
    time.sleep(1)   
    ## Move screenshots to proper folder
    path = os.path.dirname(__file__)
    shutil.move(SA_save_path + filename, path)

def Wait(resource): 
    #This function is used to wait for a resource to complete its operation
    #Poll the Event Status Register (ESR) every second to determine if the operation is complete.
   resource.write('*OPC')
   try:
       while(int(resource.query('*ESR?')) != 1):
           time.sleep(1)
   except KeyboardInterrupt:
       print('Keyboard interrupt detected. Stopping program')
       sys.exit()

def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%b%d_%H%M%S")
    return now

def CSV_Init(filename, header):
    if filename == '':
        filename = 'No_Name'
    name = input('Tester name:')
    global workbook, FILENAME
    current_time = timestamp()
    FILENAME = filename + '_' + current_time[1] + '.csv'
    with open(FILENAME, 'w', newline='') as f:
        workbook = csv.writer(f)
        workbook.writerow(['Tester:', name])
        workbook.writerow(['Unit S/N:', filename])
        workbook.writerow(['Date:', current_time[0]])
        workbook.writerow([])
        workbook.writerow(header)
        
def get_NFG(): 

    print('Start Measurement:', timestamp()[0])
    
    settings = {}
    settings['nf_gain_interested_start_Frequency'] = 2200000000
    settings['nf_gain_interested_stop_Frequency'] = 3100000000
    
    NFG = {'Frequency': [],'NF' : [],'Gain' : [],'averageNF' : None,'averageGain' : None,'amplitude_response_10MHz' : None,'amplitude_response_120MHz' : None,'amplitude_response_500MHz' : None,'amplitude_response_1000MHz' : None,'amplitude_response_10MHz_max' : None,'amplitude_response_120MHz_max' : None,'amplitude_response_500MHz_max' : None,'amplitude_response_1000MHz_max' : None,'min_NF' : None,'max_NF' : None,'min_Gain' : None,'max_Gain' : None}
       
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write('INITiate:RESTart')
    Wait(SA)

    

    #Aquire start and stop frequencies, and number of points
    freq_start = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')[0])
    freq_stop = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')[0])
    number_of_points = float(SA.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')[0])

    #Saving frequency data
    NFG['Frequency'] = []
    NFG['Frequency'].append(freq_start)
    add_next_Freq=((freq_stop-freq_start)/(number_of_points-1))
    i = 1
    next_Frequency = freq_start
    while i < number_of_points-1:
        next_Frequency = next_Frequency + add_next_Freq
        NFG['Frequency'].append(next_Frequency)
        i += 1   
    NFG['Frequency'].append(freq_stop)

    #Finding the index of the interested start and stop frequencies 
    i = 0
    flag = 0
    interested_freq = {}
    while i < len(NFG['Frequency']):
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_start_Frequency'])) and flag == 0:
            interested_freq['start'] = i
            flag = 1
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_stop_Frequency'])):
            interested_freq['stop'] = i
            break
        i += 1

    #Fetch NF array and calculate the min and max NF over the interested frequencies
    NFG['NF'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:NFIG?')
    i = interested_freq['start']
    interested_NF = []
    while i <= interested_freq['stop']:
        interested_NF.append(NFG['NF'][i])
        i += 1
    NFG['min_NF'] = min(interested_NF)
    NFG['max_NF'] = max(interested_NF)
    print ("Minimum NF: %f" %NFG['min_NF'])
    print ("Maximum NF: %f" %NFG['max_NF'])
    
    #calculating the linear noise figure values (only for interested frequencies)
    i = interested_freq['start']
    LinearNF = []
    while i <= interested_freq['stop']:
        LinearNF.append(10**(NFG['NF'][i]/10))
        i += 1
    
    #calculating the average noise figure by taking the average of the linear noise figure values and then using math to get the average noise figure (only for interested frequencies)
    i = 0
    sumLinearNF  = 0
    while i < len(LinearNF):
        sumLinearNF += LinearNF[i]
        i += 1  
    averageLinearNF = sumLinearNF / len(LinearNF)
    NFG['averageNF'] = 10 * math.log10(averageLinearNF)
    print ("Average NF: %f" %NFG['averageNF'])

    #Fetch Gain array and calculate the min and max Gain over the interested frequencies
    NFG['Gain'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
    i = interested_freq['start']
    interested_Gain = []
    while i <= interested_freq['stop']:
        interested_Gain.append(NFG['Gain'][i])
        i += 1
    NFG['min_Gain'] = min(interested_Gain)
    NFG['max_Gain'] = max(interested_Gain)
    print ("Minimum Gain: %f" %NFG['min_Gain'])
    print ("Maximum Gain: %f" %NFG['max_Gain'])
    
    #calculating the linear gain values (only for interested frequencies)
    i = interested_freq['start']
    LinearGain = []
    while i <= interested_freq['stop']:
        LinearGain.append(10**(NFG['Gain'][i]/10))
        i += 1
        
    #calculating the average gain by taking the average of the linear gain values and then using math to get the average gain (only for interested frequencies)
    i = 0 
    sumLinearGain  = 0
    while i < len(LinearGain):
        sumLinearGain += LinearGain[i]
        i += 1 
    averageLinearGain = sumLinearGain / len(LinearGain)
    NFG['averageGain'] = 10 * math.log10(averageLinearGain)
    print ("Average Gain: %f" %NFG['averageGain'])
    
    #Gather gain ripple per bandwidth step
    NFG['amplitude_response_10MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 10000000)
    NFG['amplitude_response_10MHz_max'] = max(NFG['amplitude_response_10MHz'])
    NFG['amplitude_response_120MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 120000000)
    NFG['amplitude_response_120MHz_max'] = max(NFG['amplitude_response_120MHz'])
    NFG['amplitude_response_500MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 500000000)
    NFG['amplitude_response_500MHz_max'] = max(NFG['amplitude_response_500MHz'])
    try:
        NFG['amplitude_response_1000MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 1000000000)
        NFG['amplitude_response_1000MHz_max'] = max(NFG['amplitude_response_1000MHz']) 
    except:
        pass
    print('10MHz Ripple: ' + str(NFG['amplitude_response_10MHz_max']))
    print('120MHz Ripple: ' + str(NFG['amplitude_response_120MHz_max']))
    print('500MHz Ripple: ' + str(NFG['amplitude_response_500MHz_max']))
    #print('1000MHz Ripple: ' + str(NFG['amplitude_response_1000MHz_max']))
    
    return NFG

def AmplitudeResponse(index_start_freq, index_stop_freq, freq, gain, bandwidth): 
        #Gather amplitude ripple per frequency step
        ripple = []
        temp_gain = []
        i = index_start_freq
        for values in gain[index_start_freq:index_stop_freq+1]:
            temp_gain.append(values)
            if(freq[i] - freq[index_start_freq] >= bandwidth):           
                ripple.append((max(temp_gain)-min(temp_gain))/2)
                del temp_gain[0]
            i += 1
        return ripple

def main():
    serial = input('Enter Serial Number:')
    CSV_Init(serial, HEADER)
    prev_time = 0
    input('Press Enter to begin test')
    #print(HEADER)
    while(1):       
        timer = time.time()
        if((timer - prev_time) > (PERIOD * 60)):
            current_time = timestamp()[0]
            prev_time = timer
            
            #----------------Do function-------------------
            NFG = {}
            temp = []
            NFG = get_NFG()
            now = timestamp()
            SA_Screenshot(serial, now[1])
            print('Screenshot:' , now[0], serial + '_' + now[1] + '.PNG')
            temp.append(now[0])
            temp.append(TEMPERATURE.TChamberReadWrite(TChamber,'PVAR1?')) 
            #----------------------------------------------
            with open(FILENAME, 'a', newline='') as f:
                workbook = csv.writer(f)
                workbook.writerow(temp + list(NFG.values()))
try:
    main()
except KeyboardInterrupt:
    print('Keyboard interrupt detected. Stopping program')
    sys.exit()
