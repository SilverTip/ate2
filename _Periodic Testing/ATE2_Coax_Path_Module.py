#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-04-19
#Python 3.9

#ATE2_Coax_Path_Module.py

#'ZT Module' Desktop program to interface with RCM-216 directly with UI. 

########################################
# Send commands to the RCM / ZTM box:
#   Window 1 = SP6T
#   Window 2A = SPDT
#   Window 2B = SPDT
#   Window 3 = SP6T
########################################

import urllib3
import sys
import time

#Global Variables
########################################
http = urllib3.PoolManager()

IP_Address = '10.0.10.169'

#Functions
########################################
def Init(IP):
    global IP_Address
    IP_Address = IP

def COAX_SW(command):
    # Specify the IP address
    CmdToSend = 'http://' + IP_Address + '/:' + command

    HTTP_Result = http.request('GET', CmdToSend)

    #return str(HTTP_Result.data, 'utf-8')

def OPEN():
    COAX_SW('SP6T:1:STATE:0')
    COAX_SW('SPDT:2A:STATE:1')
    COAX_SW('SPDT:2B:STATE:1')
    COAX_SW('SP6T:3:STATE:0')

def NFG():
    COAX_SW('SP6T:1:STATE:2')
    COAX_SW('SPDT:2A:STATE:1')
    COAX_SW('SPDT:2B:STATE:2')
    COAX_SW('SP6T:3:STATE:5')

def S11_S22():
    COAX_SW('SP6T:1:STATE:1')
    COAX_SW('SPDT:2A:STATE:1')
    COAX_SW('SPDT:2B:STATE:1')
    COAX_SW('SP6T:3:STATE:1')

def P1dB():
    COAX_SW('SP6T:1:STATE:4')
    COAX_SW('SPDT:2A:STATE:1')
    COAX_SW('SPDT:2B:STATE:1')
    COAX_SW('SP6T:3:STATE:2')
    
def IPLS():
    COAX_SW('SP6T:1:STATE:4')
    COAX_SW('SPDT:2A:STATE:1')
    COAX_SW('SPDT:2B:STATE:1')
    COAX_SW('SP6T:3:STATE:6')

def Input_Leakage():
    COAX_SW('SP6T:1:STATE:3')
    COAX_SW('SPDT:2A:STATE:2')
    COAX_SW('SPDT:2B:STATE:1')
    COAX_SW('SP6T:3:STATE:0')
    
def PhaseNoise():
    COAX_SW('SP6T:1:STATE:4')
    COAX_SW('SPDT:2A:STATE:1')
    COAX_SW('SPDT:2B:STATE:1')
    COAX_SW('SP6T:3:STATE:3')
    


def main():
    
    Init(IP_Address)

    # Print the model name and serial number
    try:
        sn = COAX_SW("SN?")
        mn = COAX_SW("MN?")
        print (mn, "/", sn)
    except:
        print('Cannnot connect to Switch')
        print('Verify RCM-216 IP Address by running ZT-Modular application')


    print('Path Selection')
    print('----------------------------------------')
    print('1. NFG Path')
    print('2. S11/S22 Path')
    print('3. P1dB Path')
    print('4. IPLS Pth')
    print('5. Input Leakage Path')
    print('6. Test Coax Switch')
    print('7. All Off')
    print('8. Phase Noise')
    print('9. Desense/n')
        
    while(1):   
        path = input('Select Path: [1-7]:')

        #NFG Path
        if(path == '1'):
            print('NFG Path Selected\n')
            NFG()

        #S11/S22 Path
        elif(path == '2'):
            print('S11/S22 Path Selected\n')
            S11_S22()

        #P1dB Path
        elif(path == '3'):
            print('P1dB Path Selected\n')
            P1dB()
        
        #IPLS Path
        elif(path == '4'):
            print('IPLS Path Selected\n')
            IPLS()

        #Input Leakage Path
        elif(path == '5'):
            print('Input Leakage Path Selected\n')
            Input_Leakage()

        #Test Coax Switch
        elif(path == '6'):
            print('TEST SWITCHES\n')
            time.sleep(2)
            # TEST SPDT SWITCHES 2A and 2B
            COAX_SW('SPDT:2A:STATE:1')
            COAX_SW('SPDT:2B:STATE:1')
            SW2A_State = COAX_SW('SPDT:2A:STATE?')
            SW2B_State = COAX_SW('SPDT:2B:STATE?')
            print ("SW2A =", SW2A_State)
            print ("SW2B =", SW2B_State)

            time.sleep(1)

            COAX_SW('SPDT:2A:STATE:2')
            COAX_SW('SPDT:2B:STATE:2')
            SW2A_State = COAX_SW('SPDT:2A:STATE?')
            SW2B_State = COAX_SW('SPDT:2B:STATE?')
            print ("SW2A =", SW2A_State)
            print ("SW2B =", SW2B_State)


            #TEST SWITCHES SP6T 1 & 3
            i = 1

            while(i < 7):
                COAX_SW('SP6T:1:STATE:' + str(i))
                SW1_State = COAX_SW('SP6T:1:STATE?')
                print ("SW1 =", SW1_State)
                COAX_SW('SP6T:3:STATE:' + str(i))
                SW3_State = COAX_SW('SP6T:3:STATE?')
                print ("SW3 =", SW3_State)
                time.sleep(1)
                i += 1

            i = 0   
            COAX_SW('SP6T:1:STATE:' + str(i))
            SW1_State = COAX_SW('SP6T:1:STATE?')
            print ("SW1 =", SW1_State)
            COAX_SW('SP6T:3:STATE:' + str(i))
            SW3_State = COAX_SW('SP6T:3:STATE?')
            print ("SW3 =", SW3_State)
            time.sleep(1)
            COAX_SW('SPDT:2A:STATE:0')
            COAX_SW('SPDT:2B:STATE:0')
            SW2A_State = COAX_SW('SPDT:2A:STATE?')
            SW2B_State = COAX_SW('SPDT:2B:STATE?')
            print ("SW2A =", SW2A_State)
            print ("SW2B =", SW2B_State)

        #All Off  
        elif(path == '7'):
            print('All Off')
            OPEN() 

        elif(path == '8'):
            print('Phase Noise')
            PhaseNoise()
        
        elif(path == '9'):
            print('Desense')
            Desense()

        else:
            print('Path unspecified')

#Run as stand-alone script
if __name__ == '__main__':
    main()
