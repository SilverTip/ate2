import xlsxwriter
import xlwings as xw
import json
import time
import sys
import os
import csv
import math
import numpy
import matplotlib.pyplot as plt
import numpy as np
from numpy.polynomial import Polynomial as Poly
from datetime import datetime

def calc_GD(file,start,stop,aperture):
    rows=[]
    with open(file, newline='') as f:
       csvreader = csv.reader(f)
       for row in csvreader:
           rows.append(row)

    #Fetch data array
    channel_locations = []
    j = 0
    #for channel starting locations
    for items in rows:
        for item in items:
            if 'BEGIN' in item:
                channel_locations.append(j)
            if 'END' in item:
                channel_locations.append(j)
        j += 1

    j=0
    data_types = ['dB','s','U',]
    data_row = []
    data_column = [] 
    #stored as row,column so data_locations[1] is row number and data_locations[2] is column number
    search_name = 'Phase'
    j=0
    for items in rows:
        k=0
        for item in items:
            if search_name in item:
                if j > 4: 
                    data_row.append(j)
                    data_column.append(k)
            k +=1
        j += 1
    #print(data_row)

    datapoints = []
    for i in data_row:
        for count,location in enumerate(channel_locations):
            if(i-location == 1):
                datapoints.append(channel_locations[count+1]-location)
    #print(datapoints)
    i=0
    flag = 0 
    Phase = []
    Frequency = []
    for row_num in data_row:
        if ('SC21 Phase' in rows[row_num][data_column[i]] and flag == 0):
            flag = 1
            for j in range(1,datapoints[i]-1):
                k = row_num + j
                
                Phase.append(2*math.pi*float(rows[k][data_column[i]])/360)
                Frequency.append(float(rows[k][0])/1000000)

    #print(Phase)
    #Polynomial fitting
    #npfreq = np.array(Frequency)
    #npphase = np.array(Phase)
    #print(Frequency)
    equation = Poly.fit(Frequency,Phase,3)
    coefficients = equation.convert().coef
    #derivative = equation.deriv()
    print(equation)
    convertedequation = Poly(coefficients)
    print(convertedequation)
    derivative = convertedequation.deriv()
    print(derivative)
    print(derivative.coef)
    #equation = np.poly1d(np.flip(coefficients))
    freqs = []
    polyfit = []
    groupdelay = []
    j = 0
    for i in np.arange(int(start),int(stop)+1,10):
        freqs.append(i)
        polyfit.append(convertedequation(i))
        groupdelay.append(float(derivative(i))*-1000)
        j += 1


    GroupDelayAperture = PhaseResponse(0,j,freqs,polyfit,aperture)
    equation2 = Poly.fit(freqs[:-int(4)],GroupDelayAperture,2)
    coefficients2 = equation2.convert().coef
    print(coefficients2)
    convertedequation2 = Poly(coefficients2)
    print(convertedequation2)
    polyfit2 = []
    for i in np.arange(int(start),int(stop)+1,10):
        polyfit2.append(convertedequation2(i))

    LinearTerm = coefficients2[1]
    SquareTerm = coefficients2[2]
    print(F'The Linear Term is {LinearTerm}')
    print(F'The Squared Term is {SquareTerm}')
    #print(max(GroupDelayAperture)-min(GroupDelayAperture))
    #GD2Aperture = AmplitudeResponse(0,j-int(aperture),freqs,GroupDelayAperture,aperture)
    #print(max(GD2Aperture)-min(GD2Aperture))
    #Testing Code
    #np.savetxt(r"\\ORB-SVR-FS01\Newmore\_Tian\Scripts\PhaseToGroupDelay\polyfit7.csv" , polyfit, delimiter =", ")
    np.savetxt(r"\\ORB-SVR-FS01\Newmore\_Tian\Scripts\PhaseToGroupDelay\GD7.csv" , groupdelay, delimiter =", ")
    #np.savetxt(r"\\ORB-SVR-FS01\Newmore\_Tian\Scripts\PhaseToGroupDelay\GDaperture7.csv" , GroupDelayAperture, delimiter =", ")
    #np.savetxt(r"\\ORB-SVR-FS01\Newmore\_Tian\Scripts\PhaseToGroupDelay\GDe7.csv" , polyfit2, delimiter =", ")
    #np.savetxt(r"\\ORB-SVR-FS01\Newmore\_Tian\Scripts\PhaseToGroupDelay\GD2aperture7.csv" , GD2Aperture, delimiter =", ")

    return LinearTerm, SquareTerm

def PhaseResponse(index_start_freq, index_stop_freq, freq, gain, bandwidth): 
        #Gather amplitude ripple per frequency step
        ripple = []
        temp_gain = []
        i = index_start_freq
        for values in gain[index_start_freq:index_stop_freq+1]:
            temp_gain.append(values)
            if(freq[i] - freq[index_start_freq] >= float(bandwidth)):           
                ripple.append(1000*((max(temp_gain)-min(temp_gain))/(2*math.pi*float(bandwidth))))
                del temp_gain[0]
            i += 1
        return ripple

def main():
    file = input('Enter CSV File Location: ')
    start = input('Enter the start frequency in MHz: ')
    stop = input('Enter the start frequency in MHz: ')
    ap = input('Enter the aperture in MHz: ')
    calc_GD(file, start, stop, ap)

main()