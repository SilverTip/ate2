#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2022-01-28

import pyvisa as visa
import csv
import sys
import time
import math
import json
import os
import shutil
import datetime

#import Thermotron8200PlusInterface as TEMPERATURE

#PERIOD = 0.5 #minutes
HEADER = ['Time', 'Current', 'Voltage', 'Output Power', 'Pass/Fail']
THRESHOLD = 1 #+/- value (dBm)

## Equipment Addresses ##
SA_VISA = 'TCPIP0::10.0.10.184::hislip0::INSTR'
PS_VISA = 'USB0::0x05E6::0x2230::9104855::0::INSTR'
DMM_VISA = 'TCPIP0::10.0.10.125::hislip0::INSTR'
AMM_VISA = 'GPIB2::23::INSTR'
#TChamber_VISA = 'TCPIP0::10.0.10.151::8888::SOCKET'
rm = visa.ResourceManager()
SA = rm.open_resource(SA_VISA)
SA.timeout = 10000
PS = rm.open_resource(PS_VISA)
PS.timeout = 10000
DMM = rm.open_resource(DMM_VISA)
DMM.timeout = 10000
AMM = rm.open_resource(AMM_VISA)
AMM.timeout = 10000
#TChamber = rm.open_resource(TChamber_VISA)
#TChamber.timeout = 10000

def SA_Screenshot(productNumber,time_value):     
    #SA.write(":DISPlay:FSCReen:STATe 1") 
    filename = productNumber + '_' + time_value + '.PNG'   
    #temporary storage location (becasue MXA cannot save to Production folder)
    SA_save_path = "N:\\" 
    #saving a screenshot on the equipment
    SA.write(":MMEMory:STORe:SCReen '" + SA_save_path + filename + "'")
    time.sleep(1)   
    ## Move screenshots to proper folder
    path = os.path.dirname(__file__)
    shutil.move(SA_save_path + filename, path)

def Wait(resource): 
    #This function is used to wait for a resource to complete its operation
    #Poll the Event Status Register (ESR) every second to determine if the operation is complete.
   resource.write('*OPC')
   try:
       while(int(resource.query('*ESR?')) != 1):
           time.sleep(1)
   except KeyboardInterrupt:
       print('Keyboard interrupt detected. Stopping program')
       sys.exit()

def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%b%d_%H%M%S")
    return now

def get_current(): 
    #print("starting current test") 
    time.sleep(2)  
    current = AMM.query_ascii_values('') #getting the dc current measurement
    current = round(abs(float(current[0] * 1000)), 1) #converting the measurement reading from A to mA
    #print ("Current: %f" %current)   
    return current

def CSV_Init(filename, header):
    if filename == '':
        filename = 'No_Name'
    name = input('Tester name:')
    global workbook, FILENAME
    current_time = timestamp()[0]
    FILENAME = filename + '_' + current_time[1] + '.csv'
    with open(FILENAME, 'w', newline='') as f:
        workbook = csv.writer(f)
        workbook.writerow(['Tester:', name])
        workbook.writerow(['Unit S/N:', filename])
        workbook.writerow(['Date:', current_time])
        workbook.writerow([])
        workbook.writerow(header)
        

def main():
    SN = input('Enter Unit Serial Number:')
    print('Unit will be powered on <T/2> minutes and powered off <T/2> minutes. One measurement will be taken every <T> minutes during the powered on state')
    PERIOD = float(input('Enter T (minutes):'))/2
    CSV_Init(SN, HEADER)
    prev_time = 0
    input('Press Enter to begin test')

    #initializations
    PS.write(':OUTPut:STATe 1')
    power_EN = True
    sleep(10)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    starting_power = float(SA.query(':CALCulate:MARKer1:Y?'))

    flag = True
    
    #print(HEADER)
    while(1):       
        timer = time.time()    
        if(((timer - prev_time) > (PERIOD * 60/2)) and power_EN and flag):
            #current_time = timestamp()[0]  
            flag = False
            
            #----------------Do function-------------------
            time = timestamp()[1]
            current = get_current()
            voltage = DMM.write('READ?')
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            power = float(SA.query(':CALCulate:MARKer1:Y?'))
            condition = 'Pass'
            if(abs(power-starting_power) > THRESHOLD):
                condition = 'Fail'

            values = [time,current,voltage,power,condition]

            #----------------------------------------------
            with open(FILENAME, 'a', newline='') as f:
                workbook = csv.writer(f)
                workbook.writerow(values)

        if((timer - prev_time) > (PERIOD * 60)):
            prev_time = timer
            flag = True
            if(power_EN):
                power_EN = False
                PS.write(':OUTPut:STATe 0')
            else:
                power_EN = True
                PS.write(':OUTPut:STATe 1')
try:
    main()
except KeyboardInterrupt:
    print('Keyboard interrupt detected. Stopping program')
    sys.exit()
