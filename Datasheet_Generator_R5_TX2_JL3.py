#Ported from DS_GV0.1.py
#Adjusted by Benjamin Stadnik
#Orbital Research Ltd
#2021-04-14
#Python 3.9

import xlsxwriter
import xlwings as xw
import json
import time
import sys
import os
import csv
import math
import datetime

settings_path = '\\\ORB-SVR-FS01\\Production\\_To_be_REVIEWED\\In_Progress\\ATE2\\ATE2_SETTINGS.csv'

def Generate(folder_path, filename, data, settings):
    
    workbook = xlsxwriter.Workbook(folder_path + '\\' + filename + '_Band' + str(data['Band']) +'.xlsx')

    if 'cover' in settings['Note_4']:
        worksheet= workbook.add_worksheet('Cover')
        worksheet.set_landscape()
        global bold
        bold = workbook.add_format({'bold': True,'font_name': 'Helvetica'})
        global not_bold
        not_bold = workbook.add_format({'bold': False,'font_name': 'Helvetica'})
        number_format = workbook.add_format({'num_format': '#,##0.00'})

        top = workbook.add_format({'italic': True, 'font_size': '11','top':2,'left':2,'right':2,'font_name': 'Helvetica'})
        middle = workbook.add_format({'italic': True, 'font_size': '11','left':2,'right':2,'text_wrap':True,'valign': 'top','font_name': 'Helvetica'})
        bottom = workbook.add_format({'italic': True, 'font_size': '11', 'bottom': 2, 'left':2,'right':2,'font_name': 'Helvetica'})
        borderall = workbook.add_format({'italic': True, 'font_size': '11','top':2,'bottom': 2,'left':2,'right':2,'text_wrap':True, 'valign': 'top','font_name': 'Helvetica'})
        
        cover = workbook.add_format({'font_size': '11','bg_color': '#44546A', 'font_color': '#FFFFFF', 'bold': True,'align': 'left','valign': 'top','font_name': 'Helvetica'})
        cover2 = workbook.add_format({'font_size': '11','bg_color': '#44546A', 'font_color': '#FFFFFF', 'italic': True, 'align': 'right','valign': 'top','font_name': 'Helvetica'})
        fonttitle = workbook.add_format({'font_size': '14', 'bold': True, 'align': 'center','font_name': 'Helvetica'})
        fontconfid = workbook.add_format({'font_size': '9', 'italic': True, 'align': 'center','font_name': 'Helvetica'})
        signature = workbook.add_format({'font_size': '12', 'font_color': '#D9D9D9','italic': True, 'align': 'right','valign': 'bottom','bottom':1,'font_name': 'Helvetica'})
        #column width
        worksheet.set_column(0,0,24.57) 
        worksheet.set_column(1,1,75)

        #row heights
        worksheet.set_row(0,27) 
        worksheet.set_row(1,15.5)
        worksheet.set_row(13,60) 
        worksheet.set_row(14,30)

        image1 = r'\\ORB-SVR-FS01\Newmore\_Tian\Scripts\datasheet_generator\ORBcoverlogo.PNG'
        worksheet.insert_image('A1', image1 , {'x_scale': 1, 'y_scale': 1})

        worksheet.write('B1', 'Engineering Memorandum/ Cover Page', fonttitle)
        worksheet.write('B2', 'Confidential', fontconfid)
        worksheet.write('A3', 'Project', cover) ## Client Text
        projectname = data['Customer'] + '(#' + data['Orbital_SN'][:-3].replace('-','') + ')'
        worksheet.write('B3', projectname,top) ## Client Name    
        worksheet.write('A4', 'Summary', cover) 
        worksheet.write('B4', 'Prototype test results and preliminary analyses are summarised on the following tabs/pages'
                              ' and compared against the verification matrix',middle) 
        worksheet.write('A5', 'Authour/Compiler', cover) 
        worksheet.write('A6', 'Reviewed Approved', cover) 
        worksheet.write('A7', 'Date', cover) ## Date Text
        worksheet.write('B5', '',middle)
        worksheet.write('B6', '',middle)
        worksheet.write('B7', datetime.datetime.now().strftime('%Y-%m-%d'), bottom) ## Date Field
        
        worksheet.write('B9', 'Records', bold)

        worksheet.write('A10', 'Performed by', cover2) 
        worksheet.write('A11', 'Checked by', cover2) 
        worksheet.write('A12', 'Performance Date(s)', cover2)
        worksheet.write('A13', 'DUT (s)', cover2)
        worksheet.write('A14', 'Equipment', cover2)
        worksheet.write('A15', 'Location(s)', cover2)
        worksheet.write('A16', 'Procedure', cover2)
        worksheet.write('A17', 'Outcome(s)', cover2)

        worksheet.write('B10', '', top) 
        worksheet.write('B11', '', middle) 
        worksheet.write('B12', '', middle)
        worksheet.write('B13',  data['Orbital_SN'], middle)
        worksheet.write('B14', 'PXA N9030A Signal Analyzer, N5245B Network Analyzer,E8257D PSG Analog Signal Generator,'
                        'TEKTRONIX Triple channel DC power supply, HA7062C Phase noise Analyzer, Keysight N9020B MXA Signal Analyzer', middle)
        worksheet.write('B15', 'Orbital Research Ltd., Engineering Office,\n'
                                '8652 Commerce Court, Burnaby, BC V5A 4N6', middle)
        worksheet.write('B16', 'Standard Orbital test procedures', middle)
        worksheet.write('B17', 'See following tabs/pages', bottom)

        worksheet.write('A19', 'Comments/Remarks',bold)
        worksheet.set_row(19,71.25)
        worksheet.merge_range('A20:B20','Additional Measurement and test results can be found at:\n'
                              '\n'
                              'State files can be found at:\n',borderall)

        worksheet.set_zoom(130)

        worksheet.write('A23','Name',cover)
        worksheet.write('B23','Signature (date)',cover)
        
        worksheet.set_row(23,60)
        worksheet.set_row(24,60)
        worksheet.set_row(25,60)
        worksheet.set_row(26,60)

        worksheet.write('B24','YYYY-MM-DD',signature)
        worksheet.write('B25','YYYY-MM-DD',signature)
        worksheet.write('B26','YYYY-MM-DD',signature)
        worksheet.write('B27','YYYY-MM-DD',signature)

        coverflag = 1
            
    sheet1_name = ''
    sheet2_name = ''
    try:
        if(bool(data['Temperature']['Enable'])):
            sheet1_name = str(data['Temperature']['Value']) + 'C Band ' + data['Band'] + ' Datasheet'
            sheet2_name = str(data['Temperature']['Value']) + 'C Band ' + data['Band'] + ' Data'
        else:
            sheet1_name = 'Band ' + str(data['Band']) + ' Datasheet'
            sheet2_name = 'Band ' + str(data['Band']) + ' Data'
            
    except:
        sheet1_name = 'Band ' + str(data['Band']) + ' Datasheet'
        sheet2_name = 'Band ' + str(data['Band']) + ' Data'
        
    worksheet = workbook.add_worksheet(sheet1_name)
    worksheet.set_landscape()
    bold = workbook.add_format({'bold': True,'font_name': 'Helvetica'})
    not_bold = workbook.add_format({'bold': False,'font_name': 'Helvetica'})
    bold.set_font_size(8)
    not_bold.set_font_size(8)
    number_format = workbook.add_format({'num_format': '#,##0.00'})
    not_bold_data = workbook.add_format({'bold': False,'num_format': '#,##0.00','font_name': 'Helvetica'})
    not_bold_data.set_font_size(8)
    upper, lower, gain = Get_Gain_Spec(settings['Gain_Spec'])

    # Add a format. Light red fill with dark red text.
    global format_fail
    format_fail = workbook.add_format({'bg_color': '#FFC7CE', 'font_color': '#9C0006'})
    global format_pass
    # Add a format. Green fill with dark green text.
    format_pass = workbook.add_format({'bg_color': '#C6EFCE', 'font_color': '#006100'})
    
    ## Client, PO and Date 
    worksheet.write('A2', 'Client', bold) ## Client Text
    worksheet.write('B2', data['Customer'], bold) ## Client Name    
    worksheet.write('A3', 'PO', bold) ## PO Text
    worksheet.write('B3', data['PO'], bold) # PO Number
    worksheet.write('A4', 'Date', bold) ## Date Text
    worksheet.write('B4', data['Date'], bold) ## Date Field

    ##Model, Serial and Stock
    worksheet.write('C2', 'Model', bold) ## Model Text
    worksheet.write('D2', data['Model'], bold) ## Model Number / Product Number
    worksheet.write('C3', 'Serial Number', bold) ## Unit Serial Text
    worksheet.write('D3', data['Orbital_SN'], bold) ## Serial Number
    worksheet.write('D4', data['stock_number'], bold) ## Stock Number
    worksheet.write('C4', 'Stock', bold) ## Stock Text

    ## Tester and Reviewer
    worksheet.write('E3', 'Tested By', bold) ## Tested by Text
    worksheet.write('F3', data['Tester'], not_bold) ## Tester Name
    worksheet.write('E4', 'Reviewed By', bold) ## Checked by Text
    worksheet.write('F4', 'IM', not_bold) ## Reviewer Name

    ## Logo Insert
    worksheet.insert_image('G1', 'orblogo.png')

    ##Set column width
    worksheet.set_column('B:C',20)
    worksheet.set_column('D:F',15)
    worksheet.set_column('G:H',15)

    ##SCD Number, Headers
    ##worksheet.write('A6', settings['l9'], bold) ##disabled for general datasheet
    worksheet.write('B6', 'Compliance Parameters',bold) 
    worksheet.write('C6', 'Specification', bold)
    worksheet.write('D6','Unit',bold)
    worksheet.write('E6','Status',bold)
    ##worksheet.write('F6',settings['l14'],bold) ##disabled for general datasheet
    worksheet.write('G6','Measured Parameters',bold)
    worksheet.write('H6','Spec',bold)
    worksheet.write('I6','Data',bold)
    worksheet.write('J6','Unit',bold)
    ##worksheet.write('K6',settings['l19'],bold) ##disabled for general datasheet
    worksheet.write('L6','Phase Noise',bold)
    worksheet.write('M6','Spec',bold)
    worksheet.write('N6','Data',bold)
    worksheet.write('O6','Unit',bold)

    ## Parameter Titles
    worksheet.write('B7','RF Input Frequency',not_bold)
    worksheet.write('B8','IF Output Frequency',not_bold)
    worksheet.write('B9','Local Osc Frequency',not_bold)
    worksheet.write('B10','DC Input Voltage Range',not_bold)
    worksheet.write('B11','Input Flange',not_bold)
    worksheet.write('B12','Output Connector',not_bold)
    worksheet.write('B13','Desense @-40 dBm input',not_bold)
    worksheet.write('B14','Overdrive @-20dBm input',not_bold)
    worksheet.write('B15','Size,(LengthxWidthxHeight)',not_bold)
    worksheet.write('B16','Weight',not_bold)
    worksheet.write('B17','Color',not_bold)

    ## Parameter Specs / Values
    worksheet.write('C7',settings['RF_In'],not_bold) ## RF Input Frequencies
    worksheet.write('C8',settings['IF_Out'],not_bold) ## IF Output Frequencies
    worksheet.write('C9',settings['LO_Spec'],not_bold) ## LO Frequency
    worksheet.write('C10',settings['Volt_Spec'],not_bold) ##Input Voltage Range
    worksheet.write('C11',settings['Input_Connector'],not_bold) ## Input Flange Type
    worksheet.write('C12',settings['Output_Connector'],not_bold) ## Output connector Type
    worksheet.write('C13',settings['Desense_Spec'],not_bold) ## Desense Value, usually '<0.1'
    worksheet.write('C14','no harm',not_bold) ## no harm unless special specification
    worksheet.write('C15',settings['Size'],not_bold)## Size Dimensions of DUT
    worksheet.write('C16',settings['Weight'],not_bold) ## Weight
    worksheet.write('C17',data['Color'],not_bold) ## Color
    if(settings['Tone']):
        worksheet.write('B18','22kHz Tone',not_bold)
        worksheet.write('C18',settings['Tone'],not_bold) ## Tone Enable
    
    ## Units
    worksheet.write('D7','GHz',not_bold) ## Should always be in GHz...right?
    worksheet.write('D8','MHz',not_bold) ## Should always be in MHz...right?
    worksheet.write('D9','GHz',not_bold) ## Should always be in GHz...right?
    worksheet.write('D10','VDC',not_bold)## Voltage Unit
    worksheet.write('D11','std',not_bold)## Standard?
    worksheet.write('D12','std',not_bold)## Standard?
    worksheet.write('D13','dB',not_bold) ## Desense Unit
    worksheet.write('D14','dBm',not_bold)## Overdrive Unit
    worksheet.write('D15','mm',not_bold) ## Size Unit
    worksheet.write('D16','g',not_bold)  ## Weight Unit

    ## Status
    worksheet.write('E7','confirmed',not_bold)
    worksheet.write('E8','confirmed',not_bold)
    worksheet.write('E9','confirmed',not_bold)
    worksheet.write('E10','confirmed',not_bold)
    worksheet.write('E11','confirmed',not_bold)
    worksheet.write('E12','confirmed',not_bold)
    worksheet.write('E13','0.05 max',not_bold)
    worksheet.write('E14','confirmed',not_bold)

    ## Disabled SCD Numbers
    ##worksheet.write('F7',settings['l64'],not_bold)
    ##worksheet.write('F8',settings['l65'],not_bold)
    ##worksheet.write('F9',settings['l66'],not_bold)
    ##worksheet.write('F10',settings['l67'],not_bold)
    ##worksheet.write('F11','',not_bold)
    ##worksheet.write('F12','',not_bold)
    ##worksheet.write('F13','',not_bold)
    ##worksheet.write('F14','',not_bold)
    ##worksheet.write('F15',settings['l72'],not_bold)
    ##worksheet.write('F16',settings['l73'],not_bold)
    ##worksheet.write('F17',settings['l74'],not_bold)
    ##worksheet.write('F18',settings['l75'],not_bold)


    ## Disabled SCD Numbers
    ##worksheet.write('A7',settings['l76'],not_bold)
    ##worksheet.write('A8',settings['l77'],not_bold)
    ##worksheet.write('A9',settings['l78'],not_bold)
    ##worksheet.write('A10',settings['l79'],not_bold)
    ##worksheet.write('A11',settings['l80'],not_bold)
    ##worksheet.write('A12',settings['l81'],not_bold)
    ##worksheet.write('A13',settings['l82'],not_bold)
    ##worksheet.write('A14',settings['l83'],not_bold)
    ##worksheet.write('A15',settings['l84'],not_bold)
    ##worksheet.write('A16',settings['l85'],not_bold)
    ##worksheet.write('A17',settings['l86'],not_bold)

    ## Spec Titles
    worksheet.write('G7','Noise Figure, Max',not_bold)
    worksheet.write('G8','Noise Figure, Average',not_bold)
    worksheet.write('G9','Gain',not_bold) 
    if 'Show' in settings['Note_4'] and data['NFG']['DSAdB']: 
        worksheet.write('G4','DSA Setting',bold) 
        worksheet.write('H4',str(data['NFG']['DSAdB']) + ' dB' + '(' + data['NFG']['DSAhex'] + ')',not_bold) 
    worksheet.write('G10','Max Ripple 10 MHz',not_bold) ## Usually 10 MHz is the spec but can change
    worksheet.write('G11','In Band Spurs',not_bold)

    spectype = [str(x) for x in settings["ir_p1db_spectype"].split(",")]

    if 'typ' in spectype[0]:
        worksheet.write('G12','Image Rejection, Typ',not_bold)
    else:
        worksheet.write('G12','Image Rejection, Min',not_bold)
    
    worksheet.write('G13','LO Leakage Input',not_bold)
    worksheet.write('G14','LO Leakage Output',not_bold)
    worksheet.write('G15','P1dB(Output), Min',not_bold)
    worksheet.write('G16','OIP3, Min',not_bold)
    worksheet.write('G17',('DC Current, %sV')%(settings['Voltage']),not_bold)
    worksheet.write('G18','Max Input VSWR ',not_bold) ## Add option for Isolator and without Isolator
    worksheet.write('G19','Max Output VSWR',not_bold)
    worksheet.write('G20',settings['Spe1'],not_bold) ## Comment for Extra Testing Note 1 Ex. Passed Pressure Testing
    worksheet.write('G21',settings['Spe2'],not_bold) ## Isolator and pressure test
    row_num = 19 + 3 #change this number if you are adding another parameter here, (dont change the 3, needed for spacing)

    ##Spec Values
    worksheet.write('H7',settings['NF_Spec_Max'],not_bold)
    worksheet.write('H8',settings['NF_Spec_Ave'],not_bold)
    worksheet.write('H9',settings['Gain_Spec'],not_bold)
    worksheet.write('H10',settings['Per_10M_Spec'],not_bold)
    worksheet.write('H11',settings['Spur_Spec'],not_bold)
    worksheet.write('H12',settings['ImRej_Spec'],not_bold)
    worksheet.write('H13',settings['LoLeakIn_Spec'],not_bold)
    worksheet.write('H14',settings['LoLeakOut_Spec'],not_bold)
    worksheet.write('H15',settings['P1dB_Spec'],not_bold)
    worksheet.write('H16',settings['OIP3_Spec'],not_bold)
    worksheet.write('H17',settings['Current_Spec'],not_bold)
    worksheet.write('H18',settings['S11_Spec'],not_bold)
    worksheet.write('H19',settings['S22_Spec'],not_bold)
    ##worksheet.write('H4','0132-1211-01',bold) ## SCD Specification if required

    MasterPass = []
    SpurPass = 0 
    ##Data checks for pass/fail
    MasterPass.append(TestPass(worksheet,'I7',data['NFG']['max_NF'],settings['NF_Spec_Max'],0))
    MasterPass.append(TestPass(worksheet,'I8',data['NFG']['averageNF'],settings['NF_Spec_Ave'],0))
    MasterPass.append(TestPass(worksheet,'I9',data['NFG']['averageGain'],upper,lower))
    MasterPass.append(TestPass(worksheet,'I10',data['NFG']['Ripple_10M_max'],settings['Per_10M_Spec'][3:] if settings[F'Per_10M_Spec'][3:] != '' else '',0))
    worksheet.conditional_format('I11', {'type':     'cell',
                                        'criteria': '<>',
                                        'value':   '"Pass"',
                                        'format':   format_fail})
    worksheet.conditional_format('I11', {'type':     'cell',
                                        'criteria': '=',
                                        'value':   '"Pass"',
                                        'format':   format_pass})     
    if data['spurs'] == 'Pass' and data['spurs'] is not None:
        SpurPass = 1
    print('hi')
    MasterPass.append(SpurPass)  
    MasterPass.append(TestPass(worksheet,'I12',data['min_image_rej'],1000,abs(float(settings['ImRej_Spec'])) if settings['ImRej_Spec'] != '' else ''))
    MasterPass.append(TestPass(worksheet,'I13',data['lo_leakage_in'],settings['LoLeakIn_Spec'],-1000))
    MasterPass.append(TestPass(worksheet,'I14',data['lo_leakage_out'],settings['LoLeakOut_Spec'],-1000))
    MasterPass.append(TestPass(worksheet,'I15',data['P1dB']['P1dB'],1000,settings['P1dB_Spec']))
    MasterPass.append(TestPass(worksheet,'I16',data['P1dB']['OIP3'],1000,settings['OIP3_Spec']))
    MasterPass.append(TestPass(worksheet,'I17',data['current'],settings['Current_Spec'],0))
    MasterPass.append(TestPass(worksheet,'I18',data['S11'],float(settings['S11_Spec'][:-2]) if settings['S11_Spec'] != '' else '',0))
    MasterPass.append(TestPass(worksheet,'I19',data['S22'],float(settings['S11_Spec'][:-2]) if settings['S11_Spec'] != '' else '',0))
    print('hi')
    ##Measurement Values
    worksheet.write('I7',data['NFG']['max_NF'],not_bold_data)
    worksheet.write('I8',data['NFG']['averageNF'],not_bold_data)
    worksheet.write('I9',data['NFG']['averageGain'],not_bold_data)
    worksheet.write('I10',data['NFG']['Ripple_10M_max'],not_bold_data)

    #LO LOCK
    if data['lo_lock'] is True:
        data['lo_lock'] = 'Pass'
    if data['lo_lock'] is False:
        data['lo_lock'] = 'Fail'
    worksheet.write('L16','LO Lock',bold)
    worksheet.write('M16','Pass/Fail',not_bold)
    worksheet.write('N16',data["lo_lock"],not_bold)
    
    ## Measurement Data
    worksheet.write('I11',data['spurs'],not_bold)
    worksheet.write('I12',data['min_image_rej'],not_bold_data)
    worksheet.write('I13',data['lo_leakage_in'],not_bold_data)
    worksheet.write('I14',data['lo_leakage_out'],not_bold_data)
    worksheet.write('I15',data['P1dB']['P1dB'],not_bold_data)
    worksheet.write('I16',data['P1dB']['OIP3'],not_bold_data)
    worksheet.write('I17',data['current'],not_bold_data)
    worksheet.write('I18',data['S11'],not_bold_data)
    worksheet.write('I19',data['S22'],not_bold_data)
    if(settings['Spe1']):
        worksheet.write('I20','Pass',not_bold)
    if(settings['Spe2']):
        worksheet.write('I21','Pass',not_bold)


    ## Units, Should never change?
    worksheet.write('J7','dB',not_bold)
    worksheet.write('J8','dB',not_bold)
    worksheet.write('J9','dB',not_bold)
    worksheet.write('J10','dB',not_bold)
    worksheet.write('J11','dBc',not_bold)
    worksheet.write('J12','dBc',not_bold)
    worksheet.write('J13','dBm',not_bold)
    worksheet.write('J14','dBm',not_bold)
    worksheet.write('J15','dBm',not_bold)
    worksheet.write('J16','dBm',not_bold)
    worksheet.write('J17','mA',not_bold)
    worksheet.write('J18','',not_bold) ## VSWR is unitless


    #### Disabled SCD Numbers
    ##worksheet.write('K7',settings['l121'],not_bold)
    ##worksheet.write('K8',settings['l122'],not_bold)
    ##worksheet.write('K9',settings['l123'],not_bold)
    ##worksheet.write('K10',settings['l124'],not_bold)
    ##worksheet.write('K11',settings['l125'],not_bold)
    ##worksheet.write('K12',settings['l126'],not_bold)

    ##Phase Noise Spec
    # worksheet.write('M7',settings['P10_Spec'],not_bold)
    # worksheet.write('M8',settings['P100_Spec'],not_bold)
    # worksheet.write('M9',settings['P1K_Spec'],not_bold)
    # worksheet.write('M10',settings['P10K_Spec'],not_bold)
    # worksheet.write('M11',settings['P100K_Spec'],not_bold)

    # ## Phase Noise Offsets
    # worksheet.write('L7','10 Hz',not_bold)
    # worksheet.write('L8','100 Hz',not_bold)
    # worksheet.write('L9','1 KHz',not_bold)
    # worksheet.write('L10','10 KHz',not_bold)
    # worksheet.write('L11','100 KHz',not_bold)
    
    # ## Phase Noise Units
    # worksheet.write('O7','dBc/Hz',not_bold)
    # worksheet.write('O8','dBc/Hz',not_bold)
    # worksheet.write('O9','dBc/Hz',not_bold)
    # worksheet.write('O10','dBc/Hz',not_bold)
    # worksheet.write('O11','dBc/Hz',not_bold)
    
    row_pn = 7
    if(settings['P10_Spec']):
        worksheet.write(F'M{row_pn}',settings['P10_Spec'],not_bold)
        MasterPass.append(TestPass(worksheet,F'N{row_pn}',data['phase']['10Hz'],float(settings['P10_Spec']),-1000))
        worksheet.write(F'L{row_pn}','10 Hz',not_bold)
        worksheet.write(F'O{row_pn}','dBc/Hz',not_bold)
        worksheet.write(F'N{row_pn}',data['phase']['10Hz'],not_bold_data)
        row_pn +=1
    if(settings['P100_Spec']):
        worksheet.write(F'M{row_pn}',settings['P100_Spec'],not_bold)
        MasterPass.append(TestPass(worksheet,F'N{row_pn}',data['phase']['100Hz'],float(settings['P100_Spec']),-1000))
        worksheet.write(F'L{row_pn}','100 Hz',not_bold)
        worksheet.write(F'O{row_pn}','dBc/Hz',not_bold)
        worksheet.write(F'N{row_pn}',data['phase']['100Hz'],not_bold_data)
        row_pn +=1
    if(settings['P1K_Spec']):
        worksheet.write(F'M{row_pn}',settings['P1K_Spec'],not_bold)
        MasterPass.append(TestPass(worksheet,F'N{row_pn}',data['phase']['1kHz'],float(settings['P1K_Spec']),-1000))
        worksheet.write(F'L{row_pn}','1 KHz',not_bold)
        worksheet.write(F'O{row_pn}','dBc/Hz',not_bold)
        worksheet.write(F'N{row_pn}',data['phase']['1kHz'],not_bold_data)
        row_pn +=1      
    if(settings['P10K_Spec']):
        worksheet.write(F'M{row_pn}',settings['P10K_Spec'],not_bold)
        MasterPass.append(TestPass(worksheet,F'N{row_pn}',data['phase']['10kHz'],float(settings['P10K_Spec']),-1000))
        worksheet.write(F'L{row_pn}','10 KHz',not_bold)
        worksheet.write(F'O{row_pn}','dBc/Hz',not_bold)
        worksheet.write(F'N{row_pn}',data['phase']['10kHz'],not_bold_data)
        row_pn +=1
    if(settings['P100K_Spec']):
        worksheet.write(F'M{row_pn}',settings['P100K_Spec'],not_bold)
        MasterPass.append(TestPass(worksheet,F'N{row_pn}',data['phase']['100kHz'],float(settings['P100K_Spec']),-1000))
        worksheet.write(F'L{row_pn}','100 KHz',not_bold)
        worksheet.write(F'O{row_pn}','dBc/Hz',not_bold)
        worksheet.write(F'N{row_pn}',data['phase']['100kHz'],not_bold_data)
        row_pn +=1
    if(settings['P1M_Spec']):
        worksheet.write(F'M{row_pn}',settings['P1M_Spec'],not_bold)
        MasterPass.append(TestPass(worksheet,F'N{row_pn}',data['phase']['1MHz'],float(settings['P1M_Spec']),-1000))
        worksheet.write(F'L{row_pn}','1 MHz',not_bold)
        worksheet.write(F'O{row_pn}','dBc/Hz',not_bold)
        worksheet.write(F'N{row_pn}',data['phase']['1MHz'],not_bold_data)
        row_pn +=1
    if(settings['P10M_Spec']):
        worksheet.write(F'M{row_pn}',settings['P10M_Spec'],not_bold)
        MasterPass.append(TestPass(worksheet,F'N{row_pn}',data['phase']['10MHz'],float(settings['P10M_Spec']),-1000))
        worksheet.write(F'L{row_pn}','10 MHz',not_bold)
        worksheet.write(F'O{row_pn}','dBc/Hz',not_bold)
        worksheet.write(F'N{row_pn}',data['phase']['10MHz'],not_bold_data)
        row_pn +=1
    if(settings['P100M_Spec']):
        worksheet.write(F'M{row_pn}',settings['P100M_Spec'],not_bold)
        MasterPass.append(TestPass(worksheet,F'N{row_pn}',data['phase']['100MHz'],float(settings['P100M_Spec']),-1000))
        worksheet.write(F'L{row_pn}','100 MHz',not_bold)
        worksheet.write(F'O{row_pn}','dBc/Hz',not_bold)
        worksheet.write(F'N{row_pn}',data['phase']['100MHz'],not_bold_data)
        row_pn +=1

    print('hi')
    
    ## Phase Noise Data
    # if(settings['P10_Spec']):
    #     worksheet.write('N7',data['phase']['10Hz'],not_bold_data)
    # if(settings['P100_Spec']):
    #     worksheet.write('N8',data['phase']['100Hz'],not_bold_data)
    # if(settings['P1K_Spec']):
    #     worksheet.write('N9',data['phase']['1kHz'],not_bold_data)
    # if(settings['P10K_Spec']):
    #     worksheet.write('N10',data['phase']['10kHz'],not_bold_data)
    # if(settings['P100K_Spec']):
    #     worksheet.write('N11',data['phase']['100kHz'],not_bold_data)
    # if(settings['P1M_Spec']):
    #     worksheet.write('N12',data['phase']['1MHz'],not_bold_data)
    # if(settings['P10M_Spec']):
    #     worksheet.write('N13',data['phase']['10MHz'],not_bold_data)
    # if(settings['P100M_Spec']):
    #     worksheet.write('N14',data['phase']['100MHz'],not_bold_data)
    
    if data['lo_lock'] is True:
        data['lo_lock'] = 'Pass'
    if data['lo_lock'] is False:
        data['lo_lock'] = 'Fail'
    worksheet.write('L16','LO Lock',bold)
    worksheet.write('M16','Pass/Fail',not_bold)
    worksheet.write('N16',data["lo_lock"],not_bold)
    worksheet.conditional_format('N16', {'type':     'cell',
                                        'criteria': '<>',
                                        'value':   '"Pass"',
                                        'format':   format_fail})
    worksheet.conditional_format('N16', {'type':     'cell',
                                        'criteria': '=',
                                        'value':   '"Pass"',
                                        'format':   format_pass}) 
    
    NFG_location= "A" + str(row_num) 
    PN_location= "G" + str(row_num)
    image1 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_NFG.PNG'
    image2 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_Phase_Noise.PNG'
    if(data['Script_Revision'].count('ATE2')):
        scale = {'x_scale': 0.535, 'y_scale': 0.535}
    else:
        scale = {'x_scale': 1, 'y_scale': 1} 
    worksheet.insert_image(NFG_location, image1 , scale)
    worksheet.insert_image(PN_location, image2 , scale)
    
    ###Notes under the screenshots, Usually detailing what equipment was used, uncertainty etc
    row_num +=21
    note1_location ="A" + str(row_num)
    note2_location ="A" + str(row_num+1)
    note3_location ="A" + str(row_num+2)
    worksheet.write(note1_location,settings['Note_1'],not_bold)
    worksheet.write(note2_location,settings['Note_2'],not_bold)
    worksheet.write(note3_location,settings['Note_3'],not_bold)

    
    ## Adding second Page for frequency, NF, and Gain List
    worksheet2 = workbook.add_worksheet(sheet2_name)

    #Frequency	Noise 	Gain	Per 10 MHz	Per 120 Mhz	Per 500 MHz
    worksheet2.write('A1','Averages',bold)
    worksheet2.write('A2','Minimum',bold)
    worksheet2.write('A3','Range',bold)
    worksheet2.write('A4','Maximums',bold)
    worksheet2.write('A5','Frequency',bold)
    worksheet2.write('B5','Noise',bold)
    worksheet2.write('C5','Gain',bold)
    worksheet2.write('D5','Linear Gain',bold)
    worksheet2.write('E5','Linear NF',bold)
    worksheet2.write('F5','Per 10 MHz',bold) 
    worksheet2.write('G5','Per 120 MHz',bold)
    worksheet2.write('H5','Per 500 MHz',bold)
    worksheet2.write('I5','Per 1000 MHz',bold)

    row = 6
    lenx = len(data['NFG']['Frequency'])
    for i in range(row,row+lenx):
        tmp = "A"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet2.write(tmp,data['NFG']['Frequency'][tmp_index],not_bold)

    lenx = len(data['NFG']['NF'])
    ##WRITE Noise
    for i in range(row,row+lenx):
        tmp = "B"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet2.write(tmp,data['NFG']['NF'][tmp_index],not_bold_data)
        
    lenx = len(data['NFG']['Gain'])
    ##WRITE GAIN
    for i in range(row,row+lenx):
        tmp = "C"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet2.write(tmp,data['NFG']['Gain'][tmp_index],not_bold_data)
   
    ##Labels for Ripple Specs etc
    ripple = ['10', '36', '100', '120', '200', '400', '500','900', '1000', '1400'] #Always in MHz
    col = 5
    for value in ripple:
        if(settings[F'Per_{value}M_Spec']):       
            worksheet2.write(2,col,settings[F'Per_{value}M_Spec'], bold)
            worksheet2.write(3,col,data['NFG'][F'Ripple_{value}M_max'],not_bold_data)
            worksheet2.write(4,col,F'Per {value} MHz',bold)
            for i in range(len(data['NFG']['Frequency'])):
                if int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_start_Frequency']) + (int(value) * 1000000):
                    ripple_index = i
                    break
            for i in range(len(data['NFG'][F'Ripple_{value}M'])):
                tmp = chr(ord('@')+(col+1))
                tmp += str(i+row+ripple_index)
                worksheet2.write(tmp,data['NFG'][F'Ripple_{value}M'][i],not_bold_data)
            
            MasterPass.append(TestPass(worksheet2,chr(ord('@')+(col+1))+'4',data['NFG'][F'Ripple_{value}M_max'],float(settings[F'Per_{value}M_Spec'][3:]),-1000))
            
            col += 1

    ##Write GAIN LINEAR
    for i in range(row,row+lenx):
        tmp = "D"
        tmp2 = "C"
        tmp = tmp + str(i)
        tmp2 = tmp2 + str(i)
        formula = "=10^(%s/10)" % (tmp2)
        worksheet2.write_formula(tmp,formula,not_bold_data)

    ##Write NF Linear
    for i in range(row,row+lenx):
        tmp = "E"
        tmp2 = "B"
        tmp = tmp + str(i)
        tmp2 = tmp2 + str(i)
        formula = "=10^(%s/10)" % (tmp2)
        worksheet2.write_formula(tmp,formula,not_bold_data)
        
    #Find ripple index by polling frequency index
    flag = 0
    for i in range(len(data['NFG']['Frequency'])):
        if (int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_start_Frequency'])) and flag < 1:
            index_interest_start = i
            flag = 1
        if (int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_stop_Frequency'])) and flag < 6:
            index_interest_stop = i
            flag = 6
            break
    
    #Averages, maximums and minimums
    worksheet2.write_formula('E4','=10^(B4/10)',not_bold_data)
    worksheet2.write_formula('D4','=10^(C4/10)',not_bold_data)
    worksheet2.write('B1',data['NFG']['averageNF'],not_bold_data)
    worksheet2.write('C1',data['NFG']['averageGain'],not_bold_data)    
    worksheet2.write_formula('B4','=MAX(B' + str(row+index_interest_start) + ':B' + str(row+index_interest_stop) +')',not_bold_data)
    worksheet2.write_formula('C4','=MAX(C' + str(row+index_interest_start) + ':C' + str(row+index_interest_stop) +')',not_bold_data)
    worksheet2.write_formula('C2','=MIN(C' + str(row+index_interest_start) + ':C' + str(row+index_interest_stop) +')',not_bold_data)
    worksheet2.write_formula('C3','=C4-C2',not_bold_data)
    print('hi')
    #Pass/Fail for stuff above
    MasterPass.append(TestPass(worksheet2,'C1',data['NFG']['averageGain'],upper,lower))
    MasterPass.append(TestPass(worksheet2,'B1',data['NFG']['averageNF'],float(settings['NF_Spec_Ave']),-1000))
    MasterPass.append(TestPass(worksheet2,'C2',data['NFG']['min_Gain'],1000,lower))
    MasterPass.append(TestPass(worksheet2,'C3',float(data['NFG']['max_Gain'])-float(data['NFG']['min_Gain']),upper-lower,-1000))
    MasterPass.append(TestPass(worksheet2,'C4',data['NFG']['max_Gain'],upper,-1000))
    MasterPass.append(TestPass(worksheet2,'B4',data['NFG']['max_NF'],settings['NF_Spec_Max'],-1000))
    
    ##Write phase noise data if it is available
    try:
        lenp = int(len(data['phase']['SMOOTH'])/2)
        #Phase Noise Section
        worksheet2.write('K4','Phase Noise Measurement, Smoothed', bold)
        worksheet2.write('K5','Frequency Offset, Hz', bold)
        worksheet2.write('L5','Phase Noise, dBc/Hz', bold)

        ##WRITE Frequency Offset
        for i in range(row,row+lenp):
            tmp = "K"
            tmp = tmp + str(i)
            tmp_index = i-row
            worksheet2.write(tmp,data['phase']['SMOOTH'][tmp_index*2],not_bold)

        ##WRITE phase noise smoothed data
        for i in range(row,row+lenp):
            tmp = "L"
            tmp = tmp + str(i)
            tmp_index = i-row
            worksheet2.write(tmp,data['phase']['SMOOTH'][tmp_index*2+1],not_bold_data)
    except:
        pass

    
    #Code for Master Pass 
    MP = 'Pass'
    if 0 in MasterPass:
        MP = 'Fail'
    worksheet.write('E2', 'Pass/Fail', bold) ## Tested by Text
    worksheet.write('F2', MP, bold) ## Tester Name

    workbook.close()

def TestPass(worksheet,cell,data,upper,lower):
    Pass = 0 
    if upper == None or lower == None:
        return Pass
    worksheet.conditional_format(cell, {'type':     'cell',
                                        'criteria': '>',
                                        'value':   upper,
                                        'format':   format_fail})
    worksheet.conditional_format(cell, {'type':     'cell',
                                        'criteria': 'between',
                                        'minimum':  lower,
                                        'maximum':  upper,
                                        'value':   upper,
                                        'format':   format_pass})
    worksheet.conditional_format(cell, {'type':     'cell',
                                        'criteria': '<',
                                        'value':   lower,
                                        'format':   format_fail})
    if data == None:
        return 1
    if upper == '' or lower == '':
        return Pass
    if float(lower)< float(data) < float(upper):
        Pass = 1

    return Pass
    

def Merge(folder_path, name): #Merge Datasheets for multiband units
    print('Merging Datasheets...')
    cwd = os.path.abspath(folder_path) 
    files = os.listdir(cwd)
    wb_new = xw.Book()
    for file in files: #Find all .xlsx files in folder
        if file.endswith('.xlsx'):
            wb_temp = xw.Book(cwd + '\\' + file)
            for sheet in wb_temp.sheets: #Append all sheets from .xlsx file to new file
                sheet.api.Copy(Before=wb_new.sheets[-1].api)
            wb_temp.close()
            #wb_temp.app.quit()

    wb_new.sheets[-1].delete()
    wb_new.sheets[0].activate()
    wb_new.save(folder_path + '\\' + name + '.xlsx')
    #wb_new.close()
    wb_new.app.quit()

def Load_JSON(file):
    with open(file, 'r') as f:
        data = json.load(f)
    f.close()
    return data

def GenerateFromJSON(folder_path):
    settings_master_list = Load_All_Settings(settings_path)
    print('Generating Datasheets...')
    cwd = os.path.abspath(folder_path) 
    files = os.listdir(cwd)  
    band = 0 
    for file in files: #Find all .json files in folder
        if file.endswith('.json'):
            if file.count("Settings"):
                continue
            print(file)
            data = Load_JSON(file)
            settings_list = Find_Settings(settings_master_list, data['Name'], "Name")
            settings = settings_list[band] 
            Generate(os.path.dirname(__file__), os.path.splitext(file)[0].split('_Band')[0], data, settings)
            band += 1

def Find_Settings(settings_master_list, selected, key):
    #print(selected)
    settings_list = []
    for i in range(len(settings_master_list)):
        if(selected == settings_master_list[i][0][key]):
            for settings in settings_master_list[i]:
                settings_list.append(settings)
    return settings_list

def Load_All_Settings(file):
    #Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)
    f.close

    #print(matrix)

    #Genrate list of dictionaries from matrix, grouping bands together if model name left blank 
    key_list = matrix[0]
    output_list = []
    temp_list = []
    matrix.reverse()
    for rows in matrix:
        temp_dict = {}
        for index, value in enumerate(rows):
            temp_dict[key_list[index]] = value
        temp_list.insert(0,temp_dict)
        if(rows[0] != ''):
            output_list.append(temp_list)
            temp_list = []
    output_list.reverse()

    output_list.pop(0)
    #print(output_list)
    
    return output_list

def Get_Gain_Spec(spec):
    if not spec:
        return None

    # Split spec string to list of numbers
    temp = ''
    numbers = []
    spec += ' '
    flag = False
    for character in spec:
        if character.isdigit() or character == '.':
            flag = True
            temp += character
        elif (flag):
            numbers.append(float(temp))
            temp = ''
            flag = False

    # Interpret list of numbers
    gain = None
    lower = None
    upper = None
    if (len(numbers) > 2):  # Specific Spec ex:60(+3,-1)
        gain = numbers[0]
        upper = gain + numbers[1]
        lower = gain - numbers[2]
    elif (len(numbers) > 1):
        if (numbers[1] > 20):  # Range Spec ex:55-65
            lower = numbers[0]
            upper = numbers[1]
            gain = (lower + upper) / 2
        else:  # General Spec ex:55 +/-1
            gain = numbers[0]
            lower = gain - numbers[1]
            upper = gain + numbers[1]
    elif (len(numbers) == 1 and 'Min' in spec):
        lower = numbers[0]
        gain = lower + 2
        upper = gain + 7
    else:  # No Spec
        gain = numbers[0]
        upper = gain + 3
        lower = gain - 3

    return upper, lower, gain


def main():
    message = input("Enter 'M' to merage all datasheets in current directory. Enter 'G' to generate datasheets from RAW JSON data.")
    if(message == 'M'):
        Merge(os.path.dirname(__file__), 'Merged Datasheets')
        print('Datasheets merged. Exit program.')
    elif(message == 'G'):
        GenerateFromJSON(os.path.dirname(__file__))
        print('Generated datasheets. Exit program.')
    else:
        print('Exit program.')
        sys.exit()


if(__name__ == '__main__'):
	main()

#Change Log
#-------------------------
#Revision 2.0 - 20210902 - Added Max and Ave NF to Datasheet. Added Max and Ave NF Spec to LNB Settings. Changed 'data' and 'settings' to ATE2 naming convention. Changed raw data starting row from '6' to variable 'row'. Ripple now starts at cooresponding frequency. Added special note 1 and 2 back to datasheet
#Revision 3 - 20211109 - added "filename" variable to differentiate from "data['Orbital_SN']" dependancy. Added standalone application merge and generate functionality.
#R4 - 20220420 - Transposed ATE2_Settings.csv. Upgraded Load_All_Settings() for transposed matrix
#R5 - 20220621 - Find_Settings has find key. Merge function updated compatibility for Win7.
