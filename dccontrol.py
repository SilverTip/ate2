# dccontrol.py
# Orbital Research Ltd.
# uATE project

# Version History:
# -----------------------------------------------------------------------------------------------
# 20231102 - Imported from ATE2_DC_Control_Module.py
#          - Improve functionality to class structure
#          - Create socket server to manage serial relay controller
#          - Create socket and serial command lines for troubleshooting
# 20240220 - Updated Server + Client from TCP to UDP: avoid handshaking, server ignore client dropout
# -----------------------------------------------------------------------------------------------

import time
import serial
import socket
import traceback
import struct

class Relay:
    def __init__(self, ser: serial.Serial, name: str, letter: str, state_name: tuple):
        self.ser = ser
        self.name = name
        self.letter = letter.upper()  # range{A-H}
        self.state_name = state_name  # (off state, on state)
        self.state = 0
        self.begin_command = b'\x1b\x5b'
        self.off_command = b'\x30'
        self.on_command = b'\x31'
        self.relay_map = {
            'A': b'\x41',
            'B': b'\x42',
            'C': b'\x43',
            'D': b'\x44',
            'E': b'\x45',
            'F': b'\x46',
            'G': b'\x47',
            'H': b'\x48'
        }

    def set_state(self, state: bool):
        new_state = self.off_command
        if state:
            new_state = self.on_command
        cmd = self.begin_command + new_state + self.relay_map[self.letter]
        self.ser.write(cmd)

    def off(self):
        self.state = 0
        self.set_state(bool(self.state))  # NC Position

    def on(self):
        self.state = 1
        self.set_state(bool(self.state))  # NO Position

    def toggle(self):  # Toggle relay position
        self.state = not self.state
        self.set_state(self.state)


class SerialController:
    def __init__(self, com_port: str):
        self.com_port = com_port
        self.mac = 'None'
        self.ser = serial.Serial(
            port=str(self.com_port),
            baudrate=115200,
            parity=serial.PARITY_ODD,
            stopbits=serial.STOPBITS_TWO,
            bytesize=serial.EIGHTBITS
        )
        # Connect to BV4627
        count = 0
        while count < 12:  # Spam startup command to BV4627. Experimentally determined min_delay = 1.2s
            self.ser.write(b"\x0d")
            time.sleep(0.1)
            count += 1

        self.idn = 'ByVac,BV4627,Unknown,Unknown'   # self.get_model_number()

        self.master_relay = Relay(self.ser,'master_switch',"A",('Off', 'On'))
        self.input_src_relay = Relay(self.ser,'input_source',"B",('Ext', 'OVC'))
        self.ovc_l1_relay = Relay(self.ser,'ovc_l1', "D",('Open', 'GND'))
        self.ovc_l2_relay = Relay(self.ser,'ovc_l2',"C",('Open', 'GND'))
        self.pos_ctrl_relay = Relay(self.ser,'pos_ctrl',"F", ('Off', 'On'))
        self.pos_input_relay = Relay(self.ser,'pos_input',"E",('AC_Supply', '12V_Battery'))
        self.placeholder = Relay(self.ser,'None',"G",('None', 'None'))
        self.tone_relay = Relay(self.ser,'tone_generator',"H",('Off', 'On'))

        self.relays = [
            self.master_relay,
            self.input_src_relay,
            self.ovc_l2_relay,
            self.ovc_l1_relay,
            self.pos_input_relay,
            self.pos_ctrl_relay,
            self.placeholder,
            self.tone_relay
        ]

    # def serial_write(self, message: bytes):
    #     self.ser.write(message)
    #
    # def serial_read(self) -> bytes:
    #     reply = b'no reply'
    #     if self.ser.in_waiting:
    #         reply = self.ser.read(self.ser.in_waiting)
    #
    #     return reply
    #
    # def serial_query(self, message: bytes) -> bytes:
    #     self.serial_write(message)
    #     time.sleep(0.1)
    #     reply = self.serial_read()
    #
    #     return reply
    #
    # def get_model_number(self):
    #     # The instructions are quite unclear about this.
    #     # ID is 2 bytes, 16bit number, high bit first
    #     # Firmware is 2 bytes, {first byte}.{second_byte}
    #     id_cmd = b"\x1b\x5b\x3f\x33\x31\x64"
    #     firmware_cmd = b"\x1b\x5b\x3f\x33\x31\x66"
    #     self.ser.write(id_cmd)
    #     time.sleep(0.25)
    #     idn = int.from_bytes(self.ser.read(2), byteorder='big')
    #     self.ser.write(firmware_cmd)
    #     time.sleep(0.25)
    #     firmware_high = int.from_bytes(self.ser.read(1), byteorder='big')
    #     firmware_low = int.from_bytes(self.ser.read(1), byteorder='big')
    #
    #     return f'ByVac,BV4627,{idn},{firmware_high}.{firmware_low}'

    def close(self):
        #self.off() # Preserve relay states after controller communications are closed.
        self.ser.close()

    # def binary_set_relays(self, bits: bytes):
    #     number = int(bits, 16)
    #     bits_string = [int(digit) for digit in bin(number)[2:].zfill(8)]
    #
    #     for index, bit in enumerate(bits_string):
    #         if bit:
    #             self.relays[index].off()
    #         else:
    #             self.relays[index].on()
    #
    # def binary_get_relays(self):
    #     states = []
    #     for relay in self.relays:
    #         states.append(int(relay.state))
    #     out = 0
    #     for bit in states:
    #         out = (out << 1) | bit
    #
    #     return bytes(hex(out), 'utf-8')

    def off(self):
        for relay in self.relays:
            relay.off()

    def ovc(self, state: int):
        self.input_src_relay.on()
        self.master_relay.on()
        if state == 0:
            # Under 10V = Open circuit
            self.ovc_l1_relay.off()
            self.ovc_l2_relay.off()
        elif state == 1:
            # 13V
            self.ovc_l1_relay.on()
            self.ovc_l2_relay.on()
        elif state == 2:
            # 18V
            self.ovc_l1_relay.off()
            self.ovc_l2_relay.on()
        elif state == 3:
            # 22V
            self.ovc_l1_relay.on()
            self.ovc_l2_relay.off()

    def ext(self, state: int):
        if state:
            self.input_src_relay.off()
            self.master_relay.on()
        else:
            self.input_src_relay.off()
            self.master_relay.off()

    def pos(self, state: int):
        if state == 0:
            self.pos_input_relay.off()
            self.pos_ctrl_relay.off()
        elif state == 1:
            # Switching supply
            self.pos_input_relay.off()
            self.pos_ctrl_relay.on()
        elif state == 2:
            # Battery
            self.pos_input_relay.on()
            self.pos_ctrl_relay.on()

    def tone(self, state: bool):
        if state:
            self.tone_relay.on()
        else:
            self.tone_relay.off()

class Server:
    # UDP server designed to run on raspberry pi
    def __init__(self):
        self.host = "10.0.10.195"
        self.port = 65432
        self.serial_port = '/dev/ttyUSB0'
        self.controller = SerialController(self.serial_port)

    def main(self):
        print(f'Begin DC Control Server: {self.host}:{self.port}')
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.bind((self.host, self.port))
        print('Server running...')
        try:
            while True:
                data, address = s.recvfrom(1024)
                print(f"[RX] Address: {address}, Data: {data}")
                reply = control_command(self.controller, data)
                s.sendto(reply, address)
                print(f"[TX] Address: {address}, Data: {reply}")
        except:
            print(traceback.format_exc())

class Client:
    def __init__(self, ate_version: int, hostport: str):
        temp = hostport.split(':')
        host = str(temp[0])
        port = int(temp[1])
        self.address = (host, port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.idn = "ATE2 DC Control Panel"

    def write(self, message: str):
        self.sock.sendto(str.encode(message), self.address)

    def read(self) -> str:
        byte_reply, address = self.sock.recvfrom(1024)
        reply = byte_reply.decode('utf-8')

        return reply

    def query(self, message: str):
        self.write(message)
        time.sleep(0.05)
        reply = self.read()

        return reply

    def close(self):
        time.sleep(0.05)
        self.sock.close()
        
def get_state(controller: SerialController):
    ext = 0
    if controller.master_relay.state and not controller.input_src_relay.state:
        ext = 1
    ovc = 0
    if controller.input_src_relay.state:
        ovc_state = f'{controller.ovc_l1_relay.state}{controller.ovc_l2_relay.state}'
        if ovc_state == '11':
            ovc = 1
        elif ovc_state == '01':
            ovc = 2
        elif ovc_state == '10':
            ovc = 3
    pos = 0
    if controller.pos_ctrl_relay.state:
        if controller.pos_input_relay.state:
            pos = 2
        else:
            pos = 1
    tone = 0
    if controller.tone_relay.state:
        tone = 1

    return str.encode(f'$statr,{ext},{ovc},{pos},{tone},*')

def verbose_state(controller: SerialController):
    info_string = ""
    info = []
    for relay in controller.relays:
        info.append([relay.letter, relay.name, relay.state, relay.state_name[relay.state]])
    for r in info:
        line = f'{r[0]}, {r[1]: <15}, {r[2]} ({r[3]})\n'
        info_string += line

    return str.encode(info_string)

def command_list():
    command_list = "\nStatus message: $statr,{ext},{ovc},{pos},{tone},*\n\n\
    Command list:\n\
    h or help -> show this help menu\n\
    close -> close interface\n\
    $off,* -> return $statr\n\
    $ovc,<state>,* -> state: 0=off, 1=13V, 2=18V, 3=24V -> return $statr\n\
    $ext,<state>,* -> state: 0=off, 1=on -> return $statr\n\
    $tone,<state>,* -> state: 0=off, 1=on -> return $statr\n\
    $pos,<state>,* -> state: 0=off, 1=switching supply, 2=battery supply -> return $statr\n\
    $getst,* -> return $statr\n\
    $getre,* -> return verbose statement with relay letter, name, and state\n\
    $setre,<letter>,<state>,* -> letter: {a-h}, state: 0=off, 1=on\
     -> return verbose statement with relay letter, name, and state\n\n"

    #Advanced commands:\n\
    #$$read,* -> read bytes directly from serial controller -> return hex\n\
    #$$write,<bytes>,* -> bytes: {hex | ascii}, write hex to serial controller -> return None\n\
    #$$query,<bytes>,* -> bytes: {hex | ascii}, write hex to serial controller -> return hex\n


    return command_list

def control_command(controller: SerialController, message: bytes) -> bytes:
    ack_flag = False
    command = message.decode('utf-8').split(',')
    end = command.pop(-1)
    if end != '*':
        return str.encode(f'$ACK,({message})')
    # if command[0].count('$$'):
    #     if command[0] == '$$read':
    #         return controller.serial_read()
    #     elif command[0] == '$$write':
    #         controller.serial_write(command[1].encode('utf-8'))
    #     elif command[0] == '$$query':
    #         return controller.serial_query(command[1].encode('utf-8'))
    if len(command) == 3:
        if not command[2].isnumeric():
            ack_flag = True
        elif command[0] == '$setre':
            valid = False
            for relay in controller.relays:
                if command[1].lower() == relay.letter.lower():
                    valid = True
                    if command[2] == '1':
                        relay.on()
                    else:
                        relay.off()
            if not valid:
                ack_flag = True
            else:
                return verbose_state(controller)
        else:
            ack_flag = True
    elif len(command) == 2:
        if not command[1].isnumeric():
            ack_flag = True
        elif command[0] == '$ovc':
            if int(command[1]) > 3:
                ack_flag = True
            controller.ovc(int(command[1]))
        elif command[0] == '$ext':
            if int(command[1]) > 1:
                ack_flag = True
            controller.ext(int(command[1]))
        elif command[0] == '$pos':
            if int(command[1]) > 2:
                ack_flag = True
            controller.pos(int(command[1]))
        elif command[0] == '$tone':
            if int(command[1]) > 1:
                ack_flag = True
            controller.tone(bool(int(command[1])))
        else:
            ack_flag = True
    elif len(command) == 1:
        if command[0] == '$off':
            controller.off()
        elif command[0] == '$getst':
            return get_state(controller)
        elif command[0] == '$getre':
            return verbose_state(controller)
        else:
            ack_flag = True
    else:
        ack_flag = True

    if ack_flag:
        return str.encode(f'$ACK,({message})')
    else:
        return get_state(controller)

def serial_command_line(port: str):
    controller = SerialController(port)
    print(command_list())
    while True:
        command = input('Command: ').lower()
        if command == 'h' or command == 'help':
            print(command_list())
        elif command == 'close':
            controller.close()
            break
        else:
            reply = control_command(controller, str.encode(command))
            print(reply.decode('utf-8'))

def socket_command_line(addressport: str):
    conn = Client(2, addressport)
    print(command_list())
    while True:
        command = input('Command: ').lower()
        if command == 'h' or command == 'help':
            print(command_list())
        elif command == 'close':
            conn.close()
            break
        else:
            reply = conn.query(command)
            print(reply)

if __name__ == '__main__':
    if socket.gethostname() == 'raspberrypi':
        server = Server()
        server.main()
    else:
        socket_command_line('10.0.10.195:65432')
        # serial_command_line('/dev/ttyUSB0')
        # serial_command_line('COM3')


