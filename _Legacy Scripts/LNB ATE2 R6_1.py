#LNB ATE2 RX.py
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-11-03

current_revision = 6

import pyvisa as visa
import csv
import sys
import time
import math
import json
import os
import shutil
import tkinter as tk
from tkinter import ttk
import datetime
import socket
import numpy as np
from pynput.keyboard import Key, Controller
from subprocess import Popen, PIPE
from PIL import Image, ImageOps

#Custom modules
import ATE2_DC_Control_Module as DC
import ATE2_Coax_Path_Module as Path
import ATE2_Datasheet_Generator_R4 as DS_Gen
import Orbital_Image_Filter_R4 as ImageFilter
import Thermotron_8200_Plus_Module_R1 as Thermo
import LNB_Communication_R5 as COMM


## Equipment Addresses ##
SA_VISA = 'TCPIP0::10.0.10.184::hislip0::INSTR'              #MXA 9020B Spectrum Analyzer
SG_VISA = 'GPIB2::5::INSTR'                                  #Anritsu 68367C Signal Generator - GPIB commands only. No SCPI
PS_VISA = 'USB0::0x05E6::0x2230::9104855::0::INSTR'          #Keithley Instruments 2230-30-1 Power Supply
PM_VISA = 'USB0::0x0957::0x2B18::MY48100947::0::INSTR'       #Agilent U2001A Power Meter
DMM_VISA = 'TCPIP0::10.0.10.125::hislip0::INSTR'             #Keysight 34461A Voltmeter / Temperature Probe
AMM_VISA = 'GPIB2::23::INSTR'                                #HP3478A Ammeter
VNA_VISA = 'TCPIP0::10.0.10.120::inst0::INSTR'               #Fieldfox N9918A
DC_Control_Panel_Port = 'COM3'                               #Custom DC control panel
RCM_216_IP = '10.0.10.177'                                   #Minicircuits RCM-216 
TChamber_VISA = 'TCPIP0::10.0.10.139::8888::SOCKET'          #Thermotron8200+

ATE2_path = "\\\SVR-1\\Production\\_To_be_REVIEWED\\In_Progress\\ATE2"
settings_path = '\\\SVR-1\\Production\\_To_be_REVIEWED\\In_Progress\\ATE2\\ATE2_SETTINGS.csv'  
State_path = '\\\SVR-1\\Newmore\\_1_Instr-State-Files\\MXA_N9020B_States\\ATE2\\'


AUTOST_PERIOD = 60



def main():
    print("*******BEGIN NEW TEST*******")
    
    #Declaring dictionary to manage and store test results
    data = {'NFG': 
                {'Frequency': [],'loss_before' : None,'loss_after' : None,'NF' : [],'Gain' : [],'averageNF' : None,'averageGain' : None,'amplitude_response_10MHz' : [],'amplitude_response_120MHz' : [],'amplitude_response_500MHz' : [], 'amplitude_response_1000MHz' : [],'amplitude_response_10MHz_max' : None,'amplitude_response_120MHz_max' : None,'amplitude_response_500MHz_max' : None,'amplitude_response_1000MHz_max' : None,'min_NF' : None,'max_NF' : None,'min_Gain' : None,'max_Gain' : None},
            'P1dB': {'Input' : [] , 'Output' : [], 'P1dB' : None, 'OIP3' : None}, 'image_rej': None, 'spurs': None, 'lo_leakage_out': None, 'current': None, 'lo_leakage_in': None, 'S11': None, 'S22': None,
            'phase': {'RAW' : [], '10Hz': None, '100Hz' : None, '1kHz' : None, '10kHz' : None, '100kHz' : None, '1MHz' : None, '10MHz' : None, '100MHz' : None},
            'RS485_enable' : False, 'CC_enable' : False, 'IP_enable' : False, 'RS232_enable': False, 'V_switch' : {'Enable' : False, 'V_Switch_Up': None, 'V_Switch_Down': None},
            'date': None, 'time': None, 
            'Customer': None, 'PO': None, 'Tester': None, 'Color': None, 'Band': None, 'Manufacturer_SN': None, 'Orbital_SN': 'No_Name', 'stock_number': None, 'Model' : None,
            'Serial_Port' : None, 'Attenuation' : None, 'ATE2_Script_Revision' : current_revision, 'IP' : None, 'IP_Port': None, 
            'Temperature_enable': False, 'TemperatureProfile':None, 'Temperature': None
            }
          
    #Get equipment names and append them to data   
    data.update(Resources)

    #Path
    print(ATE2_path) #Obtain program directory path
    
    #Obtaining unit info from input fields
    data['Orbital_SN'] = str(SNText.get())
    if data['Orbital_SN'] == '':
        data['Orbital_SN'] = 'No_Name'      
    data['stock_number'] = str(StockText.get())
    data['Manufacturer_SN'] = str(MnfText.get())
    data['Customer'] = str(CxText.get())
    data['PO'] = str(POText.get())
    data['Color'] = str(ColorText.get())
    data['Tester'] = str(YouText.get())
    data['Serial_Port'] = USB_Port_Text.get()      
    data['Attenuation'] = float(Attenuation.get()) 
    data['IP'] = str(IP_Text.get())
    data['IP_Port'] = str(IP_Port_Text.get())    
 
    #Obtaining user selected settings
    model_name = ModelDropdown.get()
    settings_list = DS_Gen.Find_Settings(settings_master_list, model_name)
    
    #Generate Folder 
    folder_path = Make_Folder(ATE2_path, data['Orbital_SN'])  #Making Folder for Unit 
    
    #Check for communications 
    data['RS485_enable'] = bool(settings_list[0]['RS485_EN'])
    data['RS232_enable'] = bool(settings_list[0]['RS232_EN'])
    data['V_switch']['Enable'] = bool(settings_list[0]['Vsw_EN'])
    print('V Switching enabled: ' + str(data['V_switch']['Enable']))
    data['CC_enable'] = bool(settings_list[0]['CC_EN'])
    data['IP_enable'] = bool(settings_list[0]['IP_EN'])

    global coms
    coms = OpenCommunications(data)

    #Record Model No
    data['Model'] = settings_list[0]['Model']

    #Determine if temperature profile selected
    if(TemperatureDropdown.get() != 'None'):
        data['Temperature_enable'] = True
        print('Temperature Profile Enabled')

    txt_filename = folder_path + '\\' + data['Orbital_SN'] +'_Communications_Log.txt' 
    if(RecordComm.get()):
        f = open(txt_filename, 'x')
        f.close()

    #Run ATE with or without temperature profile
    if(data['Temperature_enable']): #if temperature profile enabled: run temperature profile with instances of the ATE determined by the profile 
        #Load Temperature Profile
        profile = Thermo.LoadProfile(TemperatureDropdown.get())
        print('Profile: ' + str(TemperatureDropdown.get()))
        data['TemperatureProfile'] = str(TemperatureDropdown.get())

        #Determine testing time
        testDuration = 0
        for temp, period in profile:
            testDuration += period
        print('Test Duration (Hours):', round(testDuration,1))
        
        #Execute temperature profile
        Thermo.START(TChamber)
        for temperature, period in profile:
            current_temp = Thermo.TEMP(TChamber)
            print("Current Temperature: " + str(current_temp) + 'C')
            if(temperature == 'ATE'):
                Thermo.HOLD(TChamber)
                name = data['Orbital_SN'] + '_' + str(current_temp)
                data['Temperature'] = current_temp
                subfolder = Make_Folder(folder_path,timestamp()[1] + '_' + data['Temperature'] + 'C')
                RunATE(settings_list,data,name,subfolder)
                Thermo.RESUME(TChamber)
            elif(type(temperature) is str):
                pass
            else:
                ramp_rate = round(abs(float(current_temp) - float(temperature)) / (float(period)*60),2) #Time in hours
                Thermo.SET(TChamber,temperature)
                time.sleep(0.25)
                Thermo.RAMP(TChamber,ramp_rate)
                print('Time:', Thermo.TimeStamp()[1], 'Current Temperature:', current_temp, 'Next Temperature:', temperature, 'Ramp Rate(C/min):', ramp_rate, 'Step Duration (Hours):', period) 
                Sleep(period * 60 * 60, txt_filename) #in seconds
                
          
        print('Test Completed @', Thermo.TimeStamp()[1])
        Thermo.STOP(TChamber)

    else: #Run ATE once
        RunATE(settings_list,data,data['Orbital_SN'],folder_path)


    #Pwr_off() 
    #Merge datasheets
    

    #close com port
    if(coms != None):
        coms.close()
       
    print("*******TEST COMPLETE*******")

def RunATE(settings_list,data,name,folder_path):
    print('Running ATE2...')

    #Get Date and Time
    data['date'] = str(datetime.datetime.now().date())   
    data['time'] = str(datetime.datetime.now().time())
    print (data['date'])
    print (data['time'])
    

    #Begin NFG Test
    if(not data['Temperature_enable']): #Skip if in the temperature chamber
        input('Connect Noise Source to DUT. Press ENTER to continue.')
    for settings in settings_list:       
        data['Band'] = Change_Band(settings, data, coms)
        print('Starting Band ' + str(data['Band']) + ' NFG test')
        if(Resources['SA'] != None):
            data['current'] = get_current()
            data['NFG'] = get_NFG(settings, name, folder_path)
        #Create .json file for raw band data
        Save_JSON(folder_path, name, data)
        if(NFG_Only.get()):
            #Generate band datasheet
            DS_Gen.Generate(folder_path, name, data, settings)
          
    #Begin Other Testing
    if(NFG_Only.get() == False):
        if(not data['Temperature_enable']): #Skip if in the temperature chamber
            input('Connect Coax Cable to DUT. Press ENTER to continue.')
        for settings in settings_list:
            data['Band'] = Change_Band(settings, data, coms)
            data = Load_JSON(folder_path, name, data) #Load BandX NFG data into current data dictionary  
            print('Starting Band ' + str(settings['Band']) + ' test')
            if(Resources['AMM'] != None):
                data['current'] = get_current()
            if(Resources['SG'] and Resources['SA'] != None):
                data['P1dB'] = get_p1db(settings)
                data['phase'] = get_phase(settings, name, folder_path)
                data['image_rej'] = get_image_rejection(settings, name, folder_path)
                data['V_switch'] = get_V_Switch(settings)
                data['spurs'] = get_spurs(settings, name, folder_path)
                data['lo_leakage_out'] = get_lo_leakage_output(settings, name, folder_path)
                data['lo_leakage_in'] = get_lo_leakage_input(settings, name, folder_path)
            if(not data['Temperature_enable']): #Skip if in the temperature chamber
                if(Resources['VNA'] != None):
                    data['S11'] = get_vswrIP(settings, name, folder_path)
                    data['S22'] = get_vswrOP(settings, name, folder_path)
      
           #Create .json file for raw band data
            Save_JSON(folder_path, name, data)

            #Generate band datasheet
            DS_Gen.Generate(folder_path, name, data, settings)

    print ("ATE2 Test completed.\n")

    #Merge data
    DS_Gen.Merge(folder_path, name + ' Datasheet')

def Save_JSON(folder_path, name, data):
    data_file = folder_path + "\\" + name + '_Band' + data['Band'] + '.json'
    with open(data_file,'w') as f:
        json.dump(data,f)
    f.close()

def Load_JSON(folder_path, name, data):
    data_file = folder_path + "\\" + name + '_Band' + data['Band'] + '.json'
    with open(data_file, 'r') as f:
        data = json.load(f)
    f.close()
    return data 

def Change_Band(settings, data, coms):
    Pwr_on(settings)  #Change input voltage depending on band   
    band = int(settings['Band'])
    if(bool(settings['RS485_EN'])): #If RS485 enabled, change band
        try:
            gain = COMM.attenuation(int(settings['Atten_type']),float(Attenuation.get()),True)
            message1 = "$setst,%s,*" %(int(band)-1)
            print(message1)
            reply1 = coms.send(message1)
            print(reply1)
            message2 = "$setda,%s,%s,*" %(int(band)-1,gain)
            print(message2)
            reply2 = coms.send(message2)
            print(reply2)    
        except Exception as e:
            print(e)
            print('Failed to change unit band')
    elif(bool(settings['RS232_EN'])): #If RS485 enabled, change band
        try:
            gain = COMM.attenuation(int(settings['Atten_type']),float(Attenuation.get()),True)
            message1 = "$setst,%s,*" %(int(band)-1)
            print(message1)
            reply1 = coms.send(message1)
            print(reply1)
            message2 = "$setda,%s,%s,*" %(int(band)-1,gain)
            print(message2)
            reply2 = coms.send(message2)
            print(reply2) 
        except Exception as e:
            print(e)
            print('Failed to change unit band')  
    elif(bool(settings['CC_EN'])): #If contact closure is enabled, pause to allow user to change band
        input('Switch to Band ' + settings('Band') + '. Press Enter to continue.') 
    elif(bool(settings['IP_EN'])):
        try:
            gain = COMM.attenuation(int(settings['Atten_type']),float(Attenuation.get()),True)
            message1 = "$setst,%s,*" %(int(band)-1)
            print(message1)
            reply1 = coms.send(message1)
            print(reply1)
            message2 = "$setda,%s,%s,*" %(int(band)-1,gain)
            print(message2)
            reply2 = coms.send(message2)
            print(reply2)
        except Exception as e:
            print(e)
            print('***Failed to change unit band***')
    time.sleep(1) #Give controller time to detect new voltage and switch
    
    return settings['Band']
    
def OpenCommunications(data):
    coms = None
    if(data['RS485_enable']):
        if(data['Serial_Port']):
            try:
                coms = COMM.RS485_Connection(data['Serial_Port'])
                reply = coms.send('$build,*')
                if(reply != []):
                    data['Build_Info'] = reply
                    print('Successfully connected to unit via COM' + str(data['Serial_Port']))
                else:
                    print('Sucessfully opened COM' + data['Serial_Port'] + ' but was unable to communicate with device.')
                    StopATE()
            except Exception as e:
                print(e)
                print('ATE2 was unable to connect to unit')
                StopATE()
        else:
            print('Unit has serial communication enabled but COM port was not specified.')
            StopATE()
    elif(data['RS232_enable']):
        if(data['Serial_Port']):
            try:
                coms = COMM.RS232_Connection(data['Serial_Port'])
                reply = coms.send('$build,*')
                if(reply != ['']):
                    data['Build_Info'] = reply
                    print('Successfully connected to unit via COM' + str(data['Serial_Port']))
                else:
                    print('Sucessfully opened COM' + data['Serial_Port'] + ' but was unable to communicate with device.')
                    StopATE()
            except Exception as e:
                print(e)
                print('ATE2 was unable to connect to unit')
                StopATE()
        else:
            print('Unit has serial communication enabled but COM port was not specified.')
            StopATE()
    elif(data['IP_enable']):
        if(data['IP'] and data['IP_Port']):
            try:
                coms = COMM.Socket_Connection(data['IP'],int(data['IP_Port']))
                reply = coms.send('$build,*')
                if(reply != ['']):
                    data['Build_Info'] = reply
                    print('Successfully connected to unit via COM' + str(data['Serial_Port']))
                else:
                    print('Sucessfully opened COM' + data['Serial_Port'] + ' but was unable to communicate with device.')
                    StopATE()
            except Exception as e:
                print(e)
                print('ATE2 was unable to connect to unit')
                StopATE()
        else:
            print('Unit has socket communication enabled but either IP or Port were not specified.')
            StopATE()
    return coms

def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%b%d_%H%M%S")
    return now

def Pwr_on(settings):
    if(settings['Voltage'] == False):
        return
    else:
        PS.write('APPL CH2, ' + str(settings['Voltage']) + ' , 0.75')
        time.sleep(1)
        PS.write(':OUTPut:STATe 1')
    
def Pwr_off(): 
    PS.write(':OUTPut:STATe 0')

def Make_Folder(path, name): 
    if(name == ''):
        name == 'No_Name' #If productNumber is left blank, the shutil.rmtree() function will nuke ATE2 script directory (ask me how I know)
    dst = path + '\\' + name
    print ("Creating Folder: " + dst)
    if(os.path.exists(dst)):
        pass
    else:
        os.mkdir(dst)

    return dst

def SA_Screenshot(productNumber,tag,band,folder_path): 
    #Fullscreen
    SA.write(":DISPlay:FSCReen:STATe 1") 
    filename = productNumber + "_Band" + str(band) + '_' + tag + '.PNG'   
    #temporary storage location (becasue MXA cannot save to Production folder)
    SA_save_path = "\\\SVR-1\\Newmore\\"
    #saving a screenshot on the equipment
    SA.write(":MMEMory:STORe:SCReen '" + SA_save_path + filename + "'")   
    time.sleep(1)   
    ## Move screenshots to proper folder
    shutil.move(SA_save_path + filename, folder_path + '\\' + filename)  
    #Include image processing for datasheet (greyscaling, inversion, etc)
    ImageFilter.Filter(folder_path, filename, True)

def VNA_Screenshot(productNumber,tag,band, folder_path): 
    #Setup filename and folder path
    filename = productNumber + "_Band" + str(band) + '_' + tag + '.PNG'
    #saving a screenshot on the equipment                                 
    VNA.write(':MMEMory:STORe:IMAGe "TESTZ"')
    #fetching the image data in an array
    fetch_image_data = VNA.query_binary_values('MMEMory:DATA? "TESTZ.png"',datatype='B',is_big_endian=False,container=bytearray)
    #creating a file in testing folder with the product number and tag, then write the image data to it
    save_dir = open(folder_path + '\\' + filename, 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()
    #deleting the image on the equipment
    VNA.write(":MMEM:DEL 'TESTZ.PNG'")
    VNA.write("*CLS")
    #Include image processing for datasheet (greyscaling, inversion, etc)
    ImageFilter.Filter(folder_path, filename, True)

def Sleep(seconds, filename):
    #instead of time.sleep() everywhere, we can do useful functions in the meantime, such as record communications to .txt
    time1 = time.time()
    time2 = time.time()
    while(time2-time1 < seconds):
        if(RecordComm.get()):
            try:
                reply = coms.send('$getst,*')
                print(reply)
                message = ''
                for items in reply:
                    message = message + ' ' + str(items).strip('\r\n')
                f = open(filename, 'a') 
                f.write(str('\n' + timestamp()[0]) + ' ' + message) 
                f.close()
            except Exception as e:
                pass
        time.sleep(29) #loop every 30s (for now)
        time2 = time.time()

def Wait(resource): 
    #This function is used to wait for a resource to complete its operation
    #Poll the Event Status Register (ESR) every second to determine if the operation is complete.
   resource.write('*OPC')
   try:
       while(int(resource.query('*ESR?')) != 1):
           time.sleep(1)
           #print('waiting...')
   except KeyboardInterrupt:
       StopATE()
  
def get_NFG(settings, name, folder_path): 
    
    NFG = {'Frequency': [],'loss_before' : None,'loss_after' : None,'NF' : [],'Gain' : [],'averageNF' : None,'averageGain' : None,'amplitude_response_10MHz' : None,'amplitude_response_120MHz' : None,'amplitude_response_500MHz' : None,'amplitude_response_1000MHz' : None,'amplitude_response_10MHz_max' : None,'amplitude_response_120MHz_max' : None,'amplitude_response_500MHz_max' : None,'amplitude_response_1000MHz_max' : None,'min_NF' : None,'max_NF' : None,'min_Gain' : None,'max_Gain' : None}
        
    print ("starting NF and Gain test")
    
    #set NFG Path
    Path.NFG()

    #Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['nf_gain_state_file'] + "'")
    Wait(SA)

    #Add limits
    
    
    #Start NFG Sweep  
    SA.write(":DISPlay:FSCReen:STATe 0")
    SA.write(":DISPlay:FSCReen:STATe 1")
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write('INITiate:RESTart')
    Wait(SA)

    #Aquire start and stop frequencies, and number of points
    freq_start = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')[0])
    freq_stop = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')[0])
    number_of_points = float(SA.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')[0])

    #Saving frequency data
    NFG['Frequency'] = []
    NFG['Frequency'].append(freq_start)
    add_next_Freq=((freq_stop-freq_start)/(number_of_points-1))
    i = 1
    next_Frequency = freq_start
    while i < number_of_points-1:
        next_Frequency = next_Frequency + add_next_Freq
        NFG['Frequency'].append(next_Frequency)
        i += 1   
    NFG['Frequency'].append(freq_stop)

    #Finding the index of the interested start and stop frequencies 
    i = 0
    flag = 0
    interested_freq = {}
    while i < len(NFG['Frequency']):
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_start_Frequency'])) and flag == 0:
            interested_freq['start'] = i
            flag = 1
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_stop_Frequency'])):
            interested_freq['stop'] = i
            break
        i += 1
    
    #Detect compensation type and store value[s]
    loss_after_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:AFTer:MODE?')
    loss_before_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:BEFore:MODE?')
    if "FIX" in loss_after_mode:  
        NFG['loss_after'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:VALue?')
    elif "TABL" in loss_after_mode:  
        NFG['loss_after'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:TABLe:DATA?')
    else:
        pass
    if "FIX" in loss_before_mode:
        NFG['loss_before'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:VALue?')
    elif "TABL" in loss_before_mode:
        NFG['loss_before'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:TABLe:DATA?')
    else:
        pass

    #Fetch NF array and calculate the min and max NF over the interested frequencies
    NFG['NF'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:NFIG?')
    i = interested_freq['start']
    interested_NF = []
    while i <= interested_freq['stop']:
        interested_NF.append(NFG['NF'][i])
        i += 1
    NFG['min_NF'] = min(interested_NF)
    NFG['max_NF'] = max(interested_NF)
    print ("Minimum NF: %f" %NFG['min_NF'])
    print ("Maximum NF: %f" %NFG['max_NF'])
    
    #calculating the linear noise figure values (only for interested frequencies)
    i = interested_freq['start']
    LinearNF = []
    while i <= interested_freq['stop']:
        try:
            LinearNF.append(10**(NFG['NF'][i]/10))
        except:
            LinearNF.append(0)
        i += 1

    
    #calculating the average noise figure by taking the average of the linear noise figure values and then using math to get the average noise figure (only for interested frequencies)
    i = 0
    sumLinearNF  = 0
    while i < len(LinearNF):
        sumLinearNF += LinearNF[i]
        i += 1  
    averageLinearNF = sumLinearNF / len(LinearNF)
    NFG['averageNF'] = 10 * math.log10(averageLinearNF)
    print ("Average NF: %f" %NFG['averageNF'])

    #Fetch Gain array and calculate the min and max Gain over the interested frequencies
    NFG['Gain'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
    i = interested_freq['start']
    interested_Gain = []
    while i <= interested_freq['stop']:
        interested_Gain.append(NFG['Gain'][i])
        i += 1
    NFG['min_Gain'] = min(interested_Gain)
    NFG['max_Gain'] = max(interested_Gain)
    print ("Minimum Gain: %f" %NFG['min_Gain'])
    print ("Maximum Gain: %f" %NFG['max_Gain'])
    
    #calculating the linear gain values (only for interested frequencies)
    i = interested_freq['start']
    LinearGain = [] 
    while i <= interested_freq['stop']:
        try:
            LinearGain.append(10**(NFG['Gain'][i]/10))
        except:
            LinearGain.append(0)
        i += 1 
        
    #calculating the average gain by taking the average of the linear gain values and then using math to get the average gain (only for interested frequencies)
    i = 0 
    sumLinearGain  = 0
    while i < len(LinearGain):
        sumLinearGain += LinearGain[i]
        i += 1 
    averageLinearGain = sumLinearGain / len(LinearGain)
    NFG['averageGain'] = 10 * math.log10(averageLinearGain)
    print ("Average Gain: %f" %NFG['averageGain'])
    
    #Gather gain ripple per bandwidth step
    NFG['amplitude_response_10MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 10000000)
    NFG['amplitude_response_10MHz_max'] = max(NFG['amplitude_response_10MHz'])
    NFG['amplitude_response_120MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 120000000)
    NFG['amplitude_response_120MHz_max'] = max(NFG['amplitude_response_120MHz'])
    NFG['amplitude_response_500MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 500000000)
    NFG['amplitude_response_500MHz_max'] = max(NFG['amplitude_response_500MHz'])
    try:
        NFG['amplitude_response_1000MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 1000000000)
        NFG['amplitude_response_1000MHz_max'] = max(NFG['amplitude_response_1000MHz']) 
    except:
        pass
    print('10MHz Ripple: ' + str(NFG['amplitude_response_10MHz_max']))
    print('120MHz Ripple: ' + str(NFG['amplitude_response_120MHz_max']))
    print('500MHz Ripple: ' + str(NFG['amplitude_response_500MHz_max']))
    print('1000MHz Ripple: ' + str(NFG['amplitude_response_1000MHz_max']))

    #Scale state file for screenshot
    SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:RLEVel " + str(round(NFG['averageNF'],1)))
    SA.write("DISPlay:NFIGure:TRACe:GAIN:Y:SCALe:RLEVel " + str(round(NFG['averageGain'],0)))
    SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:RPOSition CENTer")
    SA.write("DISPlay:NFIGure:TRACe:GAIN:Y:SCALe:RPOSition CENTer")
    time.sleep(1)
    
    SA_Screenshot(name, 'NFG', settings['Band'], folder_path)
    
    return NFG

def AmplitudeResponse(index_start_freq, index_stop_freq, freq, gain, bandwidth): 
        #Gather amplitude ripple per frequency step
        ripple = []
        temp_gain = []
        i = index_start_freq
        for values in gain[index_start_freq:index_stop_freq+1]:
            temp_gain.append(values)
            if(freq[i] - freq[index_start_freq] >= bandwidth):           
                ripple.append((max(temp_gain)-min(temp_gain))/2)
                del temp_gain[0]
            i += 1
        return ripple

def get_p1db(settings):

    print ("starting P1dB test")  
    P1dB = {'Input' : [] , 'Output' : [], 'P1dB' : None, 'OIP3' : None}
    powerMeterPowerMax = 20
    powerMeterFreq = 1000000000
    gain = []
    running_average = []
    
    Path.P1dB() ##set P1dB Path
    
    SG.write('F1 ' + str(float(settings['LO']) + powerMeterFreq) + ' HZ') #setting the signal generator frequency   
    PM.write(':INITiate1:CONTinuous %d' % (1)) #triggering the power meter to take measurements continuosly 
    PM.write(':SENSe:FREQuency:FIXed %G' % (powerMeterFreq))
    
    P1dB['Input'].append((float(settings['Gain']) * -1)) #Start at about 0dBm @ Output of LNB
    powerSweepTo = P1dB['Input'][-1] + 30 #End at 20dB higher than the initial input
    
    flag = True
    while P1dB['Input'][-1] < powerSweepTo:
        SG.write('L1 ' + str(P1dB['Input'][-1] + float(settings['p1db_cable_loss'])) + ' DM') #setting the signal generator amplitude  
        SG.write('RF1')
        time.sleep(0.4)    
        P1dB['Output'].append(float(PM.query(':FETCh1?')) + float(settings['p1db_muxT_and_cable_loss'])) #fetch measurement, compensate for loss, and add it to P1dB['Output'] list
        
        gain.append(P1dB['Output'][-1] - P1dB['Input'][-1]) #Calculating gain at present step
        running_average = np.average(gain) #Calculate average gain over all steps
        difference = running_average - gain[-1] #Find the difference between the average gain and the current gain
        
        if(difference > 1 and flag == True): #If the difference between the average gain and the current gain is greater than 1dB, 1dB compression point found.
            P1dB['P1dB'] = P1dB['Output'][-1] - (difference - 1) #Record P1dB, adjusting for overshoot
            P1dB['OIP3'] = P1dB['P1dB'] + 10 #P1dB + 10 = OIP3
            flag = False
        
        P1dB['Input'].append(P1dB['Input'][-1] + 1)

    SG.write('RF0')

    
    print('P1dB: ' + str(P1dB['P1dB']))
    return P1dB

def get_phase(settings, name, folder_path):

    phase = {'RAW' : [], '10Hz': None, '100Hz' : None, '1kHz' : None, '10kHz' : None, '100kHz' : None, '1MHz' : None, '10MHz': None, '100MHz': None}
    
    print ("starting Phase Noise test")
    
    carrier = 1000000000
    
    ##setting IPLS Path
    Path.IPLS()

    #Loading state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + str(settings['phase_noise_state_file'])+"'")
    Wait(SA)

    time.sleep(1)
    
    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + carrier) + ' HZ')
    SG.write('L1 ' + str(((float(settings['Gain'])) * -1) + float(settings['phase_noise_loss'])) + ' DM')
    SG.write('RF1')
    time.sleep(1)
    
    #Start Phase Sweep
    SA.write('SENSe:FREQuency:CARRier ' + str(carrier))
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write('INITiate:IMMediate')
    Wait(SA)
    
    #Gather all raw data from frequency plot
    phase['RAW'] = SA.query_ascii_values(':FETCH:LPlot?')
    
    #Gather phase noise markers
    phase['10Hz'] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    phase['100Hz'] = SA.query_ascii_values('CALC:LPLot:MARK2:Y?')[0]
    phase['1kHz'] = SA.query_ascii_values('CALC:LPLot:MARK3:Y?')[0]
    phase['10kHz'] = SA.query_ascii_values('CALC:LPLot:MARK4:Y?')[0]
    phase['100kHz'] = SA.query_ascii_values('CALC:LPLot:MARK5:Y?')[0]
    phase['1MHz'] = SA.query_ascii_values('CALC:LPLot:MARK6:Y?')[0]
    try: #I dont know how this will behave - will it return zero or cause equipment timeout? (I hope it returns zero)
        phase['10MHz'] = SA.query_ascii_values('CALC:LPLot:MARK7:Y?')[0]
        phase['100MHz'] = SA.query_ascii_values('CALC:LPLot:MARK8:Y?')[0]
    except:
        pass

    
    SA_Screenshot(name, 'Phase_Noise', settings['Band'], folder_path)

    SG.write('RF0')

    print(phase)

    return phase

def get_image_rejection(settings, name, folder_path):

    #gainAverage is measured by the get_nf_gain function and the product number is entered by the user
    print ("starting image rejection test")
    Path.IPLS()
    
    signal_freq = 1000000000
    
    #zeroDbTolerance is the tolerance used to check if the marker value of SA is close to zero when the frequency is set to verify and amplitude is set to gain average
    zeroDbTolerance = 1

    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['image_rejection_state_file'] + "'")
    
    #waiting for the state file to load
    Wait(SA)

    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + signal_freq) + ' HZ')
    #powerIn is the amplitude to be set on SG. We use the average gain and compensate for the cable loss. 
    powerIn = (float(settings['Gain']) + float(settings['image_rejection_cable_loss_verification'])) * -1
    #print('Initial PowerIn ' + str(powerIn))
    #setting amplitude of the signal generator to powerIn
    SG.write('L1 ' + str(powerIn) + ' DM')
    #turning RF ON
    SG.write('RF1')
    
    #restrating the sweep
    SA.write('SENSe:FREQuency:CENTer ' + str(signal_freq))
    SA.write(':INITiate:IMMediate')
    Wait(SA)

    #read the maximum marker value
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))      
    time.sleep(1)
    
    #verifying if the measurement is close to 0dB
    if((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
        print('Signal generator out of range. Adjusting...')
        difference = measurement
        powerIn -= difference
        SG.write('L1 ' + str(powerIn) + ' DM')
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        Wait(SA)
        SA.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(1)
        measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
        time.sleep(1)

        #If the SA measurement is still no 0dBm, finely tune:
        while((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
            print('Tuning to 0dBm...')
            if(measurement > zeroDbTolerance):
                  powerIn -= 1
            if(measurement < zeroDbTolerance):
                  powerIn += 1
            SG.write('L1 ' + str(powerIn) + ' DM')
            SA.write(':INITiate:IMMediate')
            Wait(SA)
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
            time.sleep(1)
              


    #setting the frequency on the signal generator to image rejection frequency to get the image rejection value
    SG.write('F1 ' + str(float(settings['LO']) - signal_freq) + ' HZ')
    SA.write('SENSe:FREQuency:CENTer ' + str(signal_freq))
    time.sleep(1)
    SA.write(':INITiate:IMMediate')
    Wait(SA)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    imageRejection = float(SA.query(':CALCulate:MARKer1:Y?'))
    time.sleep(1)
    imageRejection -= float(settings['image_rejection_cable_loss'])
    print ("Image Rejection: %f" %imageRejection)
    
    #Take screenshot
    SA_Screenshot(name,'Image_Rejection', settings['Band'], folder_path)
    
    #Turn off Signal Generator
    SG.write('RF0')

    return imageRejection

def get_spurs(settings, name, folder_path): 

    #gainAverage is measured by the get_nf_gain function and the product number is entered by the user
    print ("starting in band spur test")
    Path.IPLS()
    ##IN_BAND_SPURS_cable_loss is the loss of the cable that connects SG and the waveguide adapter on the LNB or the RF switch at the specific RF Frequency
    signal_freq = 1000000000
    cable_loss = float(settings['in_band_spur_cable_loss'])
    #zeroDbTolerance is the tolerance used to check if the marker value of SA is close to zero when the frequency is set to verify and amplitude is set to gain average
    zeroDbTolerance = 1

    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['in_band_spur_state_file'] + "'")
    
    #waiting for the state file to load
    Wait(SA)
    #Change bandwidth
    SA.write(':SENSe:FREQuency:STOP ' + str(settings['nf_gain_interested_stop_Frequency']))
    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + signal_freq) + ' HZ')
    #powerIn is the amplitude to be set on SG. We use the average gain and compensate for the cable loss. 
    powerIn = (float(settings['Gain']) - cable_loss) * -1
    #print('Initial PowerIn ' + str(powerIn))
    #setting amplitude of the signal generator to powerIn
    SG.write('L1 ' + str(powerIn) + ' DM')
    #turning RF ON
    SG.write('RF1')
    
    #waiting for the sweep to complete and then read the maximum marker value
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) + 1
    
    #restrating the sweep
    SA.write(':INITiate:IMMediate')  
    time.sleep(sweepTime)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))      
    time.sleep(1)
    
    #verifying if the measurement is close to 0dB
    if((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
        print('Signal generator out of range. Adjusting...')
        difference = measurement
        powerIn = powerIn - difference
        SG.write('L1 ' + str(powerIn) + ' DM')
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        time.sleep(3)
        SA.write(':CALCulate:MARKer1:MAXimum')
        measurement = float(SA.query(':CALCulate:MARKer1:Y?'))

        #If the SA measurement is still no 0dBm, finely tune:
        while((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
            print('Tuning to 0dBm...')
            if(measurement > zeroDbTolerance):
                  powerIn = powerIn - 1
            if(measurement < zeroDbTolerance):
                  powerIn = powerIn + 1
            SG.write('L1 ' + str(powerIn) + ' DM')
            SA.write(':INITiate:IMMediate')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
            time.sleep(1)
              
    #Calculate spur
    time.sleep(sweepTime)
    SA.write('CALCulate:MARKer1:MAXimum:NEXT') #calculate marker peak next
    #SA.write(':CALCulate:MARKer1:MAXimum')
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
    time.sleep(1)
    
    if measurement > float(settings['in_band_spur_limit']):
        value = 'Fail'
    else:
        value = 'Pass'

    #Take screenshot
    SA_Screenshot(name,'In_Band_Spurs', settings['Band'], folder_path)
    
    #Turn off Signal Generator
    SG.write('RF0')
    print('In-band spur test: ' + value)
    return value

def get_lo_leakage_output(settings, name, folder_path): 
    #product number is entered by the user
    print ("starting LO leakage output test")
    
    Path.IPLS()
    
    #lo_leakage_output_muxT_and_cable_loss is the loss of the muxT, to LNB cable, and to RX/modem cable at the LO Frequency
    lo_leakage_output_muxT_and_cable_loss = float(settings['lo_leakage_output_muxT_and_cable_loss'])
    
    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['lo_leakage_state_file'] + "'")
    #waiting 20 seconds for the state file to load
    Wait(SA)

    #restarting the sweep
    
    SA.write('SENSe:FREQuency:CENTer ' + str(settings['LO']))
    SA.write(':INITiate:IMMediate')
    #waiting for the sweep to complete
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
    time.sleep(sweepTime)

    #setting marker1 to LO
    SA.write(':CALCulate:MARKer1:X:CENTer ' + str(settings['LO']))
    #getting the result of marker1
    result = SA.query(':CALCulate:MARKer1:Y?')
    time.sleep(2)
    #LoLeakageOut: we compensate for the muxT and cable losses that go from the lnb throught the muxT and to the modem
    LoLeakageOut = float(result) + lo_leakage_output_muxT_and_cable_loss

    print ("LO Leakage Out: %f" %float(LoLeakageOut))

    SA_Screenshot(name, 'LO_Leakage_Output', settings['Band'], folder_path)   
    
    return float(LoLeakageOut)

def get_lo_leakage_input(settings, name, folder_path): 
    #product number is entered by the user
    print ("starting LO leakage input test")
    
    #Set Path
    Path.Input_Leakage()
    
    #cables_loss_at_lo_frequency is the loss of cable that connects the unit to the spectrum analyzer (SA) at LO frequency
    cables_loss_at_lo_frequency = float(settings['lo_leakage_input_cable_loss'])
    
    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['lo_leakage_state_file'] + "'")
    #waiting 20 seconds for the state file to load
    Wait(SA)
    #restarting the sweep
    SA.write('SENSe:FREQuency:CENTer ' + str(settings['LO']))
    SA.write(':INITiate:IMMediate')
    #waiting for the sweep to complete
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
 
    time.sleep(sweepTime)

    #setting marker1 to LO
    SA.write(':CALCulate:MARKer1:X:CENTer ' + str(settings['LO']))
    #getting the result of marker1
    result = SA.query(':CALCulate:MARKer1:Y?')
    time.sleep(2)
    #LoLeakageOut: we compensate for the muxT and cable losses that go from the lnb throught the muxT and to the modem
    LoLeakageIn = float(result) + cables_loss_at_lo_frequency

    print ("LO Leakage In: %f" %float(LoLeakageIn))
    
    SA.write('INITiate:CONTinuous 0')

    SA_Screenshot(name, 'LO_Leakage_Input', settings['Band'], folder_path)   
 
    return float(LoLeakageIn)

def get_current(): 
    print("starting current test") 
    time.sleep(2)  
    current = AMM.query_ascii_values('') #getting the dc current measurement
    current = round(abs(float(current[0] * 1000)), 1) #converting the measurement reading from A to mA
    print ("Current: %f" %current)
    
    return current

def get_vswrIP(settings, name, folder_path): 
    print ("starting vswr_S11 test")
    
    ##setting Path
    Path.S11_S22()
      
    VNA.write('*CLS')
    VNA.write("MMEM:LOAD '%s'"%settings['S11State'])
    Wait(VNA)
    VNA.write('INITiate:CONTinuous 1')
    time.sleep(1)
    VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    temp_values = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    S11 = temp_values[0]
    print ("S11: %f" %S11)
  
    time.sleep(4) #Wait for notification to disappear
    VNA_Screenshot(name, 'S11', settings['Band'], folder_path)
    
    VNA.write('INITiate:CONTinuous 0')
    
    return S11

def get_vswrOP(settings, name, folder_path):  
    print ("starting vswr_S22 test")
    
    ##setting Path
    Path.S11_S22()
    VNA.write('*CLS')                                    
    VNA.write("MMEM:LOAD '%s'"%settings['S22State'])
    Wait(VNA)
    VNA.write('INITiate:CONTinuous 1')
    time.sleep(1)
    VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
 
    temp_values = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    S22 = temp_values[0]
    print ("S22: %f" %S22)
    
    time.sleep(4) #Wait for notification to disappear
    VNA_Screenshot(name, 'S22', settings['Band'], folder_path)
    
    VNA.write('INITiate:CONTinuous 0')

    return S22

def get_V_Switch(settings):
 
    Vsw = {'Enable' : False, 'V_Switch_Up': None, 'V_Switch_Down': None} 

    Path.IPLS()

    carrier = 1000000000
    threshold = -40
    
    #Check if Voltage switching enabled
    if(bool(settings['Vsw_EN']) == False):
        print('Voltage switching test not enabled')
        return Vsw
    else:
        Vsw['Enable'] = True
        print('Voltage switching enabled. Begin test.')
    
    #Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + str(settings['image_rejection_state_file'])+"'")
    Wait(SA)
    
    #Setup SG and SA with frequency and power level
    SA.write('SENSe:FREQuency:CENTer ' + str(carrier))
    SA.write('SENSe:BANDwidth:RESolution 1000 ') 
    SA.write('SENSe:SWEep:TIME 500 ms') 
    SG.write('F1 ' + str(float(settings['LO']) + carrier) + ' HZ')
    powerIn = (float(settings['Gain']) * -1) + float(settings['image_rejection_cable_loss'])
    SG.write('L1 ' + str(powerIn) + ' DM')
    SG.write('RF1')
    
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
    time.sleep(sweepTime)

    print("Beginning voltage up test:" + str(settings['V_Switch_Up'])) 
    #Voltage switch Up Test
    #sweep +/-1V around switch up point
    if(bool(settings['V_Switch_Up']) == True):       
        for voltage in np.arange(float(settings['V_Switch_Up'])-1,float(settings['V_Switch_Up'])+1,0.1):
            PS.write('APPL CH2, ' + str(voltage) + ' , 0.5')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if(temp < threshold):
                Vsw['V_Switch_Up'] = round(voltage, 3)
                print('Voltage switch up: ' + str(Vsw['V_Switch_Up']))
                break
    else:
        print('Voltage up test not enabled')
    
    print("Beginning voltage down test:" + str(settings['V_Switch_Down']))
    #Voltage switch Down Test
    #sweep +/-1V around switch down point
    if(bool(settings['V_Switch_Down']) == True):       
        for voltage in np.arange(float(settings['V_Switch_Down'])+1,float(settings['V_Switch_Down'])-1,-0.1): 
            PS.write('APPL CH2, ' + str(voltage) + ' , 0.5')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if(temp < threshold):
                Vsw['V_Switch_Down'] = round(voltage, 3)
                print('Voltage switch down: ' + str(Vsw['V_Switch_Down']))
                break
    else:
        print('Voltage down test not enabled')

    SG.write('RF0')
    Pwr_on(settings)
    print(Vsw)    
    return Vsw
    
def Equipment_Init(): 
    global rm, SA, SG, PS, PM, DMM, AMM, VNA, TChamber, Resources
    Resources = {}
    error_str = ' Communication Error. ATE functionality may be limited. Press Enter to acknowledge and continue without resource.'
    rm = visa.ResourceManager()
    try:
        SA = rm.open_resource(SA_VISA)
        SA.timeout = 10000
        Resources['SA'] = SA.query('*IDN?').split('\n')[0]
    except:
        input('Spectrum Analyzer' + error_str)
        Resources['SA'] = None
    try:
        SG = rm.open_resource(SG_VISA)
        SG.timeout = 10000
        Resources['SG'] = SG.query('*IDN?')
    except:
        input('Signal Generator' + error_str)
        Resources['SG'] = None
    try:
        PS = rm.open_resource(PS_VISA)
        PS.timeout = 10000
        Resources['PS'] = PS.query('*IDN?').split('\n')[0]
    except:
        input('Power Supply' + error_str)
        Resources['PS'] = None
    try:
        PM = rm.open_resource(PM_VISA)
        PM.timeout = 10000
        Resources['PM'] = PM.query('*IDN?').split('\n')[0]
    except:
        input('Power Meter' + error_str)
        Resources['PM'] = None
    try:
        DMM = rm.open_resource(DMM_VISA)
        DMM.timeout = 10000
        Resources['DMM'] = DMM.query('*IDN?').split('\n')[0]
    except:
        input('Digital Multimeter' + error_str)
        Resources['DMM'] = None
    try:
        AMM = rm.open_resource(AMM_VISA)
        AMM.timeout = 10000
        Resources['AMM'] = 'HP 3478A Ammeter 2301A10048'
    except:
        input('Ammeter'+ error_str)
        Resources['AMM'] = None
    try:
##        raise Exception
        VNA = rm.open_resource(VNA_VISA)
        VNA.timeout = 10000
        Resources['VNA'] = VNA.query('*IDN?')
    except:
        input('VNA' + error_str)
        Resources['VNA'] = None
    try:  
        DC.Init(DC_Control_Panel_Port)
        Resources['DC_Control_Panel'] = 'Orbtial Research Custom DC Control Panel'
    except:
        input('DC Control Panel'+ error_str)
        Resources['DC_Control_Panel'] = None
    try:
        Path.Init(RCM_216_IP)
        Resources['Switch'] = Path.COAX_SW('MN?') + ' '  + Path.COAX_SW('SN?') + ' ' + Path.COAX_SW('FIRMWARE?')
    except:
        input('Coax Switch' + error_str)
        Resources['Switch'] = None
        input('If the Coax switch is not connected, should you really be using the ATE?')
        input('I mean... what\'s the point?')
        input('Maybe you should consider just using the equipment manually?')
        input('Don\'t be lazy, fix the problem. HINT: It\'s probably the IP address')
        print('Stopping ATE')      
        sys.exit()
    try:
        TChamber = Thermo.Init(rm, TChamber_VISA)
        Resources['TChamber'] = Thermo.ReadWrite(TChamber, '*IDN?')+' '+Thermo.ReadWrite(TChamber, 'VRSN?')
    except:
        input('Temperature Chamber' + error_str)
        Resources['TChamber'] = None

    print("Equipment List:")
    print("----------------------")
    for keys,values in Resources.items():
        print(str(keys)+' : '+ str(values))
    print("----------------------")
    print('\n' + 'Launching ATE2 GUI')

def GUI(settings_master_list):
    global master
    master = tk.Tk() ## Initialize Menu variable
    master.title('ATE2 Program R' + str(current_revision))
    #master.iconphoto(False, PhotoImage(file = 'orblogo4.png'))

    #Variable List
    global ModelDropdown, SNText, StockText, MnfText, ColorText, CxText, POText, YouText, USB_Port_Text, Attenuation, TemperatureDropdown,IP_Text,IP_Port_Text,RecordComm,NFG_Only
    TemperatureProfiles = ['None']
    RecordComm = tk.IntVar()
    NFG_Only = tk.IntVar()
    atten = []
    for num in range(64):
        atten.append(num/2)

    for (dirpath, dirnames, filenames) in os.walk(ATE2_path + '\\_Temperature Profiles'):
        TemperatureProfiles.extend(filenames)

    ModelOptions = Settings_List_Options(settings_master_list)

    #Model Frame
    ModelFrame = tk.LabelFrame(master,text="Settings Menu")
    ModelFrame.grid(row=1,column=1,columnspan=3,sticky='W',padx=2,pady=2)
    
    #Unit Profile Dropdown Menu
    tk.Label(ModelFrame,text="Model: ").grid(row=1,column=1,sticky='W')
    ModelDropdown = ttk.Combobox(ModelFrame,value=ModelOptions) ##Makes the menu
    ModelDropdown.set('None') ## Set Default Value
    ModelDropdown.grid(row=1,column=2,sticky='W',ipadx=115) ## Positions the Menu
    tk.Checkbutton(ModelFrame,text='NFG Testing ONLY',variable=NFG_Only).grid(row=2,column=1,columnspan=2,sticky='W')

    #Temperature Frame
    TemperatureFrame = tk.LabelFrame(master,text="Temperature Menu")
    TemperatureFrame.grid(row=2,column=1,columnspan=3,sticky='W',padx=2,pady=2)

    #Temperature Profile Dropdown Menu
    tk.Label(TemperatureFrame,text="Profile: ").grid(row=1,column=1,sticky='W')
    TemperatureDropdown = ttk.Combobox(TemperatureFrame,value=TemperatureProfiles) ##Makes the menu
    TemperatureDropdown.set('None') ## Set Default Value
    TemperatureDropdown.grid(row=1,column=2,sticky='W',ipadx=115) ## Positions the Menu
    tk.Checkbutton(TemperatureFrame,text='Log communication during cycle',variable=RecordComm).grid(row=2,column=1,columnspan=2,sticky='W')

    #Communication Frame
    CommunicationFrame = tk.LabelFrame(master,text="Communication Menu")
    CommunicationFrame.grid(row=3,column=1,columnspan=3,sticky='W',padx=2,pady=2)


    ## Serial Port
    tk.Label(CommunicationFrame,text='COM Port:').grid(row=1,column=1,sticky='E')
    USB_Port_Text = tk.Entry(CommunicationFrame)
    USB_Port_Text.grid(row=1,column=2,sticky='W',ipadx=2)

    ## IP Address
    tk.Label(CommunicationFrame,text='IP:').grid(row=2,column=1,sticky='E')
    IP_Text = tk.Entry(CommunicationFrame)
    IP_Text.grid(row=2,column=2,sticky='W',ipadx=2)

    ## IP Port
    tk.Label(CommunicationFrame,text='Port:').grid(row=3,column=1,sticky='E')
    IP_Port_Text = tk.Entry(CommunicationFrame)
    IP_Port_Text.grid(row=3,column=2,sticky='W',ipadx=2)

    ## Combobox for attenuation value
    AttName = tk.Label(CommunicationFrame,text="Attenuation:").grid(row=6,column=1,sticky='W')
    Attenuation = ttk.Combobox(CommunicationFrame,value=atten)
    Attenuation.set('0.5')
    Attenuation.grid(row=6,column=2,sticky='W')
    tk.Label(CommunicationFrame,text='            Gain = Max Gain - Attenuation').grid(row=6,column=3,sticky='E')
    
    #Information Frame
    InfoFrame = tk.LabelFrame(master,text="Information Menu")
    InfoFrame.grid(row=4,column=1,columnspan=3,sticky='W',padx=2,pady=2)

    #Serial Number
    SN = tk.Label(InfoFrame,text='Serial Number:').grid(row=1,column=1,sticky='E')
    SNText = tk.Entry(InfoFrame)
    SNText.grid(row=1,column=2,sticky='W')

    #Stock Number
    Stock = tk.Label(InfoFrame,text='Stock Number:').grid(row=1,column=3,sticky='E')
    StockText = tk.Entry(InfoFrame)
    StockText.grid(row=1,column=4,sticky='W')

    #Manufacturer S/N if present
    Mnf = tk.Label(InfoFrame,text='Supplier S/N:').grid(row=2,column=1,sticky='E')
    MnfText = tk.Entry(InfoFrame)
    MnfText.grid(row=2,column=2,sticky='W')

    #Unit Color
    Color = tk.Label(InfoFrame,text='Colour:').grid(row=2,column=3,sticky='E')
    ColorText = tk.Entry(InfoFrame)
    ColorText.grid(row=2,column=4,sticky='W')

    #Customer Name
    Cx = tk.Label(InfoFrame,text='Customer:').grid(row=3,column=1,sticky='E')
    CxText = tk.Entry(InfoFrame)
    CxText.grid(row=3,column=2,sticky='W')

    #PO Number
    PO = tk.Label(InfoFrame,text='PO Number:').grid(row=3,column=3,sticky='E')
    POText = tk.Entry(InfoFrame)
    POText.grid(row=3,column=4,sticky='W')

    #Tester Name
    You = tk.Label(InfoFrame,text='Tester:').grid(row=4,column=1,sticky='E')
    YouText = tk.Entry(InfoFrame)
    YouText.grid(row=4,column=2,sticky='W')
            
    #Button to Begin Testing
    width = 10
    tk.Button(master,text='OFF',command=StopATE,width=width).grid(row=7,column=1,sticky='W',padx=2,pady=2)
    Stop = tk.Label(master,text ='Press CTRL+C to stop ATE during test.').grid(row=7,column=2) 
    tk.Button(master,text='BEGIN',command=StartATE,width=width).grid(row=7,column=3,sticky='E',padx=2,pady=2)

    master.mainloop()

def test():
    pass

def StartATE():
    try:
        main()
    except KeyboardInterrupt:
        print('Keyboard interrupt detected.')
        StopATE()

def StopATE():
    Pwr_off()
    try:
        SG.write('RF0')
        SA.write(':ABORt')
        Thermo.STOP(TChamber)
        print('Stopping Program.')
    except:
        #Sometimes equipment may not be connected
        pass
    sys.exit()     
    
def Settings_List_Options(settings_list):
    options = []
    for i in range(len(settings_list)):
        options.append(settings_list[i][0]['Model']) 
    return options  
 
settings_master_list = DS_Gen.Load_All_Settings(settings_path) #Load settings
Equipment_Init() #Initialize connection to all measurement equipment
GUI(settings_master_list)#Call GUI

#Version Log:
#------------------------------
#Revision 1.0 - Ported over code from LNB ATE V1.6.py
#Revision 2.0 Rearranged NFG test within main() to account for non-remote NS inbetween tests
#Revision 2.1 Added Image Filter for prettier screenshots. Orbital_Image_Filter_R3.py
#Revision 2.2 - 20210902 - Inlcuded new datasheet generator Revision 2.0
#Revision 3.0 - 20211103 - Remove Attenuation2 and 3. Added optional temperature profile testing. Updated to ATE2_Datasheet_Generator_R3.py
#Revision 4.0 - 20220315 - Revamp waiting time for temperature cycle to include $getst messages. Updated Communication Menu to reflect WBKaLNB's and connection protocols with LNB_Communication_R1.py. Updated equipment connection reliability and recording
#Revision 5 - 20220318 - Added subfolders to temperature cycle testing to save datasheets
#R6 - 20220421 - Transposed ATE2_Settings.csv, accounted for it in ATE2_Datasheet_Generator_R4.py. Added NFG only testing option. Added attenuation management when changing bands. Upgraded to LNB_Communication_R5.py
