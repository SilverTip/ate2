from PIL import Image, ImageEnhance, ImageOps
import os

PATH = os.path.dirname(__file__)

orbital_light_blue = (0,170,238)
orbital_dark_blue = (5,22,80)
dark_green = (1,68,33)
purple = (255,0,255)

ORBLOGO = 'orblogo4.PNG'
IMAGE_INPUT = 'input.png'
IMAGE_OUTPUT = 'output.png'

def Filter(file, inversion):

    #file = folder + '\\' + name

    #Open image(s)
    orb = Image.open(ORBLOGO)
    try:
        image = Image.open(file)
    except:
        print('Image not found')
        return
    
    #Convert to PIL standard
    image = image.convert('RGB') # convert image to RGB

    #Check if inversion is enabled
    if(inversion == True):
        image = ImageOps.invert(image) #invert colours
    
    width, height = image.size
    orb_width, orb_height = orb.size
    corner = 0
    for x in range(width):  
        for y in range(height):  
            # access current pixel broken down into RGB values
            r, g, b = image.getpixel((x,y))
            new_pixel = (r,g,b)
            
            #Modify pixel:
            if(r > 200 and g < 200 and b > 150): #if pink-ish, make orbital light blue
                new_pixel = orbital_light_blue
            elif(r > 190 and g < 100 and b < 100 and r < 250): #if brown, make orbital dark blue
                new_pixel = orbital_dark_blue
            elif(r > 190 and g < 100 and b < 100): #if red, turn green
                new_pixel = purple
            elif(r == 0 and g == 255 and b == 255): #If cyan, turn red
                new_pixel = (255,0,0) 
                
            # reassign the pixel value in the image to be the changed version
            image.putpixel((x,y), new_pixel)

    #Paste Orbtial logo lower right
    image.paste(orb, (width - orb_width - corner, height - orb_height - corner)) #Append Orbital logo bottom right
    image.save(file)


def main():
    filename = IMAGE_INPUT
    Filter(PATH, filename, inversion=True)

#If module is run as script, run main
if __name__ == '__main__':
    main()

#Version
#R4 - 20210816 - Updated trace colours to blue and purple for best contrast
