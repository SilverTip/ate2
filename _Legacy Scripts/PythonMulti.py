#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
'''


import logging

class rawTcpBackend:
    '''
    rawTcpBackend : Works without any PyVisa / NIVisa installation
    only implements Visa write / query messages
    works only for Ethernet / TCP/IP Connections
    but without any other drivers!
    bare Python solution
    '''

    def __init__(self,interface):
        import socket

        txt = interface.split('::')

        self.write_termination = '\n'
        self.read_termination  = '\n'

        if(txt[0] != 'TCPIP'):  raise ValueException('not a Valid interface')
        self.tcp_ip = txt[1]
        if(txt[2] == 'INSTR'):  self.tcp_port = 111
        else:                   self.tcp_port = int(txt[2])

        self.sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.sock.connect( (self.tcp_ip, self.tcp_port) )

    def __del__(self):
        self.sock.close()

    def query(self,string):
        self.write(string)

        self.sock.settimeout(0.5)
        data = self.sock.recv(1024)

        return data.decode('UTF-8').strip(self.read_termination)

    def write(self,string):
        try :
            self.sock.sendall( (string + self.write_termination).encode('ASCII') )
        except socket.error:
            print('error')

class HS9000():
    '''
    HS9000 : base Class for HS9000 low PhaseNoise SignalGenerator
    '''

    def __init__(self,interface,chNbr,backend = 'PyVisa',logLevel = logging.NOTSET):
        '''
        Needs a PyVisa conform interface String: example : "192.168.99.124::9760::SOCKET"
           HS9000 listens to TCP Port 9760 -> ::9760::SOCKET
        Needs a Channel Number 1-4
        Different Backends are available :
            PyVisa        : pip install PyVisa
            PyVISA-py     : pip install PyVisa PyVISA-py
            rawTcpBackend : simple TCP implementation
        Logging is realized with python logging package directly to the console
           Options : logging.NOTSET / logging.DEBUG / logging.INFO
           logging.INFO  : Echo
           logging.DEBUG : return Value from HS9000
        '''

        self.chNbr      = chNbr
        self.interface  = interface

        self.log = logging.getLogger('HS9000')
        self.log.setLevel(logLevel)
        formatter = logging.Formatter('[%(name)s %(levelname)s : %(funcName)s] \t %(message)s')
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        self.log.addHandler(handler)

        if(backend == 'PyVisa'):
            import visa
            rm = visa.ResourceManager()
            self._inst = rm.open_resource(interface)

        elif(backend == 'PyVISA-py'):
            import visa
            rm = visa.ResourceManager('@py')
            self._inst = rm.open_resource(interface)

        elif(backend == 'rawTcpBackend'):
            self._inst = rawTcpBackend(interface)
        else:
            raise ValueException('Unknown backend')

        ''' write / read Termination not Visa Conform : needs \n character only '''
        self._inst.write_termination = '\n'
        self._inst.read_termination  = '\n'

        self.enableOutput(False)        # standard initialisation procedure
        self.setPowerLevel('-100dBm')

    def __repr__(self):
        return self.name()

    def name(self):
        '''
        returns a String descriptiom if the Singal Generator
        '''
        # monkey test
        self._inst.query(':CH1:TEMP?') # if it runs without failure, it works ;)
        # monkey test
        return 'HS9003A : interface = {} : Channel Number = {}'.format(self.interface,self.chNbr)

    def setPowerLevel(self,PowerString):
        '''
        sets Output Power Level: example setPowerLevel('-10dBm')
        '''
        answer = self._inst.query(':CH{}:PWR:{}'.format(self.chNbr,PowerString))

        self.log.info( PowerString)
        self.log.debug(answer)

    def getPowerLevel(self):
        '''
        returns Output Power Level
        '''
        answer = self._inst.query(':CH{}:PWR?'.format(self.chNbr))

        self.log.debug(answer)
        return answer

    def setFreq(self,frequencyString):
        '''
        sets Output Frequncy: example setFreq('100.250MHz')
        '''
        answer = self._inst.query(':CH{}:FREQ:{}'.format(self.chNbr,frequencyString))

        self.log.info( frequencyString)
        self.log.debug(answer)

    def getFreq(self):
        '''
        returns Output Frequncy
        '''
        answer = self._inst.query(':CH{}:FREQ?'.format(self.chNbr))

        self.log.debug(answer)
        return answer

    def enableOutput(self,enable):
        '''
        enables Output : example enableOutput(True)
        '''
        if(enable == True): answer = self._inst.query(':CH{}:PWR:RF:ON'.format( self.chNbr))
        else:               answer = self._inst.query(':CH{}:PWR:RF:OFF'.format(self.chNbr))

        self.log.info( str(enable))
        self.log.debug(answer)

    def enableExtRef(self,enable):
        '''
        enables External or Internal Reference : example enableExtRef(True)
        '''
        if(enable == True): answer = self._inst.query(':REF:EXT:10MHz')
        else:               answer = self._inst.query(':REF:INT:100MHz')

        self.log.info( str(enable))
        self.log.debug(answer)

    def getReference(self):
        '''
        returns current Reference
        '''
        answer = self._inst.query(':REF:INT:100MHz')

        self.log.debug(answer)
        return answer

    def getTemp(self):
        '''
        returns current Temperature
        '''
        answer = self._inst.query(':REF:STATUS?')

        self.log.debug(answer)
        return answer

if __name__ == '__main__':

    sig = HS9000('TCPIP::10.0.10.135::9760::SOCKET',1,logLevel=logging.DEBUG,backend = 'rawTcpBackend')
    #sig = HS9000('TCPIP::192.168.99.124::9760::SOCKET',1,logLevel=logging.DEBUG,backend = 'PyVISA-py')
    #sig = HS9000('TCPIP::192.168.99.124::9760::SOCKET',1,logLevel=logging.INFO)

    print( sig.name() )
    sig.enableExtRef(True)
    sig.enableOutput(True)
    sig.setPowerLevel('-10dBm')
    sig.setFreq('10.111111MHz')
    print(sig.getFreq())
    print(sig.getPowerLevel())
    print(sig.getTemp())
    print(sig.getReference())
