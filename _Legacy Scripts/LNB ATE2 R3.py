#LNB ATE2 R3.py
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-11-03

current_revision = 3

import pyvisa as visa
import csv
import sys
import time
import math
import json
import os
import shutil
import tkinter as tk
from tkinter import ttk
#from os import walk
#from os import listdir
import datetime
import socket
import numpy as np
from pynput.keyboard import Key, Controller
from subprocess import Popen, PIPE
from PIL import Image, ImageOps

#Custom modules
import ATE2_DC_Control_Module as DC
import ATE2_Coax_Path_Module as Path
import ATE2_Datasheet_Generator_R3 as DS_Gen
import Orbital_Image_Filter_R4 as ImageFilter
import Thermotron_8200_Plus_Module_R1 as Thermo


## Equipment Addresses ##
SA_VISA = 'TCPIP0::10.0.10.184::hislip0::INSTR'              #MXA 9020B Spectrum Analyzer
SG_VISA = 'GPIB2::5::INSTR'                                  #Anritsu 68367C Signal Generator - GPIB commands only. No SCPI
PS_VISA = 'USB0::0x05E6::0x2230::9104855::0::INSTR'          #Keithley Instruments 2230-30-1 Power Supply
PM_VISA = 'USB0::0x0957::0x2B18::MY48100947::0::INSTR'       #Agilent U2001A Power Meter
DMM_VISA = 'TCPIP0::10.0.10.125::hislip0::INSTR'             #Keysight 34461A Voltmeter / Temperature Probe
AMM_VISA = 'GPIB2::23::INSTR'                                #HP3478A Ammeter
VNA_VISA = 'TCPIP0::10.0.10.131::inst0::INSTR'               #Fieldfox N9918A
DC_Control_Panel_Port = 'COM3'                               #Custom DC control panel
RCM_216_IP = '10.0.10.169'                                   #Minicircuits RCM-216 
TChamber_VISA = 'TCPIP0::10.0.10.151::8888::SOCKET'          #Thermotron8200+

ATE2_path = "\\\SVR-1\Production\_To_be_REVIEWED\In_Progress\ATE2"
settings_path = '\\\SVR-1\Production\_To_be_REVIEWED\In_Progress\ATE2\ATE2_SETTINGS.csv'  
State_path = '\\\SVR-1\\Newmore\\_1_Instr-State-Files\\MXA_N9020B_States\\ATE2\\'

def main():
    print("*******BEGIN NEW TEST*******")
    
    #Declaring dictionary to manage and store test results
    data = {'NFG': 
                {'Frequency': [],'loss_before' : None,'loss_after' : None,'NF' : [],'Gain' : [],'averageNF' : None,'averageGain' : None,'amplitude_response_10MHz' : [],'amplitude_response_120MHz' : [],'amplitude_response_500MHz' : [], 'amplitude_response_1000MHz' : [],'amplitude_response_10MHz_max' : None,'amplitude_response_120MHz_max' : None,'amplitude_response_500MHz_max' : None,'amplitude_response_1000MHz_max' : None,'min_NF' : None,'max_NF' : None,'min_Gain' : None,'max_Gain' : None},
            'P1dB': {'Input' : [] , 'Output' : [], 'P1dB' : None, 'OIP3' : None}, 'image_rej': None, 'spurs': None, 'lo_leakage_out': None, 'current': None, 'lo_leakage_in': None, 'S11': None, 'S22': None,
            'phase': {'RAW' : [], '10Hz': None, '100Hz' : None, '1kHz' : None, '10kHz' : None, '100kHz' : None, '1MHz' : None, '10MHz' : None, '100MHz' : None},
            'RS485_enable' : False, 'CC_enable' : False, 'V_switch' : {'Enable' : False, 'V_Switch_Up': None, 'V_Switch_Down': None},
            'SA': None, 'SG': None, 'PS': None, 'PM': None, 'DMM': None, 'AMM': None, 'VNA': None, 'TChamber': None,
            'date': None, 'time': None, 
            'Customer': None, 'PO': None, 'Tester': None, 'Color': None, 'Band': None, 'Manufacturer_SN': None, 'Orbital_SN': 'No_Name', 'stock_number': None, 'Model' : None,
            'Port' : None, 'Attenuation' : None, 'Script_Revision' : current_revision, 
            'Temperature_enable': False, 'TemperatureProfile':None, 'Temperature': None
            }
          
    #Get equipment names    
    EQID(data) 

    #Path
    print(ATE2_path) #Obtain program directory path
    
    #Obtaining unit info from input fields
    data['Orbital_SN'] = str(SNText.get())
    if data['Orbital_SN'] == '':
        data['Orbital_SN'] = 'No_Name'      
    data['stock_number'] = str(StockText.get())
    data['Manufacturer_SN'] = str(MnfText.get())
    data['Customer'] = str(CxText.get())
    data['PO'] = str(POText.get())
    data['Color'] = str(ColorText.get())
    data['Tester'] = str(YouText.get())
    data['Port'] = USB_Port_Text.get()      
    data['Attenuation'] = float(Attenuation.get())         
 
    #Obtaining user selected settings
    model_name = ModelDropdown.get()
    settings_list = DS_Gen.Find_Settings(settings_master_list, model_name)
    
    #Generate Folder 
    folder_path = Make_Folder(data['Orbital_SN'])  #Making Folder for Unit 
    
    #Check for band switching
    data['RS485_enable'] = bool(settings_list[0]['RS485_EN'])
    data['V_switch']['Enable'] = bool(settings_list[0]['Vsw_EN'])
    print('V Switching enabled: ' + str(data['V_switch']['Enable']))
    data['CC_enable'] = bool(settings_list[0]['CC_EN'])

    #Record Model No
    data['Model'] = settings_list[0]['Model']

    #Determine if temperature profile selected
    if(TemperatureDropdown.get() != 'None'):
        data['Temperature_enable'] = True
        print('Temperature Profile Enabled')

    #Run ATE with or without temperature profile
    if(data['Temperature_enable']): #if temperature profile enabled: run temperature profile with instances of the ATE determined by the profile
        
        
        #Load Temperature Profile
        profile = Thermo.LoadProfile(TemperatureDropdown.get())
        print('Profile: ' + str(TemperatureDropdown.get()))
        data['TemperatureProfile'] = str(TemperatureDropdown.get())

        #Determine testing time
        testDuration = 0
        for temp, period in profile:
            testDuration += period
        print('Test Duration (Hours):', round(testDuration,1))
        
        #Execute temperature profile
        Thermo.START(TChamber)
        for temperature, period in profile:
            current_temp = Thermo.TEMP(TChamber)
            print("Current Temperature: " + str(current_temp) + 'C')
            if(temperature == 'ATE'):
                Thermo.HOLD(TChamber)
                name = data['Orbital_SN'] + '_' + str(current_temp)
                data['Temperature'] = current_temp
                RunATE(settings_list,data,name,folder_path)
                Thermo.RESUME(TChamber)
            elif(type(temperature) is str):
                pass
            else:
                ramp_rate = round(abs(float(current_temp) - float(temperature)) / (float(period)*60),2) #Time in hours
                Thermo.SET(TChamber,temperature)
                time.sleep(0.25)
                Thermo.RAMP(TChamber,ramp_rate)
                print('Time:', Thermo.TimeStamp()[1], 'Current Temperature:', current_temp, 'Next Temperature:', temperature, 'Ramp Rate(C/min):', ramp_rate, 'Step Duration (Hours):', period) 
                time.sleep(period * 60 * 60) #in seconds
                
          
        print('Test Completed @', Thermo.TimeStamp()[1])
        Thermo.STOP(TChamber)

    else: #Run ATE once
        RunATE(settings_list,data,data['Orbital_SN'],folder_path)

    Pwr_off() 
    #Merge datasheets
    DS_Gen.Merge(folder_path, data['Orbital_SN'] + ' Datasheet')
       
    print("*******TEST COMPLETE*******")

def RunATE(settings_list,data,name,folder_path):
    print('Running ATE2...')

    #Get Date and Time
    data['date'] = str(datetime.datetime.now().date())   
    data['time'] = str(datetime.datetime.now().time())
    print (data['date'])
    print (data['time'])
    

    #Begin NFG Test
    if(not data['Temperature_enable']): #Skip if in the temperature chamber
        input('Connect Noise Source to DUT. Press ENTER to continue.')
    for settings in settings_list:       
        data['Band'] = Change_Band(settings, data)
        print('Starting Band ' + str(data['Band']) + ' NFG test')
        data['current'] = get_current()
        data['NFG'] = get_NFG(settings, name, folder_path)
        #Create .json file for raw band data
        Save_JSON(folder_path, name, data)
          
##    #Begin Other Testing
##    if(not data['Temperature_enable']): #Skip if in the temperature chamber
##        input('Connect Coax Cable to DUT. Press ENTER to continue.')
##    for settings in settings_list:      
##        data = Load_JSON(folder_path, name, settings['Band']) #Load BandX NFG data into current data dictionary 
##        data['Band'] = Change_Band(settings, data)
##        print('Starting Band ' + str(settings['Band']) + ' test')
##        data['current'] = get_current() 
##        data['P1dB'] = get_p1db(settings)
##        data['phase'] = get_phase(settings, name, folder_path)
##        data['image_rej'] = get_image_rejection(settings, name, folder_path)
##        data['V_switch'] = get_V_Switch(settings)
##        data['spurs'] = get_spurs(settings, name, folder_path)
##        data['lo_leakage_out'] = get_lo_leakage_output(settings, name, folder_path)
##        data['lo_leakage_in'] = get_lo_leakage_input(settings, name, folder_path)
##        if(not data['Temperature_enable']): #Skip if in the temperature chamber
##            pass
##            #data['S11'] = get_vswrIP(settings, name, folder_path)
##            #data['S22'] = get_vswrOP(settings, name, folder_path)
##   
##        #Create .json file for raw band data
##        Save_JSON(folder_path, name, data)

        #Generate band datasheet
        DS_Gen.Generate(folder_path, name, data, settings)

        print ("ATE2 Test completed.\n")

def Save_JSON(folder_path, name, data):
    data_file = folder_path + "\\" + name + '_Band' + data['Band'] + '.json'
    with open(data_file,'w') as f:
        json.dump(data,f)
    f.close()

def Load_JSON(folder_path, name, band):
    data_file = folder_path + "\\" + name + '_Band' + str(band) + '.json'
    with open(data_file, 'r') as f:
        data = json.load(f)
    f.close()
    return data 

def Change_Band(settings, data):
    Pwr_on(settings)  #Change input voltage depending on band
    if(bool(settings['RS485_EN']) == True): #If RS485 enabled, change band
        RS485_SW(data['Port'], settings['Band'], float(data['Attenuation']))   
    if(bool(settings['CC_EN']) == True): #If contact closure is enabled, pause to allow user to change band
        input('Switch to Band ' + settings('Band') + '. Press Enter to continue.')   
    time.sleep(1) #Give controller time to detect new voltage and switch
    
    return settings['Band']
    
def Pwr_on(settings): 
    PS.write('APPL CH2, ' + str(settings['Voltage']) + ' , 0.75')
    time.sleep(1)
    PS.write(':OUTPut:STATe 1')
    
def Pwr_off(): 
    PS.write(':OUTPut:STATe 0')

def Make_Folder(productNumber): 
    if(productNumber == ''):
        productNumber == 'No_Name' #If productNumber is left blank, the shutil.rmtree() function will nuke ATE2 script directory (ask me how I know)
    path = os.getcwd() 
    dst = str(path) + '\\' + productNumber
    print ("Creating Folder...")
    if(os.path.exists(dst)):
        shutil.rmtree(dst) #Careful with this function. Can Nuke entire directory trees
        time.sleep(1)
    os.mkdir(dst)
    return dst

def SA_Screenshot(productNumber,tag,band,folder_path): 
    
    #SA.write(":DISPlay:FSCReen:STATe 0") 
    SA.write(":DISPlay:FSCReen:STATe 1") 

    filename = productNumber + "_Band" + str(band) + '_' + tag + '.PNG'
    
    #temporary storage location (becasue MXA cannot save to Production folder)
    SA_save_path = "\\\SVR-1\\Newmore\\" 

    #saving a screenshot on the equipment
    SA.write(":MMEMory:STORe:SCReen '" + SA_save_path + filename + "'")
    
    time.sleep(1)
    
    ## Move screenshots to proper folder
    shutil.move(SA_save_path + filename, folder_path + '\\' + filename)
    
    #Include image processing for datasheet (greyscaling, inversion, etc)
    ImageFilter.Filter(folder_path, filename, True)

def VNA_Screenshot(productNumber,tag,band, folder_path): 

    #Setup filename and folder path
    filename = productNumber + "_Band" + str(band) + '_' + tag + '.PNG'

    #saving a screenshot on the equipment                                 
    VNA.write(':MMEMory:STORe:IMAGe "TESTZ"')
    
    #fetching the image data in an array
    fetch_image_data = VNA.query_binary_values('MMEMory:DATA? "TESTZ.png"',datatype='B',is_big_endian=False,container=bytearray)

    #creating a file in testing folder with the product number and tag, then write the image data to it
    save_dir = open(folder_path + '\\' + filename, 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    VNA.write(":MMEM:DEL 'TESTZ.PNG'")
    VNA.write("*CLS")

    #Include image processing for datasheet (greyscaling, inversion, etc)
    ImageFilter.Filter(folder_path, filename, True)

def Wait(resource): 
    #This function is used to wait for a resource to complete its operation
    #Poll the Event Status Register (ESR) every second to determine if the operation is complete.
   resource.write('*OPC')
   try:
       while(int(resource.query('*ESR?')) != 1):
           time.sleep(1)
           #print('waiting...')
   except KeyboardInterrupt:
       StopATE()
  
def get_NFG(settings, name, folder_path): 
    
    NFG = {'Frequency': [],'loss_before' : None,'loss_after' : None,'NF' : [],'Gain' : [],'averageNF' : None,'averageGain' : None,'amplitude_response_10MHz' : None,'amplitude_response_120MHz' : None,'amplitude_response_500MHz' : None,'amplitude_response_1000MHz' : None,'amplitude_response_10MHz_max' : None,'amplitude_response_120MHz_max' : None,'amplitude_response_500MHz_max' : None,'amplitude_response_1000MHz_max' : None,'min_NF' : None,'max_NF' : None,'min_Gain' : None,'max_Gain' : None}
        
    print ("starting NF and Gain test")
    
    #set NFG Path
    Path.NFG()

    #Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['nf_gain_state_file'] + "'")
    Wait(SA)

    #Add limits
    
    
    #Start NFG Sweep  
    SA.write(":DISPlay:FSCReen:STATe 0")
    SA.write(":DISPlay:FSCReen:STATe 1")
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write('INITiate:RESTart')
    Wait(SA)

    #Aquire start and stop frequencies, and number of points
    freq_start = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')[0])
    freq_stop = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')[0])
    number_of_points = float(SA.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')[0])

    #Saving frequency data
    NFG['Frequency'] = []
    NFG['Frequency'].append(freq_start)
    add_next_Freq=((freq_stop-freq_start)/(number_of_points-1))
    i = 1
    next_Frequency = freq_start
    while i < number_of_points-1:
        next_Frequency = next_Frequency + add_next_Freq
        NFG['Frequency'].append(next_Frequency)
        i += 1   
    NFG['Frequency'].append(freq_stop)

    #Finding the index of the interested start and stop frequencies 
    i = 0
    flag = 0
    interested_freq = {}
    while i < len(NFG['Frequency']):
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_start_Frequency'])) and flag == 0:
            interested_freq['start'] = i
            flag = 1
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_stop_Frequency'])):
            interested_freq['stop'] = i
            break
        i += 1
    
    #Detect compensation type and store value[s]
    loss_after_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:AFTer:MODE?')
    loss_before_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:BEFore:MODE?')
    if "FIX" in loss_after_mode:  
        NFG['loss_after'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:VALue?')
    elif "TABL" in loss_after_mode:  
        NFG['loss_after'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:TABLe:DATA?')
    else:
        pass
    if "FIX" in loss_before_mode:
        NFG['loss_before'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:VALue?')
    elif "TABL" in loss_before_mode:
        NFG['loss_before'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:TABLe:DATA?')
    else:
        pass

    #Fetch NF array and calculate the min and max NF over the interested frequencies
    NFG['NF'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:NFIG?')
    i = interested_freq['start']
    interested_NF = []
    while i <= interested_freq['stop']:
        interested_NF.append(NFG['NF'][i])
        i += 1
    NFG['min_NF'] = min(interested_NF)
    NFG['max_NF'] = max(interested_NF)
    print ("Minimum NF: %f" %NFG['min_NF'])
    print ("Maximum NF: %f" %NFG['max_NF'])
    
    #calculating the linear noise figure values (only for interested frequencies)
    i = interested_freq['start']
    LinearNF = []
    while i <= interested_freq['stop']:
        try:
            LinearNF.append(10**(NFG['NF'][i]/10))
        except:
            LinearNF.append(0)
        i += 1

    
    #calculating the average noise figure by taking the average of the linear noise figure values and then using math to get the average noise figure (only for interested frequencies)
    i = 0
    sumLinearNF  = 0
    while i < len(LinearNF):
        sumLinearNF += LinearNF[i]
        i += 1  
    averageLinearNF = sumLinearNF / len(LinearNF)
    NFG['averageNF'] = 10 * math.log10(averageLinearNF)
    print ("Average NF: %f" %NFG['averageNF'])

    #Fetch Gain array and calculate the min and max Gain over the interested frequencies
    NFG['Gain'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
    i = interested_freq['start']
    interested_Gain = []
    while i <= interested_freq['stop']:
        interested_Gain.append(NFG['Gain'][i])
        i += 1
    NFG['min_Gain'] = min(interested_Gain)
    NFG['max_Gain'] = max(interested_Gain)
    print ("Minimum Gain: %f" %NFG['min_Gain'])
    print ("Maximum Gain: %f" %NFG['max_Gain'])
    
    #calculating the linear gain values (only for interested frequencies)
    i = interested_freq['start']
    LinearGain = [] 
    while i <= interested_freq['stop']:
        try:
            LinearGain.append(10**(NFG['Gain'][i]/10))
        except:
            LinearGain.append(0)
        i += 1 
        
    #calculating the average gain by taking the average of the linear gain values and then using math to get the average gain (only for interested frequencies)
    i = 0 
    sumLinearGain  = 0
    while i < len(LinearGain):
        sumLinearGain += LinearGain[i]
        i += 1 
    averageLinearGain = sumLinearGain / len(LinearGain)
    NFG['averageGain'] = 10 * math.log10(averageLinearGain)
    print ("Average Gain: %f" %NFG['averageGain'])
    
    #Gather gain ripple per bandwidth step
    NFG['amplitude_response_10MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 10000000)
    NFG['amplitude_response_10MHz_max'] = max(NFG['amplitude_response_10MHz'])
    NFG['amplitude_response_120MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 120000000)
    NFG['amplitude_response_120MHz_max'] = max(NFG['amplitude_response_120MHz'])
    NFG['amplitude_response_500MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 500000000)
    NFG['amplitude_response_500MHz_max'] = max(NFG['amplitude_response_500MHz'])
    try:
        NFG['amplitude_response_1000MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 1000000000)
        NFG['amplitude_response_1000MHz_max'] = max(NFG['amplitude_response_1000MHz']) 
    except:
        pass
    print('10MHz Ripple: ' + str(NFG['amplitude_response_10MHz_max']))
    print('120MHz Ripple: ' + str(NFG['amplitude_response_120MHz_max']))
    print('500MHz Ripple: ' + str(NFG['amplitude_response_500MHz_max']))
    print('1000MHz Ripple: ' + str(NFG['amplitude_response_1000MHz_max']))

    #Scale state file for screenshot
    SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:RLEVel " + str(round(NFG['averageNF'],1)))
    SA.write("DISPlay:NFIGure:TRACe:GAIN:Y:SCALe:RLEVel " + str(round(NFG['averageGain'],0)))
    SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:RPOSition CENTer")
    SA.write("DISPlay:NFIGure:TRACe:GAIN:Y:SCALe:RPOSition CENTer")
    time.sleep(1)
    
    SA_Screenshot(name, 'NFG', settings['Band'], folder_path)
    
    return NFG

def AmplitudeResponse(index_start_freq, index_stop_freq, freq, gain, bandwidth): 
        #Gather amplitude ripple per frequency step
        ripple = []
        temp_gain = []
        i = index_start_freq
        for values in gain[index_start_freq:index_stop_freq+1]:
            temp_gain.append(values)
            if(freq[i] - freq[index_start_freq] >= bandwidth):           
                ripple.append((max(temp_gain)-min(temp_gain))/2)
                del temp_gain[0]
            i += 1
        return ripple

def get_p1db(settings):
    print ("starting P1dB test")  
    P1dB = {'Input' : [] , 'Output' : [], 'P1dB' : None, 'OIP3' : None}
    powerMeterPowerMax = 20
    powerMeterFreq = 1500000000
    gain = []
    running_average = []
    
    Path.P1dB() ##set P1dB Path
    
    SG.write('F1 ' + str(float(settings['LO']) + powerMeterFreq) + ' HZ') #setting the signal generator frequency   
    PM.write(':INITiate1:CONTinuous %d' % (1)) #triggering the power meter to take measurements continuosly 
    PM.write(':SENSe:FREQuency:FIXed %G' % (powerMeterFreq))
    
    P1dB['Input'].append((float(settings['Gain']) * -1)) #Start at about 0dBm @ Output of LNB
    powerSweepTo = P1dB['Input'][-1] + 20 #End at 20dB higher than the initial input
    
    flag = True
    while P1dB['Input'][-1] < powerSweepTo:
        SG.write('L1 ' + str(P1dB['Input'][-1] + float(settings['p1db_cable_loss'])) + ' DM') #setting the signal generator amplitude  
        SG.write('RF1')
        time.sleep(0.4)    
        P1dB['Output'].append(float(PM.query(':FETCh1?')) + float(settings['p1db_muxT_and_cable_loss'])) #fetch measurement, compensate for loss, and add it to P1dB['Output'] list
        
        gain.append(P1dB['Output'][-1] - P1dB['Input'][-1]) #Calculating gain at present step
        running_average = np.average(gain) #Calculate average gain over all steps
        difference = running_average - gain[-1] #Find the difference between the average gain and the current gain
        
        if(difference > 1 and flag == True): #If the difference between the average gain and the current gain is greater than 1dB, 1dB compression point found.
            P1dB['P1dB'] = P1dB['Output'][-1] - (difference - 1) #Record P1dB, adjusting for overshoot
            P1dB['OIP3'] = P1dB['P1dB'] + 10 #P1dB + 10 = OIP3
            flag = False
        
        P1dB['Input'].append(P1dB['Input'][-1] + 1)

    SG.write('RF0')

    
    print('P1dB: ' + str(P1dB['P1dB']))
    return P1dB

def get_phase(settings, name, folder_path):

    phase = {'RAW' : [], '10Hz': None, '100Hz' : None, '1kHz' : None, '10kHz' : None, '100kHz' : None, '1MHz' : None, '10MHz': None, '100MHz': None}
    
    print ("starting Phase Noise test")
    
    carrier = 1500000000
    
    ##setting IPLS Path
    Path.IPLS()

    #Loading state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + str(settings['phase_noise_state_file'])+"'")
    Wait(SA)

    time.sleep(1)
    
    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + carrier) + ' HZ')
    SG.write('L1 ' + str(((float(settings['Gain'])) * -1) + float(settings['phase_noise_loss'])) + ' DM')
    SG.write('RF1')
    time.sleep(1)
    
    #Start Phase Sweep
    SA.write('SENSe:FREQuency:CARRier ' + str(carrier))
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write('INITiate:IMMediate')
    Wait(SA)
    
    #Gather all raw data from frequency plot
    phase['RAW'] = SA.query_ascii_values(':FETCH:LPlot?')
    
    #Gather phase noise markers
    phase['10Hz'] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    phase['100Hz'] = SA.query_ascii_values('CALC:LPLot:MARK2:Y?')[0]
    phase['1kHz'] = SA.query_ascii_values('CALC:LPLot:MARK3:Y?')[0]
    phase['10kHz'] = SA.query_ascii_values('CALC:LPLot:MARK4:Y?')[0]
    phase['100kHz'] = SA.query_ascii_values('CALC:LPLot:MARK5:Y?')[0]
    phase['1MHz'] = SA.query_ascii_values('CALC:LPLot:MARK6:Y?')[0]
    try: #I dont know how this will behave - will it return zero or cause equipment timeout? (I hope it returns zero)
        phase['10MHz'] = SA.query_ascii_values('CALC:LPLot:MARK7:Y?')[0]
        phase['100MHz'] = SA.query_ascii_values('CALC:LPLot:MARK8:Y?')[0]
    except:
        pass

    
    SA_Screenshot(name, 'Phase_Noise', settings['Band'], folder_path)

    SG.write('RF0')

    print(phase)

    return phase

def get_image_rejection(settings, name, folder_path): 
    #gainAverage is measured by the get_nf_gain function and the product number is entered by the user
    print ("starting image rejection test")
    Path.IPLS()
    
    signal_freq = 1000000000
    
    #zeroDbTolerance is the tolerance used to check if the marker value of SA is close to zero when the frequency is set to verify and amplitude is set to gain average
    zeroDbTolerance = 1

    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['image_rejection_state_file'] + "'")
    
    #waiting for the state file to load
    Wait(SA)

    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + signal_freq) + ' HZ')
    #powerIn is the amplitude to be set on SG. We use the average gain and compensate for the cable loss. 
    powerIn = (float(settings['Gain']) + float(settings['image_rejection_cable_loss_verification'])) * -1
    #print('Initial PowerIn ' + str(powerIn))
    #setting amplitude of the signal generator to powerIn
    SG.write('L1 ' + str(powerIn) + ' DM')
    #turning RF ON
    SG.write('RF1')
    
    #restrating the sweep
    SA.write('SENSe:FREQuency:CENTer ' + str(signal_freq))
    SA.write(':INITiate:IMMediate')
    Wait(SA)

    #read the maximum marker value
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))      
    time.sleep(1)
    
    #verifying if the measurement is close to 0dB
    if((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
        print('Signal generator out of range. Adjusting...')
        difference = measurement
        powerIn -= difference
        SG.write('L1 ' + str(powerIn) + ' DM')
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        Wait(SA)
        SA.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(1)
        measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
        time.sleep(1)

        #If the SA measurement is still no 0dBm, finely tune:
        while((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
            print('Tuning to 0dBm...')
            if(measurement > zeroDbTolerance):
                  powerIn -= 1
            if(measurement < zeroDbTolerance):
                  powerIn += 1
            SG.write('L1 ' + str(powerIn) + ' DM')
            SA.write(':INITiate:IMMediate')
            Wait(SA)
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
            time.sleep(1)
              


    #setting the frequency on the signal generator to image rejection frequency to get the image rejection value
    SG.write('F1 ' + str(float(settings['LO']) - signal_freq) + ' HZ')
    SA.write('SENSe:FREQuency:CENTer ' + str(signal_freq))
    time.sleep(1)
    SA.write(':INITiate:IMMediate')
    Wait(SA)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    imageRejection = float(SA.query(':CALCulate:MARKer1:Y?'))
    time.sleep(1)
    imageRejection -= float(settings['image_rejection_cable_loss'])
    print ("Image Rejection: %f" %imageRejection)
    
    #Take screenshot
    SA_Screenshot(name,'Image_Rejection', settings['Band'], folder_path)
    
    #Turn off Signal Generator
    SG.write('RF0')

    return imageRejection

def get_spurs(settings, name, folder_path): 
    #gainAverage is measured by the get_nf_gain function and the product number is entered by the user
    print ("starting in band spur test")
    Path.IPLS()
    ##IN_BAND_SPURS_cable_loss is the loss of the cable that connects SG and the waveguide adapter on the LNB or the RF switch at the specific RF Frequency
    signal_freq = 1000000000
    cable_loss = float(settings['in_band_spur_cable_loss'])
    #zeroDbTolerance is the tolerance used to check if the marker value of SA is close to zero when the frequency is set to verify and amplitude is set to gain average
    zeroDbTolerance = 1

    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['in_band_spur_state_file'] + "'")
    
    #waiting for the state file to load
    Wait(SA)
    #Change bandwidth
    SA.write(':SENSe:FREQuency:STOP ' + str(settings['nf_gain_interested_stop_Frequency']))
    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + signal_freq) + ' HZ')
    #powerIn is the amplitude to be set on SG. We use the average gain and compensate for the cable loss. 
    powerIn = (float(settings['Gain']) - cable_loss) * -1
    #print('Initial PowerIn ' + str(powerIn))
    #setting amplitude of the signal generator to powerIn
    SG.write('L1 ' + str(powerIn) + ' DM')
    #turning RF ON
    SG.write('RF1')
    
    #waiting for the sweep to complete and then read the maximum marker value
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) + 1
    
    #restrating the sweep
    SA.write(':INITiate:IMMediate')  
    time.sleep(sweepTime)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))      
    time.sleep(1)
    
    #verifying if the measurement is close to 0dB
    if((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
        print('Signal generator out of range. Adjusting...')
        difference = measurement
        powerIn = powerIn - difference
        SG.write('L1 ' + str(powerIn) + ' DM')
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        time.sleep(3)
        SA.write(':CALCulate:MARKer1:MAXimum')
        measurement = float(SA.query(':CALCulate:MARKer1:Y?'))

        #If the SA measurement is still no 0dBm, finely tune:
        while((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
            print('Tuning to 0dBm...')
            if(measurement > zeroDbTolerance):
                  powerIn = powerIn - 1
            if(measurement < zeroDbTolerance):
                  powerIn = powerIn + 1
            SG.write('L1 ' + str(powerIn) + ' DM')
            SA.write(':INITiate:IMMediate')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
            time.sleep(1)
              
    #Calculate spur
    time.sleep(sweepTime)
    #SA.write('CALCulate:MARKer1:MAXimum:NEXT') #calculate marker peak next
    #SA.write(':CALCulate:MARKer1:MAXimum')
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
    time.sleep(1)
    
    if measurement < float(settings['in_band_spur_limit']):
        value = 'Pass'
    else:
        value = 'Pass'

    #Take screenshot
    SA_Screenshot(name,'In_Band_Spurs', settings['Band'], folder_path)
    
    #Turn off Signal Generator
    SG.write('RF0')
    print('In-band spur test: ' + value)
    return value

def get_lo_leakage_output(settings, name, folder_path): 
    #product number is entered by the user
    print ("starting LO leakage output test")
    
    Path.IPLS()
    
    #lo_leakage_output_muxT_and_cable_loss is the loss of the muxT, to LNB cable, and to RX/modem cable at the LO Frequency
    lo_leakage_output_muxT_and_cable_loss = float(settings['lo_leakage_output_muxT_and_cable_loss'])
    
    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['lo_leakage_state_file'] + "'")
    #waiting 20 seconds for the state file to load
    Wait(SA)

    #restarting the sweep
    
    SA.write('SENSe:FREQuency:CENTer ' + str(settings['LO']))
    SA.write(':INITiate:IMMediate')
    #waiting for the sweep to complete
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
    time.sleep(sweepTime)

    #setting marker1 to LO
    SA.write(':CALCulate:MARKer1:X:CENTer ' + str(settings['LO']))
    #getting the result of marker1
    result = SA.query(':CALCulate:MARKer1:Y?')
    time.sleep(2)
    #LoLeakageOut: we compensate for the muxT and cable losses that go from the lnb throught the muxT and to the modem
    LoLeakageOut = float(result) + lo_leakage_output_muxT_and_cable_loss

    print ("LO Leakage Out: %f" %float(LoLeakageOut))

    SA_Screenshot(name, 'LO_Leakage_Output', settings['Band'], folder_path)   
    
    return float(LoLeakageOut)

def get_lo_leakage_input(settings, name, folder_path): 
    #product number is entered by the user
    print ("starting LO leakage input test")
    
    #Set Path
    Path.Input_Leakage()
    
    #cables_loss_at_lo_frequency is the loss of cable that connects the unit to the spectrum analyzer (SA) at LO frequency
    cables_loss_at_lo_frequency = float(settings['lo_leakage_input_cable_loss'])
    
    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['lo_leakage_state_file'] + "'")
    #waiting 20 seconds for the state file to load
    Wait(SA)
    #restarting the sweep
    SA.write('SENSe:FREQuency:CENTer ' + str(settings['LO']))
    SA.write(':INITiate:IMMediate')
    #waiting for the sweep to complete
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
 
    time.sleep(sweepTime)

    #setting marker1 to LO
    SA.write(':CALCulate:MARKer1:X:CENTer ' + str(settings['LO']))
    #getting the result of marker1
    result = SA.query(':CALCulate:MARKer1:Y?')
    time.sleep(2)
    #LoLeakageOut: we compensate for the muxT and cable losses that go from the lnb throught the muxT and to the modem
    LoLeakageIn = float(result) + cables_loss_at_lo_frequency

    print ("LO Leakage In: %f" %float(LoLeakageIn))
    
    SA.write('INITiate:CONTinuous 0')

    SA_Screenshot(name, 'LO_Leakage_Input', settings['Band'], folder_path)   
 
    return float(LoLeakageIn)

def get_current(): 
    print("starting current test") 
    time.sleep(2)  
    current = AMM.query_ascii_values('') #getting the dc current measurement
    current = round(abs(float(current[0] * 1000)), 1) #converting the measurement reading from A to mA
    print ("Current: %f" %current)
    
    return current

def get_vswrIP(settings, name, folder_path): 
    print ("starting vswr_S11 test")
    
    ##setting Path
    Path.S11_S22()
      
    VNA.write('*CLS')
    VNA.write("MMEM:LOAD '%s'"%settings['S11State'])
    Wait(VNA)
    VNA.write('INITiate:CONTinuous 1')
    time.sleep(1)
    VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    temp_values = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    S11 = temp_values[0]
    print ("S11: %f" %S11)
  
    time.sleep(4) #Wait for notification to disappear
    VNA_Screenshot(name, 'S11', settings['Band'], folder_path)
    
    VNA.write('INITiate:CONTinuous 0')
    
    return S11

def get_vswrOP(settings, name, folder_path):  
    print ("starting vswr_S22 test")
    
    ##setting Path
    Path.S11_S22()
    VNA.write('*CLS')                                    
    VNA.write("MMEM:LOAD '%s'"%settings['S22State'])
    Wait(VNA)
    VNA.write('INITiate:CONTinuous 1')
    time.sleep(1)
    VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
 
    temp_values = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    S22 = temp_values[0]
    print ("S22: %f" %S22)
    
    time.sleep(4) #Wait for notification to disappear
    VNA_Screenshot(name, 'S22', settings['Band'], folder_path)
    
    VNA.write('INITiate:CONTinuous 0')

    return S22

def get_V_Switch(settings):
 
    Vsw = {'Enable' : False, 'V_Switch_Up': None, 'V_Switch_Down': None} 

    Path.IPLS()

    carrier = 1000000000
    threshold = -40
    
    #Check if Voltage switching enabled
    if(bool(settings['Vsw_EN']) == False):
        print('Voltage switching test not enabled')
        return Vsw
    else:
        Vsw['Enable'] = True
        print('Voltage switching enabled. Begin test.')
    
    #Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + str(settings['image_rejection_state_file'])+"'")
    Wait(SA)
    
    #Setup SG and SA with frequency and power level
    SA.write('SENSe:FREQuency:CENTer ' + str(carrier))
    SA.write('SENSe:BANDwidth:RESolution 1000 ') 
    SA.write('SENSe:SWEep:TIME 500 ms') 
    SG.write('F1 ' + str(float(settings['LO']) + carrier) + ' HZ')
    powerIn = (float(settings['Gain']) * -1) + float(settings['image_rejection_cable_loss'])
    SG.write('L1 ' + str(powerIn) + ' DM')
    SG.write('RF1')
    
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
    time.sleep(sweepTime)

    print("Beginning voltage up test:" + str(settings['V_Switch_Up'])) 
    #Voltage switch Up Test
    #sweep +/-1V around switch up point
    if(bool(settings['V_Switch_Up']) == True):       
        for voltage in np.arange(float(settings['V_Switch_Up'])-1,float(settings['V_Switch_Up'])+1,0.1):
            PS.write('APPL CH2, ' + str(voltage) + ' , 0.5')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if(temp < threshold):
                Vsw['V_Switch_Up'] = round(voltage, 3)
                print('Voltage switch up: ' + str(Vsw['V_Switch_Up']))
                break
    else:
        print('Voltage up test not enabled')
    
    print("Beginning voltage down test:" + str(settings['V_Switch_Down']))
    #Voltage switch Down Test
    #sweep +/-1V around switch down point
    if(bool(settings['V_Switch_Down']) == True):       
        for voltage in np.arange(float(settings['V_Switch_Down'])+1,float(settings['V_Switch_Down'])-1,-0.1): 
            PS.write('APPL CH2, ' + str(voltage) + ' , 0.5')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if(temp < threshold):
                Vsw['V_Switch_Down'] = round(voltage, 3)
                print('Voltage switch down: ' + str(Vsw['V_Switch_Down']))
                break
    else:
        print('Voltage down test not enabled')

    SG.write('RF0')
    Pwr_on(settings)
    print(Vsw)    
    return Vsw

def RS485_SW(port,band,att):
     
    #change att value to hex
    gain = hex(int(abs((att - 31.5)*2)))
    
    ## set controller 
    k = Controller()
    ## Open RS485 Tool
    p = Popen(["trika.exe"])
    
    time.sleep(1)
    k.type("connect %s" %port)
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("s $dbg,2,0,*")
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("s $dbg,1,3,*")
    k.press(Key.enter)
    ## Changing bands
    k.type("s $setst,%s,0,*" %band)
    k.press(Key.enter)
    time.sleep(0.5)
    ##Changing Gain
    k.type("s $setda,%s,%s,*" %(int(band)-1,gain)) #DSA band = n-1
    k.press(Key.enter)
    time.sleep(0.5)
    ##disconnecting and closing
    k.type("disc")
    k.press(Key.enter)
    p.terminate()
    
def Equipment_Init(): 
    global rm, SA, SG, PS, PM, DMM, AMM, VNA, TChamber
    rm = visa.ResourceManager()
    SA = rm.open_resource(SA_VISA)
    SA.timeout = 10000
    SG = rm.open_resource(SG_VISA)
    SG.timeout = 10000
    PS = rm.open_resource(PS_VISA)
    PS.timeout = 10000
    PM = rm.open_resource(PM_VISA)
    PM.timeout = 10000
    DMM = rm.open_resource(DMM_VISA)
    DMM.timeout = 10000
    AMM = rm.open_resource(AMM_VISA)
    AMM.timeout = 10000
    #VNA = rm.open_resource(VNA_VISA)
    #VNA.timeout = 10000
    DC.Init(DC_Control_Panel_Port)
    Path.Init(RCM_216_IP)
    TChamber = Thermo.Init(rm, TChamber_VISA)

def EQID(data): 
    data['SA'] = SA.query('*IDN?')
    data['SG'] = SG.query('*IDN?')
    data['PS'] = PS.query('*IDN?')
    data['PM'] = PM.query('*IDN?')
    data['DMM'] = DMM.query('*IDN?')
    #data['VNA'] = VNA.query('*IDN?')
    data['AMM'] = 'HP 3478A Ammeter 2301A10048'
    data['TChamber'] = Thermo.ReadWrite(TChamber, '*IDN?')
    print(data['SA'])
    print(data['SG'])
    print(data['PS'])
    print(data['PM'])
    print(data['DMM'])
    print(data['VNA']) 

def GUI(settings_master_list):
    global master
    master = tk.Tk() ## Initialize Menu variable
    master.title('ATE2 Program R' + str(current_revision))
    #master.iconphoto(False, PhotoImage(file = 'orblogo4.png'))

    #Variable List
    global ModelDropdown, SNText, StockText, MnfText, ColorText, CxText, POText, YouText, USB_Port_Text, Attenuation, TemperatureDropdown
    TemperatureProfiles = ['None']
    atten = []
    for num in range(61):
        atten.append(num/2)

    for (dirpath, dirnames, filenames) in os.walk(ATE2_path + '\\_Temperature Profiles'):
        TemperatureProfiles.extend(filenames)

    ModelOptions = Settings_List_Options(settings_master_list)

    #Model Frame
    ModelFrame = tk.LabelFrame(master,text="Settings Menu")
    ModelFrame.grid(row=1,column=1,columnspan=2,sticky='W')
    
    #Unit Profile Dropdown Menu
    tk.Label(ModelFrame,text="Model: ").grid(row=2,column=1,sticky='W')
    ModelDropdown = ttk.Combobox(ModelFrame,value=ModelOptions) ##Makes the menu
    ModelDropdown.set('None') ## Set Default Value
    ModelDropdown.grid(row=2,column=2,sticky='W',ipadx=115) ## Positions the Menu

    #Temperature Frame
    TemperatureFrame = tk.LabelFrame(master,text="Temperature Menu")
    TemperatureFrame.grid(row=2,column=1,columnspan=2,sticky='W')

    #Temperature Profile Dropdown Menu
    tk.Label(TemperatureFrame,text="Profile: ").grid(row=2,column=1,sticky='W')
    TemperatureDropdown = ttk.Combobox(TemperatureFrame,value=TemperatureProfiles) ##Makes the menu
    TemperatureDropdown.set('None') ## Set Default Value
    TemperatureDropdown.grid(row=2,column=2,sticky='W',ipadx=115) ## Positions the Menu

    #RS485 Frame
    RS485Frame = tk.LabelFrame(master,text="RS485 Menu")
    RS485Frame.grid(row=3,column=1,columnspan=2,sticky='W')

    ## USB Port
    USB_Port = tk.Label(RS485Frame,text='Port Number:').grid(row=5,column=1,sticky='W')
    USB_Port_Text = tk.Entry(RS485Frame)
    USB_Port_Text.grid(row=5,column=2,sticky='W',ipadx=2)

    ## Combobox for attenuation value
    AttName = tk.Label(RS485Frame,text="Attenuation:").grid(row=6,column=1,sticky='W')
    Attenuation = ttk.Combobox(RS485Frame,value=atten)
    Attenuation.set('0.5')
    Attenuation.grid(row=6,column=2,sticky='W')
    tk.Label(RS485Frame,text=' Unit Gain = Max Gain - Attenuation').grid(row=6,column=3,sticky='W')
    
    #Information Frame
    InfoFrame = tk.LabelFrame(master,text="Information Menu")
    InfoFrame.grid(row=4,column=1,columnspan=2,sticky='W')

    #Serial Number
    SN = tk.Label(InfoFrame,text='Serial Number:').grid(row=1,column=1,sticky='W')
    SNText = tk.Entry(InfoFrame)
    SNText.grid(row=1,column=2,sticky='W')

    #Stock Number
    Stock = tk.Label(InfoFrame,text='Stock Number:').grid(row=1,column=3,sticky='W')
    StockText = tk.Entry(InfoFrame)
    StockText.grid(row=1,column=4,sticky='W')

    #Manufacturer S/N if present
    Mnf = tk.Label(InfoFrame,text='Supplier S/N:').grid(row=2,column=1,sticky='W')
    MnfText = tk.Entry(InfoFrame)
    MnfText.grid(row=2,column=2,sticky='W')

    #Unit Color
    Color = tk.Label(InfoFrame,text='Color:').grid(row=2,column=3,sticky='W')
    ColorText = tk.Entry(InfoFrame)
    ColorText.grid(row=2,column=4,sticky='W')

    #Customer Name
    Cx = tk.Label(InfoFrame,text='Customer:').grid(row=3,column=1,sticky='W')
    CxText = tk.Entry(InfoFrame)
    CxText.grid(row=3,column=2,sticky='W')

    #PO Number
    PO = tk.Label(InfoFrame,text='PO Number:').grid(row=3,column=3,sticky='W')
    POText = tk.Entry(InfoFrame)
    POText.grid(row=3,column=4,sticky='W')

    #Tester Name
    You = tk.Label(InfoFrame,text='Tester:').grid(row=4,column=1,sticky='W')
    YouText = tk.Entry(InfoFrame)
    YouText.grid(row=4,column=2,sticky='W')
            
    #Button to Begin Testing
    b1 = tk.Button(master,text='OFF',command=StopATE).grid(row=7,column=1,sticky='W')
    Stop = tk.Label(master,text ='Press CTRL+C to stop ATE during test.').grid(row=7,column=2) 
    b1 = tk.Button(master,text='Begin',command=StartATE).grid(row=7,column=3,sticky='E')

    master.mainloop()

def StartATE():
    try:
        main()
    except KeyboardInterrupt:
        print('Keyboard interrupt detected.')
        StopATE()

def StopATE():
    Pwr_off()
    SG.write('RF0')
    SA.write(':ABORt')
    Thermo.STOP(TChamber)
    print('Stopping Program.')
    sys.exit()     
    
def Settings_List_Options(settings_list):
    options = []
    for i in range(len(settings_list)):
        options.append(settings_list[i][0]['Model']) 
    return options  
 
settings_master_list = DS_Gen.Load_All_Settings(settings_path) #Load settings
Equipment_Init() #Initialize connection to all measurement equipment
GUI(settings_master_list)#Call GUI

#Version Log:
#------------------------------
#Revision 1.0 - Ported over code from LNB ATE V1.6.py
#Revision 2.0 Rearranged NFG test within main() to account for non-remote NS inbetween tests
#Revision 2.1 Added Image Filter for prettier screenshots. Orbital_Image_Filter_R3.py
#Revision 2.2 - 20210902 - Inlcuded new datasheet generator Revision 2.0
#Revision 3.0 - 20211103 - Remove Attenuation2 and 3. Added optional temperature profile testing. Updated to ATE2_Datasheet_Generator_R3.py
