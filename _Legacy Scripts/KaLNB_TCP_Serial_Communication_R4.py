#KALNB_TCP_Serial_Communication
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-25 Import code form Attenuation Sweep LNB ATE2 R2 - Serial and TCP Connection. Rewrite to class structure
#R2 - 2022-02-15 Consoliodate read/write methods to simple 'send' method which reads and writes. Carriage Return ('\r\n') on messages
#R3 - 2022-03-04 Minor bug fixes - added .decode() to socket reply.
#R4 - 2022-03-10 Replies are now lists of string separated by \n, ignoring the sent command


import socket
import serial
import sys
import time,sys,os 

KALNB_baud = 38400
DKa_TriKa_baud = 9600

class SerialConn:
    def __init__(self, port,baud):
        self.ser = serial.Serial(
        port = 'COM'+str(port),
        baudrate = baud,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        )

    def send(self, message):
        #message = bytes(str(message),'ascii')
        for x in message:
            self.ser.write(x.encode('ascii'))
        time.sleep(0.1)
        character = ''
        word = ''
        reply = []
        while(self.ser.in_waiting >= 1):
            character = self.ser.read(1)
            character = character.decode('ascii')
            word += character
        reply = word.split('\n')
        reply.pop(0)
        
        return reply

    def close(self):
        self.ser.close()


class SocketConn:
    def __init__(self, IP, Port):
        self.server_address = (str(IP),int(Port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(5)
        self.sock.connect(self.server_address)

    def send(self,message):
        message = bytes(str(message)+'\r\n','ascii')
        self.sock.sendall(message)
        time.sleep(0.1)
        word = self.sock.recv(1024).decode('ascii')
        reply = word.split('\n')
        reply.pop(0)
        
        return reply

    def close(self):
        self.sock.close()

if __name__ == '__main__':
    pass
