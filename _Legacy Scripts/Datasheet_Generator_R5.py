#Ported from DS_GV0.1.py
#Adjusted by Benjamin Stadnik
#Orbital Research Ltd
#2021-04-14
#Python 3.9

import xlsxwriter
import xlwings as xw
import json
import time
import sys
import os
import csv

#settings_path = '\\\SVR-1\\Production\\_To_be_REVIEWED\\In_Progress\\ATE1\\ATE1_SETTINGS.csv'

def Generate(folder_path, filename, data, settings):
    
    workbook = xlsxwriter.Workbook(folder_path + '\\' + filename + '_Band' + str(data['Band']) +'.xlsx')
    sheet1_name = ''
    sheet2_name = ''
    try:
        if(bool(data['Temperature']['Enable'])):
            sheet1_name = str(data['Temperature']['Value']) + 'C Band ' + data['Band'] + ' Datasheet'
            sheet2_name = str(data['Temperature']['Value']) + 'C Band ' + data['Band'] + ' Data'
        else:
            sheet1_name = 'Band ' + str(data['Band']) + ' Datasheet'
            sheet2_name = 'Band ' + str(data['Band']) + ' Data'
            
    except:
        sheet1_name = 'Band ' + str(data['Band']) + ' Datasheet'
        sheet2_name = 'Band ' + str(data['Band']) + ' Data'
        
    worksheet = workbook.add_worksheet(sheet1_name)
    worksheet.set_landscape()
    bold = workbook.add_format({'bold': True})
    not_bold = workbook.add_format({'bold': False})
    bold.set_font_size(8)
    not_bold.set_font_size(8)

    ## Client, PO and Date 
    worksheet.write('A2', 'Client', bold) ## Client Text
    worksheet.write('B2', data['Customer'], bold) ## Client Name    
    worksheet.write('A3', 'PO', bold) ## PO Text
    worksheet.write('B3', data['PO'], bold) # PO Number
    worksheet.write('A4', 'Date', bold) ## Date Text
    worksheet.write('B4', data['Date'], bold) ## Date Field

    ##Model, Serial and Stock
    worksheet.write('C2', 'Model', bold) ## Model Text
    worksheet.write('D2', data['Model'], bold) ## Model Number / Product Number
    worksheet.write('C3', 'Serial Number', bold) ## Unit Serial Text
    worksheet.write('D3', data['Orbital_SN'], bold) ## Serial Number
    worksheet.write('D4', data['stock_number'], bold) ## Stock Number
    worksheet.write('C4', 'Stock', bold) ## Stock Text

    ## Tester and Reviewer
    worksheet.write('E3', 'Tested By', bold) ## Tested by Text
    worksheet.write('F3', data['Tester'], not_bold) ## Tester Name
    worksheet.write('E4', 'Reviewed By', bold) ## Checked by Text
    worksheet.write('F4', 'IM', not_bold) ## Reviewer Name

    ## Logo Insert
    worksheet.insert_image('G1', 'orblogo.png')

    ## Open Screenshots, invert their colours, insert to worksheet
    image1 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_NFG.PNG'
    image2 = folder_path + '\\' + filename + '_Band' + str(data['Band']) + '_Phase_Noise.PNG'
    if(data['Script_Revision'].count('ATE2')):
        scale = {'x_scale': 0.535, 'y_scale': 0.535}
    else:
        scale = {'x_scale': 1, 'y_scale': 1} 
    worksheet.insert_image('A22', image1 , scale)
    worksheet.insert_image('G22', image2 , scale)

    ##Set column width
    worksheet.set_column('B:C',20)
    worksheet.set_column('D:F',15)
    worksheet.set_column('G:H',15)

    ##SCD Number, Headers
    ##worksheet.write('A6', settings['l9'], bold) ##disabled for general datasheet
    worksheet.write('B6', 'Compliance Parameters',bold) 
    worksheet.write('C6', 'Specification', bold)
    worksheet.write('D6','Unit',bold)
    worksheet.write('E6','Status',bold)
    ##worksheet.write('F6',settings['l14'],bold) ##disabled for general datasheet
    worksheet.write('G6','Measured Parameters',bold)
    worksheet.write('H6','Spec',bold)
    worksheet.write('I6','Data',bold)
    worksheet.write('J6','Unit',bold)
    ##worksheet.write('K6',settings['l19'],bold) ##disabled for general datasheet
    worksheet.write('L6','Phase Noise',bold)
    worksheet.write('M6','Spec',bold)
    worksheet.write('N6','Data',bold)
    worksheet.write('O6','Unit',bold)

    ## Parameter Titles
    worksheet.write('B7','RF Input Frequency',not_bold)
    worksheet.write('B8','IF Output Frequency',not_bold)
    worksheet.write('B9','Local Osc Frequency',not_bold)
    worksheet.write('B10','DC Input Voltage Range',not_bold)
    worksheet.write('B11','Input Flange',not_bold)
    worksheet.write('B12','Output Connector',not_bold)
    worksheet.write('B13','Desense @-40 dBm input',not_bold)
    worksheet.write('B14','Overdrive @-20dBm input',not_bold)
    worksheet.write('B15','Size,(LengthxWidthxHeight)',not_bold)
    worksheet.write('B16','Weight',not_bold)
    worksheet.write('B17','Color',not_bold)

    ## Parameter Specs / Values
    worksheet.write('C7',settings['RF_In'],not_bold) ## RF Input Frequencies
    worksheet.write('C8',settings['IF_Out'],not_bold) ## IF Output Frequencies
    worksheet.write('C9',settings['LO_Spec'],not_bold) ## LO Frequency
    worksheet.write('C10',settings['Volt_Spec'],not_bold) ##Input Voltage Range
    worksheet.write('C11',settings['Input_Connector'],not_bold) ## Input Flange Type
    worksheet.write('C12',settings['Output_Connector'],not_bold) ## Output connector Type
    worksheet.write('C13',settings['Desense_Spec'],not_bold) ## Desense Value, usually '<0.1'
    worksheet.write('C14','no harm',not_bold) ## no harm unless special specification
    worksheet.write('C15',settings['Size'],not_bold)## Size Dimensions of DUT
    worksheet.write('C16',settings['Weight'],not_bold) ## Weight
    worksheet.write('C17',data['Color'],not_bold) ## Color

    ## Units
    worksheet.write('D7','GHz',not_bold) ## Should always be in GHz...right?
    worksheet.write('D8','MHz',not_bold) ## Should always be in MHz...right?
    worksheet.write('D9','GHz',not_bold) ## Should always be in GHz...right?
    worksheet.write('D10','VDC',not_bold)## Voltage Unit
    worksheet.write('D11','std',not_bold)## Standard?
    worksheet.write('D12','std',not_bold)## Standard?
    worksheet.write('D13','dB',not_bold) ## Desense Unit
    worksheet.write('D14','dBm',not_bold)## Overdrive Unit
    worksheet.write('D15','mm',not_bold) ## Size Unit
    worksheet.write('D16','g',not_bold)  ## Weight Unit

    ## Status
    worksheet.write('E7','confirmed',not_bold)
    worksheet.write('E8','confirmed',not_bold)
    worksheet.write('E9','confirmed',not_bold)
    worksheet.write('E10','confirmed',not_bold)
    worksheet.write('E11','confirmed',not_bold)
    worksheet.write('E12','confirmed',not_bold)
    worksheet.write('E13','0.05 max',not_bold)
    worksheet.write('E14','confirmed',not_bold)


    ## Disabled SCD Numbers
    ##worksheet.write('F7',settings['l64'],not_bold)
    ##worksheet.write('F8',settings['l65'],not_bold)
    ##worksheet.write('F9',settings['l66'],not_bold)
    ##worksheet.write('F10',settings['l67'],not_bold)
    ##worksheet.write('F11','',not_bold)
    ##worksheet.write('F12','',not_bold)
    ##worksheet.write('F13','',not_bold)
    ##worksheet.write('F14','',not_bold)
    ##worksheet.write('F15',settings['l72'],not_bold)
    ##worksheet.write('F16',settings['l73'],not_bold)
    ##worksheet.write('F17',settings['l74'],not_bold)
    ##worksheet.write('F18',settings['l75'],not_bold)


    ## Disabled SCD Numbers
    ##worksheet.write('A7',settings['l76'],not_bold)
    ##worksheet.write('A8',settings['l77'],not_bold)
    ##worksheet.write('A9',settings['l78'],not_bold)
    ##worksheet.write('A10',settings['l79'],not_bold)
    ##worksheet.write('A11',settings['l80'],not_bold)
    ##worksheet.write('A12',settings['l81'],not_bold)
    ##worksheet.write('A13',settings['l82'],not_bold)
    ##worksheet.write('A14',settings['l83'],not_bold)
    ##worksheet.write('A15',settings['l84'],not_bold)
    ##worksheet.write('A16',settings['l85'],not_bold)
    ##worksheet.write('A17',settings['l86'],not_bold)

    ## Spec Titles
    worksheet.write('G7','Noise Figure, Max',not_bold)
    worksheet.write('G8','Noise Figure, Ave',not_bold) 
    worksheet.write('G9','Gain',not_bold)
    worksheet.write('G10','Max Ripple 10 MHz',not_bold) ## Usually 10 MHz is the spec but can change
    worksheet.write('G11','In Band Spurs',not_bold)
    worksheet.write('G12','Image Rejection',not_bold)
    worksheet.write('G13','LO Leakage Input',not_bold)
    worksheet.write('G14','LO Leakage Output',not_bold)
    worksheet.write('G15','P1dB(Output)',not_bold)
    worksheet.write('G16','OIP3',not_bold)
    worksheet.write('G17',('DC Current, %sV')%(settings['Voltage']),not_bold)
    worksheet.write('G18','Input VSWR',not_bold) ## Add option for Isolator and without Isolator
    worksheet.write('G19','Output VSWR',not_bold)
    worksheet.write('G20',settings['Spe1'],not_bold) ## Comment for Extra Testing Note 1 Ex. Passed Pressure Testing
    worksheet.write('G21',settings['Spe2'],not_bold) ## Isolator and pressure test


    ##Spec Values
    worksheet.write('H7',settings['NF_Spec_Max'],not_bold)
    worksheet.write('H8',settings['NF_Spec_Ave'],not_bold)
    worksheet.write('H9',settings['Gain_Spec'],not_bold)
    worksheet.write('H10',settings['Per_10M_Spec'],not_bold)
    worksheet.write('H11',settings['Spur_Spec'],not_bold)
    worksheet.write('H12',settings['ImRej_Spec'],not_bold)
    worksheet.write('H13',settings['LoLeakIn_Spec'],not_bold)
    worksheet.write('H14',settings['LoLeakOut_Spec'],not_bold)
    worksheet.write('H15',settings['P1dB_Spec'],not_bold)
    worksheet.write('H16',settings['OIP3_Spec'],not_bold)
    worksheet.write('H17',settings['Current_Spec'],not_bold)
    worksheet.write('H18',settings['S11_Spec'],not_bold)
    worksheet.write('H19',settings['S22_Spec'],not_bold)
    ##worksheet.write('H4','0132-1211-01',bold) ## SCD Specification if required

    ##Measurement Values
    worksheet.write('I7',data['NFG']['max_NF'],not_bold)
    worksheet.write('I8',data['NFG']['averageNF'],not_bold)
    worksheet.write('I9',data['NFG']['averageGain'],not_bold)
    worksheet.write('I10',data['NFG']['amplitude_response_10MHz_max'],not_bold)
    ## Measurement Data
    worksheet.write('I11',data['spurs'],not_bold)
    worksheet.write('I12',data['image_rej'],not_bold)
    worksheet.write('I13',data['lo_leakage_in'],not_bold)
    worksheet.write('I14',data['lo_leakage_out'],not_bold)
    worksheet.write('I15',data['P1dB']['P1dB'],not_bold)
    worksheet.write('I16',data['P1dB']['OIP3'],not_bold)
    worksheet.write('I17',data['current'],not_bold)
    worksheet.write('I18',data['S11'],not_bold)
    worksheet.write('I19',data['S22'],not_bold)
    if(settings['Spe1']):
        worksheet.write('I20','Pass',not_bold)
    if(settings['Spe2']):
        worksheet.write('I21','Pass',not_bold)


    ## Units, Should never change?
    worksheet.write('J7','dB',not_bold)
    worksheet.write('J8','dB',not_bold)
    worksheet.write('J9','dB',not_bold)
    worksheet.write('J10','dB',not_bold)
    worksheet.write('J11','dBc',not_bold)
    worksheet.write('J12','dBc',not_bold)
    worksheet.write('J13','dBm',not_bold)
    worksheet.write('J14','dBm',not_bold)
    worksheet.write('J15','dBm',not_bold)
    worksheet.write('J16','dBm',not_bold)
    worksheet.write('J17','mA',not_bold)
    worksheet.write('J18','',not_bold) ## VSWR is unitless


    #### Disabled SCD Numbers
    ##worksheet.write('K7',settings['l121'],not_bold)
    ##worksheet.write('K8',settings['l122'],not_bold)
    ##worksheet.write('K9',settings['l123'],not_bold)
    ##worksheet.write('K10',settings['l124'],not_bold)
    ##worksheet.write('K11',settings['l125'],not_bold)
    ##worksheet.write('K12',settings['l126'],not_bold)

    ## Phase Noise Spec
    worksheet.write('L7','10 Hz',not_bold)
    worksheet.write('L8','100 Hz',not_bold)
    worksheet.write('L9','1 KHz',not_bold)
    worksheet.write('L10','10 KHz',not_bold)
    worksheet.write('L11','100 KHz',not_bold)
    

    ##Phase Noise Spec
    worksheet.write('M7',settings['P10_Spec'],not_bold)
    worksheet.write('M8',settings['P100_Spec'],not_bold)
    worksheet.write('M9',settings['P1K_Spec'],not_bold)
    worksheet.write('M10',settings['P10K_Spec'],not_bold)
    worksheet.write('M11',settings['P100K_Spec'],not_bold)

    ## Phase Noise Data
    worksheet.write('N7',data['phase']['10Hz'],not_bold)
    worksheet.write('N8',data['phase']['100Hz'],not_bold)
    worksheet.write('N9',data['phase']['1kHz'],not_bold)
    worksheet.write('N10',data['phase']['10kHz'],not_bold)
    worksheet.write('N11',data['phase']['100kHz'],not_bold)
     

    ## Phase Noise Units
    worksheet.write('O7','dBc/Hz',not_bold)
    worksheet.write('O8','dBc/Hz',not_bold)
    worksheet.write('O9','dBc/Hz',not_bold)
    worksheet.write('O10','dBc/Hz',not_bold)
    worksheet.write('O11','dBc/Hz',not_bold)
    
    if(settings['P1M_Spec']):
        worksheet.write('L12','1 MHz',not_bold)
        worksheet.write('M12',settings['P1M_Spec'],not_bold)
        worksheet.write('N12',data['phase']['1MHz'],not_bold)
        worksheet.write('O12','dBc/Hz',not_bold)
    if(settings['P10M_Spec']):
        worksheet.write('L13','10 MHz',not_bold)
        worksheet.write('M13',settings['P10M_Spec'],not_bold)
        worksheet.write('N13',data['phase']['10MHz'],not_bold)
        worksheet.write('O13','dBc/Hz',not_bold) 
    if(settings['P100M_Spec']):
        worksheet.write('L14','100 MHz',not_bold)
        worksheet.write('M14',settings['P100M_Spec'],not_bold)
        worksheet.write('N14',data['phase']['100MHz'],not_bold)
        worksheet.write('O14','dBc/Hz',not_bold)
        
    ###Notes under the screenshots, Usually detailing what equipment was used, uncertainty etc
    worksheet.write('A47',settings['Note_1'],not_bold)
    worksheet.write('A48',settings['Note_2'],not_bold)
    worksheet.write('A49',settings['Note_3'],not_bold)

    ## Adding second Page for frequency, NF, and Gain List
    worksheet2 = workbook.add_worksheet(sheet2_name)

    #Frequency	Noise 	Gain	Per 10 MHz	Per 120 Mhz	Per 500 MHz
    worksheet2.write('A4','Averages',bold)
    worksheet2.write('A5','Frequency',bold)
    worksheet2.write('B5','Noise',bold)
    worksheet2.write('C5','Gain',bold)
    worksheet2.write('D5','Linear Gain',bold)
    worksheet2.write('E5','Linear NF',bold)
    worksheet2.write('F5','Per 10 MHz',bold) 
    worksheet2.write('G5','Per 120 MHz',bold)
    worksheet2.write('H5','Per 500 MHz',bold)
    worksheet2.write('I5','Per 1000 MHz',bold)

    row = 6
    lenx = len(data['NFG']['Frequency'])
    for i in range(row,row+lenx):
        tmp = "A"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet2.write(tmp,data['NFG']['Frequency'][tmp_index],not_bold)

    lenx = len(data['NFG']['NF'])
    ##WRITE Noise
    for i in range(row,row+lenx):
        tmp = "B"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet2.write(tmp,data['NFG']['NF'][tmp_index],not_bold)
        
    lenx = len(data['NFG']['Gain'])
    ##WRITE GAIN
    for i in range(row,row+lenx):
        tmp = "C"
        tmp = tmp + str(i)
        tmp_index = i-row
        worksheet2.write(tmp,data['NFG']['Gain'][tmp_index],not_bold)

    ##Labels for Ripple Specs etc
    worksheet2.write('F2','Amplitude Response, Max', bold)
    worksheet2.write('F3',settings['Per_10M_Spec'], bold)
    worksheet2.write('G3',settings['Per_120M_Spec'], bold)
    worksheet2.write('H3',settings['Per_500M_Spec'], bold)
    worksheet2.write('I3',settings['Per_1G_Spec'], bold)

    ##AVERAGES
    worksheet2.write_formula('E4','=10^(B4/10)',not_bold)
    worksheet2.write_formula('D4','=10^(C4/10)',not_bold)
    worksheet2.write('B4',data['NFG']['averageNF'],not_bold)
    worksheet2.write('C4',data['NFG']['averageGain'],not_bold)


    ## Calculating the Ripple measurement Values
    ##Amplitude Per 10M
    worksheet2.write('F4',data['NFG']['amplitude_response_10MHz_max'],not_bold)
    ##Amplitude Per 120M
    worksheet2.write('G4',data['NFG']['amplitude_response_120MHz_max'],not_bold)
    ##Amplitude Per 500M
    worksheet2.write('H4',data['NFG']['amplitude_response_500MHz_max'],not_bold)
    ##Amplitude Per 1000M
    worksheet2.write('I4',data['NFG']['amplitude_response_1000MHz_max'],not_bold)

    ##Write GAIN LINEAR
    for i in range(row,row+lenx):
        tmp = "D"
        tmp2 = "C"
        tmp = tmp + str(i)
        tmp2 = tmp2 + str(i)
        formula = "=10^(%s/10)" % (tmp2)
        worksheet2.write_formula(tmp,formula,not_bold)

    ##Write NF Linear
    for i in range(row,row+lenx):
        tmp = "E"
        tmp2 = "B"
        tmp = tmp + str(i)
        tmp2 = tmp2 + str(i)
        formula = "=10^(%s/10)" % (tmp2)
        worksheet2.write_formula(tmp,formula,not_bold)
        
    #Find ripple index by polling frequency index
    flag = 0
    for i in range(len(data['NFG']['Frequency'])):
        if (int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_start_Frequency']) + 10000000) and flag < 1:
            index_10M = i
            flag = 1
        if (int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_start_Frequency']) + 120000000) and flag < 2:
            index_120M = i
            flag = 2
        if (int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_start_Frequency']) + 500000000) and flag < 3:
            index_500M = i
            flag = 3
        if (int(data['NFG']['Frequency'][i]) >= int(settings['nf_gain_interested_start_Frequency']) + 1000000000) and flag < 4:
            index_1G = i
            flag = 4
            break
            
    ##Write per 10mhz    
    for i in range(len(data['NFG']['amplitude_response_10MHz'])):
        tmp = "F"
        tmp += str(i+row+index_10M)
        worksheet2.write(tmp,data['NFG']['amplitude_response_10MHz'][i],not_bold)

    ##Write per 120mhz
    for i in range(len(data['NFG']['amplitude_response_120MHz'])):
        tmp = "G"
        tmp += str(i+row+index_120M)
        worksheet2.write(tmp,data['NFG']['amplitude_response_120MHz'][i],not_bold)
        
    ##Write per 500mhz
    for i in range(len(data['NFG']['amplitude_response_500MHz'])):
        tmp = "H"
        tmp += str(i+row+index_500M)
        worksheet2.write(tmp,data['NFG']['amplitude_response_500MHz'][i],not_bold)
        
    ##Write per 1000mhz
    for i in range(len(data['NFG']['amplitude_response_1000MHz'])):
        tmp = "I"
        tmp += str(i+row+index_1G)
        worksheet2.write(tmp,data['NFG']['amplitude_response_1000MHz'][i],not_bold)


    workbook.close()


def Merge(folder_path, name): #Merge Datasheets for multiband units
    print('Merging Datasheets...')
    cwd = os.path.abspath(folder_path) 
    files = os.listdir(cwd)
    wb_new = xw.Book()
    for file in files: #Find all .xlsx files in folder
        if file.endswith('.xlsx'):
            wb_temp = xw.Book(folder_path + '\\' + file)
            for sheet in wb_temp.sheets: #Append all sheets from .xlsx file to new file
                sheet.api.Copy(Before=wb_new.sheets[-1].api)
            wb_temp.close()
            #wb_temp.app.quit()

    wb_new.sheets[-1].delete()
    wb_new.sheets[0].activate()
    wb_new.save(folder_path + '\\' + name + '.xlsx')
    #wb_new.close()
    wb_new.app.quit()

def Load_JSON(file):
    with open(file, 'r') as f:
        data = json.load(f)
    f.close()
    return data

def GenerateFromJSON(folder_path):
    settings_master_list = Load_All_Settings(settings_path)
    print('Generating Datasheets...')
    cwd = os.path.abspath(folder_path) 
    files = os.listdir(cwd)  
    for file in files: #Find all .json files in folder
        if file.endswith('.json'):
            print(file)
            data = Load_JSON(file)
            settings_list = Find_Settings(settings_master_list, data['Model'])
            for item in settings_list:
                if(data['Band'] == item['Band']):
                    print(item['Band'])
                    settings = item
            Generate(os.path.dirname(__file__), os.path.splitext(file)[0].split('_Band')[0], data, settings)

def Find_Settings(settings_master_list, selected, key):
    print(selected)
    settings_list = []
    for i in range(len(settings_master_list)):
        if(selected == settings_master_list[i][0][key]):
            for settings in settings_master_list[i]:
                settings_list.append(settings)
    return settings_list

def Load_All_Settings(file):
    #Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)
    f.close

    #print(matrix)

    #Genrate list of dictionaries from matrix, grouping bands together if model name left blank 
    key_list = matrix[0]
    output_list = []
    temp_list = []
    matrix.reverse()
    for rows in matrix:
        temp_dict = {}
        for index, value in enumerate(rows):
            temp_dict[key_list[index]] = value
        temp_list.insert(0,temp_dict)
        if(rows[0] != ''):
            output_list.append(temp_list)
            temp_list = []
    output_list.reverse()

    output_list.pop(0)
    #print(output_list)
    
    return output_list 

def main():
    message = input("Enter 'M' to merage all datasheets in current directory. Enter 'G' to generate datasheets from RAW JSON data.")
    if(message == 'M'):
        Merge(os.path.dirname(__file__), 'Merged Datasheets')
        print('Datasheets merged. Exit program.')
    elif(message == 'G'):
        GenerateFromJSON(os.path.dirname(__file__))
        print('Generated datasheets. Exit program.')
    else:
        print('Exit program.')
        sys.exit()


if(__name__ == '__main__'):
	main()

#Change Log
#-------------------------
#Revision 2.0 - 20210902 - Added Max and Ave NF to Datasheet. Added Max and Ave NF Spec to LNB Settings. Changed 'data' and 'settings' to ATE2 naming convention. Changed raw data starting row from '6' to variable 'row'. Ripple now starts at cooresponding frequency. Added special note 1 and 2 back to datasheet
#Revision 3 - 20211109 - added "filename" variable to differentiate from "data['Orbital_SN']" dependancy. Added standalone application merge and generate functionality.
#R4 - 20220420 - Transposed ATE2_Settings.csv. Upgraded Load_All_Settings() for transposed matrix
#R5 - 20220621 - Find_Settings has find key. Merge function updated compatibility for Win7.
