
import pyvisa as visa
import serial
import time

SG_VISA = 'TCPIP::10.0.10.135::9760::SOCKET'
PORT = 15

class HSX_RS232_Connection:
    def __init__(self, port):
        self.ser = serial.Serial(
        port = 'COM'+str(port),
        baudrate = 115200,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        )

    def query(self, message):
        self.write(message)
        time.sleep(0.1)
        character = ''
        word = ''
        reply = []
        while(self.ser.in_waiting > 0):
            character = self.ser.read(1)
            character = character.decode('ascii')
            word += character
        
        unfiltered_reply = word.split('\n')
        
        for item in unfiltered_reply:
            if(item != '' and not item.count(message)):
                reply.append(item)
        
        return reply[0]

    def write(self, message):
        tx = bytes(str(message)+'\n\r','ascii') 
        self.ser.write(tx)

    def close(self):
        self.ser.close()


def TestCommands(inst):
    frequency_commands = ['10','10Hz','1MHz','10MHz','222MHz','3000MHz','3000M','19GHz','16MHz','18000000000MHz','18000000000Hz']
    template = ':CH1:FREQ:'
    print(inst.query('*IDN?'))
    print(F"[SEND]: {template+'MIN?'} || [RETURN]: {inst.query(template+'MIN?')}")
    print(F"[SEND]: {template+'MAX?'} || [RETURN]: {inst.query(template+'MAX?')}")
    for freq in frequency_commands:
        command = template + freq
        print(F"[SEND]: {command} || [RETURN]: {inst.query(command)}")

def CommandLine(inst):
    while(True):
        try:
            command = input('Command')
            print(F"[SEND]{command} || [RETURN]{inst.query(command)}")

        except KeyboardInterrupt:
            inst.close()
            print("Close connection")
            break
        except Exception as e:
            print(e)

#Resources = {}

#rm = visa.ResourceManager()
#SG = rm.open_resource(SG_VISA)
#SG.write_termination = '\r'
#SG.read_termination  = '\r'
#SG.timeout = 10000
#Resources['SG'] = SG.query('*IDN?')
#SG.write(':REF:EXT:10MHz')
#print(Resources)
        
N = 2
freq =  [100,1000,12000,24000]
        
HSX = HSX_RS232_Connection(PORT)
CommandLine(HSX)
#print('Start')
#for command in commands:
    #print(F"[SEND]{command} || [RETURN]{HSX.query(command)}")
#print('End')
freq = 1000
#while(True):
#    try:
#        command1 = F":CH1:FREQ:{freq}000000Hz"
#        command2 = F":CH2:FREQ:{freq}000000Hz"
#        print(F"[SEND]{command1} || [RETURN]{HSX.query(command1)}")
#        print(F"[SEND]{command2} || [RETURN]{HSX.query(command2)}")
#        input('Press enter for next input')
#        freq += 1000
#
#    except KeyboardInterrupt:
#        HSX.close()
#        print("Close connection")
#        break
#    except Exception as e:
#        print(e)
