from PIL import Image, ImageEnhance, ImageOps

orbital_light_blue = (0,170,238)
orbital_dark_blue = (5,22,80)

def OrbFilter(folder, image_name, extension, inversion):
    #open image
    orb = Image.open('orblogo4.PNG')
    try:
        img = Image.open(folder + image_name + extension)
    except:
        print('Image not found')
        return
    
    img = img.convert('RGB') # convert image to RGB
    
    width, height = img.size
    orb_width, orb_height = orb.size
    corner = 0
    for x in range(width):  
        for y in range(height):  
            # access current pixel broken down into RGB values
            r, g, b = img.getpixel((x,y))
            new_pixel = (r,g,b)
            if((r == b and b == g)): #if pixel is a shade
                if(inversion == True):
                    new_pixel = (abs(r-255),abs(b-255),abs(g-255))
                else:
                    pass
            elif(r > 190 and g < 100 and b < 100): #if red-ish, make red
                new_pixel = (255,0,0)
            elif(r < 215 and g > 190 and b > 190): #if cyan, make orbital light blue
                new_pixel = orbital_light_blue
            elif(r > 190 and g > 190 and b < 100): #if yellow, make blue
                new_pixel = (0,0,255)
            elif(abs(r - b) > 50 or abs(b - g) > 50 or abs(r - g) > 50): #if accent colour, make orbital dark blue
                new_pixel = orbital_dark_blue 

            # reassign the pixel value in the image to be the changed version
            img.putpixel((x,y), new_pixel)

    img.paste(orb, (width - orb_width - corner, height - orb_height - corner)) #Append Orbital logo bottom right
    #img.show()
    new_image = folder + image_name + '_OrbFilter.PNG'
    img.save(new_image)

    return new_image

#file = OrbFilter('','TEST4_Band2_Phase_Noise', '.PNG', True)
#image = Image.open(file)
#image.show()
