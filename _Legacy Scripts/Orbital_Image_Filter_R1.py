#Orbital Image Filter
#Written by Benjamin Stadnik
#2021-06-24

from PIL import Image, ImageEnhance, ImageOps
import cv2 as cv
import numpy as np

def Filter(name):
    # Load image and convert to HSV colourspace
    image = cv.imread(name)
    hsv=cv.cvtColor(image,cv.COLOR_BGR2HSV)

    # Mask image
    #orb_mask=cv.inRange(hsv,np.array([50,50,170]),np.array([255,255,255]))
    #mask = cv.inRange(hsv,np.array([255,255,255]),np.array([255,255,255]))
    #mask2 = cv.inRange(hsv,np.array([0,0,0]),np.array([0,0,0]))
    #Invert Image colours
    #image = cv.bitwise_not(image) 

    image = invert(image)
    
    # Add mask
    #image[orb_mask>0]=(238,170,0)
    #image[mask>0]=(0,0,0)
    #image[mask2>0]=(255,255,255)


    cv.imwrite("result.png",image)

    return name

def invert(image): #works perfectally, but is slow
    #Get dimensions
    width = image.shape[0]
    height = image.shape[1]
      
    # loop over the image, pixel by pixel
    for x in range(width):
        for y in range(height):
            if (image[x,y][1] == image[x,y][2] and image[x,y][0] == image[x,y][2]): #if shade
                image[x,y] = [abs(image[x,y][0] - 255), abs(image[x,y][1] - 255), abs(image[x,y][2] - 255)]

    return image

name = 'Screen_0006.PNG'
Filter(name)
