#Periodic LNB ATE2 R1.0
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-09-10

current_revision = 1.0

import pyvisa as visa
import csv
import sys
import time
import math
import serial
import json
import os
import shutil
from tkinter import *
from tkinter import ttk
from os import walk
from os import listdir
import datetime
import socket
import numpy as np
from pynput.keyboard import Key, Controller
from subprocess import Popen, PIPE
from PIL import Image, ImageOps

#Custom modules
import ATE2_DC_Control_Module as DC
import ATE2_Coax_Path_Module as Path
import ATE2_Datasheet_Generator_R2 as DS_Gen
import Orbital_Image_Filter_R4 as ImageFilter
import Thermotron8200PlusInterface as Thermotron


## Equipment Addresses ##
SA_VISA = 'TCPIP0::10.0.10.184::hislip0::INSTR'              #MXA 9020B Spectrum Analyzer
SG_VISA = 'GPIB2::5::INSTR'                                  #Anritsu 68367C Signal Generator - GPIB commands only. No SCPI
PS_VISA = 'USB0::0x05E6::0x2230::9104855::0::INSTR'          #Keithley Instruments 2230-30-1 Power Supply
PM_VISA = 'USB0::0x0957::0x2B18::MY48100947::0::INSTR'       #Agilent U2001A Power Meter
DMM_VISA = 'TCPIP0::10.0.10.125::hislip0::INSTR'             #Keysight 34461A Voltmeter / Temperature Probe
AMM_VISA = 'GPIB2::23::INSTR'                                #HP3478A Ammeter
VNA_VISA = 'TCPIP0::10.0.10.131::inst0::INSTR'               #Fieldfox N9918A
DC_Control_Panel_Port = 'COM3'                               #Custom DC control panel
RCM_216_IP = '10.0.10.177'                                   #Minicircuits RCM-216 
TChamber_VISA = 'TCPIP0::10.0.10.139::8888::SOCKET'

ATE2_path = os.path.dirname(__file__)
settings_path = ATE2_path + '\\ATE2_SETTINGS.csv'  
State_path = '\\\SVR-1\\Newmore\\_1_Instr-State-Files\\MXA_N9020B_States\\ATE2\\'


def Init_Serial(COM_port):   
    #Begin serial connection
    global ser
    ser = serial.Serial(
        port='COM' + str(COM_port),
        baudrate=38400,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS
    )

def Init_Socket(data):
    global sock
    server_address = (data['IP'],int(data['TCP_Port']))
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(server_address)

def ReadWriteToLNB(message, data):
    character = ''
    reply = ''
    #If socket connection
    if(data['IP'] != ''):
        sock.sendall(message)
        time.sleep(0.5)
        try:
            reply = sock.recv(1024)
            print(reply)

    #If ser connection
    elif(data['Ser_Port'] != ''):
        ser.write(message)
        time.sleep(0.5)
        character = ''
        reply = ''
        while(ser.in_waiting > 1):    
            character = ser.read(1)
            character = character.decode("utf-8")
            if character == '\n':
                character = ' '
            reply += character
    
    return reply

def main():
   
    #Declaring dictionary to manage and store test results
    data = {'NFG': 
                {'Frequency': [],'loss_before' : None,'loss_after' : None,'NF' : [],'Gain' : [],'averageNF' : None,'averageGain' : None,'amplitude_response_10MHz' : [],'amplitude_response_120MHz' : [],'amplitude_response_500MHz' : [], 'amplitude_response_1000MHz' : [],'amplitude_response_10MHz_max' : None,'amplitude_response_120MHz_max' : None,'amplitude_response_500MHz_max' : None,'amplitude_response_1000MHz_max' : None,'min_NF' : None,'max_NF' : None,'min_Gain' : None,'max_Gain' : None},
            'P1dB': {'Input' : [] , 'Output' : [], 'P1dB' : None, 'OIP3' : None}, 'image_rej': None, 'spurs': None, 'lo_leakage_out': None, 'current': None, 'lo_leakage_in': None, 'S11': None, 'S22': None,
            'phase': {'RAW' : [], '10Hz': None, '100Hz' : None, '1kHz' : None, '10kHz' : None, '100kHz' : None, '1MHz' : None},
            'RS485_enable' : False, 'CC_enable' : False, 'V_switch' : {'Enable' : False, 'V_Switch_Up': None, 'V_Switch_Down': None},
            'SA': None, 'SG': None, 'PS': None, 'PM': None, 'DMM': None, 'AMM': None, 'VNA': None,
            'date': None, 'time': None, 
            'Customer': None, 'PO': None, 'Tester': None, 'Color': None, 'Band': None, 'Manufacturer_SN': None, 'Orbital_SN': 'No_Name', 'stock_number': None, 'Model' : None,
            'Ser_Port' : None, 'Attenuation' : None, 'IP': None, 'TCP_Port': None, 'Script_Revision' : current_revision
            }
    #Get equipment names    
    data = EQID(data)
    
    #Get info
    data = GetInfo(data)  

    data['Period'] = PeriodText.get()
    if(data['Period'] == ''):
        data['Period'] = '0.01'
    print('Testing period: (Hours)', data['Period'])

    HEADER = ['Time', 'Temperature','Frequency','loss_before','loss_after', 'NF' ,'Gain','averageNF','averageGain','amplitude_response_10MHz','amplitude_response_120MHz','amplitude_response_500MHz', 'amplitude_response_1000MHz','amplitude_response_10MHz_max','amplitude_response_120MHz_max','amplitude_response_500MHz_max','amplitude_response_1000MHz_max','min_NF','max_NF','min_Gain','max_Gain', 'P1dB In', 'P1dB Out', 'P1dB', 'OIP3', 'image_rej', 'spurs', 'current', 'lo_leakage_out','lo_leakage_in', 'S11', 'S22','phase10', 'phase100', 'phase1k', 'phase10k', 'phase100k', 'phase1M', 'DSA']
    
    CSV_Init(data, HEADER)

    if(data['Ser_Port'] != ''):    
        Init_Serial(data['Ser_Port'])    
    elif(data['IP'] != '' and data['TCP_Port'] != ''):
        Init_Socket(data)
    else:
        pass

    folder_path = Make_Folder(FILENAME)
    TextInit()

    for i in range(127):      
        #try:
        results = []
        now = timestamp()
        print(now[0])

        f = open(FILENAME + '.txt', 'a')
        DSA = i
        DSA_hex = str(hex(i))
        message = '$setda,0,' + DSA_hex + ',*\r\n'
        message = bytes(message, 'utf-8')

        print(str(now[0]) + ' TX ' + str(message))
        f.write(str(now[0]) + ' TX ' + str(message) + '\n')

        reply = ReadWriteToLNB(message, data)
        
        print(str(now[0]) + ' RX ' + str(reply))
        f.write(str(now[0]) + ' RX ' + str(reply))
        f.close()
        
        data = GetData(data, now[1])
        results.append(now[0])
        try:
            results.append(float(Thermotron.ReadWrite(TChamber,'PVAR1?')))
        except:
            results.append('ERROR')
        results = results + list(data['NFG'].values()) + list(data['P1dB'].values())
        results.append(data['image_rej'])
        results.append(data['spurs']) 
        results.append(data['current']) 
        results.append(data['lo_leakage_out']) 
        results.append(data['lo_leakage_in']) 
        results.append(data['S11']) 
        results.append(data['S22'])
        results = results + list(data['phase'].values())
        results.append('')
        results.append(DSA)
        #----------------------------------------------
        with open(FILENAME + '.csv', 'a', newline='') as f:
            workbook = csv.writer(f)
            workbook.writerow(results)
        #except Exception as e:
           # now = timestamp()
           # print(now[0])
           # print(e)
           # pass


def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%b%d_%H%M%S")
    return now

def CSV_Init(data, header):
    global workbook, FILENAME
    current_time = timestamp()
    FILENAME = data['Orbital_SN'] + '_' + str(current_time[1]) 
    with open(FILENAME + '.csv', 'w', newline='') as f:
        workbook = csv.writer(f)
        workbook.writerow(['Tester:', data['Tester']])
        workbook.writerow(['Unit S/N:', data['Orbital_SN']])
        workbook.writerow(['Date:', current_time[0]])
        workbook.writerow('')
        workbook.writerow(list(data.keys()))
        workbook.writerow(list(data.values()))
        workbook.writerow('')
        workbook.writerow(header)

def TextInit():
    f = open(FILENAME + '.txt', 'x')
    f.close

def GetInfo(data):
    #Get Date and Time
    data['date'] = str(datetime.datetime.now().date())   
    data['time'] = str(datetime.datetime.now().time())
    print (data['date'], data['time'])
    print(ATE2_path) #Obtain program directory path
    
    #Obtaining unit info from input fields
    data['Orbital_SN'] = str(SNText.get())
    if data['Orbital_SN'] == '':
        data['Orbital_SN'] = 'No_Name'      
    data['stock_number'] = str(StockText.get())
    data['Manufacturer_SN'] = str(MnfText.get())
    data['Customer'] = str(CxText.get())
    data['PO'] = str(POText.get())
    data['Color'] = str(ColorText.get())
    data['Tester'] = str(YouText.get())
    data['Ser_Port'] = str(USB_Port_Text.get())
    data['IP'] = str(IPText.get())
    data['TCP_Port'] = str(Socket_PortText.get())
        
    return data

def GetData(data, now):
    print("*******BEGIN NEW TEST*******")
    Attenuation = []
    Attenuation.append(float(Attenuation1.get()))
    Attenuation.append(float(Attenuation2.get()))
    Attenuation.append(float(Attenuation3.get())) 

    #Obtaining user selected settings
    model_name = popupMenu1.get()
    settings_list = GetSettings(model_name, settings_master_list)
       
    name = data['Orbital_SN'] + '_' + now
    #folder_path = Make_Folder(name)  #Making Folder for Unit 
    
    #Check for band switching
    data['RS485_enable'] = bool(settings_list[0]['RS485_EN'])
    data['V_switch']['Enable'] = bool(settings_list[0]['Vsw_EN'])
    print('V Switching enabled: ' + str(data['V_switch']['Enable']))
    data['CC_enable'] = bool(settings_list[0]['CC_EN'])

    #Record Model No
    data['Model'] = settings_list[0]['Model']
    
    #Begin NFG Test
    #input('Connect Noise Source to DUT. Press ENTER to continue.')

    for settings in settings_list: 
        #data['Attenuation'] = Attenuation[(int(settings['Band']) - 1)]       
        data['Band'] = Change_Band(settings, data)
        #print('Starting Band ' + str(data['Band']) + ' NFG test')     
        data['NFG'] = get_NFG(settings, name)
        #Create .json file for raw band data
        #Save_JSON(folder_path, name, settings['Band'], data)
          
    #Begin Other Testing
    #input('Connect Coax Cable to DUT. Press ENTER to continue.')
    for settings in settings_list:      
        #data = Load_JSON(folder_path, name, settings['Band']) #Load BandX NFG data into current data dictionary 
        #data['Band'] = Change_Band(settings, data)
        #print('Starting Band ' + str(settings['Band']) + ' test')
        data['current'] = get_current() 
        #data['P1dB'] = get_p1db(settings)
        #data['phase'] = get_phase(settings, name)
        #data['image_rej'] = get_image_rejection(settings, name)
        #data['V_switch'] = get_V_Switch(settings)
        #data['spurs'] = get_spurs(settings, name)
        #data['lo_leakage_out'] = get_lo_leakage_output(settings, name)
        #data['lo_leakage_in'] = get_lo_leakage_input(settings, name)
        #data['S11'] = get_vswrIP(settings, name)
        #data['S22'] = get_vswrOP(settings, name)
   
        #Create .json file for raw band data
        #Save_JSON(folder_path, name, data['Band'], data)

        #Generate band datasheet
        #DS_Gen.Generate(folder_path, data, settings)
  
    #Pwr_off() 
    #Merge datasheets
    #DS_Gen.Merge(folder_path, data['Orbital_SN'] + ' Datasheet')
    
    #print ("Test completed, Please input next unit's Info\n")
    return data

def Save_JSON(folder_path, name, band, data):
    data_file = folder_path + "\\" + name + '_Band' + str(band) + '.json'
    with open(data_file,'w') as f:
        json.dump(data,f)
    f.close()

def Load_JSON(folder_path, name, band):
    data_file = folder_path + "\\" + name + '_Band' + str(band) + '.json'
    with open(data_file, 'r') as f:
        data = json.load(f)
    f.close()
    return data 

def Change_Band(settings, data):
    Pwr_on(settings)  #Change input voltage depending on band
    if(bool(settings['RS485_EN']) == True): #If RS485 enabled, change band
        RS485_SW(data['Ser_Port'], settings['Band'], float(data['Attenuation']))   
    if(bool(settings['CC_EN']) == True): #If contact closure is enabled, pause to allow user to change band
        input('Switch to Band ' + settings('Band') + '. Press Enter to continue.')   
    time.sleep(1) #Give controller time to detect new voltage and switch
    
    return settings['Band']
    
def Pwr_on(settings): 
    PS.write('APPL CH2, ' + str(settings['Voltage']) + ' , 0.5')
    time.sleep(1)
    PS.write(':OUTPut:STATe 1')
    
def Pwr_off(): 
    PS.write(':OUTPut:STATe 0')

def Make_Folder(productNumber): 
    if(productNumber == ''):
        productNumber == 'No_Name' #If productNumber is left blank, the shutil.rmtree() function will nuke ATE2 script directory (ask me how I know)
    path = os.getcwd() 
    dst = str(path) + '\\' + productNumber
    print ("Creating Folder...")
    if(os.path.exists(dst)):
        shutil.rmtree(dst) #Careful with this function. Can Nuke entire directory trees
        time.sleep(1)
    os.mkdir(dst)
    return dst

def SA_Screenshot(productNumber,tag,band): 
    
    #SA.write(":DISPlay:FSCReen:STATe 0") 
    SA.write(":DISPlay:FSCReen:STATe 1") 

    filename = productNumber + "_Band" + str(band) + '_' + tag + '.PNG'
    
    #temporary storage location (becasue MXA cannot save to Production folder)
    SA_save_path = "\\\SVR-1\\Newmore\\" 

    #saving a screenshot on the equipment
    SA.write(":MMEMory:STORe:SCReen '" + SA_save_path + filename + "'")
    
    time.sleep(1)
    
    ## Move screenshots to proper folder
    ATE2_path = os.path.dirname(__file__)
    folder = ATE2_path + "\\" + FILENAME + "\\"
    shutil.move(SA_save_path + filename, folder + filename)
    
    #Include image processing for datasheet (greyscaling, inversion, etc)
    ImageFilter.Filter(folder, filename, True)

def VNA_Screenshot(productNumber,tag,band): 

    #Setup filename and folder path
    ATE2_path = os.path.dirname(__file__)
    folder = ATE2_path + '\\' + productNumber + '\\'
    filename = productNumber + "_Band" + str(band) + '_' + tag + '.PNG'

    #saving a screenshot on the equipment                                 
    VNA.write(':MMEMory:STORe:IMAGe "TESTZ"')
    
    #fetching the image data in an array
    fetch_image_data = VNA.query_binary_values('MMEMory:DATA? "TESTZ.png"',datatype='B',is_big_endian=False,container=bytearray)

    #creating a file in testing folder with the product number and tag, then write the image data to it
    save_dir = open(folder + filename, 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    VNA.write(":MMEM:DEL 'TESTZ.PNG'")
    VNA.write("*CLS")

    #Include image processing for datasheet (greyscaling, inversion, etc)
    ImageFilter.Filter(folder, filename, True)

def Wait(resource): 
    #This function is used to wait for a resource to complete its operation
    #Poll the Event Status Register (ESR) every second to determine if the operation is complete.
   resource.write('*OPC')
   try:
       while(int(resource.query('*ESR?')) != 1):
           time.sleep(1)
           #print('waiting...')
   except KeyboardInterrupt:
       StopATE()
  
def get_NFG(settings, name): 
    
    NFG = {'Frequency': [],'loss_before' : None,'loss_after' : None,'NF' : [],'Gain' : [],'averageNF' : None,'averageGain' : None,'amplitude_response_10MHz' : None,'amplitude_response_120MHz' : None,'amplitude_response_500MHz' : None,'amplitude_response_1000MHz' : None,'amplitude_response_10MHz_max' : None,'amplitude_response_120MHz_max' : None,'amplitude_response_500MHz_max' : None,'amplitude_response_1000MHz_max' : None,'min_NF' : None,'max_NF' : None,'min_Gain' : None,'max_Gain' : None}
        
    print ("starting NF and Gain test")
    
    #set NFG Path
    Path.NFG()

    #Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['nf_gain_state_file'] + "'")
    Wait(SA)



    #Add limits
    
    
    #Start NFG Sweep  
    SA.write(":DISPlay:FSCReen:STATe 0")
    SA.write(":DISPlay:FSCReen:STATe 1")
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write('INITiate:RESTart')
    Wait(SA)

    #Aquire start and stop frequencies, and number of points
    freq_start = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')[0])
    freq_stop = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')[0])
    number_of_points = float(SA.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')[0])

    #Saving frequency data
    NFG['Frequency'] = []
    NFG['Frequency'].append(freq_start)
    add_next_Freq=((freq_stop-freq_start)/(number_of_points-1))
    i = 1
    next_Frequency = freq_start
    while i < number_of_points-1:
        next_Frequency = next_Frequency + add_next_Freq
        NFG['Frequency'].append(next_Frequency)
        i += 1   
    NFG['Frequency'].append(freq_stop)

    #Finding the index of the interested start and stop frequencies 
    i = 0
    flag = 0
    interested_freq = {}
    while i < len(NFG['Frequency']):
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_start_Frequency'])) and flag == 0:
            interested_freq['start'] = i
            flag = 1
        if (NFG['Frequency'][i] >= float(settings['nf_gain_interested_stop_Frequency'])):
            interested_freq['stop'] = i
            break
        i += 1
    
    #Detect compensation type and store value[s]
    loss_after_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:AFTer:MODE?')
    loss_before_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:BEFore:MODE?')
    if "FIX" in loss_after_mode:  
        NFG['loss_after'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:VALue?')
    elif "TABL" in loss_after_mode:  
        NFG['loss_after'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:TABLe:DATA?')
    else:
        pass
    if "FIX" in loss_before_mode:
        NFG['loss_before'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:VALue?')
    elif "TABL" in loss_before_mode:
        NFG['loss_before'] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:TABLe:DATA?')
    else:
        pass

    #Fetch NF array and calculate the min and max NF over the interested frequencies
    NFG['NF'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:NFIG?')
    i = interested_freq['start']
    interested_NF = []
    while i <= interested_freq['stop']:
        interested_NF.append(NFG['NF'][i])
        i += 1
    NFG['min_NF'] = min(interested_NF)
    NFG['max_NF'] = max(interested_NF)
    print ("Minimum NF: %f" %NFG['min_NF'])
    print ("Maximum NF: %f" %NFG['max_NF'])
    
    #calculating the linear noise figure values (only for interested frequencies)
    i = interested_freq['start']
    LinearNF = []
    while i <= interested_freq['stop']:
        LinearNF.append(10**(NFG['NF'][i]/10))
        i += 1
    
    #calculating the average noise figure by taking the average of the linear noise figure values and then using math to get the average noise figure (only for interested frequencies)
    i = 0
    sumLinearNF  = 0
    while i < len(LinearNF):
        sumLinearNF += LinearNF[i]
        i += 1  
    averageLinearNF = sumLinearNF / len(LinearNF)
    NFG['averageNF'] = 10 * math.log10(averageLinearNF)
    print ("Average NF: %f" %NFG['averageNF'])

    #Fetch Gain array and calculate the min and max Gain over the interested frequencies
    NFG['Gain'] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
    i = interested_freq['start']
    interested_Gain = []
    while i <= interested_freq['stop']:
        interested_Gain.append(NFG['Gain'][i])
        i += 1
    NFG['min_Gain'] = min(interested_Gain)
    NFG['max_Gain'] = max(interested_Gain)
    print ("Minimum Gain: %f" %NFG['min_Gain'])
    print ("Maximum Gain: %f" %NFG['max_Gain'])
    
    #calculating the linear gain values (only for interested frequencies)
    i = interested_freq['start']
    LinearGain = []
    while i <= interested_freq['stop']:
        LinearGain.append(10**(NFG['Gain'][i]/10))
        i += 1
        
    #calculating the average gain by taking the average of the linear gain values and then using math to get the average gain (only for interested frequencies)
    i = 0 
    sumLinearGain  = 0
    while i < len(LinearGain):
        sumLinearGain += LinearGain[i]
        i += 1 
    averageLinearGain = sumLinearGain / len(LinearGain)
    NFG['averageGain'] = 10 * math.log10(averageLinearGain)
    print ("Average Gain: %f" %NFG['averageGain'])
    
    #Gather gain ripple per bandwidth step
    NFG['amplitude_response_10MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 10000000)
    NFG['amplitude_response_10MHz_max'] = max(NFG['amplitude_response_10MHz'])
    NFG['amplitude_response_120MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 120000000)
    NFG['amplitude_response_120MHz_max'] = max(NFG['amplitude_response_120MHz'])
    NFG['amplitude_response_500MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 500000000)
    NFG['amplitude_response_500MHz_max'] = max(NFG['amplitude_response_500MHz'])
    try:
        NFG['amplitude_response_1000MHz'] = AmplitudeResponse(interested_freq['start'], interested_freq['stop'], NFG['Frequency'], NFG['Gain'], 1000000000)
        NFG['amplitude_response_1000MHz_max'] = max(NFG['amplitude_response_1000MHz']) 
    except:
        pass
    print('10MHz Ripple: ' + str(NFG['amplitude_response_10MHz_max']))
    print('120MHz Ripple: ' + str(NFG['amplitude_response_120MHz_max']))
    print('500MHz Ripple: ' + str(NFG['amplitude_response_500MHz_max']))
    print('1000MHz Ripple: ' + str(NFG['amplitude_response_1000MHz_max']))

    #Scale for screenshot
    SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:RLEVel " + str(round(NFG['averageNF'], 0)))
    SA.write("DISPlay:NFIGure:TRACe:GAIN:Y:SCALe:RLEVel " + str(round(NFG['averageGain'], 0)))
    SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:RPOSition CENTer")
    SA.write("DISPlay:NFIGure:TRACe:GAIN:Y:SCALe:RPOSition CENTer")
    time.sleep(1)
    
    SA_Screenshot(name, 'NFG', settings['Band'])
    
    return NFG

def AmplitudeResponse(index_start_freq, index_stop_freq, freq, gain, bandwidth): 
        #Gather amplitude ripple per frequency step
        ripple = []
        temp_gain = []
        i = index_start_freq
        for values in gain[index_start_freq:index_stop_freq+1]:
            temp_gain.append(values)
            if(freq[i] - freq[index_start_freq] >= bandwidth):           
                ripple.append((max(temp_gain)-min(temp_gain))/2)
                del temp_gain[0]
            i += 1
        return ripple

def get_p1db(settings):
    print ("starting P1dB test")  
    P1dB = {'Input' : [] , 'Output' : [], 'P1dB' : None, 'OIP3' : None}
    powerMeterPowerMax = 20
    powerMeterFreq = 1000000000
    gain = []
    running_average = []
    
    Path.P1dB() ##set P1dB Path
    
    SG.write('F1 ' + str(float(settings['LO']) + powerMeterFreq) + ' HZ') #setting the signal generator frequency   
    PM.write(':INITiate1:CONTinuous %d' % (1)) #triggering the power meter to take measurements continuosly 
    PM.write(':SENSe:FREQuency:FIXed %G' % (powerMeterFreq))
    
    P1dB['Input'].append((float(settings['Gain']) * -1) + 5) #Start at about 5dBm @ Output of LNB
    powerSweepTo = P1dB['Input'][-1] + 20 #End at '25dBm' or lower @ Output of LNB
    
    flag = True
    while P1dB['Input'][-1] < powerSweepTo:
        SG.write('L1 ' + str(float(P1dB['Input'][-1]) + float(settings['p1db_cable_loss'])) + ' DM') #setting the signal generator amplitude  
        SG.write('RF1')
        time.sleep(0.4)    
        P1dB['Output'].append(float(PM.query(':FETCh1?')) + float(settings['p1db_muxT_and_cable_loss'])) #fetch measurement, compensate for loss, and add it to P1dB['Output'] list
        
        gain.append(P1dB['Output'][-1] - P1dB['Input'][-1]) #Calculating gain at present step
        running_average = np.average(gain) #Calculate average gain over all steps
        difference = running_average - gain[-1] #Find the difference between the average gain and the current gain
        
        if(difference > 1 and flag == True): #If the difference between the average gain and the current gain is greater than 1dB, 1dB compression point found.
            P1dB['P1dB'] = P1dB['Output'][-1] - (difference - 1) #Record P1dB, adjusting for overshoot
            P1dB['OIP3'] = P1dB['P1dB'] + 10 #P1dB + 10 = OIP3
            flag = False
        
        P1dB['Input'].append(P1dB['Input'][-1] + 1)

    SG.write('RF0')

    
    print('P1dB: ' + str(P1dB['P1dB']))
    return P1dB

def get_phase(settings, name):

    phase = {'RAW' : [], '10Hz': None, '100Hz' : None, '1kHz' : None, '10kHz' : None, '100kHz' : None, '1MHz' : None}
    
    print ("starting Phase Noise test")
    
    carrier = 1500000000
    
    ##setting IPLS Path
    Path.IPLS()

    #Loading state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + str(settings['phase_noise_state_file'])+"'")
    Wait(SA)

    time.sleep(1)
    
    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + carrier) + ' HZ')
    SG.write('L1 ' + str(((float(settings['Gain'])) * -1) + float(settings['phase_noise_loss'])) + ' DM')
    SG.write('RF1')
    time.sleep(1)
    
    #Start Phase Sweep
    SA.write('SENSe:FREQuency:CARRier ' + str(carrier))
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write('INITiate:IMMediate')
    Wait(SA)
    
    #Gather all raw data from frequency plot
    phase['RAW'] = SA.query_ascii_values(':FETCH:LPlot?')
    
    #Gather phase noise markers
    phase['10Hz'] = SA.query_ascii_values('CALC:LPLot:MARK1:Y?')[0]
    phase['100Hz'] = SA.query_ascii_values('CALC:LPLot:MARK2:Y?')[0]
    phase['1kHz'] = SA.query_ascii_values('CALC:LPLot:MARK3:Y?')[0]
    phase['10kHz'] = SA.query_ascii_values('CALC:LPLot:MARK4:Y?')[0]
    phase['100kHz'] = SA.query_ascii_values('CALC:LPLot:MARK5:Y?')[0]
    phase['1MHz'] = SA.query_ascii_values('CALC:LPLot:MARK6:Y?')[0]
    #phase['10MHz'] = SA.query_ascii_values('CALC:LPLot:MARK7:Y?')[0]
    #phase['100MHz'] = SA.query_ascii_values('CALC:LPLot:MARK8:Y?')[0]
    
    SA_Screenshot(name, 'Phase_Noise', settings['Band'])

    SG.write('RF0')

    print(phase)

    return phase

def get_image_rejection(settings, name): 
    #gainAverage is measured by the get_nf_gain function and the product number is entered by the user
    print ("starting image rejection test")
    Path.IPLS()
    
    signal_freq = 1000000000
    
    #zeroDbTolerance is the tolerance used to check if the marker value of SA is close to zero when the frequency is set to verify and amplitude is set to gain average
    zeroDbTolerance = 1

    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['image_rejection_state_file'] + "'")
    
    #waiting for the state file to load
    Wait(SA)

    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + signal_freq) + ' HZ')
    #powerIn is the amplitude to be set on SG. We use the average gain and compensate for the cable loss. 
    powerIn = (float(settings['Gain']) + float(settings['image_rejection_cable_loss_verification'])) * -1
    #print('Initial PowerIn ' + str(powerIn))
    #setting amplitude of the signal generator to powerIn
    SG.write('L1 ' + str(powerIn) + ' DM')
    #turning RF ON
    SG.write('RF1')
    
    #restrating the sweep
    SA.write('SENSe:FREQuency:CENTer ' + str(signal_freq))
    SA.write(':INITiate:IMMediate')
    Wait(SA)

    #read the maximum marker value
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))      
    time.sleep(1)
    
    #verifying if the measurement is close to 0dB
    if((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
        print('Signal generator out of range. Adjusting...')
        difference = measurement
        powerIn -= difference
        SG.write('L1 ' + str(powerIn) + ' DM')
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        Wait(SA)
        SA.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(1)
        measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
        time.sleep(1)

        #If the SA measurement is still no 0dBm, finely tune:
        while((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
            print('Tuning to 0dBm...')
            if(measurement > zeroDbTolerance):
                  powerIn -= 1
            if(measurement < zeroDbTolerance):
                  powerIn += 1
            SG.write('L1 ' + str(powerIn) + ' DM')
            SA.write(':INITiate:IMMediate')
            Wait(SA)
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
            time.sleep(1)
              


    #setting the frequency on the signal generator to image rejection frequency to get the image rejection value
    SG.write('F1 ' + str(float(settings['LO']) - signal_freq) + ' HZ')
    SA.write('SENSe:FREQuency:CENTer ' + str(signal_freq))
    time.sleep(1)
    SA.write(':INITiate:IMMediate')
    Wait(SA)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    imageRejection = float(SA.query(':CALCulate:MARKer1:Y?'))
    time.sleep(1)
    imageRejection -= float(settings['image_rejection_cable_loss'])
    print ("Image Rejection: %f" %imageRejection)
    
    #Take screenshot
    SA_Screenshot(name,'Image_Rejection', settings['Band'])
    
    #Turn off Signal Generator
    SG.write('RF0')

    return imageRejection

def get_spurs(settings, name): 
    #gainAverage is measured by the get_nf_gain function and the product number is entered by the user
    print ("starting in band spur test")
    Path.IPLS()
    ##IN_BAND_SPURS_cable_loss is the loss of the cable that connects SG and the waveguide adapter on the LNB or the RF switch at the specific RF Frequency
    signal_freq = 2000000000
    cable_loss = float(settings['in_band_spur_cable_loss'])
    #zeroDbTolerance is the tolerance used to check if the marker value of SA is close to zero when the frequency is set to verify and amplitude is set to gain average
    zeroDbTolerance = 1

    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['in_band_spur_state_file'] + "'")
    
    #waiting for the state file to load
    Wait(SA)
    #Change bandwidth
    SA.write(':SENSe:FREQuency:START ' + str(settings['spur_interested_start_Frequency']))
    SA.write(':SENSe:FREQuency:STOP ' + str(settings['spur_interested_stop_Frequency']))
    #setting the signal generator frequency
    SG.write('F1 ' + str(float(settings['LO']) + signal_freq) + ' HZ')
    #powerIn is the amplitude to be set on SG. We use the average gain and compensate for the cable loss. 
    powerIn = (float(settings['Gain']) - cable_loss) * -1
    #print('Initial PowerIn ' + str(powerIn))
    #setting amplitude of the signal generator to powerIn
    SG.write('L1 ' + str(powerIn) + ' DM')
    #turning RF ON
    SG.write('RF1')
    
    #waiting for the sweep to complete and then read the maximum marker value
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) + 1
    
    #restrating the sweep
    SA.write(':INITiate:IMMediate')  
    time.sleep(sweepTime)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))      
    time.sleep(1)
    
    #verifying if the measurement is close to 0dB
    if((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
        print('Signal generator out of range. Adjusting...')
        difference = measurement
        powerIn = powerIn - difference
        SG.write('L1 ' + str(powerIn) + ' DM')
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        time.sleep(3)
        SA.write(':CALCulate:MARKer1:MAXimum')
        measurement = float(SA.query(':CALCulate:MARKer1:Y?'))

        #If the SA measurement is still no 0dBm, finely tune:
        while((measurement > zeroDbTolerance) or (measurement < (zeroDbTolerance * -1))):
            print('Tuning to 0dBm...')
            if(measurement > zeroDbTolerance):
                  powerIn = powerIn - 1
            if(measurement < zeroDbTolerance):
                  powerIn = powerIn + 1
            SG.write('L1 ' + str(powerIn) + ' DM')
            SA.write(':INITiate:IMMediate')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
            time.sleep(1)
              
    #Calculate spur
    time.sleep(sweepTime)
    SA.write(':ABORt')
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    value = SA.query('TRAC:MATH:PEAK?')

    #Take screenshot
    SA_Screenshot(name,'In_Band_Spurs', settings['Band'])
    
    #Turn off Signal Generator
    SG.write('RF0')
    print('In-band spur test: ' + value)
    return value

def get_lo_leakage_output(settings, name): 
    #product number is entered by the user
    print ("starting LO leakage output test")
    
    Path.IPLS()
    
    #lo_leakage_output_muxT_and_cable_loss is the loss of the muxT, to LNB cable, and to RX/modem cable at the LO Frequency
    lo_leakage_output_muxT_and_cable_loss = float(settings['lo_leakage_output_muxT_and_cable_loss'])
    
    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['lo_leakage_state_file'] + "'")
    #waiting 20 seconds for the state file to load
    Wait(SA)

    #restarting the sweep
    
    SA.write('SENSe:FREQuency:CENTer ' + str(settings['LO']))
    SA.write(':INITiate:IMMediate')
    #waiting for the sweep to complete
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
    time.sleep(sweepTime)

    #setting marker1 to LO
    SA.write(':CALCulate:MARKer1:X:CENTer ' + str(settings['LO']))
    #getting the result of marker1
    result = SA.query(':CALCulate:MARKer1:Y?')
    time.sleep(2)
    #LoLeakageOut: we compensate for the muxT and cable losses that go from the lnb throught the muxT and to the modem
    LoLeakageOut = float(result) + lo_leakage_output_muxT_and_cable_loss

    print ("LO Leakage Out: %f" %float(LoLeakageOut))

    SA_Screenshot(name, 'LO_Leakage_Output', settings['Band'])   
    
    return float(LoLeakageOut)

def get_lo_leakage_input(settings, name): 
    #product number is entered by the user
    print ("starting LO leakage input test")
    
    #Set Path
    Path.Input_Leakage()
    
    #cables_loss_at_lo_frequency is the loss of cable that connects the unit to the spectrum analyzer (SA) at LO frequency
    cables_loss_at_lo_frequency = float(settings['lo_leakage_input_cable_loss'])
    
    #loading state file
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['lo_leakage_state_file'] + "'")
    #waiting 20 seconds for the state file to load
    Wait(SA)
    #restarting the sweep
    SA.write('SENSe:FREQuency:CENTer ' + str(settings['LO']))
    SA.write(':INITiate:IMMediate')
    #waiting for the sweep to complete
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
 
    time.sleep(sweepTime)

    #setting marker1 to LO
    SA.write(':CALCulate:MARKer1:X:CENTer ' + str(settings['LO']))
    #getting the result of marker1
    result = SA.query(':CALCulate:MARKer1:Y?')
    time.sleep(2)
    #LoLeakageOut: we compensate for the muxT and cable losses that go from the lnb throught the muxT and to the modem
    LoLeakageIn = float(result) + cables_loss_at_lo_frequency

    print ("LO Leakage In: %f" %float(LoLeakageIn))
    
    SA.write('INITiate:CONTinuous 0')

    SA_Screenshot(name, 'LO_Leakage_Input', settings['Band'])   
 
    return float(LoLeakageIn)

def get_current(): 
    print("starting current test") 
    time.sleep(2)  
    current = AMM.query_ascii_values('') #getting the dc current measurement
    current = round(abs(float(current[0] * 1000)), 1) #converting the measurement reading from A to mA
    print ("Current: %f" %current)
    
    return current

def get_vswrIP(settings, name): 
    print ("starting vswr_S11 test")
    
    ##setting Path
    Path.S11_S22()
      
    VNA.write('*CLS')
    VNA.write("MMEM:LOAD '%s'"%settings['S11State'])
    Wait(VNA)
    VNA.write('INITiate:CONTinuous 1')
    time.sleep(1)
    VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    temp_values = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    S11 = temp_values[0]
    print ("S11: %f" %S11)
  
    time.sleep(4) #Wait for notification to disappear
    VNA_Screenshot(name, 'S11', settings['Band'])
    
    VNA.write('INITiate:CONTinuous 0')
    
    return S11

def get_vswrOP(settings, name):  
    print ("starting vswr_S22 test")
    
    ##setting Path
    Path.S11_S22()
    VNA.write('*CLS')                                    
    VNA.write("MMEM:LOAD '%s'"%settings['S22State'])
    Wait(VNA)
    VNA.write('INITiate:CONTinuous 1')
    time.sleep(1)
    VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
 
    temp_values = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    S22 = temp_values[0]
    print ("S22: %f" %S22)
    
    time.sleep(4) #Wait for notification to disappear
    VNA_Screenshot(name, 'S22', settings['Band'])
    
    VNA.write('INITiate:CONTinuous 0')

    return S22

def get_V_Switch(settings):
 
    Vsw = {'Enable' : False, 'V_Switch_Up': None, 'V_Switch_Down': None} 

    Path.IPLS()

    carrier = 1000000000
    threshold = -40
    
    #Check if Voltage switching enabled
    if(bool(settings['Vsw_EN']) == False):
        print('Voltage switching test not enabled')
        return Vsw
    else:
        Vsw['Enable'] = True
        print('Voltage switching enabled. Begin test.')
    
    #Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + str(settings['image_rejection_state_file'])+"'")
    Wait(SA)
    
    #Setup SG and SA with frequency and power level
    SA.write('SENSe:FREQuency:CENTer ' + str(carrier))
    SA.write('SENSe:BANDwidth:RESolution 1000 ') 
    SA.write('SENSe:SWEep:TIME 500 ms') 
    SG.write('F1 ' + str(float(settings['LO']) + carrier) + ' HZ')
    powerIn = (float(settings['Gain']) * -1) + float(settings['image_rejection_cable_loss'])
    SG.write('L1 ' + str(powerIn) + ' DM')
    SG.write('RF1')
    
    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
    time.sleep(sweepTime)

    print("Beginning voltage up test:" + str(settings['V_Switch_Up'])) 
    #Voltage switch Up Test
    #sweep +/-1V around switch up point
    if(bool(settings['V_Switch_Up']) == True):       
        for voltage in np.arange(float(settings['V_Switch_Up'])-1,float(settings['V_Switch_Up'])+1,0.1):
            PS.write('APPL CH2, ' + str(voltage) + ' , 0.5')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if(temp < threshold):
                Vsw['V_Switch_Up'] = round(voltage, 3)
                print('Voltage switch up: ' + str(Vsw['V_Switch_Up']))
                break
    else:
        print('Voltage up test not enabled')
    
    print("Beginning voltage down test:" + str(settings['V_Switch_Down']))
    #Voltage switch Down Test
    #sweep +/-1V around switch down point
    if(bool(settings['V_Switch_Down']) == True):       
        for voltage in np.arange(float(settings['V_Switch_Down'])+1,float(settings['V_Switch_Down'])-1,-0.1): 
            PS.write('APPL CH2, ' + str(voltage) + ' , 0.5')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if(temp < threshold):
                Vsw['V_Switch_Down'] = round(voltage, 3)
                print('Voltage switch down: ' + str(Vsw['V_Switch_Down']))
                break
    else:
        print('Voltage down test not enabled')

    SG.write('RF0')
    Pwr_on(settings)
    print(Vsw)    
    return Vsw

def RS485_SW(port,band,att):
     
    #change att value to hex
    gain = hex(int(abs((att - 31.5)*2)))
    
    ## set controller 
    k = Controller()
    ## Open RS485 Tool
    p = Popen(["trika.exe"])
    
    time.sleep(1)
    k.type("connect %s" %port)
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("s $dbg,2,0,*")
    k.press(Key.enter)
    time.sleep(0.5)
    k.type("s $dbg,1,3,*")
    k.press(Key.enter)
    ## Changing bands
    k.type("s $setst,%s,0,*" %band)
    k.press(Key.enter)
    time.sleep(0.5)
    ##Changing Gain
    k.type("s $setda,%s,%s,*" %(int(band)-1,gain)) #DSA band = n-1
    k.press(Key.enter)
    time.sleep(0.5)
    ##disconnecting and closing
    k.type("disc")
    k.press(Key.enter)
    p.terminate()
    
def Equipment_Init(): 
    global rm, SA, SG, PS, PM, DMM, AMM, VNA, TChamber
    rm = visa.ResourceManager()
    SA = rm.open_resource(SA_VISA)
    SA.timeout = 10000
    SG = rm.open_resource(SG_VISA)
    SG.timeout = 10000
    PS = rm.open_resource(PS_VISA)
    PS.timeout = 10000
    PM = rm.open_resource(PM_VISA)
    PM.timeout = 10000
    DMM = rm.open_resource(DMM_VISA)
    DMM.timeout = 10000
    AMM = rm.open_resource(AMM_VISA)
    AMM.timeout = 10000
    #VNA = rm.open_resource(VNA_VISA)
    #VNA.timeout = 10000
    #DC.Init(DC_Control_Panel_Port)
    Path.Init(RCM_216_IP)
    TChamber = rm.open_resource(TChamber_VISA)
    Thermotron.Init(TChamber_VISA)


def EQID(data): 
    data['SA'] = SA.query('*IDN?')
    data['SG'] = SG.query('*IDN?')
    data['PS'] = PS.query('*IDN?')
    data['PM'] = PM.query('*IDN?')
    data['DMM'] = DMM.query('*IDN?')
    #data['VNA'] = VNA.query('*IDN?')
    data['AMM'] = 'HP 3478A Ammeter 2301A10048' 
    data['TChamber'] = Thermotron.ReadWrite(TChamber, 'IDEN?')
    print(data['SA'])
    print(data['SG'])
    print(data['PS'])
    print(data['PM'])
    print(data['DMM'])
    print(data['VNA'])
    print(data['TChamber'])

    return data

def GUI(settings_master_list):
    global master
    master = Tk() ## Initialize Menu variable
    master.title('Periodic ATE2 Program ' + str(current_revision))
    #master.iconphoto(False, PhotoImage(file = 'orblogo4.png'))

    #Variable List
    global popupMenu1, SNText, StockText, MnfText, ColorText, CxText, POText, YouText, PeriodText, USB_Port_Text, Attenuation1, Attenuation2, Attenuation3, IPText, Socket_PortText
    band2 = IntVar()
    band3 = IntVar()
    atten = []
    for num in range(21):
        atten.append(num/2)

    options = Settings_List_Options(settings_master_list)

    #Band Menu
    Bnd = LabelFrame(master, text="Settings File Menu")
    Bnd.grid(row=1,column=1,sticky=W)
    
    #Drop Down Menu 1
    Bnd1Name = Label(Bnd,text="Model Number:").grid(row=2,column=1,sticky=W)
    popupMenu1 = ttk.Combobox(Bnd, value=options) ##Makes the menu
    popupMenu1.set('None') ## Set Default Value
    popupMenu1.grid(row = 2, column =2,sticky=W,ipadx = 100) ## Positions the Menu

    refresh = Button(master,text='Refresh',command=Refresh).grid(row=1,column=2,sticky=E)

    #Communication Menu
    Comm = LabelFrame(master, text="Communication Menu (if applicable)")
    Comm.grid(row=2,column=1,columnspan=2)

    ##Port
    USB_Port = Label(Comm, text = 'Serial Port Number:').grid(row=5, column = 1, sticky = W)
    USB_Port_Text = Entry(Comm)
    USB_Port_Text.grid(row = 5, column = 2, sticky = W,ipadx = 2)

    IP = Label(Comm, text = 'IP Address:').grid(row=6, column = 1, sticky = W)
    IPText = Entry(Comm)
    IPText.grid(row = 6, column = 2, sticky = W)

    Socket_Port = Label(Comm, text = 'TCP Port:').grid(row=7, column = 1, sticky = W)
    Socket_PortText = Entry(Comm)
    Socket_PortText.grid(row = 7, column = 2, sticky = W)

    ## Combobox for Band 1 attenuation values
    AttName1 = Label(Comm,text="Band 1 Attenuation Value:").grid(row=8,column=1,sticky=W)
    Attenuation1 = ttk.Combobox(Comm, value=atten)
    Attenuation1.set('0.5')
    Attenuation1.grid(row=8,column = 2, sticky=W)

    ## Combobox for Band 2 attenuation values
    AttName2 = Label(Comm,text="Band 2 Attenuation Value:").grid(row=9,column=1,sticky=W)
    Attenuation2 = ttk.Combobox(Comm, value=atten)
    Attenuation2.set('0.5')
    Attenuation2.grid(row=9,column = 2, sticky=W)

    ## Combobox for Band 3 attenuation values
    AttName3 = Label(Comm,text="Band 3 Attenuation Value:").grid(row=10,column=1,sticky=W)
    Attenuation3 = ttk.Combobox(Comm, value=atten)
    Attenuation3.set('0.5')
    Attenuation3.grid(row=10,column = 2, sticky=W)
    
    #Information Menu
    Info = LabelFrame(master, text="Unit Information Menu")
    Info.grid(row=3,column=1,columnspan=2)

    #Serial Number
    SN = Label(Info, text = 'Serial Number:').grid(row=1, column = 1, sticky = W)
    SNText = Entry(Info)
    SNText.grid(row = 1, column = 2, sticky = W)

    #Stock Number
    Stock = Label(Info, text = 'Stock Number:').grid(row=1, column = 3, sticky = W)
    StockText = Entry(Info)
    StockText.grid(row = 1, column = 4, sticky = W)

    #Manufacturer S/N if present
    Mnf = Label(Info, text = 'Supplier S/N:').grid(row=2, column = 1, sticky = W)
    MnfText = Entry(Info)
    MnfText.grid(row = 2, column = 2, sticky = W)

    #Unit Color
    Color = Label(Info, text = 'Color:').grid(row=2, column = 3, sticky = W)
    ColorText = Entry(Info)
    ColorText.grid(row = 2, column = 4, sticky = W)

    #Customer Name
    Cx = Label(Info, text = 'Customer:').grid(row=3, column = 1, sticky = W)
    CxText = Entry(Info)
    CxText.grid(row = 3, column = 2, sticky = W)

    #PO Number
    PO = Label(Info, text = 'PO Number:').grid(row=3, column = 3, sticky = W)
    POText = Entry(Info)
    POText.grid(row = 3, column = 4, sticky = W)

    #Tester Name
    You = Label(Info, text = 'Tester:').grid(row=4, column = 1, sticky = W)
    YouText = Entry(Info)
    YouText.grid(row = 4, column = 2, sticky = W)

    Period = Label(Info, text = 'Period:').grid(row=6, column = 1, sticky = W)
    PeriodText = Entry(Info)
    PeriodText.grid(row = 6, column = 2, sticky = W)
    Label(Info,text="(Hours)").grid(row=6,column=3,sticky=W)
            
    #Button to Begin Testing
    Stop = Label(master, text = 'Press CTRL+C to stop ATE.').grid(row=7, column = 1, sticky=W)
    b1 = Button(master,text='Begin',command=StartATE).grid(row=7,column=2,sticky=E)

    master.mainloop()

def Refresh():
    pass

def StartATE():
    try:
        main()
    except KeyboardInterrupt:
        StopATE()

def StopATE():
    Pwr_off()
    SG.write('RF0')
    SA.write(':ABORt')
    print('Keyboard Interrupt detected. Stopping Program.')
    sys.exit()
    
def Load_Settings(file):
    #Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)
    f.close

    #Genrate list of dictionaries from matrix, grouping bands together if model name left blank 
    dictionary_list = []
    multiband = []
    for column in range(len(matrix[0])):
        if matrix[0][column] != '' and column > 0:    
            multiband = []
        temp = {}
        for row in range(len(matrix)):
            temp[matrix[row][0]] = matrix[row][column]
        multiband.append(temp)
        if matrix[0][column] != '':
            dictionary_list.append(multiband)
    dictionary_list.pop(0)
    
    return dictionary_list   
    
def Settings_List_Options(settings_list):
    options = []
    for i in range(len(settings_list)):
        options.append(settings_list[i][0]['Model']) 
    return options  

def GetSettings(model_name, settings_master_list):
    settings_list = []
    flag = False
    for i in range(len(settings_master_list)):
        if(model_name == settings_master_list[i][0]['Model']):
            for settings in settings_master_list[i]:
                settings_list.append(settings)
                flag = True
    if(flag == False):
        print('Model not found. Please review settings table.')
        sys.exit()

    return settings_list
 
settings_master_list = Load_Settings(settings_path) #Load settings
Equipment_Init() #Initialize connection to all measurement equipment
GUI(settings_master_list)#Call GUI

#Version Log:
#------------------------------
#Revision 1.0 - 20211008 - Ported over code from Periodic LNB ATE2 R1.0.py. Remove periodic function. Addition of serial tx commands and rx logging
