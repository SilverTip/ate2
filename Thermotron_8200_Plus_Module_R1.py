#Thermotron_8200_Plus_Module_R1.py
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-09-09

current_revision = 1

import pyvisa as visa
import os
import time
import datetime
import tkinter as tk
from tkinter import ttk
import json

TemperatureProfilePath = os.path.dirname(__file__) + '\\_Temperature Profiles\\'

HOUR = 60 * 60 #in seconds

def TimeStamp():
    timeS = datetime.datetime.now()
    return timeS, timeS.strftime("%Y-%m-%d %H:%M:%S")

def Init(rm, VISA):
    TChamber = rm.open_resource(VISA)
    TChamber.timeout = 10000

    return TChamber

def ReadWrite(inst, cmd): #Manually read byte string becasue visa.read_raw() and visa.query_ascii_values() doesn't work
    character = ''
    string = ''
    try:
        inst.write(cmd)
        while(character != '\n'):    
            character = inst.read_bytes(1)
            character = character.decode("utf-8")
            string += character 
    except Exception as e:
        print('Failure to communicate.')
        print(e)   
        string = 'Com Fail'

    #Filter response
    string = string.split('\n')[0]
    string = string.split('\r')[0]
    
    return string

def STOP(inst): #STOP - stop temperature chamber
    cmd = 'STOP'
    return ReadWrite(inst, cmd)

def TEMP(inst): #PVAR1? - read the current chamber chanel 1 temperature
    cmd = 'PVAR1?'
    return ReadWrite(inst, cmd)   

def START(inst): #RUNM - run chamber in manual mode
    cmd = 'RUNM'
    return ReadWrite(inst, cmd)

def HOLD(inst): #HOLD - hold current temp (even if no setpoint)
    cmd = 'HOLD'
    return ReadWrite(inst, cmd)

def RESUME(inst): #RESM - Resume manual mode (from hold function)
    cmd = 'RESM'
    return ReadWrite(inst, cmd)

def SET(inst,temp): #SETP1, - set temperature point
    cmd = 'SETP1'
    cmd = cmd + ',' + str(temp)
    return ReadWrite(inst, cmd)

def RAMP(inst,rrate): #MRMP1, - set ramp rate
    cmd = 'MRMP1'
    cmd = cmd + ',' + str(rrate)
    return ReadWrite(inst, cmd)    

def RunProfile(inst, profile):
    testDuration = 0
    for temp, period in profile:
        testDuration += period
    print('Test Duration:(Hours)', round(testDuration,1))
    
    START(inst)
    for temperature, period in profile:     
        current_temp = TEMP(inst)
        ramp_rate = round(abs(float(current_temp) - float(temperature)) / (float(period)*60),2) #Time in hours
        SET(inst,temperature)
        time.sleep(0.25)
        RAMP(inst,ramp_rate)
        print('Time:', TimeStamp()[1], 'Current Temperature:', current_temp, 'Next Temperature:', temperature, 'Ramp:(C/min):', ramp_rate, 'Duration:(Hours)', period) 
        time.sleep(period * HOUR)
      
    print('Test Completed @', TimeStamp()[1])
    STOP(inst)

def LoadProfile(ProfileName):  
    with open(TemperatureProfilePath + ProfileName,'rb') as f:
        profile = json.load(f)
        f.close
    return profile

def ProfileFilter(profile): #Remove ATE2 related commands from Temperature Profile
    for i, value in enumerate(profile):
        if(type(value[0]) is str):
            profile.pop(i)
    return profile

def main():
    profileName = TemperatureDropdown.get()
    if profileName == 'None':
        print('No profile selected')
        return
    print(profileName)
    profile = LoadProfile(profileName)
    profile = ProfileFilter(profile)
    try:
        RunProfile(TChamber, profile)
    except KeyboardInterrupt:
        print('Keyboard Interrupt detected. Stopping Program')
        pass

def CommandLine():
    print(COMMAND.get())
    message = ReadWrite(TChamber, COMMAND.get())
    print(message)

def GUI():
    global master
    master = tk.Tk() ## Initialize Menu variable
    master.title('Thermotron8200+ Program R' + str(current_revision))
    #master.iconphoto(False, PhotoImage(file = 'orblogo4.png'))

    #Variable List
    global TemperatureProfiles, TemperatureDropdown, COMMAND
    TemperatureProfiles = ['None']

    for (dirpath, dirnames, filenames) in os.walk(TemperatureProfilePath):
        TemperatureProfiles.extend(filenames)

    #Temperature Frame
    TemperatureFrame = tk.LabelFrame(master,text="Temperature Menu")
    TemperatureFrame.grid(row=1,column=1,sticky='W')

    #Temperature Profile Dropdown Menu
    tk.Label(TemperatureFrame,text="Profile:").grid(row=1,column=1,sticky='W')
    TemperatureDropdown = ttk.Combobox(TemperatureFrame,value=TemperatureProfiles) ##Makes the menu
    TemperatureDropdown.set('None') ## Set Default Value
    TemperatureDropdown.grid(row=1,column=2,sticky='W',ipadx=50) ## Positions the Menu
    b1 = tk.Button(TemperatureFrame,text=' Begin ',command=main).grid(row=1,column=3,sticky='E')

    tk.Label(TemperatureFrame,text='Press CTRL+C to stop profile').grid(row=2,column=2)
    tk.Label(TemperatureFrame,text='').grid(row=3,column=1,sticky='W')
    
    #Command line
    tk.Label(TemperatureFrame,text='Command:').grid(row=4,column=1,sticky='W')
    COMMAND = tk.Entry(TemperatureFrame)
    COMMAND.grid(row=4,column=2,sticky='W',ipadx=50)
    b2 = tk.Button(TemperatureFrame,text='  Send  ',command=CommandLine).grid(row=4,column=3,sticky='E')

    master.mainloop()


if __name__ == '__main__':
    #global rm, TChamber
    #TChamber_VISA = 'TCPIP0::10.0.10.57::8888::SOCKET'
    #rm = visa.ResourceManager()
    #TChamber = Init(rm, TChamber_VISA)
    GUI()


#Useful commands: (All channel 1 commands)
#--------------------------
#SETP1,[temperature] - set temperature setpoint
#STOP - stop temperature cycle
#RUNM - run chamber in manual mode
#HOLD - hold current setpoint. Cannot change ramp or setpoint. (Does not hold current temperature if still ramping)
#RESM - Resume manual mode (from hold function)
#MRMP1,[ramp rate] - set manual ranmp rate (degree/min)
#PVAR1? - return current temperature
