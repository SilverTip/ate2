from docx import Document
from docx.shared import Inches
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Pt 


data = (
    ('$getst', 'Get System Status', '$STATR,<CID>,<UID>,<UNIXTS>,<UPT>,<CUPT>,<SS>,<FS>,<C_LNB>,<V_LNB>,<0>,<VIN>,<0>,<TEMP>,<Bandx>,*','Response'),
    ('$getst', 'Get System Status', '$STATR,<CID>,<UID>,<UNIXTS>,<UPT>,<CUPT>,<SS>,<FS>,<C_LNB>,<V_LNB>,<0>,<VIN>,<0>,<TEMP>,<Bandx>,*','Response'),
    ('$getst', 'Get System Status', '$STATR,<CID>,<UID>,<UNIXTS>,<UPT>,<CUPT>,<SS>,<FS>,<C_LNB>,<V_LNB>,<0>,<VIN>,<0>,<TEMP>,<Bandx>,*','Response'),
    ('$getst', 'Get System Status', '$STATR,<CID>,<UID>,<UNIXTS>,<UPT>,<CUPT>,<SS>,<FS>,<C_LNB>,<V_LNB>,<0>,<VIN>,<0>,<TEMP>,<Bandx>,*','Response'),
)
table_headers = ['Commands','Description','Expected Response','Response','Pass/Fail']

def ReportMaker(data,name):
    file_path =  "\\\ORB-SVR-FS01\\Newmore\\_Tian\\Scripts\\Debug"
    document = Document()

    #Cover Page
    spacer = document.add_paragraph().add_run('\n')
    spacer.font.size = Pt(40)

    logo = document.add_picture("\\\ORB-SVR-FS01\\Newmore\\_Tian\\Scripts\\ORBLOGOforDocuments.jpg")
    logo = document.paragraphs[-1] 
    logo.alignment = WD_ALIGN_PARAGRAPH.CENTER

    spacer = document.add_paragraph().add_run('\n')
    spacer.font.size = Pt(40)

    title = document.add_paragraph()
    title.paragraph_format.alignment = WD_ALIGN_PARAGRAPH.CENTER
    temp = title.add_run('Firmware Validation Testing Results for \n')
    temp.font.size = Pt(22)
    document.add_page_break()
    for tests in data:
        if type(tests[0]) is list:
            text = tests[0][0]
        else:
            text = tests[0]
        tabletitle = document.add_paragraph().add_run(text)
        tabletitle.font.size = Pt(12)
        Resultstable = document.add_table(rows=5, cols=1)
        Resultstable.autofit = False

        hdr_cells = Resultstable.columns[0].cells
        for cell,parameter in zip(Resultstable.columns[0].cells,table_headers):
            cell.width = Inches(1.5)
            cell.text = parameter
        #hdr_cells[0].text = 'Command'
        #hdr_cells[1].text = 'Description'
        #hdr_cells[2].text = 'Expected Response'
        #hdr_cells[3].text = 'Response'
        
        row_cells = Resultstable.add_column(Inches(4)).cells
        #print(tests)
        if type(tests[0]) is list:
            row_cells[0].text = "".join(tests[0])
        else:
            row_cells[0].text = tests[0]
        row_cells[1].text = tests[1]
        row_cells[2].text = tests[2]
        if type(tests[3]) is list:
            #print("\n".join(tests[3]))
            row_cells[3].text = "".join(tests[3])
        else:
            row_cells[3].text = tests[3]
        

        spacer = document.add_paragraph().add_run('\n')
        spacer.font.size = Pt(12)
        

    document.add_page_break()

    document.save(file_path + '\\'+ name + '.docx')

#ReportMaker(data,'test2')