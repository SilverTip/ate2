################################################################################
#Written by Tian Hao Xu for Orbital Research in Python 3.9
#This module is for calculating gain variation over temperature from JSON data 
#generated from an ATE thermal cycle. This version is for ATE USE ONLY 
#The standalone version is named GainVariationTemp RX.py
################################################################################

import xlsxwriter
import matplotlib.pyplot as plt
import xlwings as xw
import json
import time
import sys
import os
import csv
import math

data_collection_location_path = "\\\SVR-1\\Shared Folders\\Production\\_To_be_REVIEWED\\In_Progress\\ATE_Data_Collection"

def GainVariation(path,docket,settings,band):
    subdirectories = [dI for dI in os.listdir(path) if os.path.isdir(os.path.join(path,dI))]
    temp_high = 0 
    temp_low = 0 
    Gainvariation = 0
    plt.clf()
    plt.cla()
    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    print(F'Generating Band {band} Gain Variation')
    for subdirectory in subdirectories:
        if docket in subdirectory:
            data_location = path + '\\' + subdirectory
            files = os.listdir(data_location)
            for file in files: #Find all .json files in folder
                if file.endswith('.json') and "Band"+str(band) in file:
                    jsonlocation = data_location + '\\' + file
                    data = Load_JSON(jsonlocation)
                    if float(data['Temperature']['Value']) >= temp_high:
                            temp_high = float(data['Temperature']['Value'])
                            gain_high = data['NFG']['Gain']
                            NF_hight = data['NFG']['NF']
                            min_gain = data['NFG']['min_Gain']
                            avg_NF_hight = data['NFG']['averageNF']
                    elif float(data['Temperature']['Value']) < temp_low:
                            temp_low = float(data['Temperature']['Value'])
                            gain_low = data['NFG']['Gain']
                            NF_lowt = data['NFG']['NF']
                            max_gain = data['NFG']['max_Gain']
                            avg_NF_lowt = data['NFG']['averageNF']
                    ax1.plot(data['NFG']['Frequency'],data['NFG']['Gain'],'-', linewidth=1, label = F"Gain at {data['Temperature']['Value']}")
                    ax1.set_ylabel('Gain')
                    ax1.set_title(F'Band {band} Gain over Temperature')
                    ax2.plot(data['NFG']['Frequency'],data['NFG']['NF'], '-', linewidth=1, label = F"NF at {data['Temperature']['Value']}")
                    ax2.set_xlabel('Frequency')
                    ax2.set_ylabel('Noise Figure')
                    ax2.set_title(F'Band {band} NF over Temperature')
        for gainhigh,gainlow,freq,NFhigh,NFlow in zip(gain_high,gain_low,data['NFG']['Frequency'],NF_hight,NF_lowt):
            if NFG_Start <= float(freq)/1000000 <= NFG_End:
                if (float(gainlow)-float(gainhigh))/2 > Gainvariation:
                    Gainvariation = (float(gainlow)-float(gainhigh))/2
                    GVfrequency = freq
                if (float(NFhigh)-float(NFlow))/2 > NFvariation:
                    NFvariation = (float(gainlow)-float(gainhigh))/2
                    NFfrequency = freq

        print(F'Gain Variation: {Gainvariation}')
        fig.suptitle(F'{docket} Gain Variation at Band {band}')
        avgNFvariation = (float(avg_NF_hight)-float(avg_NF_lowt))/2
        maxtominGain = (float(max_gain)-float(min_gain))/2
        ax1.annotate(
            F'''Worst Case Gain Variation: +/- {round(Gainvariation,3)} dB at {GVfrequency} Hz
Max to Min Gain Variation: +/- {round(maxtominGain,3)} dB
Worst Case NF Variation: +/- {round(NFvariation,3)} dB at {NFfrequency} Hz
Noise Figure Average Variation: +/- {round(avgNFvariation,3)} dB
            ''',  
            xy=(0.5, 0),
            xycoords=('axes fraction', 'figure fraction'),
            xytext=(0, 3),  
            textcoords='offset points',
            size=10, ha='center', va='bottom')
        ax1.set_yticks(range(int(min(gain_high)),int(max(gain_low)),1))
        fig.subplots_adjust(bottom=0.3)
        ax1.legend(loc='lower right')
        ax2.legend(loc='lower right')
        filename = F"{path}\\GainVariation_Band{band}.png"
        plt.savefig(filename,dpi=200)
        gain_high.clear()
        gain_low.clear()

def Load_JSON(file):
    with open(file, 'r') as f:
        data = json.load(f)
    f.close()

    return data 

def main():
    path = input('This version is for ATE use only. Please use the standalone version for manual generation')
    time.sleep(1)
    bands = input('Input number of bands: ')
    time.sleep(1)
    docket = input('Input docket: ')

    #GainVariation(path,docket,settings)
    
if(__name__ == '__main__'):
	main()
