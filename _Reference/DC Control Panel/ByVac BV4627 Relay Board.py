#Created by Benjamin Stadnik
#Orbital Research Ltd.
#2021-02-05

#Interface with ByVac BV4627 Relay Module

import sys
import pyvisa as visa
import time
import csv
import datetime
import socket
import xlsxwriter
import serial


ALL_ON = b"\x1b\x5b\x6f"
ALL_OFF = b"\x1b\x5b\x6e"

class relay:
    
    def __init__(self, name, NC_command, NO_command):
        self.name = name
        self.NO_command = NO_command
        self.NC_command = NC_command
        self.flag = 0
        
    def NC(self):
        ser.write(self.NC_command)
        self.flag = 0
        
    def NO(self):
        ser.write(self.NO_command)
        self.flag = 1
        
    def toggle(self):
        if(self.flag == 0):
            ser.write(self.NO_command)
            self.flag = 1
            print(self.name + ' = ' + str(self.flag))
        else:
            ser.write(self.NC_command)
            self.flag = 0
            print(self.name + ' = ' + str(self.flag))


A, B, C, D, E, F, G, H = 0, 0, 0, 0, 0, 0, 0, 0
A = relay('A', b"\x1b\x5b\x30\x41", b"\x1b\x5b\x31\x41")
B = relay('B', b"\x1b\x5b\x30\x42", b"\x1b\x5b\x31\x42")
C = relay('C', b"\x1b\x5b\x30\x43", b"\x1b\x5b\x31\x43")
D = relay('D', b"\x1b\x5b\x30\x44", b"\x1b\x5b\x31\x44")
E = relay('E', b"\x1b\x5b\x30\x45", b"\x1b\x5b\x31\x45")
F = relay('F', b"\x1b\x5b\x30\x46", b"\x1b\x5b\x31\x46")
G = relay('G', b"\x1b\x5b\x30\x47", b"\x1b\x5b\x31\x47")
H = relay('H', b"\x1b\x5b\x30\x48", b"\x1b\x5b\x31\x48")


#Begin serial connection on COM5
ser = serial.Serial(
    port='COM5',
    baudrate=115200,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.EIGHTBITS
)

#Connect to BV4627
print('Connecting to BV4627...')
begin_commands = b"\x0d"
count = 0
while(count < 2):
    ser.write(begin_commands)
    time.sleep(0.25)
    count = count + 0.25

#Test Relay
def Test_Relays():
    print('Testing Relays...')
    i = 0
    while(i < 2):
        A.toggle()
        time.sleep(0.2)
        B.toggle()
        time.sleep(0.2)
        C.toggle()
        time.sleep(0.2)
        D.toggle()
        time.sleep(0.2)
        E.toggle()
        time.sleep(0.2)
        F.toggle()
        time.sleep(0.2)
        G.toggle()
        time.sleep(0.2)
        H.toggle()
        time.sleep(0.2)
        i += 1
    
def helper():
    print('\n')
    print('Help Menu')
    print('--------------------------')
    print('Test - Test Relays')
    print('H - Help')
    print('On - All relays switch to NO')
    print('Off - All relays switch to NC')
    print('Q - Quit')
    print('--------------------------')
    print('\n')


def OFF():
    ser.write(ALL_OFF)
    A.flag = 0
    B.flag = 0
    C.flag = 0
    D.flag = 0
    E.flag = 0
    F.flag = 0
    G.flag = 0
    H.flag = 0
    
def ON():
    ser.write(ALL_ON)
    A.flag = 1
    B.flag = 1
    C.flag = 1
    D.flag = 1
    E.flag = 1
    F.flag = 1
    G.flag = 1
    H.flag = 1

#Main Loop
while(1):

   
    command = raw_input('Command:')

    if(command == 'Test'):
        Test_Relays()

    elif(command == 'H'):
        helper()
    elif(command == 'A'):
        A.toggle()
    elif(command == 'B'):
        B.toggle()
    elif(command == 'C'):
        C.toggle()
    elif(command == 'D'):
        D.toggle()
    elif(command == 'E'):
        E.toggle()
    elif(command == 'F'):
        F.toggle()
    elif(command == 'G'):
        G.toggle()
    elif(command == 'I'):
        H.toggle()
    elif(command == 'On'):
        ON()
    elif(command == 'Off'):
        OFF()
    
    elif(command == 'Q'):
        print('Releasing COM port...')
        ser.close()
        break
    else:
        print('Unknown command. Enter H for help menu.')
