#Created by Benjamin Stadnik
#Orbital Research Ltd.
#2021-02-05

#Interface with ByVac BV4627 Relay Module

import sys
import pyvisa as visa
import time
import csv
import datetime
import socket
import xlsxwriter
import serial

#Custom class to initialize relays
class relay:
    
    def __init__(self, name, state_NC, state_NO, NC_command, NO_command):
        self.name = name #Name of relay
        self.command = [NC_command, NO_command] #relay serial commands as governed by the BYVAC BV4627 datasheet
        self.state_name = [state_NC, state_NO] #Name of NO/NC function of the relay
        self.state = self.state_name[0]
        self.flag = 0 #Flag to determine if relay is in NC/NO state
        
    def set_state(self, state_number): #Select relay position
        ser.write(self.command[state_number])
        self.state = self.state_name[state_number]
        self.flag = state_number
        print(self.name + ' = ' + str(self.state))

    def NC(self):
        self.set_state(0) #NC Position
        
    def NO(self):
        self.set_state(1) #NO Position
        
    def toggle(self): #Toggle relay position
        if(self.flag == 0):
            self.change_state(1)
        else:
            self.change_state(0)
            
#Functions
#------------------------------------------------------------------------------------

def helper():
    print('\n')
    print('Help Menu')
    print('--------------------------')
    print('OFF - Turn ATE Off')
    print('OVC - Enable OVC. Will prompt for input')
    print('EXT - Enable EXT supply')
    print('POS - Enable POS. Will prompt for input')
    print('H - Help')
    print('Q - Quit')
    print('--------------------------')
    print('\n')

#Global Variables
#------------------------------------------------------------------------------------
   
#Init relays with their fuctions
ATE = relay('ATE', 'Off', 'On', b"\x1b\x5b\x30\x41", b"\x1b\x5b\x31\x41")                            #Relay A
ATE_Input = relay('ATE_Input', 'Ext', 'OVC', b"\x1b\x5b\x30\x42", b"\x1b\x5b\x31\x42")               #Relay B
OVC_L1 = relay('OVC_L1', 'Open', 'GND', b"\x1b\x5b\x30\x44", b"\x1b\x5b\x31\x44")                    #Relay D
OVC_L2 = relay('OVC_L2', 'Open', 'GND', b"\x1b\x5b\x30\x43", b"\x1b\x5b\x31\x43")                    #Relay C
POS = relay('POS', 'Off', 'On', b"\x1b\x5b\x30\x46", b"\x1b\x5b\x31\x46")                            #Relay F
POS_Input = relay('POS_Input', 'AC_Supply', '12V Battery', b"\x1b\x5b\x30\x45", b"\x1b\x5b\x31\x45") #Relay E   

#MAIN
#------------------------------------------------------------------------------------

#Begin serial connection on COM5
ser = serial.Serial(
    port='COM3',
    baudrate=115200,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.EIGHTBITS
)

#Connect to BV4627
print('Connecting to BV4627...')
begin_commands = b"\x0d"
count = 0
while(count < 12): #Spam startup command to the BV4627. Found to be at least 1.2seconds. Cannot connect quicker than that
    ser.write(begin_commands)
    time.sleep(0.1)
    count += 1


#Main Loop
while(1):

    command = input('Command:')
    
    if(command == 'OFF'):
        ATE.NC()
        ATE_Input.NC()
        OVC_L1.NC()
        OVC_L2.NC()
        POS.NC()
        POS_Input.NC()
        
    elif(command == 'OVC'):
        command = input('What voltage would you like to set the OVC? [OFF/13/18/22]')
        if(command == '13'):
            OVC_L1.NO()
            OVC_L2.NO()
            ATE_Input.NO()
            ATE.NO()
        elif(command == '18'):
            OVC_L1.NC()
            OVC_L2.NO()
            ATE_Input.NO()
            ATE.NO()
        elif(command == '22'):
            OVC_L1.NO()
            OVC_L2.NC()
            ATE_Input.NO()
            ATE.NO()
        elif(command == 'OFF'):
            OVC_L1.NC()
            OVC_L2.NC()
            ATE_Input.NC()
            ATE.NC()
        else:
            print('Unknown command.')
            ATE_Input.NC()
            ATE.NC()
            
    elif(command == 'EXT'):
        ATE_Input.NC()
        ATE.NO()
        
    elif(command == 'POS'):
        command = input('POS Input? [AC/BAT/OFF]')
        if(command == 'AC'):
            POS_Input.NC()
            POS.NO()
        elif(command == 'BAT'):
            POS_Input.NO()
            POS.NO()
        elif(command == 'OFF'):
            POS_Input.NC()
            POS.NC()
        else:
            print('Unknown command')
            POS.NC()
            
    elif(command == 'H'):
        helper()
    elif(command == 'Q'):
        print('Releasing COM port...')
        ser.close()
        break
    else:
        print('Unknown command. Enter H for help menu.')
