# LNB ATE2 RX.py
# Written by Benjamin Stadnik
# Orbital Research Ltd.
# 2021-11-03

import pyvisa as visa
import sys
import time
import math
import json
import os
import tkinter as tk
from tkinter import ttk
import datetime
import numpy as np
import base64
import logging
import serial
import shutil
import traceback

# Custom modules
import ATE2_Coax_Path_Module as Path
import Datasheet_Generator_R5_TX2_JL3 as DS_Gen
import Orbital_Image_Filter_R6 as ImageFilter
import Thermotron_8200_Plus_Module_R1 as Thermo
import LNB_Communication_R10 as COMM
import ATE_Data_Collection_R2 as Collect
import ATE_GainVariationTemp_R1 as GainVar
import dccontrol

## Equipment Addresses ##
SA_VISA = 'TCPIP0::10.0.10.186::hislip0::INSTR'  # MXA 9020B Spectrum Analyzer
SG_VISA = 'TCPIP0::10.0.10.208::inst0::INSTR'  #Agilent Technologies, E8257D, MY51111865, C.06.18, 00:30:03:12:4A:BE
PS_VISA = 'USB0::0x05E6::0x2230::9104855::0::INSTR'  # Keithley Instruments 2230-30-1 Power Supply
PM_VISA = 'USB0::0x0957::0x2E18::MY50000381::0::INSTR'  # Agilent U2001A Power Meter
DMM_VISA = 'TCPIP0::10.0.10.161::inst0::INSTR'  # Keysight 34461A Voltmeter / Temperature Probe
AMM_VISA = 'TCPIP0::10.0.10.42::hislip0::INSTR'  # # Keysight 34461A Voltmeter / Temperature Probe
#VNA_VISA = None
VNA_VISA = 'TCPIP0::DESKTOP-TD5BQCS::hislip_PXI10_CHASSIS1_SLOT1_INDEX0::INSTR' # P9735A PXIe
RCM_216_IP = '10.0.10.184'  # Minicircuits RCM-216
TChamber_VISA = 'TCPIP0::10.0.10.183::8888::SOCKET'  # Thermotron8200+
OSC_VISA = None
#OSC_VISA = 'TCPIP0::10.0.10.190::INSTR'
DC_VISA = "10.0.10.195:65432"

ATE2_PATH = "\\\ORB-SVR-FS01\\Production\\_To_be_REVIEWED\\In_Progress\\ATE2"
settings_path = '\\\ORB-SVR-FS01\\Production\\_To_be_REVIEWED\\In_Progress\\ATE2\\ATE2_SETTINGS.csv'
State_path = '\\\ORB-SVR-FS01\\Newmore\\_1_Instr-State-Files\\MXA_N9020B_States\\ATE2\\'

GLOBALWAITTIME = datetime.timedelta(seconds=0)

class HSX_RS232_Connection:
    def __init__(self, port):
        self.ser = serial.Serial(
            port='COM' + str(port),
            baudrate=115200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
        )

    def query(self, message):
        self.write(message)
        time.sleep(0.1)
        character = ''
        word = ''
        reply = ['']
        while (self.ser.in_waiting > 0):
            character = self.ser.read(1)
            character = character.decode('ascii')
            word += character

        unfiltered_reply = word.split('\n')
        reply = []

        for item in unfiltered_reply:
            if (item != '' and not item.count(message)):
                reply.append(item)

        return reply[0]

    def write(self, message):
        tx = bytes(str(message) + '\n\r', 'ascii')
        self.ser.write(tx)

    def close(self):
        self.ser.close()


def main(folder_path):
    # Declaring dictionary to manage and store test results
    data = {'NFG':
        {
            'Frequency': [], 'NF': [], 'Gain': [],
            'Before_DUT_Loss_Table_Name': None, 'Before_DUT_Loss_Table': None,
            'After_DUT_Loss_Table_Name': None, 'After_DUT_Loss_Table': None,
            'amplitude_response_10MHz': None, 'amplitude_response_120MHz': None,
            'amplitude_response_500MHz': None, 'amplitude_response_1000MHz': None,
            'amplitude_response_10MHz_max': None, 'amplitude_response_120MHz_max': None,
            'amplitude_response_500MHz_max': None, 'amplitude_response_1000MHz_max': None,
            'averageNF': None, 'min_NF': None, 'max_NF': None,
            'averageGain': None, 'min_Gain': None, 'max_Gain': None
        },
        'P1dB':
            {
                'Input': [], 'Output': [], 'P1dB': None, 'OIP3': None
            },
        'phase':
            {
                'RAW': [], 'SMOOTH': [],
                '10Hz': None, '100Hz': None, '1kHz': None, '10kHz': None,
                '100kHz': None, '1MHz': None, '10MHz': None, '100MHz': None
            },
        'image_rej_input': None, 'min_image_rej': None, 'spurs_input': None, 'spurs': None,
        'lo_leakage_out': None, 'current': None, 'lo_leakage_in': None, 'S11': None, 'S22': None,
        'Band_Switching': None, 'Serial_Port': None, 'IP': None, 'IP_Port': None, 'Attenuation': None,
        'Date': None, 'Time': None, 'Voltage': None, 'Voltage_PS': None, 'lo_lock': None,
        'Customer': None, 'PO': None, 'Tester': None, 'Color': None,
        'Band': None, 'Manufacturer_SN': None, 'Orbital_SN': 'No_Name', 'stock_number': None, 'Model': None,
        'Script_Revision': os.path.basename(__file__), 'Name': None,
        'Temperature':
            {
                'Enable': False, 'Profile': None, 'Value': None
            }
    }

    # Get equipment names and append them to data
    data.update(Resources)

    # Obtaining unit info from input fields
    data['Orbital_SN'] = str(SNText.get())
    if data['Orbital_SN'] == '':
        data['Orbital_SN'] = 'No_Name'
    data['stock_number'] = str(StockText.get())
    data['Manufacturer_SN'] = str(MnfText.get())
    data['Customer'] = str(CxText.get())
    data['PO'] = str(POText.get())
    data['Color'] = str(ColorText.get())
    data['Tester'] = str(YouText.get())
    data['Serial_Port'] = USB_Port_Text.get()
    data['Attenuation'] = Attenuation.get()
    data['IP'] = str(IP_Text.get())
    data['IP_Port'] = str(IP_Port_Text.get())

    # Obtaining user selected settings
    model_name = ModelDropdown.get()
    settings_list = DS_Gen.Find_Settings(settings_master_list, model_name, 'Name')
    if not settings_list:
        logger.warning("No Model Selected")
        return

    # Record Model No
    data['Name'] = settings_list[0]['Name']
    data['Model'] = settings_list[0]['Model']

    spectype = [str(x) for x in settings_list[0]["ir_p1db_spectype"].split(",")]
    logger.info(F"Image Rejection is set to take the {spectype[0]} value")
    logger.info(F"P1dB is set to take the {spectype[1]} value")
    # Check for band switching
    if not settings_list[0]['Band_Switching']:  # Replace blank string with NoneType
        data['Band_Switching'] = None
    else:
        data['Band_Switching'] = settings_list[0]['Band_Switching']

    # Power on unit+
    delay = 10
    if (settings_list[0]['Startup_Delay'] != ''):
        delay = settings_list[0]['Startup_Delay']
    voltage = settings_list[0]['Voltage']
    if (voltage):
        Pwr_on(voltage, data)  # Change input voltage to first band
        if (delay):
            logger.info(F"Power startup delay: {delay} seconds")
            time.sleep(int(delay))

    global coms
    if COMM_DISABLE.get():
        logger.warning("Communications disabled by tester")
    else:
        coms = OpenCommunications(data)

    if (coms):
        data['Build_Info'] = TestCommunications(coms)

    # Save unit settings
    with open(F"{folder_path}//{data['Model']}_Settings.json", 'w') as f:
        json.dump(settings_list, f)

    # Determine if temperature profile selected
    if (TemperatureDropdown.get() != 'None'):
        data['Temperature']['Enable'] = True
        logger.info('Temperature Profile Enabled')
        if (coms):
            logger.info(F"AUTOST PERIOD: {AUTOST.get()} minutes")

    # Run ATE with or without temperature profile
    if (data['Temperature']['Enable']):
        # if temperature profile enabled: run temperature profile with instances of the ATE determined by the profile
        Temperature_Cycle(settings_list, data, folder_path)
    else:  # Run ATE once
        RunATEbyBand(settings_list, data, data['Orbital_SN'], folder_path)

    # close com port
    if (coms):
        coms.close()

    logger.debug(F"Total time wating on SA: {GLOBALWAITTIME}")

    if not PWR_ON.get():
        Pwr_off()

    logger.handlers.clear()


def Temperature_Cycle(settings_list, data, folder_path):
    # Load Temperature Profile
    profile = Thermo.LoadProfile(TemperatureDropdown.get())
    data['Temperature']['Profile'] = str(TemperatureDropdown.get())
    logger.info(F"Profile: {data['Temperature']['Profile']}")

    # Determine testing time
    testDuration = 0
    for temp, period in profile:
        testDuration += period
    logger.info(F'Test Duration (Hours): {round(testDuration, 1)}')

    # Execute temperature profile
    Thermo.START(TChamber)
    for temperature, period in profile:
        current_temp = Thermo.TEMP(TChamber)
        logger.info("Current Temperature: " + str(current_temp) + 'C')
        if (temperature == 'ATE'):
            Thermo.HOLD(TChamber)
            temp_time = timestamp()[1]
            data['Temperature']['Value'] = current_temp
            name = str(data['Orbital_SN']) + '_' + temp_time + '_' + str(current_temp) + 'C'
            subfolder = Make_Folder(folder_path, name)
            RunATEbyBand(settings_list, data, name, subfolder)
            Thermo.START(TChamber)
        elif (type(temperature) is str):
            pass
        else:
            ramp_rate = round(abs(float(current_temp) - float(temperature)) / (float(period) * 60), 2)  # Time in hours
            Thermo.SET(TChamber, temperature)
            time.sleep(0.25)
            Thermo.RAMP(TChamber, ramp_rate)
            logger.info(
                F"Time: {Thermo.TimeStamp()[1]} | Current Temperature: {current_temp} | Next Temperature: {temperature} | Ramp Rate(C/min): {ramp_rate} | Step Duration (Hours): {period}")
            Sleep(period * 60 * 60)  # in seconds

    logger.info(F'Test Completed @ {Thermo.TimeStamp()[1]}')
    Thermo.STOP(TChamber)

    # This generates Gain and NFG Variation plots for each band for temp cycled units
    if testDuration > 0:
        try:
            logger.info(F'Generating NFG Variation Plots')
            band = 1
            for settings in settings_list:
                GainVar.GainVariation(folder_path, data(['Orbital_SN']), settings, band)
                band += 1
        except:
            logger.error(F'Could not generate NFG Variation Plots')


def RunATE(settings_list, data, name, folder_path):
    logger.info('Running ATE2')

    # Get Date and Time
    data['Date'] = str(datetime.datetime.now().date())
    data['Time'] = str(datetime.datetime.now().time())
    logger.info(data['Date'])
    logger.info(data['Time'])

    # Begin NFG Test
    if (not data['Temperature']['Enable']):  # Skip if in the temperature chamber
        input('Connect Noise Source to DUT. Press ENTER to continue.')
    band = 1
    for settings in settings_list:
        data['Band'] = Change_Band(settings, data, coms, band)
        data['Band_Switching'] = settings['Band_Switching']
        if Resources['DMM']:
            data['Voltage'] = get_Voltage()
        if Resources['AMM']:
            # time.sleep(int(1))
            data['current'] = get_Current()
        if Resources['SA']:
            data['lo_leakage_out'], LO_Lock = get_lo_leakage_output(settings, name, folder_path, band)
            while LO_Lock == 'False':
                data['lo_leakage_out'], LO_Lock = get_lo_leakage_output(settings, name, folder_path, band)
            data['NFG'] = get_NFG(settings, name, data['Serial_Port'], folder_path, band, coms)
            # pass
        # Create .json file for raw band data
        Save_JSON(folder_path, name, data, band)
        if (NFG_Only.get()):
            # Generate band datasheet
            DS_Gen.Generate(folder_path, name, data, settings)
            logger.info(F"Generate Band {band} datasheet: {name}.xlsx")
        band += 1

    # Begin S11/S22 Testing
    if (NFG_Only.get() == False):
        if (not data['Temperature']['Enable']):  # Skip if in the temperature chamber
            input('Connect Coax Cable to DUT. Press ENTER to continue.')
            if Resources['VNA']:
                S11 = get_vswr('S11', settings, name, folder_path, band)
                S22 = get_vswr('S22', settings, name, folder_path, band)
                band = 1
                for settings in settings_list:
                    data = Load_JSON(folder_path, name, band)
                    data['S11'] = S11
                    data['S22'] = S22
                    Save_JSON(folder_path, name, data, band)
                    band += 1

        # Begin Other testing
        band = 1
        for settings in settings_list:
            data = Load_JSON(folder_path, name, band)  # Load BandX NFG data into current data dictionary
            data['Band'] = Change_Band(settings, data, coms, band)
            if (Resources['SG'] and Resources['SA'] != None):
                data['P1dB'] = get_p1db(settings, data)
                data['phase'] = get_phase(settings, name, folder_path, band)
                data['image_rej_input'], data["image_rej"] = get_image_rejection(settings, name, folder_path, band)
                # data['V_switch'] = get_V_Switch(settings)
                data['spurs_input'], data["spurs_pass"] = get_spurs(settings, name, folder_path, band)
                # data['lo_leakage_out'] = get_lo_leakage_output(settings, name, folder_path, band)
                data['lo_leakage_in'] = get_lo_leakage_input(settings, name, folder_path, band)
            
            print("Done all test")

            # Create .json file for raw band data
            Save_JSON(folder_path, name, data, band)
            print("Saving json")

            # Generate band datasheet
            DS_Gen.Generate(folder_path, name, data, settings)

            logger.info(F"Generate Band {band} datasheet: {name}.xlsx")
            band += 1

    logger.info("ATE2 Test completed.\n")

    # Merge data
    try:
        DS_Gen.Merge(folder_path, name + ' Datasheet')
        logger.info("Merged Datasheets")
    except:
        logger.error('Could not merge datasheets')

    if (NFG_Only.get() == False):
        try:
            Collect.DataCollection(folder_path, data['Model'], 1)
            logger.info("Collecting Data")
        except:
            logger.error('Could not collect data')


def RunATEbyBand(settings_list, data, name, folder_path):
    logger.info('Running ATE2')
    vibecheck = 0

    # Get Date and Time
    data['Date'] = str(datetime.datetime.now().date())
    data['Time'] = str(datetime.datetime.now().time())
    logger.info(data['Date'])
    logger.info(data['Time'])

    extra = False
    if ('Horizon' in data['Model']):
        extra = True

    if (not data['Temperature']['Enable']):  # Skip if in the temperature chamber
        input('Connect Noise Source to DUT. Press ENTER to continue.')
    band = 1
    print(NFG_Only.get())
    for settings in settings_list:
        data['Band'] = Change_Band(settings, data, coms, band)
        if (not No_NFG.get()):
            if (not data['Temperature']['Enable']):  # Skip if in the temperature chamber
                input('Connect Noise Source to DUT. Press ENTER to continue.')
            if Resources['DMM']:
                data['Voltage'] = float(DMM.query("READ?"))
                logger.info(F'Voltage is now {data["Voltage"]}')
                data['Voltage_PS'] = float(PS.query('FETC:VOLT? CH2'))
                # print("test")
                # data['current'] = get_Current()
            if Resources['AMM']:
                time.sleep(int(0.1))
                data['current'] = get_Current()
            if Resources['SA']:
                if 'LNA' not in settings["Nickname"]:
                    data["lo_lock"], data["lo_leakage_out"], data['lo_leak_out_harmonics'], data[
                        'max_harmonic'] = get_lo_leakage(settings, name, folder_path, band, 'Output', extra)
                    while data["lo_lock"] == 'False':
                        data["lo_lock"], data["lo_leakage_out"], data['lo_leak_out_harmonics'], data[
                            'max_harmonic'] = get_lo_leakage(settings, name, folder_path, band, 'Output', extra)
                data['NFG'] = get_NFG(settings, name, data['Serial_Port'], folder_path, band, coms)
            if (NFG_Only.get()):
                # Generate band datasheet
                DS_Gen.Generate(folder_path, name, data, settings)
                Save_JSON(folder_path, name, data, band)
                logger.info(F"Generate Band {band} datasheet: {name}.xlsx")
                band += 1
            else:
                if (not data['Temperature']['Enable']):  # Skip if in the temperature chamber
                    input('Connect Coax Cable to DUT. Press ENTER to continue.')
                if (Resources['SG'] and Resources['PM']):
                    data['P1dB'] = get_p1db(settings, data)
                if Resources['VNA']:
                    S11 = get_vswr('S11', settings, name, folder_path, band)
                    S22 = get_vswr('S22', settings, name, folder_path, band)
                    data['S11'] = S11
                    data['S22'] = S22
                # Begin Other testing
                if (Resources['SG'] and Resources['SA'] and 'LNA' not in settings["Nickname"]):    
                        # input('Connect POS to DUT. Press ENTER to continue.')
                    data['phase'] = get_phase(settings, name, folder_path, band)
                        # input('Connect to SG. Press ENTER to continue.')
                    data['image_rej_input'], data["image_rej"], data["min_image_rej"] = get_image_rejection(settings, name, folder_path, band)
                    # data['V_switch'] = get_V_Switch(settings)
                    data['spurs_input'], data["spurs"] = get_spurs(settings, name, folder_path, band, extra)
                    # data['lo_leakage_out'] = get_lo_leakage_output(settings, name, folder_path, band)
                    data["lo_leakage_in"], data['lo_leak_in_harmonics'] = get_lo_leakage(settings, name, folder_path, band,
                                                                                         'Input', extra)

            Save_JSON(folder_path, name, data, band)
            if (No_NFG.get() == False and NFG_Only.get() == False):
                # Generate band datasheet
                DS_Gen.Generate(folder_path, name, data, settings)
                logger.info(F"Generate Band {band} datasheet: {name}.xlsx")
                band += 1

    logger.info("ATE2 Test completed.\n")

    # Thermo.START(TChamber)
    # Merge data
    try:
        DS_Gen.Merge(folder_path, name + ' Datasheet')
        logger.info("Merged Datasheets")
    except:
        logger.error('Could not merge datasheets')

    if (NFG_Only.get() == False and No_NFG.get() == False):
        try:
            Collect.DataCollection(folder_path, data['Model'].strip(), 1)
            print(data['Model'].strip())
            logger.info("Collecting Data")
        except:
            logger.error('Could not collect data')


def Save_JSON(folder_path, name, data, band):
    data_file = F'{folder_path}\\{name}_Band{band}.json'
    with open(data_file, 'w') as f:
        json.dump(data, f)
    f.close()


def Load_JSON(folder_path, name, band):
    data_file = F'{folder_path}\\{name}_Band{band}.json'
    with open(data_file, 'r') as f:
        data = json.load(f)
    f.close()
    return data


def setup_logger(name, log_file, formatter, stream_en):
    level = logging.DEBUG

    handler = logging.FileHandler(log_file)
    handler.setFormatter(logging.Formatter(formatter))

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    if (stream_en):
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter(formatter))
        logger.addHandler(stream_handler)

    return logger


def Change_Band(settings, data, coms, band):
    logger.info("Changing Band..")

    if (str(data['Band_Switching']).upper().count("VOLTAGE")):
        Pwr_on(settings['Voltage'], data)  # Change input voltage depending on band
        if (settings['Startup_Delay']):
            logger.info(F"Power startup delay: {settings['Startup_Delay']} seconds")
            time.sleep(float(settings['Startup_Delay']))

    if (Resources["OSC"]):
        if (settings['Band_Switching'].count('22k')):
            OSC.write(':WGEN1:FUNCtion SQUare')
            OSC.write(':WGEN1:FREQuency 22000')
            OSC.write(':WGEN1:VOLTage 0.8')
            OSC.write(':WGEN1:OUTPut 1')
            logger.info("Set OSC ref to 22k")
            time.sleep(2)
        else:
            OSC.write(':WGEN1:OUTPut 0')
            logger.debug("Set OSC ref off")

    if Resources['DC']:
        DC = dccontrol.Client(2, DC_VISA)
        if (settings['Band_Switching'].count('22k')):
            logger.info(DC.query("$tone,1,*"))
        else:
            logger.info(DC.query("$tone,0,*"))
        DC.close()

    if (coms):
        band_offset = int(band)
        if (int(settings['Atten_Type']) == 1 or int(settings['Atten_Type']) == 2):
            band_offset -= 1
        messages = [F'$setst,{band_offset},*']
        if not data['Attenuation'].isalpha():
            DSA_hex = COMM.attenuation(int(settings["Atten_Type"]), float(data['Attenuation']), True)
        messages.append(F'$setda,{band_offset},*')
        messages.append('$getst,*')
        messages.append('w 1')
        try:
            for message in messages:
                logger.debug(message)
                reply = coms.query(message)
                logger.debug(reply)
        except Exception as e:
            logger.error(e)
            logger.error('Failed to change unit band')
        time.sleep(2)

    return band


def OpenCommunications(data):
    coms = None

    if not data['Band_Switching']:
        return None

    method = data['Band_Switching'].upper()
    logger.info(F"Band Switching Method: {method}")

    try:
        if (method.count('RS')):
            if (data['Serial_Port']):
                if (method.count('RS232')):
                    coms = COMM.RS232_Connection(data["Serial_Port"])
                else:
                    coms = COMM.RS485_Connection(data["Serial_Port"])
            else:
                logger.error("Serial Port not specified on serial enabled unit")
                raise Exception
        elif (method == 'IP'):
            if (data['IP'] and data['IP_Port']):
                coms = COMM.Socket_Connection(data["IP"], int(data["IP_Port"]))
            else:
                logger.error("IP or Port not specified on IP enabled unit")
                raise Exception
        elif (method == 'VOLTAGE'):
            pass
        elif (method == 'BDC'):
            pass
        elif (method == 'CC'):
            pass
    except Exception as e:
        logger.error(e)
        logger.error(
            '***NO CONNECTION***\nUnable to establish communication channel with unit.\nPlease review connection settings and try again.')
        StopATE()

    return coms


def TestCommunications(coms):
    reply = []
    build = None
    logger.info("Testing unit communications...")
    try:
        reply = coms.query('$build,1,*')
        if (reply):
            logger.info(F"Build: {reply}")
            build = reply
        else:
            raise Exception
    except Exception as e:
        logger.error(e)
        logger.error(
            '***TEST COMMUNICATION FAILED***\nCommunication method failed to communicate with unit.\nPlease review connection settings and try again.')
        StopATE()

    logger.info(F"Build: {build}")

    messages = ["$getst,*", "w 1"]

    for info in messages:
        logger.debug(info)
        reply = coms.query(info)
        logger.debug(reply)

    return build


def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%m%d_%H%M%S")
    now[2] = current_time
    return now


def Pwr_on(voltage, data):
    if not voltage:
        return
    voltage = float(voltage)
    DC_Panel_Loss = 0
    v_offset = 0
    if (data['Temperature']['Enable']):  # and not data['voltage_corrected']
        if (float(data['Temperature']['Value']) < 0):
            v_offset = abs(float(data['Temperature'][
                                     'Value'])) / 100  # Scaled up voltage during reduced temperatures to account for voltage drop
            logger.info(F"Voltage offset due to temperature: {v_offset}V")
    if Resources['DC']:
        print("Trying to set OVC")
        state = 0
        if 10 < voltage <= 15:
            state = 1
        elif 15 < voltage <= 20:
            state = 2
        elif 20 < voltage <= 24:
            state = 3
        DC = dccontrol.Client(2, DC_VISA)
        logger.info(DC.query(f"$ovc,{state},*"))
        DC.close()
    else:
        v = voltage + v_offset + DC_Panel_Loss
        PS.write('APPL CH2, ' + str(v) + ' , 1')
        PS.write(':OUTPut:STATe 1')
        if ('LNA' not in data['Model']):
            get_volt_curr(voltage)
        logger.info(F"Set External Voltage: {v}V")


def Pwr_off():
    if Resources['DC']:
        DC = dccontrol.Client(2, DC_VISA)
        logger.info(DC.query('$ovc,0,*'))
        DC.close()
    else:
        PS.write('APPL CH2, ' + str(0) + ' , 1')
        logger.info("Unit power off")


def Make_Folder(path, name):
    if (name == ''):
        name == 'No_Name'
    dst = F"{path}\\{name}"
    if (os.path.exists(dst)):
        number = 1
        while (1):
            new_name = F"{dst}_{number}"
            if not os.path.exists(new_name):
                dst = new_name
                break
            number += 1

    time.sleep(1)
    os.mkdir(dst)

    return dst


def SA_Screenshot(productNumber, tag, band, folder_path):
    SA.write(":DISPlay:FSCReen:STATe 1")
    temp_file = 'D:\\Users\Instrument\Documents\TEMP.PNG'
    #  Save temp file to MXA
    SA.write('*CLS')
    SA.write(F":MMEMory:STORe:SCReen '{temp_file}'")
    Wait(SA)
    #  Send temp file as byte array to ATE2 computer memory
    fetch_image_data = SA.query_binary_values(F'MMEMory:DATA? "{temp_file}"',
                                              datatype='B',
                                              is_big_endian=False,
                                              container=bytearray
                                              )
    temp_path = 'C:\\temp\TEMP.PNG'
    #   Save byte array data to .png on ATE2 computer
    with open(temp_path, 'wb') as f:
        f.write(fetch_image_data)
    time.sleep(1.5)
    # Filter image on ATE2 computer
    ImageFilter.Filter(temp_path, True)
    filename = productNumber + "_Band" + str(band) + '_' + tag + '.PNG'
    path = F"{folder_path}\\{filename}"
    #   Move filtered image to testing directory
    shutil.copy2(temp_path, path)  # 20230530 - BS - \\silvertip.local\data\Production doesn't like file manipulation
    logger.info(F'Saved screenshot: {filename}')


def SA_Save_Data(productNumber, tag, band, folder_path):  # Doesn't work for some reason
    filename = productNumber + "_Band" + str(band) + '_' + tag + '.csv'
    path = F"{folder_path}\\{filename}"
    SA.write(F':MMEMory:STORe:RESults "{path}"')
    logger.debug(F'Saved data: {filename}')


def VNA_Screenshot(productNumber, tag, folder_path, band):
    filename = productNumber + "_Band" + str(band) + '_' + tag + '.PNG'
    temp_path = 'C:\\test\\TEMP.PNG'
    VNA.write(':MMEMory:STORe:IMAGe "TESTZ"')
    fetch_image_data = VNA.query_binary_values('MMEMory:DATA? "TESTZ.png"',
                                               datatype='B',
                                               is_big_endian=False,
                                               container=bytearray
                                               )

    path = F"{folder_path}\\{filename}"
    with open(temp_path, 'wb') as f:
        f.write(fetch_image_data)
    VNA.write(":MMEM:DEL 'TESTZ.PNG'")
    VNA.write("*CLS")
    ImageFilter.Filter(temp_path, True)
    shutil.copy2(temp_path, path)  # 20230530 - BS - \\silvertip.local\data\Production doesn't like file manipulation
    logger.debug(F'Saved screenshot: {filename}')


def VNA_Save_Data(productNumber, tag, folder_path, band):
    # Setup filename and folder path
    filename = productNumber + '_Band' + str(band) + '_' + tag + '.CSV'
    VNA.write("MMEM:STOR:FDAT 'MyFile.csv'")
    fetch_data = VNA.query_binary_values('MMEMory:DATA? "MyFile.csv"', datatype='B', is_big_endian=False,
                                         container=bytearray)
    path = F"{folder_path}\\{filename}"
    with open(path, 'wb') as f:
        f.write(fetch_data)
    VNA.write(":MMEM:DEL 'MyFile.csv'")
    VNA.write("*CLS")
    logger.debug(F'Saved data: {filename}')


def Sleep(duration):
    # instead of time.sleep() everywhere, we can do useful functions in the meantime, such as record communications to .txt
    time1 = time.time()
    time2 = time.time()
    while (time2 - time1 < duration):
        if (coms):
            try:
                reply = coms.query('$getst,*')
                logger.debug(F"{timestamp()[0]} : {reply}")
            except Exception as e:
                logger.error("Failed to receive communication:" + str(e))
        time.sleep(int(AUTOST.get()) * 60)
        time2 = time.time()


def Wait(resource):
    # This function is used to wait for a resource to complete its operation
    # Poll the Event Status Register (ESR) every second to determine if the operation is complete.
    global GLOBALWAITTIME
    time.sleep(2)
    print("Writing OPC")
    resource.write('*OPC')
    logger.debug("Waiting on resource.")
    before = timestamp()[2]
    flag = False
    print("Polling ESR Register")
    try:
        while not flag:
            time.sleep(2)
            flag = bool(int(resource.query('*ESR?')))
    except KeyboardInterrupt:
        logger.error("Exit wait function.")
        pass
    print("Done Polling ESR")
    after = timestamp()[2]
    duration = after - before
    GLOBALWAITTIME += duration
    logger.debug(F"Waited {duration} on resource.")


def NFG_Sweep(NFG, settings, interested_freq):
    SA.write('*CLS')
    SA.write('INIT:CONT 0')
    SA.write('INITiate:RESTart')
    Wait(SA)

    # Fetch NF array and calculate the min and max NF over the interested frequencies
    NFG["NF"] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:NFIG?')
    # Not working???
    i = interested_freq["start"]
    interested_NF = []
    while i <= interested_freq["stop"]:
        interested_NF.append(NFG["NF"][i])
        i += 1
    NFG["min_NF"] = min(interested_NF)
    NFG["max_NF"] = max(interested_NF)

    # calculating the linear noise figure values (only for interested frequencies)
    i = interested_freq["start"]
    LinearNF = []
    while i <= interested_freq["stop"]:
        try:
            LinearNF.append(10 ** (NFG["NF"][i] / 10))
        except:
            LinearNF.append(0)
        i += 1

    # calculating the average noise figure by taking the average of the linear noise figure values and then using math to get the average noise figure (only for interested frequencies)
    i = 0
    sumLinearNF = 0
    while i < len(LinearNF):
        sumLinearNF += LinearNF[i]
        i += 1
    averageLinearNF = sumLinearNF / len(LinearNF)
    NFG["averageNF"] = 10 * math.log10(averageLinearNF)

    # Fetch Gain array and calculate the min and max Gain over the interested frequencies
    NFG["Gain"] = SA.query_ascii_values(':FETCH:NFIG:ARR:DATA:CORR:GAIN?')
    i = interested_freq["start"]
    interested_Gain = []
    while i <= interested_freq["stop"]:
        interested_Gain.append(NFG["Gain"][i])
        i += 1
    NFG["min_Gain"] = min(interested_Gain)
    NFG["max_Gain"] = max(interested_Gain)

    # calculating the linear gain values (only for interested frequencies)
    i = interested_freq["start"]
    LinearGain = []
    while i <= interested_freq["stop"]:
        try:
            LinearGain.append(10 ** (NFG["Gain"][i] / 10))
        except:
            LinearGain.append(0)
        i += 1

    # calculating the average gain by taking the average of the linear gain values and then using math to get the average gain (only for interested frequencies)
    i = 0
    sumLinearGain = 0
    while i < len(LinearGain):
        sumLinearGain += LinearGain[i]
        i += 1
    averageLinearGain = sumLinearGain / len(LinearGain)
    NFG["averageGain"] = 10 * math.log10(averageLinearGain)

    return NFG


def get_NFG(settings, name, COM, folder_path, band, coms):
    NFG = {
        'Frequency': [], 'NF': [], 'Gain': [],
        'Before_DUT_Loss_Table_Name': None, 'Before_DUT_Loss_Table': None,
        'After_DUT_Loss_Table_Name': None, 'After_DUT_Loss_Table': None,
        'averageNF': None, 'min_NF': None, 'max_NF': None,
        'averageGain': None, 'min_Gain': None, 'max_Gain': None,
        'DSAdB': None, 'DSAhex': None
    }

    logger.info(F'Start Band {band} NFG test')

    # set NFG Path
    SG.Output(False)
    Path.NFG()

    # Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['nf_gain_state_file'] + "'")
    logger.info(F"Load state: {settings['nf_gain_state_file']}")
    Wait(SA)

    # Load Limits
    upper, lower, gain = Get_Gain_Spec(settings['Gain_Spec'])
    logger.info(F"NF Limits:")
    logger.info(F"    Max: {settings['NF_Spec_Max']} dB")
    logger.info(F"Gain Limits:")
    logger.info(F"    Max: {upper} dB")
    logger.info(F"    Min: {lower} dB")
    limits = [
        {'number': 1, 'value': settings["NF_Spec_Max"]},
        {'number': 3, 'value': upper},
        {'number': 4, 'value': lower}
    ]
    start = settings['nf_gain_interested_start_Frequency']
    stop = settings['nf_gain_interested_stop_Frequency']

    for lim in limits:
        SA.write(F":CALC:LIM{lim['number']}:CLE")
        if (lim['value']):
            SA.write(F":CALCulate:LLINe{lim['number']}:DATA {start},{lim['value']},1,{stop},{lim['value']},1")

    # Aquire start and stop frequencies, and number of points
    freq_start = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STARt?')[0])
    freq_stop = float(SA.query_ascii_values(':SENSe:NFIGure:FREQuency:STOP?')[0])
    points = int(SA.query_ascii_values(':SENSe:NFIGure:SWEep:POINts?')[0])
    step = (freq_stop - freq_start) / (points - 1)

    # Saving frequency data
    NFG["Frequency"] = [(freq_start + step * x) for x in range(points)]

    # Finding the index of the interested start and stop frequencies
    i = 0
    flag = 0
    interested_freq = {}
    while i < len(NFG["Frequency"]):
        if (NFG["Frequency"][i] >= float(settings["nf_gain_interested_start_Frequency"])) and flag == 0:
            interested_freq["start"] = i
            flag = 1
        if (NFG["Frequency"][i] >= float(settings["nf_gain_interested_stop_Frequency"])):
            interested_freq["stop"] = i
            break
        i += 1

    # Load Loss Table
    SA.write('*CLS')
    NFG['Before_DUT_Loss_Table_Name'] = Load_Loss_Table('BEFore', settings['Before_DUT_Loss_Table_Name'])
    NFG['After_DUT_Loss_Table_Name'] = Load_Loss_Table('AFTer', settings['After_DUT_Loss_Table_Name'])

    # DSA Adjust, add something here to get the DSA if coms is available 
    if (coms and (('DSA' in settings['Note_4']) or Tune_DSA_EN.get())):
        NFG['DSAdB'], NFG['DSAhex'] = Tune_DSA(NFG, settings, band, interested_freq)
    elif (coms):
        band_offset = band - 1
        message = F'$setda,{band_offset},*'
        try:
            reply = coms.query(message)
            logger.debug(reply)
            for item in reply:
                if item.count('SETDAR'):
                    hexDSA = item.split(',')[-2]
                    setDSAHex = hexDSA
                    DSA = float(COMM.attenuation(int(settings["Atten_Type"]), hexDSA, False))
                    NFG['DSAdB'] = DSA
                    NFG['DSAhex'] = setDSAHex
            if (DSA == None):
                raise Exception
        except Exception as e:
            logger.error(e)
            logger.error(F'Failed to poll unit DSA for Band {band}')
            return None, None

    # Start NFG Sweep
    # Start NFG Sweep
    SA.write(':SENSe:NFIGure:AVERage:STATe ON')
    SA.write(":SENSe:NFIGure:AVERage:COUNt 10")
    logger.debug(F"NFIG Averaging ON: {bool(SA.query(':SENSe:NFIGure:AVERage:STATe?'))}")
    NFG = NFG_Sweep(NFG, settings, interested_freq)

    # Detect compensation type and store value[s]
    loss_after_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:AFTer:MODE?')
    loss_before_mode = SA.query(':SENSe:NFIGure:CORRection:LOSS:BEFore:MODE?')
    if "FIX" in loss_after_mode:
        NFG["After_DUT_Loss_Table"] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:VALue?')
    elif "TABL" in loss_after_mode:
        NFG["After_DUT_Loss_Table"] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:AFTer:TABLe:DATA?')
    else:
        pass
    if "FIX" in loss_before_mode:
        NFG["Before_DUT_Loss_Table"] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:VALue?')
    elif "TABL" in loss_before_mode:
        NFG["Before_DUT_Loss_Table"] = SA.query_ascii_values(':SENSe:NFIGure:CORRection:LOSS:BEFore:TABLe:DATA?')
    else:
        pass

    logger.info("-----------------------")
    logger.info(F"Band {band} Results:")
    logger.info("Noise Figure:")
    logger.info("    Min: %f" % round(NFG["min_NF"], 3))
    logger.info("    Max: %f" % round(NFG["max_NF"], 3))
    logger.info("    Average: %f" % round(NFG["averageNF"], 3))
    logger.info("Gain:")
    logger.info("    Min: %f" % round(NFG["min_Gain"], 3))
    logger.info("    Max: %f" % round(NFG["max_Gain"], 3))
    logger.info("    Average: %f" % round(NFG["averageGain"], 3))
    logger.info("Max Ripple:")
    ripple = ['10', '36', '40', '100', '120', '200', '250', '400', '500', '900', '1000','1400']
    for value in ripple:
        if (settings[F'Per_{value}M_Spec']):
            try:
                NFG[F"Ripple_{value}M"] = AmplitudeResponse(interested_freq["start"], interested_freq["stop"],
                                                            NFG["Frequency"], NFG["Gain"], int(value) * 1000000)
                NFG[F"Ripple_{value}M_max"] = max(NFG[F"Ripple_{value}M"])
                logger.info(F"    {value}MHz: {round(NFG[F'Ripple_{value}M_max'], 3)}")
            except:
                logger.warning(
                    F"Ripple for {value}MHz not applicable. Please review band start and stop frequency in the settings")
    logger.info("-----------------------")
    # logger.info(F"    10MHz: {round(NFG['amplitude_response_10MHz_max'],3)}")
    # logger.info(F"    120MHz: {round(NFG['amplitude_response_120MHz_max'],3)}")
    # logger.info(F"    500MHz: {round(NFG['amplitude_response_500MHz_max'],3)}")
    # logger.info(F"    1000MHz: {round(NFG['amplitude_response_1000MHz_max'],3)}")
    # logger.info("-----------------------")

    # Scale state file for screenshot
    logger.info('Auto scale windows')
    if (settings['Band_Switching'].count('BDC')):
        SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:PDIVision 1")
    SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:RLEVel " + str(round(NFG['averageNF'], 1)))
    SA.write("DISPlay:NFIGure:TRACe:GAIN:Y:SCALe:RLEVel " + str(round(NFG['averageGain'], 0)))
    SA.write("DISPlay:NFIGure:TRACe:NFIGure:Y:SCALe:RPOSition CENTer")
    SA.write("DISPlay:NFIGure:TRACe:GAIN:Y:SCALe:RPOSition CENTer")
    time.sleep(1.5)

    SA_Screenshot(name, 'NFG', band, folder_path)
    SA_Save_Data(name, 'NFG', band, folder_path)

    return NFG


def Tune_DSA(NFG, settings, band, interested_freq):
    logger.info(F'Start Band {band} DSA tune')

    attempt_limit = 4
    DSA_step = 0.5 / int(settings['Atten_Type'])  # version 1 = 0.5dB/step, version 2 = 0.25dB/step
    band_offset = band - 1
    DSA = None
    setDSAHex = None
    message = F'$setda,{band_offset},*'
    logger.debug(message)
    try:
        reply = coms.query(message)
        logger.debug(reply)
        for item in reply:
            if item.count('SETDAR'):
                hexDSA = item.split(',')[-2]
                setDSAHex = hexDSA
                DSA = float(COMM.attenuation(int(settings["Atten_Type"]), hexDSA, False))
        if (DSA == None):
            raise Exception
    except Exception as e:
        logger.error(e)
        logger.error(F'Failed to poll unit DSA for Band {band}')
        return None, None

        # Sweep NFG
    SA.write(":SENSe:NFIGure:AVERage:STATe 0")
    # SA.write(":SENSe:NFIGure:AVERage:COUNt 1")
    NFG = NFG_Sweep(NFG, settings, interested_freq)
    averageGain = NFG['averageGain']
    logger.debug(F'Current DSA: {DSA}dB')
    logger.debug(F'Average Gain: {averageGain}')

    attempts = 0
    target_gain = float(settings['Gain'])
    while (averageGain > target_gain + DSA_step or averageGain < target_gain - DSA_step):
        if (attempts > attempt_limit):
            logger.error(F"Could not change DSA to have gain of {target_gain}dBm")
            logger.error(F"Tuning attempts > {attempt_limit}")
            logger.error("Please review unit or unit gain settings")
            return DSA, setDSAHex
        logger.debug(F'Tuning output power to target {target_gain}dBm...')
        gainoffset = averageGain - target_gain
        DSAchange = round(gainoffset / DSA_step) * DSA_step
        logger.debug(F"Difference between Avereage Gain and Target Gain: {DSAchange}dB")
        # Updated to say that the DSA has been tuned to target gain
        if (DSAchange < DSA_step and DSAchange > -DSA_step):
            logger.debug(F'Unit has been tuned to within {target_gain} dB +- {DSA_step} dB')
            logger.debug(F'Current DSA: {DSA}dB')
            logger.debug(F'Average Gain: {averageGain}')
            break
        DSA += DSAchange
        if (DSA < 0): # Safety Check here
            logger.debug(F"Can't set DSA to negative value, setting to 0 db")
            DSA = 0
            attempts = attempt_limit
        elif (int(settings['Atten_Type']) == 1 and DSA > 31.5):
            logger.debug(F"Can't set DSA to above 31.5 dB, setting to 31.5 db")
            DSA = 31.5
            attempts = attempt_limit
        elif (int(settings['Atten_Type']) == 2 and DSA > 31.75):
            logger.debug(F"Can't set DSA to above 31.75 dB, setting to 31.75 db")
            DSA = 31.75
            attempts = attempt_limit
        logger.debug(F"New DSA Setting: {DSA}dB")
        setDSAHex = COMM.attenuation(int(settings["Atten_Type"]), float(DSA), True)
        message = F'$setda,{band_offset},{setDSAHex},*'
        logger.debug(message)
        try:
            reply = coms.query(message)
            logger.debug(reply)
        except Exception as e:
            logger.error(e)
            logger.error(F'Failed to poll unit DSA for Band {band}')
            return

        # Sweep NFG
        NFG = NFG_Sweep(NFG, settings, interested_freq)
        averageGain = NFG['averageGain']
        # Added more info for after the DSA is tuned
        logger.debug(F'Current DSA: {DSA}dB') 
        logger.debug(F'Average Gain: {averageGain}')
        attempts += 1

    if (attempts == 0 and averageGain < target_gain):
        logger.info(
            F"Unit average gain at lowest DSA setting cannot reach target gain. Cannot tune DSA to target gain {target_gain}dBm")

    logger.info(F"Average Gain is {averageGain}dBm. Stopping DSA adjustment")

    return DSA, setDSAHex


def Get_Gain_Spec(spec):
    if not spec:
        return None

    # Split spec string to list of numbers
    temp = ''
    numbers = []
    spec += ' '
    flag = False
    for character in spec:
        if character.isdigit() or character == '.':
            flag = True
            temp += character
        elif (flag):
            numbers.append(float(temp))
            temp = ''
            flag = False

    # Interpret list of numbers
    gain = None
    lower = None
    upper = None
    if (len(numbers) > 2):  # Specific Spec ex:60(+3,-1)
        gain = numbers[0]
        upper = gain + numbers[1]
        lower = gain - numbers[2]
    elif (len(numbers) > 1):
        if (numbers[1] > 20):  # Range Spec ex:55-65
            lower = numbers[0]
            upper = numbers[1]
            gain = (lower + upper) / 2
        else:  # General Spec ex:55 +/-1
            gain = numbers[0]
            lower = gain - numbers[1]
            upper = gain + numbers[1]
    elif (len(numbers) == 1 and 'Min' in spec):
        lower = numbers[0]
        gain = lower + 2
        upper = gain + 7
    else:  # No Spec
        gain = numbers[0]
        upper = gain +6
        lower = gain - 6

    return upper, lower, gain


def Load_Loss_Table(location, table_name):
    if not table_name:
        return None
    try:
        SA.write(F':MMEM:LOAD:LOSS {location}, "{State_path}{table_name}”')
        logger.info(F"Loaded Loss Table: {table_name}")
    except:
        table_name = F'Error: {location} Loss table {State_path}{table_name} not found'
        logger.error(table_name)

    time.sleep(0.1)

    return table_name


def AmplitudeResponse(index_start_freq, index_stop_freq, freq, gain, bandwidth):
    # Gather amplitude ripple per frequency step
    ripple = []
    temp_gain = []
    i = index_start_freq
    for values in gain[index_start_freq:index_stop_freq + 1]:
        temp_gain.append(values)
        if (freq[i] - freq[index_start_freq] >= bandwidth):
            ripple.append((max(temp_gain) - min(temp_gain)) / 2)
            del temp_gain[0]
        i += 1
    return ripple


def get_p1db(settings, data):
    logger.info(F'Start P1dB test')
    P1dB = {'Input': [], 'Output': [], 'P1dB': None, 'OIP3': None}
    powerMeterPowerMax = 14
    powerMeterFreq = int(settings['nf_gain_interested_start_Frequency'])
    gain = []
    running_average = []

    Path.P1dB()  ##set P1dB Path
    P1dB['P1dB'] = -1  # If -1 Test failed
    P1dB['OIP3'] = -1  # If -1 Test failed

    powerMeterMax = 25
    steps = 30
    fixed_offset = 0.5  # IP1dB = (Measured_OP1dB) - (NFG_Gain - shift). Gives us closer value to RF lab results (determined experimentally)

    PM.write(':INITiate1:CONTinuous %d' % (1))  # triggering the power meter to take measurements continuosly

    start_gain = float(settings['Gain'])
    stop_gain = float(settings['Gain'])
    mid_gain = float(settings['Gain'])
    if (data['NFG']['Gain']):  # If NFG data available, use specific gain at frequency point
        start_gain = float(data['NFG']['Gain'][0])
        stop_gain = float(data['NFG']['Gain'][-1])
        mid_gain = float(data['NFG']['Gain'][int(len(data['NFG']['Gain']) / 2)])

    start_freq = int(settings["nf_gain_interested_start_Frequency"])
    stop_freq = int(settings["nf_gain_interested_stop_Frequency"])
    mid_freq = (start_freq + stop_freq) / 2

    interested_freq = [(start_freq, start_gain), (mid_freq, mid_gain), (stop_freq, stop_gain)]

    results = {'Data': [], 'P1dB': None, 'OIP3': None, "IP1dB": None, "IIP3": None}
    spectype = [str(x) for x in settings["ir_p1db_spectype"].split(",")]

    for freq, gain in interested_freq:

        trace = {
            "Frequency": freq, 'Ave_Gain': None, 'NFG_Gain': None,
            'Input': [], 'Input_adjusted': [], 'Output': [], 'Gain_Compression': [],
            'P1dB': None, 'OIP3': None, "IP1dB": None, "IIP3": None
        }

        if 'LNA' in settings["Nickname"]:
            SG.SetFreq(freq)  # setting the signal generator frequency
        else:
            SG.SetFreq(float(settings["LO"]) + freq)
        PM.write(':SENSe:FREQuency:FIXed %G' % (freq))

        start_power = int(gain) * -1  # Start at about 0dBm @ Output of LNB
        input_power = start_power
        stop_power = start_power + steps  # End at 25dB higher than the initial input
        gain_list = []

        while input_power < stop_power:
            SG_power = input_power + float(settings["p1db_cable_loss"])
            if not SG_power_check(SG_power):
                logger.warning(
                    F"Break from P1dB measurements: Input power exceed unit safety. Input power = {SG_power}")
                break
            if (input_power == start_power):
                SG.Output(True)
            time.sleep(0.4)
            output_power = float(PM.query(':FETCh1?')) + float(settings["p1db_muxT_and_cable_loss"])  # fetch measurement, compensate for loss, and add it to P1dB["Output"] list
            if (output_power < -10):  # correct outliers
                if (trace["Output"] == []):
                    output_power = 0.1234
                else:
                    output_power = trace["Output"][-1] + 1
            gain_list.append(output_power - input_power)
            trace["Input"].append(input_power)
            trace["Output"].append(output_power)
            if (output_power >= powerMeterMax):
                logger.warning(
                    F"Break from P1dB measurements: Output power exceed power meter safety. Output power = {output_power}")
                break

            input_power += 1

        SG.Output(False)

        average_gain = np.average(gain_list[0:int(len(gain_list) / 2)])
        trace["Ave_Gain"] = average_gain
        trace['NFG_Gain'] = gain
        offset = gain - average_gain
        flag = True
        for i, v in enumerate(trace['Output']):
            input_adjusted = trace['Input'][i] - offset - fixed_offset
            trace["Input_adjusted"].append(input_adjusted)
            current_gain = trace['Output'][i] - input_adjusted
            difference = gain - current_gain  # 1 < difference < 2.
            trace['Gain_Compression'].append(current_gain - gain)
            overshoot = difference - 1  # 0 < overshoot < 1
            #print(overshoot)
            if (difference > 1 and flag):  # If the difference between the average gain and the current gain is greater than 1dB, 1dB compression point found.
                flag = False
                trace["P1dB"] = output_power - overshoot  # Record P1dB, adjusting for overshoot
                trace["OIP3"] = trace["P1dB"] + 10  # P1dB + 10 = OIP3
                trace["IP1dB"] = input_adjusted - overshoot  # Record IP1dB, adjusting for overshoot
                trace["IIP3"] = trace["IP1dB"] + 10  # P1dB + 10 = OIP3

                if ('min' in spectype[1] and (results['P1dB'] == None or trace["P1dB"] < results['P1dB'])):  # Record min value
                    results["P1dB"] = trace["P1dB"]
                    results["OIP3"] = trace["OIP3"]
                    results["IP1dB"] = trace["IP1dB"]
                    results["IIP3"] = trace["IIP3"]
                elif ('typ' in spectype[1] and freq == mid_freq):
                    results["P1dB"] = trace["P1dB"]
                    results["OIP3"] = trace["OIP3"]
                    results["IP1dB"] = trace["IP1dB"]
                    results["IIP3"] = trace["IIP3"]

        logger.debug(F'P1dB @ {float(freq / 1000000000)}GHz')
        logger.debug('  P1dB: ' + str(trace["P1dB"]))
        logger.debug('  OIP3: ' + str(trace["OIP3"]))
        if (settings['IP1dB_Spec']):
            logger.debug('  IP1dB: ' + str(trace["IP1dB"]))
            logger.debug('  IIP3: ' + str(trace["IIP3"]))

        results['Data'].append(trace)

    logger.info(F'Results:')
    logger.info(F'---------------:')
    logger.info('   P1dB: ' + str(results["P1dB"]))
    logger.info('   OIP3: ' + str(results["OIP3"]))
    if (settings['IP1dB_Spec']):
        logger.info('   IP1dB: ' + str(results["IP1dB"]))
        logger.info('   IIP3: ' + str(results["IIP3"]))

    return results


def get_phase(settings, name, folder_path, band):
    phase = {
        'RAW': [], 'SMOOTH': [],
        '10Hz': None, '100Hz': None, '1kHz': None,
        '10kHz': None, '100kHz': None, '1MHz': None,
        '10MHz': None, '100MHz': None}

    logger.info(F'Start Band {band} Phase Noise test')

    carrier = float(settings['nf_gain_interested_start_Frequency'])

    ##setting IPLS Path
    Path.IPLS()

    # Loading state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + str(settings['phase_noise_state_file']) + "'")
    logger.info(F"Load state: {settings['phase_noise_state_file']}")
    Wait(SA)

    time.sleep(1)

    # setting the signal generator frequency and power
    SG.SetFreq(float(settings['LO']) + carrier)

    power = ((float(settings['Gain'])) * -1) + float(settings['phase_noise_loss'])
    print(power)
    if not SG_power_check(power):
        return None
    SG.Output(True)
    time.sleep(1)

    powerIn = Locate_PN_carrier(settings)

    # Start Phase Sweep
    SA.write('SENSe:FREQuency:CARRier ' + str(carrier))
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write(':SENSe:FREQuency:CARRier:SEARch')
    time.sleep(70)

    # Gather all raw data from frequency plot
    phase['RAW'] = SA.query_ascii_values(':FETCH:LPlot3?')
    phase['SMOOTH'] = SA.query_ascii_values(':FETCH:LPlot4?') # Gather measured phase noise smoothed data over number of points from a log scale, unit:dBc/Hz
    print(phase['SMOOTH'])

    for i in range(1,13):
        if SA.query(f'CALC:LPL:MARK{i}:MODE?') == 'POS': # POS == on, OFF == off
            print("Marker")
            freq = SA.query(f'CALC:LPLot:MARK{i}:X?')
            power = SA.query(f'CALC:LPLot:MARK{i}:Y?')
            decade = str(quantiphy.Quantity(freq, 'Hz')).replace(" ", "") # Ex: 4E6 -> 4MHz
            phase[decade] = power
        else:
            try:
                phase['10Hz'] = phase['SMOOTH'][phase['SMOOTH'].index(10) + 1]
                phase['100Hz'] = phase['SMOOTH'][phase['SMOOTH'].index(100) + 1]
                phase['1kHz'] = phase['SMOOTH'][phase['SMOOTH'].index(1000) + 1]
                phase['10kHz'] = phase['SMOOTH'][phase['SMOOTH'].index(10000) + 1]
                phase['100kHz'] = phase['SMOOTH'][phase['SMOOTH'].index(100000) + 1]
                phase['1MHz'] = phase['SMOOTH'][phase['SMOOTH'].index(1000000) + 1]
            except:
                pass
            try:
                phase['10MHz'] = phase['SMOOTH'][phase['SMOOTH'].index(10000000) + 1]
                phase['100MHz'] = phase['SMOOTH'][phase['SMOOTH'].index(100000000) + 1]
            except:
                pass

    temp_values = SA.query(':CALC:LPL:TRAC2:FAIL?')
    test = int(temp_values[0])

    if test > 0:
        lim_pass = 'Fail'
    else:
        lim_pass = 'Pass'
    logger.info(F"Band {band} Phase Noise pass Limit: {lim_pass}")

    # print ("phase noise test result"+ str(lim_pass))
    if lim_pass == "Fail":
        SA_Screenshot(name, 'Phase_Noise_Fail', band, folder_path)
        SA_Save_Data(name, 'Phase_Noise_Fail', band, folder_path)
        logger.info(F"Retesting Phase Noise.")
        SA.write('SENSe:FREQuency:CARRier ' + str(carrier))
        SA.write("CALCulate:MARKer:TABLe:STATe 1")
        SA.write('INIT:CONT 0')
        SA.write('*CLS')
        SA.write(':SENSe:FREQuency:CARRier:SEARch')
        time.sleep(70)

        # Gather all raw data from frequency plot
        phase['RAW'] = SA.query_ascii_values(':FETCH:LPlot3?')
        phase['SMOOTH'] = SA.query_ascii_values(':FETCH:LPlot4?') # Gather measured phase noise smoothed data over number of points from a log scale, unit:dBc/Hz

        for i in range(1, 13):
            if SA.query(f'CALC:LPL:MARK{i}:MODE?') == 'POS':  # POS == on, OFF == off
                print("Marker")
                freq = SA.query(f'CALC:LPLot:MARK{i}:X?')
                power = SA.query(f'CALC:LPLot:MARK{i}:Y?')
                decade = str(quantiphy.Quantity(freq, 'Hz')).replace(" ", "")  # Ex: 4E6 -> 4MHz
                phase[decade] = power
            else:
                try:
                    phase['10Hz'] = phase['SMOOTH'][phase['SMOOTH'].index(10) + 1]
                    phase['100Hz'] = phase['SMOOTH'][phase['SMOOTH'].index(100) + 1]
                    phase['1kHz'] = phase['SMOOTH'][phase['SMOOTH'].index(1000) + 1]
                    phase['10kHz'] = phase['SMOOTH'][phase['SMOOTH'].index(10000) + 1]
                    phase['100kHz'] = phase['SMOOTH'][phase['SMOOTH'].index(100000) + 1]
                    phase['1MHz'] = phase['SMOOTH'][phase['SMOOTH'].index(1000000) + 1]
                except:
                    pass
                try:
                    phase['10MHz'] = phase['SMOOTH'][phase['SMOOTH'].index(10000000) + 1]
                    phase['100MHz'] = phase['SMOOTH'][phase['SMOOTH'].index(100000000) + 1]
                except:
                    pass

        temp_values = SA.query(':CALC:LPL:TRAC1:FAIL?')
        test = int(temp_values[0])

        if test > 0:
            lim_pass = 'Fail'
        else:
            lim_pass = 'Pass'

    SA_Screenshot(name, 'Phase_Noise_withTrace1', band, folder_path)
    SA_Save_Data(name, 'Phase_Noise_withTrace1', band, folder_path)
    SA.write(":TRACe1:LPLot:TYPE BLANk")
    time.sleep(2) 

    limitline = SA.query(":CALCulate:LPLot:LLINe1:DISPlay?")
    if limitline or limitline == 'ON':
        SA.write(":CALCulate:LPLot:LLINe1:DISPlay OFF")
        time.sleep(1)
        SA_Screenshot(name, 'Phase_Noise_withoutLimit', band, folder_path)
        SA_Save_Data(name, 'Phase_Noise_withoutLimit', band, folder_path)
        SA.write(":CALCulate:LPLot:LLINe1:DISPlay ON")
        time.sleep(1)

    SA.write(":TRACe1:LPLot:TYPE BLANk")
    time.sleep(1) 

    SA_Screenshot(name, 'Phase_Noise', band, folder_path)
    SA_Save_Data(name, 'Phase_Noise', band, folder_path)

    SG.Output(False)

    # logger.info(phase)
    logger.info(F"Band {band} Phase Noise pass Limit: {lim_pass}")

    return phase


def Locate_PN_carrier(settings):
    zeroDbTolerance = 1
    tuning_attempt_limit = 5
    inital_power_limit = 20
    pn_carrier_level = float(settings["pn_carrier_level"])
    powerIn = ((float(settings['Gain'])) * -1) + float(settings['phase_noise_loss'])

    SA.write(":SENSe:LPLot:AVERage:STATe 0")
    time.sleep(1)

    # Check carrier power
    # SA.write('SENSe:FREQuency:CARRier ' + str(carrier))
    SA.write("CALCulate:MARKer:TABLe:STATe 1")
    SA.write('INIT:CONT 0')
    SA.write('*CLS')
    SA.write(':SENSe:FREQuency:CARRier:SEARch')
    # Wait(SA)
    time.sleep(14)

    measurement = SA.query(':FETCh:LPLot1?')
    end = measurement.find(',')
    carrierp = float(measurement[0:end])
    logger.info(F"Carrier Power is {carrierp}dB ")

    # verifying if the measurement is close to 0dB
    if (abs(carrierp) > inital_power_limit):
        logger.error(F"Could not calibrate output signal power to {pn_carrier_level}dBm")
        logger.error(F"Initial output power > {inital_power_limit}dB from {pn_carrier_level}dBm")
        logger.error("Please review unit or unit gain settings")
        SA.write(":SENSe:LPLot:AVERage:STATe 1")
        SA.write(":SENSe:LPLot:AVERage:COUNt 5")
        return powerIn
    elif (abs(carrierp - pn_carrier_level) > zeroDbTolerance):
        logger.info(F'Output signal power not within 0dBm +/-{zeroDbTolerance}')
        logger.info(F'Adjusting Signal Generator input by {carrierp}dBm.')
        powerIn = powerIn - float(carrierp) * 1.05 + pn_carrier_level
        if not SG_power_check(powerIn):
            return powerIn
        time.sleep(1)

        SA.write(':SENSe:FREQuency:CARRier:SEARch')
        # Wait(SA)
        time.sleep(14)
        measurement = SA.query(':FETCh:LPLot1?')
        end = measurement.find(',')
        carrierp = float(measurement[0:end])

        # If the SA measurement is still not 0dBm, finely tune:
        attempts = 0
        while (abs(abs(carrierp) - pn_carrier_level) > zeroDbTolerance):
            logger.warning(F'Tuning output power to {pn_carrier_level}dBm...')
            if (carrierp - pn_carrier_level > zeroDbTolerance):
                powerIn = powerIn - 1
            elif (carrierp - pn_carrier_level < zeroDbTolerance):
                powerIn = powerIn + 1
            if (attempts > tuning_attempt_limit):
                logger.error(F"Could not calibrate output signal power to {pn_carrier_level}dBm")
                logger.error(F"Tuning attempts > {tuning_attempt_limit}")
                logger.error("Please review unit or unit gain settings")
                SA.write(":SENSe:LPLot:AVERage:STATe 1")
                SA.write(":SENSe:LPLot:AVERage:COUNt 5")
                return powerIn
            if not SG_power_check(powerIn):
                return powerIn

            SA.write(':SENSe:FREQuency:CARRier:SEARch')
            time.sleep(14)
            # Wait(SA)
            measurement = SA.query(':FETCh:LPLot1?')
            end = measurement.find(',')
            carrierp = float(measurement[0:end])

            time.sleep(2)
            attempts += 1

    SA.write(":SENSe:LPLot:AVERage:STATe 1")
    SA.write(":SENSe:LPLot:AVERage:COUNt 5")
    return powerIn


def get_LO_Lock(settings):
    logger.info("Start LO lock test")

    freq_threshold = 5  # Hz

    Path.IPLS()

    SA.write("*CLS")
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['lo_leakage_state_file'] + "'")
    logger.info(F"Load state: {settings['lo_leakage_state_file']}")
    Wait(SA)

    SA.write('SENSe:FREQuency:CENTer ' + str(settings['LO']))
    SA.write(':INITiate:IMMediate')
    # waiting for the sweep to complete
    time.sleep(10)

    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    freq = float(SA.query(':CALCulate:MARKer1:X?'))

    if (abs(int(settings['LO']) - int(freq)) > freq_threshold):
        logger.warning('LO lock NOT detected')
        LO_lock = False
    else:
        logger.info('LO lock detected')
        LO_lock = True

    return LO_lock


def get_image_rejection(settings, name, folder_path, band):
    logger.info(F'Start Band {band} Image Rejection test')
    Path.IPLS()
    start_freq = int(settings["nf_gain_interested_start_Frequency"])
    stop_freq = int(settings["nf_gain_interested_stop_Frequency"])
    mid_freq = (start_freq + stop_freq) / 2

    signal_freqs = [start_freq,mid_freq,stop_freq]

    SA.write("*CLS")
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['image_rejection_state_file'] + "'")
    logger.info(F"Load state: {settings['image_rejection_state_file']}")
    Wait(SA)

    SA.write(F"SENSe:BANDwidth:RESolution {int(settings['lo_bw'])} Hz")
    SA.write(F'SENSe:FREQuency:SPAN {int(settings["lo_span"])} Hz')
    time.sleep(3)

    imageRejection = []
    powerIn = []
    #setting the signal generator frequency
    SA.write(F":DISPlay:ANNotation:TITLe:DATA '{name} Band {band} Image Rejection Test'")
    for signal_freq in signal_freqs:
        SG.SetFreq(float(settings['LO']) + signal_freq*float(settings['USB_LSB'])) 
        SA.write('SENSe:FREQuency:CENTer ' + str(signal_freq))
        # Adjust SG so Output power is 0dBm
        powerIntemp,IFlvl=(Locate_0dBm_Output(settings, 0))
        powerIn.append(powerIntemp)
        SA_Screenshot(name,F'Image_Rejection_{signal_freq}_IF_{powerIntemp}_dB', band, folder_path)
        # setting the frequency on the signal generator to image rejection frequency to get the image rejection value
        SG.SetFreq(float(settings['LO']) - signal_freq)
        SA.write('SENSe:FREQuency:CENTer' + str(signal_freq))
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        time.sleep(1)
        SA.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(1)
        imageRejectiontemp = -float(SA.query(':CALCulate:MARKer1:Y?')) + IFlvl + float(settings['image_rejection_cable_loss'])
        time.sleep(1)
        #imageRejection += float(settings['image_rejection_cable_loss'])
        logger.info(F"Image Rejection at {signal_freq}: %f" %imageRejectiontemp)
        imageRejection.append(imageRejectiontemp)
        
        #Take screenshot
        SA_Screenshot(name,F'Image_Rejection_{signal_freq}_Image_{powerIntemp}_dB', band, folder_path)#,powerIntemp)
    
    spectype = [str(x) for x in settings["ir_p1db_spectype"].split(",")]

    if 'typ' in spectype[0]:
        image_rej = np.median(imageRejection)
    else:
        image_rej = min(imageRejection)

    #Turn off Signal Generator
    SG.Output(False)

    return powerIn,imageRejection,image_rej


def get_spurs(settings, name, folder_path, band, extra):
    logger.info(F'Start Band {band} In-Band Spur test')
    Path.IPLS()

    bound_width = 10000000
    signal_freq = int(settings["nf_gain_interested_start_Frequency"])

    SA.write("*CLS")
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings['in_band_spur_state_file'] + "'")
    logger.info(F"Load state: {settings['in_band_spur_state_file']}")
    Wait(SA)

    # Change bandwidth + bounds
    SA.write(F":SENSe:FREQuency:START {float(settings['nf_gain_interested_start_Frequency']) - bound_width}")
    SA.write(F":SENSe:FREQuency:STOP {float(settings['nf_gain_interested_stop_Frequency']) + bound_width}")
    SA.write(':CALCulate:MARKer:PEAK:THReshold %G' % (-float(settings['in_band_spur_limit'])))
    SA.write(':DISPlay:WINDow:TRACe:Y:DLINe %G' % (-float(settings['in_band_spur_limit']) + 10))
    SA.write(':CALCulate:MARKer:PEAK:TABLe:READout %s' % ('LTDLine'))
    SA.write(':CALCulate:MARKer:PEAK:TABLe:STATe %d' % (1))
    SG.SetFreq(float(settings['LO']) + signal_freq)

    time.sleep(3)
    # Adjust SG so Output power is 0dBm, Horizon wants it tested at output power 12 dBm
    if extra == True:
        powerIn,notused = Locate_0dBm_Output(settings, 12)
        SA.write(':CALCulate:MARKer:PEAK:THReshold -70')
        SA.write(':DISPlay:WINDow:TRACe:Y:DLINe -38')
        # SA.write(':CALCulate:MARKer:PEAK:SORT AMPLitude')
    else:
        powerIn,notused = Locate_0dBm_Output(settings, 0)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)

    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
    time.sleep(sweepTime)

    temp_values = SA.query(':TRACe:MATH:PEAK:POINts?')
    peak_points = int(temp_values[0])

    if peak_points > 0:
        result = 'Fail'
    else:
        result = 'Pass'

    if extra == True:
        IFLevel = float(SA.query(':CALCulate:MARKer1:Y?'))
        temp = SA.query(':FETCh:SANalyzer7?')
        response = temp.split(",")
        print(temp)
        print(response)
        print(float(response[1]))
        time.sleep(8)
        highest_spur = IFLevel - float(response[1])
        if highest_spur > float(settings['in_band_spur_limit']):
            result = 'Pass'
            logger.info(F'Highest Spur Detected: {highest_spur} dBc')

    # Take screenshot
    SA_Screenshot(name, 'In_Band_Spurs', band, folder_path)

    # Turn off Signal Generator
    SG.Output(False)

    logger.info(F'In-Band Spur test: {result}')

    SA.write(':CALCulate:MARKer:PEAK:TABLe:STATe %d' % (0))

    return powerIn, result


def Locate_0dBm_Output(settings, wanteddBm):
    zeroDbTolerance = 0.5
    tuning_attempt_limit = 5
    inital_power_limit = 24
    time_delay = float(SA.query(':SENSe:SWEep:TIME?'))

    # Set Initial Guess
    powerIn = (float(settings["Gain"])) * -1 + 10 + wanteddBm  # adjusted to account for Horizon desired output powers
    if not SG_power_check(powerIn):
        return powerIn,0
    SG.Output(True)

    # Sweep
    SA.write(':INITiate:IMMediate')
    time.sleep(time_delay)
    time.sleep(2)
    SA.write(':CALCulate:MARKer1:MAXimum')
    time.sleep(1)
    measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
    logger.info(F"Measurement is {measurement}dB away from {wanteddBm}dBm")

    # verifying if the measurement is close to 0dB
    if (abs(measurement) > inital_power_limit):
        logger.error("Could not calibrate output signal power to 0dBm")
        logger.error(F"Initial output power > {inital_power_limit}dB from 0dBm")
        logger.error("Please review unit or unit gain settings")
        return powerIn,measurement
    elif (abs(measurement - wanteddBm) > zeroDbTolerance):
        logger.info(F'Output signal power not within {wanteddBm}dBm +/-{zeroDbTolerance}')
        logger.info(F'Adjusting Signal Generator input by {abs(measurement)}dBm.')
        powerIn = powerIn - float(measurement) + wanteddBm
        if not SG_power_check(powerIn):
            return powerIn,measurement
        time.sleep(1)
        SA.write(':INITiate:IMMediate')
        time.sleep(time_delay)
        time.sleep(2)
        SA.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(1)
        measurement = float(SA.query(':CALCulate:MARKer1:Y?'))

        # If the SA measurement is still no 0dBm, finely tune:
        attempts = 0
        print("test")
        while (abs(abs(measurement) - wanteddBm) > zeroDbTolerance):
            logger.warning('Tuning output power to 0dBm...')
            if (measurement - wanteddBm > zeroDbTolerance):
                powerIn = powerIn - 1
            elif (measurement - wanteddBm < zeroDbTolerance):
                powerIn = powerIn + 1
            if (attempts > tuning_attempt_limit):
                logger.error("Could not calibrate output signal power to 0dBm")
                logger.error(F"Tuning attempts > {tuning_attempt_limit}")
                logger.error("Please review unit or unit gain settings")
                return powerIn,measurement
            if not SG_power_check(powerIn):
                return powerIn,measurement
            time.sleep(1)
            SA.write(':INITiate:IMMediate')
            time.sleep(time_delay)
            time.sleep(2)
            SA.write(':CALCulate:MARKer1:MAXimum')
            time.sleep(1)
            measurement = float(SA.query(':CALCulate:MARKer1:Y?'))
            time.sleep(2)
            attempts += 1

    return powerIn,measurement


def get_lo_leakage(settings, name, folder_path, band, test, harmonic_test):
    resolution_bandwidth = 3
    span = 500
    freq_threshold = 20
    ref_lvl = -50

    LO_lock = None
    LO_Leak = None
    maxharmonic = 0

    logger.info(F'Start Band {band} {test} LO Leakage test')
    if (test == 'Input'):
        Path.Input_Leakage()
    else:
        Path.IPLS()

    cable_loss_list = [float(x) for x in settings[F"lo_leakage_{test.lower()}_cable_loss"].split(",")]

    # Load state file
    SA.write("*CLS")

    SA.write("MMEM:LOAD:STAT 1,'" + State_path + settings["lo_leakage_state_file"] + "'")
    logger.debug(F"Load state: {settings['lo_leakage_state_file']}")
    Wait(SA)
    # SA.write(F":DISPlay:ANNotation:TITLe:DATA '{name} Band {band} {test} LO Leakage Test'")
    ##    SA.write(F"DISPlay:WINDow1:TRACe:Y:SCALe:RLEVel {ref_lvl} dbm")
    SA.write(F"SENSe:BANDwidth:RESolution {int(settings['lo_bw'])} Hz")
    SA.write(F'SENSe:FREQuency:SPAN {int(settings["lo_span"])} Hz')
    SA.write('INITiate:CONTinuous 1')
    time.sleep(0.5)

    harmonics = [1]
    if (harmonic_test):
        harmonics += [2, 4]
    result = []
    if (len(harmonics) != len(cable_loss_list)):
        i = 0
        while i < (len(harmonics) - len(cable_loss_list)):
            cable_loss_list.append(0)
    for index, harmonic in enumerate(harmonics):
        center_freq = int(settings['LO']) / harmonic
        SA.write('SENSe:FREQuency:CENTer ' + str(center_freq))
        time.sleep(5)
        sweepTime = float(SA.query(':SENSe:SWEep:TIME?'))*3
        time.sleep(sweepTime)
        SA.write(':CALCulate:MARKer1:MAXimum')
        time.sleep(1)
        output_freq = float(SA.query(':CALCulate:MARKer1:X?'))
        output = SA.query(':CALCulate:MARKer1:Y?')
        corrected_result = float(output) + cable_loss_list[index]
        if corrected_result > maxharmonic or maxharmonic == 0:
            maxharmonic = corrected_result
        d = {"Harmonic": harmonic, "Frequency": output_freq, "Power": corrected_result}
        result.append(d)

        if (harmonic == 1 and test == 'Output'):
            if (abs(int(settings['LO']) - int(output_freq)) < freq_threshold):
                LO_lock = True
            else:
                LO_lock = False

            logger.info(F'LO Lock: {LO_lock}')

        logger.debug(F"LO Leakage {test}: {d}")

        if (harmonic > 1):
            SA_Screenshot(name, F'LO_Leakage_{test}_{harmonic}_harmonic', band, folder_path)
        else:
            LO_Leak = corrected_result
            SA_Screenshot(name, F'LO_Leakage_{test}', band, folder_path)

        time.sleep(5)

    SA.write('INITiate:CONTinuous 0')

    if (test == 'Input'):
        logger.info(F"LO Leakage {test}: {LO_Leak}")
        return LO_Leak, result

    else:
        return LO_lock, LO_Leak, result, maxharmonic


def get_Current():
    print("Getting current")
    if Resources['AMM']:
        current = AMM.query('READ?')  # getting the dc current measurement
        current = round(abs(float(current))*1000, 3)  # converting the measurement reading from A to mA
    elif Resources['PS']:
        current = float(PS.query('FETC:CURR? CH2'))
        current = round(abs(float(current[0] * 1000)), 1)  # converting the measurement reading from A to mA
    logger.info("Measured Current: %f" % current)

    return current


def get_Voltage():
    voltage = float(DMM.query("READ?"))
    logger.info("Measured Voltage: %f" % voltage)

    return voltage


def get_vswr(port, settings, name, folder_path, band):
    logger.info(F"starting {port} test")

    ##setting Path
    Path.S11_S22()

    VNA.write('*CLS')
    VNA.write(F"MMEM:LOAD '{settings[F'{port}State']}'")
    logger.info(F"Load state: {settings[F'{port}State']}")
    Wait(VNA)
    VNA.write('INITiate:CONTinuous 1')
    if port == 'S11':
        time.sleep(100)
    else: 
        time.sleep(10)
    VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    temp_values = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    S = temp_values[0]
    logger.info(F"{port}: %f" % S)

    time.sleep(4)  # Wait for notification to disappear

    VNA_Screenshot(name, port, folder_path, band)
    VNA_Save_Data(name, port, folder_path, band)

    VNA.write('INITiate:CONTinuous 0')

    return S


def get_V_Switch(settings, data):
    Vsw = {'Enable': False, 'V_Switch_Up': None, 'V_Switch_Down': None}

    Path.IPLS()

    carrier = float(settings['nf_gain_interested_start_Frequency'])
    bound_width = 50000000
    threshold = -60

    # Check if Voltage switching enabled
    if (bool(settings['Vsw_EN']) == False):
        logger.info('Voltage switching test not enabled')
        return Vsw
    else:
        Vsw['Enable'] = True
        logger.info('Voltage switching enabled. Begin test.')

    # Load state file
    SA.write('*CLS')
    SA.write("MMEM:LOAD:STAT 1,'" + State_path + str(settings['image_rejection_state_file']) + "'")
    logger.info(F"Load state: {settings['image_rejection_state_file']}")
    Wait(SA)

    # Setup SG and SA with frequency and power level
    SA.write('SENSe:FREQuency:CENTer ' + str(carrier))
    SA.write(F":SENSe:FREQuency:START {float(settings['nf_gain_interested_start_Frequency']) - bound_width}")
    SA.write(F":SENSe:FREQuency:STOP {float(settings['nf_gain_interested_stop_Frequency']) + bound_width}")
    SA.write('SENSe:BANDwidth:RESolution 10 ')
    SG.SetFreq(float(settings["LO"]) + carrier)
    powerIn = (float(settings["Gain"]) * -1) + float(settings["image_rejection_cable_loss"])
    if not SG_power_check(powerIn):
        return None, None
    SG.Output(True)

    powerIn,notused = Locate_0dBm_Output(settings, 0)

    sweepTime = float(SA.query(':SENSe:SWEep:TIME?')) * 2
    SA.write(':INITiate:IMMediate')
    time.sleep(sweepTime)

    logger.info("Beginning voltage up test:" + str(settings['V_Switch_Up']))
    # Voltage switch Up Test
    # sweep +/-1V around switch up point
    if (bool(settings['V_Switch_Up']) == True):
        for voltage in np.arange(float(settings['V_Switch_Up']) - 1, float(settings['V_Switch_Up']) + 1, 0.1):
            PS.write('APPL CH2, ' + str(voltage) + ' , 0.9')
            SA.write(':INITiate:IMMediate')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if (temp < threshold):
                Vsw['V_Switch_Up'] = round(voltage, 3)
                logger.info('Voltage switch up: ' + str(Vsw['V_Switch_Up']))
                break
    else:
        logger.info('Voltage up test not enabled')

    logger.info("Beginning voltage down test:" + str(settings['V_Switch_Down']))
    # Voltage switch Down Test
    # sweep +/-1V around switch down point
    if (bool(settings['V_Switch_Down']) == True):
        for voltage in np.arange(float(settings['V_Switch_Down']) + 1, float(settings['V_Switch_Down']) - 1, -0.1):
            PS.write('APPL CH2, ' + str(voltage) + ' , 0.9')
            SA.write(':INITiate:IMMediate')
            time.sleep(sweepTime)
            SA.write(':CALCulate:MARKer1:MAXimum')
            temp = float(SA.query(':CALCulate:MARKer1:Y?'))
            if (temp < threshold):
                Vsw['V_Switch_Down'] = round(voltage, 3)
                logger.info('Voltage switch down: ' + str(Vsw['V_Switch_Down']))
                break
    else:
        logger.info('Voltage down test not enabled')

    SG.Output(False)

    Pwr_on(settings['Voltage'], data)
    logger.info(Vsw)
    return Vsw


def get_volt_curr(voltage):  # added function to set desired voltage at the input of unit for current measurement
    logger.info(F"Increasing Voltage to Settings Value of {voltage}...")
    time.sleep(1)
    volt_init = float(DMM.query("READ?"))
    volt_targ = voltage

    volt_temp = volt_init
    volt_update = volt_targ
    while (volt_temp < volt_targ):
        volt_diff = volt_targ - volt_temp
        if volt_diff <= 0:
            break

        volt_update = volt_update + volt_diff / 1.25
        # logger.info(F"Adjusting Voltage by {volt_diff/2}...")
        PS.write('APPL CH2, ' + str(volt_update) + ' , 0.9')
        time.sleep(3)
        volt_temp = float(DMM.query("READ?"))


    time.sleep(1)
    volt_DMM = float(DMM.query("READ?"))
    logger.info(F'Voltage is now {volt_DMM}')
    volt_PS = float(PS.query('FETC:VOLT? CH2'))   
    return volt_DMM, volt_PS

def SG_power_check(power):
    Max_Power = -5
    if (power > Max_Power):
        logger.error("ERROR: SIGNAL GENERATOR MAXIMUM OUTPUT POWER REACHED")
        return False

    # SG.write(F":CH1:PWR:{power}dBm")
    SG.SetPower(power)

    return True


class SigGen:
    def __init__(self, root_manager, address):
        self.VISA = address
        self.INSTR = root_manager.open_resource(self.VISA)
        self.IDN = self.GetIDN()
        self.INSTR.timeout = 30000

        if (self.IDN.count("Agilent")):
            self.name = 'KS'
        elif (self.IDN.count("ANRITSU")):
            self.name = 'AN'
        elif (self.IDN.count("Holzworth")):
            self.name = 'HW'
            self.INSTR.write_termination = '\n'
            self.INSTR.read_termination = '\n'
            self.INSTR.timeout = 10000
            self.INSTR.write(':REF:EXT:10MHz')
            self.INSTR.write(":CH2:PWR:20dBm")

    def GetIDN(self):
        return self.INSTR.query('*IDN?').split('\n')[0]

    def SetFreq(self, freq):
        if (self.name == 'KS'):
            self.INSTR.write(F':SOURce:FREQuency:FIXed {freq}')
        elif (self.name == 'AN'):
            self.INSTR.write(F'F1 {freq} HZ')
        elif (self.name == 'HW'):
            self.INSTR.write(F":CH1:FREQ:{freq}Hz")
            self.INSTR.write(F":CH2:FREQ:{freq}Hz")

    def SetPower(self, power):
        if (self.name == 'KS'):
            self.INSTR.write(F":SOURce:POWer:LEVel:IMMediate:AMPLitude {power}")
        elif (self.name == 'AN'):
            self.INSTR.write(F'L1 {power} DM')
        elif (self.name == 'HW'):
            self.INSTR.write(F":CH1:PWR:{power}dBm")

    def Output(self, state):
        if (self.name == 'KS'):
            self.INSTR.write(F':OUTPut:STATe {int(state)}')
        elif (self.name == 'AN'):
            self.INSTR.write(F'RF{int(state)}')
        elif (self.name == 'HW'):
            if (state):
                self.INSTR.write(":CH1:PWR:RF:ON")
                self.INSTR.write(":CH2:PWR:RF:ON")
            else:
                self.INSTR.write(":CH1:PWR:RF:OFF")
                self.INSTR.write(":CH2:PWR:RF:OFF")


def Equipment_Init():
    global rm, SA, SG, PS, PM, DMM, AMM, VNA, TChamber, OSC, DC, Resources
    Resources = {}
    error_str = ' Communication Error. ATE functionality may be limited.\
    Press Enter to acknowledge and continue without resource.'
    rm = visa.ResourceManager()
    try:
        SA = rm.open_resource(SA_VISA)
        SA.timeout = 20000
        Resources['SA'] = SA.query('*IDN?').strip('\n')
    except:
        input('Spectrum Analyzer' + error_str)
        Resources['SA'] = None
    try:
        SG = SigGen(rm, SG_VISA)
        SG.Output(False)
        print(True)
        Resources["SG"] = SG.GetIDN()
    except Exception as e:
        print(e)
        input('Signal Generator' + error_str)
        Resources["SG"] = None
    try:
        PS = rm.open_resource(PS_VISA)
        PS.timeout = 10000
        Resources['PS'] = PS.query('*IDN?').strip('\n')
    except:
        input('Power Supply' + error_str)
        Resources['PS'] = None
    try:
        PM = rm.open_resource(PM_VISA)
        PM.timeout = 10000
        Resources['PM'] = PM.query('*IDN?').strip('\n')
    except:
        input('Power Meter' + error_str)
        Resources['PM'] = None
    try:
        DMM = rm.open_resource(DMM_VISA)
        DMM.timeout = 10000
        Resources['DMM'] = DMM.query('*IDN?').strip('\n')
    except:
        input('Digital Multimeter' + error_str)
        Resources['DMM'] = None
    try:
        AMM = rm.open_resource(AMM_VISA)
        AMM.timeout = 10000
        #Resources['AMM'] = 'HP 3478A Ammeter 2301A10048'
        Resources['AMM'] = AMM.query('*IDN?').strip('\n')
        #AMM.write('OUTPUT 723:H5')
        #AMM.write('OUTPUT 723:T1')
    except:
        input('Ammeter' + error_str)
        Resources['AMM'] = None
    try:
        Resources['VNA'] = None
        if VNA_VISA:
            VNA = rm.open_resource(VNA_VISA)
            VNA.timeout = 10000
            Resources['VNA'] = VNA.query('*IDN?').strip('\n')
    except:
        input('VNA' + error_str)
        Resources['VNA'] = None
    try:
        DC = dccontrol.Client(2, DC_VISA)
        Resources['DC'] = 'Orbtial Research Custom DC Control Panel'
        DC.close()
    except:
        input('DC Control Panel' + error_str)
        Resources['DC'] = None
    try:
        Path.Init(RCM_216_IP)
        messages = ['MN?', 'SN?', 'FIRMWARE?']
        Resources['Switch'] = Path.COAX_SW('MN?')
    except Exception:
        input('Coax Switch' + error_str)
        Resources['Switch'] = None
    try:
        TChamber = Thermo.Init(rm, TChamber_VISA)
        Resources['TChamber'] = Thermo.ReadWrite(TChamber, '*IDN?') + ' ' + Thermo.ReadWrite(TChamber, 'VRSN?')
    except Exception as e:
        print(e)
        input('Temperature Chamber' + error_str)
        Resources['TChamber'] = None
    try:
        Resources["OSC"] = None
        if OSC_VISA:
            OSC = rm.open_resource(OSC_VISA)
            OSC.timeout = 10000
            Resources["OSC"] = OSC.query('*IDN?').strip('\n')
    except:
        input('Oscilloscope' + error_str)
        Resources["OSC"] = None

    print("Equipment List:")
    print("----------------------")
    for keys, values in Resources.items():
        print(str(keys) + ' : ' + str(values))
    print("----------------------")
    print('\n' + 'Launching ATE2 GUI')


def GUI(settings_master_list):
    master = tk.Tk()  ## Initialize Menu variable
    master.title(os.path.basename(__file__))

    # Tkinter Icon
    enocdedImage = 'iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAADs4SURBVHhe7X0HgFvFtba3uGLihjHGBowNhGZaQjGEUAKhGBywQw31kVCMIZQXAo/QbCA/LaHbuJf1uuLeO+69997W3pW0klZdW3z+883ea2Qxu95y79WVNN97HwZnV3M1d853zsycOVOHFBQU0hZKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABTSBseYZfwPnaUaS2JZZgBjPk9vA+3x/wvaCUoAFNICUbbAwuJjdDBURrsCpbS5qJTWeEpoaWEJzXeW0Kz8Ypp1hHk4SrMOxvGAjJGf/z32Zw8x8RlHi2muo5iWuEpEOzv8pZQXLiMvP0PZMfvIgBIAhaQHzCneq8ezsLiM1ntLaOrRKOUcDNM3u8PUc1uQXt0QpKdWB6nrUj91XVBEXecwp3tO5NSTMPZnZzHxGQuL6JGVfnppfZA+2BqkgfvDNLsgStt9JRQuLfvF8+kRgtVQAqCQ1Aix9TgiZbQ3UEZbfKW0mr3tSva6K9n7rmQvvJK9Ov6ceShCfXaG6P/WB+jZVT7qtthHN833UseZHmo92UMNfyykhiNc1HCokxoOdFDDATHsJ2PBz/+u/xx+bzBzGH/GKBc1m+imC1kUbprnpSeX++j9jQEasjtEi49Ejj/XSkeJeOY9HJUURo9ZLgJKABRsDxgFqHv5WB7msHqhq5iGcUj+6Y4wvcYevTt73u6LfNR9dhF1n8WcW0RPz/FSl5le+v1UN/1mspsuYuM8e7ybThvLhj+mkOqMdFGdXGaOk+oMc1CdoTEcIiP/nP7v+s/h90B8Bn9W3dEsAvz5aOdybvPWaW7qNstLz831lj8bRwrdOfLAMw/az6LgLqEoTw+Ofz/te5sJJQAKtkaUrQDz5qNs6PDyO3juvsPLxLyaDWZmXpQ+3x6iv64M0B9+KqIO7HFbjyuk1sOd1Jq9c+u+TPbOrdgzN2ejbcKG2pg9dKMcF9UfzkbKhprJnr8OCBEA2XBPThYN2d+z5xefwZ+XwczmNtDOKdwe2j6Nn+GMQfxMeLb+TP5ZPPMTK/zUe1eYNruLxffC9zwQKiMfVhJNhBIABdsAQz2eCO8RIo/PK6avd0eo58Yg9VzDZIPpucxPPTiU78Le/Tc8Fz9rgpsasDeH4WWwsWWwgWX0d1LGQOZgJof3GWz8GWyMwtuP0Dw/OAoGzUQ0wF67xsTvg/g8TQjQTgaLgGibBUA8ywCQn4//Ds98BT//A/OK6F2eKvRczt9tfZD674uIxcrY/jAaSgAUEg4M7AjHvEXs7ZyRY1TAnq8gWM4VPJ/vtzdMf18XpFt/8tFFkzh8H8PkMPsiNuj2bMxnsLdvygbdUPfmulHrhq0bt07dSMe65fyxlpR9JtqLfYYYgcAzN8ktpDb8PS5kQbgIZDG7f5mPRh6IlvcF94mHIyGjAwIlAAoJRynPe3fzIF/gKKahPBfuuy1MfdkD9t0UpA/WBOjxRUV0Pc/fT+e5dCaMnL1oJnv3TA6jMwdpXlX36jAq3ch/ZI5j49MpM9ZE8Pjz8PPhWfmZ8fz4HuJ7gSxw7Se76dklPurLUU/fzSGxg+FmETASSgAULAOGLra84MUwtxfkvwiUlNH0gmJ6f0uQ7lzko04TPdSJDaITz6cvZ0/ZfqSTWrKR1BfhtGbo+p8j2YB0Dx/v2WXGZzfGRgd6VIDvxWzM3/F8/u9O/Gcn/pke6wIinwB9Vsx9Z4QUKAFQsAwI8/cGSmlZYQlN4jn9pP1RmrQnQj/uDtEb7Olv4zlws/GFlMVz5KzvCyjrBybPkzMHl8+VhWHASHTvHm9MdvLyNaUeGfD3xLpB5kAHZfXhfuhXQFfN8NB3W0Oiz35yFov+rC2UACiYAgxNjE94fJ1Odvnj8yL05mb29Dyfv3Oal+7kAf9HNvoreNC3YS9YDx6Q58J1YPDadtrxubzw7hplxpMK1EUM3xFRASId9AP3SQv+79/x/34nT4UQDbg4DEC/8v/XGEoAFEyBn+N8ePsV7O0XHSmmRYejNPFAhF5d46dr53gpmw06m+fv2ezps3VPP5QHOwwe22nCw8cZeip4+OowVgzGlq8TZPV1UDZHBBdP99CkA2FadLSY8iPIGKgZlAAo1BrwQCJxJYYw/hEHI/TK+gA9xaH9UzO99BCHsNdOclNrGLbu2YZo3p7DXTGfh9dLZQ9fY3KfIArCYudQjga4jx6a7qWnFvjEdAB9zv9fbSgBUKg1kKyCQzabikppk6uENjlLaNz+MHVf7acL2FM1H+qg5v0c1JS9fCM2+LowdggAkmbiF+2SZfEukeQ+Q4JRs/7crywIn24Jin7HNmF1oQRAodrAMBOE12Fu9ZVS7sEovbkpRG8u9dObPL9/er6Xrp3moVMRug5xUMYg5mD29PBg+sq97unTLbSvDfW+4j7MwBYoRwOdZ3pEv6/1lIr3UR0oAVCoFrCFB09ziD3+QX8pHWTjH82h/t9WBaj9NC+1Z8/UfqCT2nBY34T/PQueXizigWzw8R5fsWZE/6FPuX9bcl+j3wfuidDBQPXWA5QAKFQLMP4FPOf8eneYvlwfpC/XBunZxT76DRt/Js/fM9nTI0FHpLsKb8+saNtOsZYsF1SRQDTQQY9z5PXllpD2pqoGJQAKlQIJJ8HSY+Rjw/dFj9E2nud/vC1E18zz0lVs8FfluqgDs1kuD0Z4+xEc5scn5yhvbx5FJMB9nOukdhwFXDXJI94TkqyqAiUACpViX6CUZhcU08i9ERrJhv/VpiB1nl9E9Xn+jq27rL4FwvtkDNO8Peb1ak5vPVloRdIQvwe8J6zLVAVKABROALaTMM/X69tNPxql7jiIM81Dt7KBX8cGfhZCfXh79joiaQeGr3t7hKVKAKwnhBfvhKOAWzkiyDkYEUlCJ4MSAIXjQGopcs3nsMefcyhKcw5E6c21frp0hpeyhzop+9t8ysIBHOzd4yjtWB5wyT63h1idIFiaiMUSxlUZf/E72mclQgh5+pX9XT79bblf1CI8GZQApDng8WNTdQfsj9ADy/z0ABv9A1M8dM2EQmoBzw5PzyJwQsJO7GBPBsoM8gQB4O8jM3B8f5343jr1v4v/eV0IEiEAeB5+Tx35Od7nqQDeb2VQApDGgMc/ECyj1YUltDq/mGYciojadY154Dbm+WTjHwqo/mCeV2I1H1tOybiYh2cGZQaMxTOdiGj0RczKiFRlMf2p7Ge1n9E/+xeCEfN8ZpCNv94AB904zysiusqgBCDNAIcArwA62OOP4lD/pdUBemluEf0PE7XrxJweNe6QuCPm+NrAxeBKhFerDnWPLp5Te24YH74TG604d88eUpC/H+oJYPFMnMHHeQQQ/43tTJAF8DiHgPzzw5j4M/Z/w8/i9/TPwOfxf59QiQjRE4QBz4NI4fhzmkBury23ge3ayoIAJQBpBD3M381eYbenlBbyXP/FNQFqw2F+Gx6wZ/BgbYz8fDFIYwaqnT1/vIfXDR5E1ALDZ2LRsgELwK/YCFvwd2zFwtaG/zwbSTTMDvzdO7Chdhjmog4sEh345wT5dzrw7wvy53Xgz+3AfSL+1P8eP4Ofxe/h9/E5/Hnn8p9t+bNP57aacVuN+Wey0bf8O4J4Rv2Z9e8h+441IX92fRapexb5xMGsiqAEIA2A1w8GWAFmstF/vDVIH68I0P8u9dN10z3lNfRYAES6Ljw+wlYYvmxgJZqxHhPPCCNChALvDi+reXZRBxDfB7UB+fvVZyNrO95NV0310C0zPNRltpcem1dEz//ko38u8dFbbChv4U/uE8FlGpczV2hcCQboLY6YxJ/63+Nn8LP672qf99piHz05v4ju5bZumOahiyZ5qAk/s6gPyFGDeEY20gwhuvwdIASx37VW5M+CsHF/bfZVvBioBCDFgSQeV5QZKqNdvlJ6Z0uQLpnupkvYU53PHqs5G87xOSu8ku6NpIMqQdSfR/eSutdElMLPjYMxDXiwn8pG1ZSNqgWLWYsfmPgzh8lG15YN//dsjP8DI94QoP9uD1PO3jDNPBylda4obXaV/MzCCuiOo+xnYj5nhSNKo/eH6Qtu6+8caXVb4qeLZnjFuf4WPD3AMzbnP0/hqQW+w/H+HxPznWtDbucUFsORh8LaaPgllACkONZ6S2ngvggN3ByiLzcE6ba5XsrkgSbq6fFcVSTwwOPrK9eygZQI6p4eBoHn07y8fhxWEDX4WQCa889cPMFNt0zxUFc2sKdnMqdrfy4ooqfZO7+wLkAfbgvRkAMRmnI0Kq7sQrIMyo2fUIvfQPpZfHEN2SJuaywLzbd7IvT6hhA9zVHC07P5ufgZH5npoU6T3XQ6ohmIALZYURcBgozvHt8v1SJHGywAzy31aaPhl1ACkGLgcSfm+noiT182/hs41L2BjegaZmvMi2FIoL6dB48jHUAWMza8F/+O5yt/VhQLyWJvma0VxBDsx+TvhNt3HuKw+6NNQRrBXn1xXpQWs8GJP48W02JHsShDtoHFcDcbJA4yOSLHxH0DYe4s9JkZwHvAUekCbmt/sIy2seCscpfSYp6GLT7C5GeccjBC/7s2QJezEGSzGGf3RoEUFmcIgdh5qaUI8GdcwIKPZ5FBCUCKARdgLuHBPpU93dS9EfoLq38WG7moK8fGgzmymG/CuGINLtHU5/Pw9hAneEPMjXOd1IAHMMqFdfzRRTfxPP5u9vaCkz10N8/hX2Tv/j0b/k/OEtrPxo3BXsriJ/6Mo/DO3E/8h2mGHwu9HT0qOOF5+EFwZ+GkI1F6lacld88porv5+93CPAveH1EAT22EUNdYCArFjsQaj3w7UAlACiB2YG30logLLztP81BnNvDzMXAwkGBMMCoYmB5a2kIA8CxMfi5RBBPbazw1EYKFUmFYRZ/oods5ivn7Kj/14zB+CgubIEc3U/KKaSEbPi7QOMLhPBY6kwmYfiA6wJRkyiGOCPh7DdoRort56pLFQpjFEQG2Ko9P02ryzoY66KPt8lOCSgBSAGIAsREsYWPozS/6Mp771h1QQHW/x+ApnycLD1LbcNII6ot48PaIRHRvz8/YiNmWvX1HDluv5//teh7010/1UJclfvpga4jGs6fcFywVJxRjiV0u2D3/qyVe3WhAwPEd9O9zOFxK/+b3eP0Mr+iHc7E4iHeIvqrJusBIJ/2BBUUGJQBJCAxyPaTEXfOjD0XomRUBemaWl+5hz98Sho4FMswjMWhgbLKBYSWPz+mZPJix+CiSZ3hakok5L4vAWRPddA97+rfY0/ffFCrnzrCoNoS6d9v9peL2oFQHFg+xcNh/V5j6bwzRowuLyi9EwRQO71NETXH9WxlHu6gd97+s55QAJCECbAS7cR02z/W3uIrppTV+askvuiXP9ZqyUYktJX0BSd86ix8UVlBvG0YPap4MyTAteCC3H+6ki4cyWagunuSmzot89DF7+vmOKDmwUAdyWI9rs7GYhgW7JIvwawQIO5J3cC8i+qDPnhBdPNkt+qoRBAD9WJ2FWx4HSArCVerxUAKQJMCrE+R/7GRPiBTPt5f66e0lPurEXj8jl+fQSD3VF/kQKiZqjv8Lb4/kHCS+lD9fE/7fbpzpoR6Li+jD5X76cBlzY5B+4PnvPEeJWKU//n1jmE6I/d64NvzDTUHRV+04SkI/isiuqpEARHiwU5z7iIcSgCQBEnry2Bvm+cto4uEo/ZG95XlsWOexQTXDYMAcEV4fRpcojw+KqEMzfvZUuH77V/ycZ7AAnMkD90z+78tmeei19QGakRehXZ5S2uVm+krFAEXSUhguUOE4UIYN/YO+umOhj87kKK8eojz0sewdyDjURct5WhEPJQBJgi08AL7bE6Zv1gWpx1IftWVPoB88EbX39FVi2cu3gjB6PANSiRGFYOWaB+lp/Ew3TPXQ03O91IPn9z2W+On/Ngdp3JEoe/rSmLWMcirT/yXQJ3r//GdnWPTjmehz9upVXhTkMYJkpHgoAbApYl96kD0AFvqu5Rf/W/agF7C3bwiPD+rJPIn2+qM51GeDr8eevlG/Amr0HZP/GzsSSHSZwYNvVX4xreIQf723PBlHNidVqBwimYj78bo5XmrUp6A86pO9k3jyuPmWHUg8lADYFLELfWN3hKn7Ch/VZUPD3rg4ZoqwH4Zn9Txfb0+E+EyEokhWyXHSKRwBXD6+kO6f4qZuEzzUbb6PXue5/YS86PF77HQm65ZdogGHgP57e3OIuk3ylEdciLxONg7YWWBrMR5KAGwGGEX8Qt9tHOZdjAs2sPqLFw7Pr4d+VgmA3g7+lIT42YMK6KwpHnpxlZ+m7Q/T7H1Rmn2kPAUXeQpG3GSr8DNWuUto9v4oZWOrFwJcBQHotS2o/fbPUAJgM8gW+nCB5s8v02LDF+Q2ITjaNp4sxMfhIvzsaH7uUGlZ+VkEzVsp2zce6Ff08c0svqhzcNIFQX5v721RAmBLwD70+b5soU/6Qq2ixNvLQnyFxCB3d4TOmOAu3/6VvT+dLABYfI2HEgAbIHa+L13ok71Qiyjz9rIQXyExKCo+Rnct9tMpEIDKogAeR+9vVWsAtgG8viD/I3a+L13ok71QC4iwXubtaxPiu91uWrp0KeXm5tI333xD7733Hr344ov00EMP0R/+8Ae64oor6KKLLqILLriAOnToIIh/v/jii6ljx4504403Urdu3ah79+70/vvv0/fff08//vgjLVq0iPLz87VW0gfo+v77ItSOowCxIFvRbhCPpe94jMVDCUCCUFFij3ShzypKwv2aevtAIEBz586lL7/8kp5//nm66aabqFWrVlSnTh1TiTbuuOMO+uc//0kjRoygrVu3Umlp1W7JSVZgHHVe7Ct/fxUJAL/T8XkR7Td+hhKABKGixJ7yF6YZvqULffJwv6re3uv10tSpU+mNN96g6667jrKzs6UGmgiecsopdN9999GAAQNSMkrAK8EefyZuaqrojAA7l9Xu4vJfiIESAAuBo55BtqRKE3tkL88MGrC4t23bNurZsyddddVVlJGRITU+O/Kaa66hXr160YYNG7RvkvxA6bHmmAYgN0P2voc4KS/0y0hICYCFQA26sQcjlSf2yF6eCazp4p5u9JiPy4wr2QgxyMnJoWg0uXcyEJk9uSrAnh5OJG4cYVrAEQCcTzyUAJgMdLm+xTf0QEQU5aw0scciVmdxz+fziQW7VDF6Gc844wz64IMPknqKgMpIcCbldzbGvG8eW7gXQWL/SgDMRuwW31+X+8VZ+IQk9khC/pN5+507d9LLL79Mp556qtRoUpH16tWjZ555ho4cOaL1QvIAot0GTmVMnACMdlHbyR7tp06EEgATAKEV5H/EbvGJUtxaxpzVlIX8Mm9/jB96xowZdNddd0kNJF0I0fv000+Tbmpw+zyvdjI05v2PctGdC+WlwZUAmICKtvhOeCkWsyoLfPPnz6dOnTpJDSJdiRyEadOmaT1kf7y/LchTyjgB4Gkm7kSQQQmACah8i88iViPkX7Nmjdg7lxmAYjnvvffepFgfmO8sLr88Rc8HwMLyEIeoqSiDEgCDYKstPmZVQv4dO3bQAw88IB3wir/kmWeeKTIO7YwifsnidKCeDzCGxx//dxEGqARKAAyEXbb4wMpC/kgkIlJw69atKx3oihUzKyuLPv/8c7FWYkfgniNRL1Afb6NddPY4VASWP68SgFoAXhQeFZ4VSNgWXzVSeOHBLrzwQungVqw6u3btKrIf7YgsXKembwXy+Lttgbob0FTMPxhN6BZfVVN4n3vuOelgVqwZcWjJjtuFjcfzuNO3Agc76VOOSiuCEoBaAJdLHvKX0ktscB2GOOkUNsDyevy/NFIzebIVfuTot27dWjqIFWtH7BLk5eVpPW0PtJrM40LUiSykuv0ctKWo4sNQSgBqAdzn9tW6IHWa4RF3uKEo5i/SMM1gFVf4Mdd/9dVXpQNX0Tief/75dPjwYdHndkDbaZ5yAeBp6JU8PvQcDxmUANQACKv97P3/uzMsDB+0Mp23Kiv8e/bsEYd0ZANW0Xied955dOjQIW2EJBZtIAAYixyVvrbar/2tHEoAaoDDoTIaxcbfdYGv3PPDI1vh+TXGh/zxmDJlCjVt2lQ6UBXNI4qXHD16VHsLicNpU7QIgOf/k45WnsmoBKCagJdFUgUMv52+0h+femkGZWE/h/yxQOGLd955Rzo4Fa0hMikx9UokGmnHgnEACMeEK4MSgGpgxL4wNR3vpqZ9Y1f6raEs7I9FcXExPfLII9JBqWgtn3zyyYTmCWTC++fwOJnlpWhlCwAMJQCVILbr/rHcT4/N9VIGK2sG7tyXGKmZrCzsD4VC1LlzZ+lgtDNRNahNmzb029/+lm6//Xa6++676Z577hEHkfT6gKeffrr0d+1O1CpMBDBmEZUiAa2y7T8dSgCqgHc2hzjcd1JrhOFY7Kus+qqRrELYD6DenmwQ2oX169cXhTdeeOEF6tevH61evZocDgeVlWkZVCcBTuQdPHiQZs+eTR9++KHIy2/ZsqW0LbsQx4pxxsJq4Br1Ouygzs5x0j7JEe94KAGQINbzf7HST7fP9lLmEAdl4DJGK+b7Gk8W9judTuE9ZQMw0Tz33HPplVdeoXnz5onpidFAiI2SXm+99ZZoS/YMiWb79u0pGPzlIq2ZWOstoTrf59NrS3yVbv/pUAIQByzy6XiQw/4r2PBbIbd6BBs/PH9FRRcNpqwkdyyw74xS2bKBlyjCK8MgN2/ebOkcGG0tW7ZMrIHYrTbhv/71L+0prcHAfWFq9G0+ravg9F88lABUgOHbwtSeDTCrd375Nh/2VRNUtSce2G+2k9dDFeBhw4YlfPUbQBnwRx99lDIzM6XPajUxFdi1a5f2dObjqYVFdNPYQpEPUhUoAdAQO1u6eZ6XbmaDPzV2mw+nqxKQ0x8LhHSoz4fFMdlgs5q/+93vaPHixdrT2QuYHlx55ZXS57aaWKC1AhgfF/Dc//9tPHHcVAYlAHH4dFOQske5KLuPRam93EbGAMfxjMLKEnxKSkpssdp/ySWX0OTJk217JFYH1h5Q6NMOdxRMmjRJeyrzsDdQRu04ilwqqf9fEZQAxKDbTA/diCQKrPSjqILZq/0cZdTnF9bih3IB0I/wVoSXXnpJOrisYuPGjal3795Jd9MOVuOx3Sj7TlYRUzZs15qJsXlR6jLHS0cjVdtdAZQAaHhvQ4AaDXJQ/f6O8muwzS7ggQsc4ub8eyrJ2vrqq6+kA8sq3nbbbbR//37taZIPWDe57LLLpN/NKvbt21d7GnPw0fYQfcqs6vwfSHsBmHi0mF5c4KNbprJRDmXjN9vzYx2BxSVj6In37MP4KxKAiRMnJmx1G16/T58+tg/3q4KioiKRcCT7nlYQUyez+hGf+9amAC1yVW/LNe0F4I0NQTpziJOawvhxlh/Gb+Z5fiwossicye3o23yVYd26ddSoUSPpgDKbOOuOVfVUQjgcFjcMy76vFZwzZ472JMaitOwYfbQtKK4Lrw7SWgB6LffTzTO9IrU3QxRShOc3yfuLHQT2/MNcYr5/y7wiMeevDJgzJqp8FxYb7VryqrbAFeWJyqFAFqMZCJeW0dQj0Qoq/1WMtBQAR7CMXlkfoIvY6E/DSj/m/BYs+KFYI4y/U24hvbP15AtCf//736WDyGwieaWqabrJCqQWJ2JhEFM5M/ICAsVldDRc/XeWdgIwhef8/TaE6Hr2/Mi2E0k+Zi74xW3z4e8qW+zTgVBRNoDMJCreDh48WHuC1Mfy5csTskUIYTcaoZKyant/IG0EACm+xTxP6r4uQJ2GF1JLrMIjxdfM3P6Ybb7nVvlpk6ukSsbv8Xiobdu20sFjFpGxNmHCBO0J0geffPKJtD/MJO4XMHoxsLSGn5c2ArCYjW/ynghdgfp93xeIubipST4QmCFOOn9cIT013Us5B6ueJvv4449LB45ZbNCggbgPMB2BnIYbbrhB2i9mcu3atdoTJBYpLwBIjyzlqdGrG4N01zg3tYDHz+Gw3yzPH7PNl80hf5dFPlp0OFolzw+MHTtWOmDMIkLgmTNnaq2nJ3BDktVTgV69emmtJxYpLwBbfaXCAC/nOT9q9sMwTfX8Mdt8d4130392hYUAQYhOBpyRb9GihXTAmMVBgwZprac3rL4zAQeo7ICUFQDYG4zuSzbAp6Z76HQs9HFILkJzmeHWlpJtPkw51nur5vmBHj16SAeLWbT6qKqdgV0BrIPI+skMYjcAgp9opKwAoHIvFt26LPJTix/yKRs3pop9/jjDNYraNl+zEa7j23xYdKxqWqbVYSjOzqdCdp+RePnll6V9ZRaHDh2qtZw4pJwAYEhjXI/lsP+txT66YKKHMgagbr+Jxs/EliKIf68J7r//fukgMYO4LwAZcQonAtd8YStU1mdm8Omnn9ZaThxSTgAsv65L2+e/eopbCE5NgAs7ZQPEDDZs2JC2bdumtawQDxQmlfWbGUQ5t0Qj5QQg9rquzP4Oc8/0S/b5qwuE4VgQkg0QM5ioarXJgtzcXGm/mUGIcaIzLlNGAJDoo1/XdRXPw1vkOs29rkuyz1+Vlf54jB49Wjo4zCC8m5r3Vw4U8cQJSFn/mUEry4XJkDICcMJ1XX1NvK6rlvv8sUC5a1wnJRsYRhPbi3a4tioZYGUi1rhx47RWE4OUEAD9uq5bx7vNv66rFvv88UCNfNmgMIOo5KNQNQwZMkTah2awZ8+eWquJQUoIwNyjxdRzfZCy+xVQpri1xyTPX8t9/lggFL/00kulg8JoduzYUdQTVKgadu/eLe1HM/jAAw9orSYGSS0A8Lo4BPHgQh/dgFp+ONOPeb/MgGvLWu7zx8PK035z587VWlWoCiDOrVq1kval0cSNSYlEUgvApqJSWpMfpcZslA0GsOcfbYLxo04Af36zEYV0VY6LHlzhF4uNtQUKQ8gGhNG87777tBYVqoOuXbtK+9NonnfeeVqLiUFSCgDMD97/A/bCL8/3ir14UWDT8HP9CPmdlMnicuU0L321JkjDD1Z+33pVgJVfK2r8Iakl0avMyQqrjgljcTaRSEoBKIweo9089756ThG1HcieH2G/0RV9EPKPdFGTHKdIKPqf1QE65Cul/GqUXK4IVqWcPvbYY1qLCtWFVacycYNRInMBkk4A4P0XOIrp45UBajHOLbLwyo3fYAGIy/DLYc+PLfTaBv+oTHvqqadKB4PR3Lhxo9aqQnWxatUqaZ+aQRSASRSSSgBgfC72wJ9sC9KlPB+vh4o+SMgxOs1XkuF3MGSMSn/55ZfSQWA0cc++Qs2Bm5dl/WoG9+zZo7VqPZJKALDhNmhHmLrMQz0/HPAxYdGvggy/muzzxwOryyi1LRsERtOud/YlC/CurCrHvnr1aq1V65E0AoDttgD/43eTPHQWDB/e38g0XwMz/CoCavzLBoDRvP7667UWFWoDq0qyz58/X2vReiSNACwtLKEJe8OUNdhJmbhOC3N+kZxjEA3M8KsIuDtfNgCMJq7qVqg9rr76amn/Gs1ERmtJIQDw/qKm33g2UqT5Gnm23+AMv4qAkNKKvH8sMOJAi0LtYdUNQitXrtRatB62F4Awu+ClR4vFffl1e+cbn+xjcIZfRcANtbKXbzT/+te/ai0q1BZW3SOIqWGiYGsBQPiNq47/yl65pajp5zB23o9pBBt+Zl8HXTHNIy4Mmees3uWKVcWbb74pfflGc8mSJVqLCrWFVdmaW7Zs0Vq0HrYWgL2BMpqVF6WWPO+va3RNP2wdjnJRNnv/i4Y46ZlVfnFlWBGOFhoMhP/t27eXvnwj+etf/1q0pWAMHnzwQWk/G81EZmvaVgAwjr/nuXj3n4ooYzCy/WD8RgkAfw6LScZAJ53KkUWvZX4aeShSXk+wvHlDgW0e2Ys3mh9++KHWooIReOihh6T9bDT379+vtWg9bCsAR9gbd17ko3YI+3HG38hkH0QS7P3PYAG4dKaHdrpLKa8GFytWFW+88Yb0xRvNzZs3ay0qGAEkU8n62WiiGGmiYEsBgCl+uylI50xwi4M4hh7ywWfh2PBwJ/1tThG9ye1grcHMwBmhuezFG8lzzz1Xhf8G4+abb5b2tdEMBAJai9bDdgIQZev3l5TR1RymN0JdP3h/mSHXlGNcVK+/gxqxACzIi9I6j7mFMqDuspduNHHASMFY/OY3v5H2tZFs0qSJ1lpiYDsBWMsGOWZniLIGOigDd/jJjLimhJiw9792nJu6zisif2mZ4dt98RgxYoT0xRvN2bNnay0qGAUrIreLLrpIay0xsJUAYAH+v7tCdBvm6Ej4MfSIbyFlDHGIsmHvrg+K3YUSC0JmK+6c+9WvfiUKjCoYizZt2kj720jedtttWmuJga0EYH5BMd23sIiyv9cSfoxK9dUE5QyOAG7mz5zrKBZiYwWs8CJ//vOftdYUjALWU1C3X9bfRvKJJ57QWkwMbCMACMUfXean87FIZ3R1n1GFlNWngG6a46XhO8J0wKCjvSeDVfP/b7/9VmtRwSi4XOyAJH1tNHE+JJGwhQDA+NexV24y1k31cNDHYONHqu8VLCpvbg6SN3qMDCjpVyVYdcvM+vXrtRYVjMLatWulfW00Ey3eCRcA2GKw7Bi9ttBHGbkI1Y1c9ce830mZ/JlfrArQdJ5iWGT7As8++6z0pRtJzP9LS409uKRANGHCBGl/G820vxjEz5PxTe4SOpsNVZzxN8r7a6m+zfhz2/G8/0BRKbnY+1sJK4p/3HnnnVprCkbim2++kfa30UTpsUQioQIAc9xYVEI9V/rLE34Qrhu18MeflTHQQTdN9dA/1gRMT/aJh1Xzf5X+aw7+8Y9/SPvbaPr9fq3FxCChAgDvP/xAhC7Bfr/w/hJDrgmx6j/CRU1/cNCb6wK0gSMMqzF58mTpCzeaCxYs0FpUMBJWHARq166d1lrikFABmMVz8meX+Mrr+8FojfL+EBQWgMeneWnykajpyT4y9OrVS/rSjSRKSqviH+bgsssuk/a5kcRtzYlGwgQA+/Avrw/QBaK+HxuszJBrxELK7F9A2TwF+OlQ1LBqvtVFt27dpC/dSKJmnYLxwD2K9erVk/a5kXz99de1FhOHhAnAbPb+V83wsLHy3B/Hc6XGXE1iAZG9/6WjC+mOn3yG1/SrDqwo//Xwww9rrSkYie3bt0v722jaoXZjQgQAIfkDK/x0Gry/kdt+SPj5oYBeXhmg8YcTlxqLyz9kL9xo/vvf/9ZaVDASY8aMkfa30dy0aZPWYuKQEAFY7iqhX43nUB2FPoza9sO5gVwXXcuiMj6vmMKJmPhrWLZsmfSFG00sNCoYj7ffflva30ayfv36triy3XIBQEj+/Eo/ZYu5v1Hev1CcHMwc6qTeG4K03Z+Yeb+O/v37S1+60dy7d6/WooKRsKIQyG9/+1uttcTCcgHY7i2lM4TBsvEbte03ykWNWAB+zdHE0WAZhRLo/YHXXntN+tKNJG6tSeSlkqkKHAI688wzpX1uJO1SvdlSAYBZvr8uWG78htX2ZzEZ7KRzJxTSe2sCopZgonHHHXdIX7qRvPzyy7XWFIwE6vPJ+ttofvfdd1qLiYWlApAfKqMLx2Gl3kDvP6aQmg900J0Li2ibwZd51BRI8JC9dCPZpUsXrTUFIzF8+HBpfxvNpUuXai0mFpYKQJ/tYTFPN8z7YwFxsIO6TPfQFzvDCdvyiwXC8rp160pfupHs0aOH1qKCkXjhhRek/W0kMX2zSwEXywQgwvPya6d4jPP+yBoc7aL6vfOp784QbfXZw/vn5+dLX7rR/Oyzz7QWFYxEx44dpf1tJO10gMsyAZhwMEpZ8P64ftuIlF+kDuc46U8T3LSpqMQW3h+w6g6AUaNGaS0qGAWPx0MZGRnS/jaSdhJvSwQAi/J3zC4qz9E3Yt8fAsLTiKyBBTRjX4TcVlX4qAKsOkeOXAMFYzFt2jRpXxtN3BNpF1giAEtcJZSNWvyoymuE90fSD08lbmTvX1Jm7THfkwGru7KXbjQPHz6stahgFKw4AtysWTNbbd+aLgD4qs+uDZTX+UPYLjPo6nK4izKHuWjIjnB5IzYCarzJXrzRDIft992THSjRLetrI9m1a1etNXvAdAHAaby2k2G0LAAyY64u2ftn8VTiskkeckfslwjz+OOPS1+8kcQug7oFyFjs3r1b2tdG0y77/zpMF4DcQxGqj7Rfo274Ye9/ymgXfbI5ZKvQX8ctt9wiffFGsnnz5lprCkbhyy+/lPa10dy2bZvWoj1gqgAUs4P+22p/+d6/EYt/Y9xUd5iTLpnpob1+exbCtKKQxDnnnKO1pmAUcEGHrK+NJFKM7Ra5mSoAjugx+o3Y+0f4b4AAsPdvPqaQ/m9LyDbbfvGwohAo9qoVjAOOb1uRvPXYY49pLdoHpgoA7vlrBu+PW35kBl0dcgSRNRje30sbi+xbBvvss8+Wvnwjef3112utKRgB5FTI+tloDho0SGvRPjBVAD7ZEKQMXPRhhPcf6aKmI1z02sby67ztitNPP1368o3kjTfeqLWmYASsOP4LHjhwQGvRPjBNAJD8gyu+jVn848/hSOKCqR5anoAKv9UBLuqQvXwjec0112itKdQWR48epaysLGk/G8lLLrlEa9FeME0AfnIVU5ZR9f54ClEvxynyCRJ81P+ksKKY5BVXXKG1plBbfP7559I+NpqffPKJ1qK9YJoAPKMn/8gMujpE5mCui5qPKxSFRO0MZHjJXr7RTPSd8qkCrMhfeuml0j42kjhfYNfMTVMEANt/IvkHN/3IjLq6HOakPy4qSmidv6ogFApJB4DRRMVhhdoDOfmy/jWat99+u9ai/WCKAHiKj1Ej1PsbY8D8f3QhZQ530oD9Ee3T7Qtc8yQbAEazTZs2WosKtcHzzz8v7V+jaYfy3xXBFAFYXlhCmeKabwMEgIXkvEluOhq2X9pvPBBSWnGcFHvW6kbg2qGgoIAaNGgg7V8jecopp1AgENBatR9MEYB/bgiWz/9rm/3Hv496f2/z59l87e84mjRpIh0IRjMvL09rUaEmeOedd6T9ajSffPJJrUV7wnABwB79Bcj+Q+EPmVFXhxz+t+T5/xab1PqrCqxIBAKXL1+utahQXWCqhmO5sn41mnPnztVatScMFwDc+JuJsl9GHP1l439mgc/WiT/xsGJVGRw9erTWokJ1YdXBn7POOsv2pdsNF4C8YCnV4bC99nX/CsWtwUvy7b31F48bbrhBOhiMJvavFaoPFOO0KkpDbQi7w3ABmJgXpTpDOAKorQCMdtGZw522T/yJx1133SUdDEbzqaee0lpUqA6++OILaX+awa1bt2qt2heGC0B3JAAZceXXCBfdMserfWry4KGHHpIOBqOprgavPpxOp2WLtIgEkwGGC8AVs7zGJADx/P/drSHtU5MHVtSV01lYWKi1qlAVvPjii9J+NIOzZ8/WWrU3DBeAVhPYeGu7ADiW5/9DnbTAmVzzf2DAgAHSAWEGp0+frrWqcDJs2bLFkkM/IE5rJkvJNkMFoLjsWPmtv7Xd/x9dSG1ZRHwlSbYAwFiyZIl0UJhB7GUrnBwwxj/+8Y/SPjSD8+fP11q2PwwVAHe0rPzmn9qkAGPxkEXk4UW+pEn+iQXCctmgMIMoP6ZwcvTt21faf2YQNSGTCYYKAOr0lZf/roUAjGEOd9IPe+yf+18RrCgKonPz5s1aqwoyoNov0nFlfWcGFy5cqLWcHDBUAFa4isXiXa2mAPhd/ozVHnsX/qgMv//976WDwwy+/fbbWqsK8SgpKaFOnTpJ+80MorBossFQAZhyJCoKd0oNu6oc66K6OU4qtNF1X9XFs88+Kx0gZrB9+/ZJs+BkNT766CNpn5lFrP8kGwwVgMH7wrXPAeDpQ/sJ7qSc/+uwcicAnDVrltaygg5sw2VnZ0v7ywza6cbf6sBQAfh6FwsAdgFkhl1Vjiqkexb6tE9MThw6dEg6SMwiqgSrKOBnIAPPqoQfnStWrNBaTy4YKgD/3RmqvQDw7/9rc1D7xOTFxRdfLB0oZnHmzJlay+kNh8NB5557rrSPzGIyp2UbKgB99hgwBeDfzzmQvDsAOl555RXpYDGL1113XdpHAbgwFdGQrH/MIm77cbvd2hMkHwwVgJz9BgjAcCetKky+DMB4WHXXfCxzc3O11tMPkUjEsvr+sZwyZYr2BMkJQwVg/OFI7XcBhrkoL5T85a6CwaAlJcJj2aJFC8rPz9eeIH0Az9+5c2dpn5jJJ554QnuC5IWhArDAES3PBJQZdlU51EWRZKoAUgmsTD/ViePIdi9CYSR8Ph/deuut0r4wk6gpkMyhvw5DBWCnv6Q8Eaiml4EgCYgFhJJ6E/BnoGqPbPCYzc8++0x7gtQGbvW5+uqrpX1gJjMzM2nx4sXaUyQ3DBUAf0kZZeIm4JpWA0YKcS4EIDVQXFxMrVu3lg4iM4nKxD/++KP2FKmJVatWifLosu9vNnv27Kk9RfLDUAE4xv/XCDcB11gAOALIdWqflhp47733pIPIbNavX58WLVqkPUVqYejQoeL7yb632bz55ptTqiS7oQIAtJqkGbLMwE9G7SBQKgFXQll1Dj2eqHy7adMm7UmSH1jpf/XVV6Xf1Qqec845dOTIEe1pUgOGC8Cfl/u1rcAaiACOAvMUIlXWAHR069ZNOqCsIDLikul8ekXANV5WVVyWETss27dv154mdWC4AIw6FKE6Ax3lC3oyI6+MmgBgKpFKWLBggXRQWUXcJJSTk6M9TXIBVXzffffdhEVRYKNGjVL2HgbDBeBoqJTqD2YBqGlNgBwXOaOpd+3VvffeKx1cVvKNN94Qe+bJAhzoQdET2XexihCeqVOnak+UejBcAIIlx+h0hP81vRlouJOWu5M/EzAeu3btsjwxSEacUbD7wZV169YlJIdCxkGDBmlPlZowXACQw3PrT0XlCUE1uRtghJO+2ZM8Xqo6ePPNN6WDzGpiH/v1118XZbLtBBj+Y489ZskFq1Xhxx9/rD1Z6sJwAQC+3h2mDNwOXJPdgFEu6rrcr31SagFZa4nIC6iIKJWFMuaJTB9GrsTIkSMtu1GpquzRo0daHK4yRQD2BsuoNTICMQ2obhQwupBaT/FQcYpms2IPWzbgEsmGDRvS008/LY4Uo4yW2YDRz5kzh1566SVbCaJO9EW6XL9uigBgGtB9oa/8jsDqRgH88xkjXZQfTk0FQJ7+3XffLR14dmDLli3p+eefp7Fjx4rCJkYAorJx40YaOHAgPfroo9S0aVNp23Zgr1690upYtSkCAKwoKKaG/Qs4CqjmbgCyCIc6aU0KLgTqwNy7bdu20gFoN+K8+3333Scuuvz666+FMKD2HRY1Dxw4IBKdDh48SHv37qX169eLy0pQEu3DDz8UQoI6BYgwZJ9tJ2KBNlm3SmsD0wQAl3p2neYR+/qY10uNXUbtQNAnG5O/KlBlwGESK2vWKVZMZEwiVyMdYZoAALMORih7iJMyhjnYuKs6FeCfY8G4doKbkvBioGqhd+/e0gGpaB1RVTkVM/yqClMFoISn8TdN9VDjXI4CkB5c5QXBQsoa6KQFR1J3GqADYbJsYCqaT0xPUEMwnWGqAAA5uyN07iQ3ZfblKGA0e/dxMoOXcJiT/jLfJ6YSqQysiD/44IPSAapoHh955BEKhZLv9mmjYboAeKPH6M/L/HQJEoNQMRgiUJVIgKcBTYa5aL0reW8IqiqwSv7www9LB6qisTzttNNo1KhRWs8rmC4AcOBDD0TpgxV+ymSvnjFE3xo8yZoAtgP5Z19f6hPbiikeCAgRgFeSDVpFY4hTmQUFBVqPKwCmCwBwJFxG6wuLqR0b9em4PBRbg1WJBHJddE6uk/YVlVJRqq8IMiACf/nLX6SDV7HmVF6/YlgiADBdX0kZ/e/KAHWd5RW7AiISGHWSKIBFInOwgz5bFaBVnpKUjwIAZKC9/PLL0oGsWH0qr185LBEAoOTYMY4CSqj3jhA1HeqgU/uzCGBnoLJMQUQII5x0eY6LftgbJk/0WMovCuoYMWKEpddapxqV168aLBMAAMa7hj35Y/O8dM9UD2UjQQhTAggBpgTxAoAdAxaIrL4F9JdFRTRsd5i8qXpIQALccWf1FWPJzgYNGogDTi5X6hSXNROWCgDgYi++4EiUBu0KU+PxbsruXUCZgxzs6WMEIHarEFHAcBd1GOkSOQW7/CUiQShNAgHy+/0if1422BV/Jgp34Fp2pCYrVB2WCwAMFwa8K1hGXZb66Y9s5L9G/j+igBO2CjVBEGJQSJkDnZTN0ULfbUGaeaSYjqboYSEZcDhl3Lhxll96mQyE4eOGnh07dmi9pVAdWC4AOrCqP4kNeRxHAn9d5qOsURzqf5/Phu6gDFwvJqYETD0agDAMd9IfJrjp/gVF9JOzREwpUuQSoSoB5bxwyAY16mTGkE7E4Z3nnntOHEJSqDkSJgCw2zBbcJiFYNiBCF0zu4iuGeakCznU/xW2CRERwOixU6BHBKNcVL+fgxrw3/97c4hWHC2mTUWlFEqXlUENOKabrtOCVq1aiTm+CvWNQcIEIBYb2Ii/3x2h79cF6JXlfrqQ5/qZQ5yU2d9BmYOclIGpARYMQRQaYQG4fbqXXphbRB9tD1EeTwcQCaRDwlAs1q5dK+6mT9QlGVYR3w/p0ijOaUXBknSCLQQgwB4cc/qjgTKaeiRKD7EIXDCmkC5gY2/HbMbRQBaiAZ08FWg6yEFncDRw4zwvzTwcpR3uEtrpLyV38TFKn9WBcuBAC+rXJUuNgaqyU6dO1KdPn5S4hNOusIUAwGsL8j/2BEpp0P4Ivbs6QO8u89MLC4uo0zQPNURqcA5HA1gjGMCRQZ8Cyvwun87kqKDH/CL6YKmfPtwWpmUsBLhdGJ8lqH12OgDeccyYMeKe/GQowiFju3bt6O2331aLehbBFgIQiyBHAweCZbTNU0rbCktoWl6EeqzxU4fpHjqdjf10nhaAzVgIGjObMDsMKKALOSLoOMNL3+4Mi9ThAo4GCjiiKODIwstRAdYJ0mnBECfdEDK/8MILdNZZZ0mNzQ48//zz6ZlnnqEhQ4bQvn37tKdXsAq2EwDYqD6fBw+FS8VtQ69sCNKzS3z07KwienZ2EXWZ4aFrp7ip3UgXNerH0cD3+VRvqJMemualr5b46QeOHn5YE6QfdodpdkExbfeXpN1ioQ5sI6ImH64Nx1kDXLGVqGpEHTt2pBdffFFk6aXaPXvJCNsJQDxgtPs5IljFof0yRzEtO1LOvjtD9DJHBn+YWyQuIsFtRA1659MF3+bT774toJt4evD7IU66cY6X3tkcoklHIhwNlIpqw8hDgBZAYNJTEsov2sQiIi6+eOWVV6hz5850zTXXiBC8NinIzZs3pyuvvJL+9Kc/iTMNn3/+uZiW4DKSwsJCrXUFu8D2AgADhaHCYGO5qYjnu3lRMe9/bGWAusznqGCCm+7LcdH9g5yCfxpdSDfN9VL3tQH6YmeQph2N0gIWEaQj7/CV0uFQGRWxIpRhsUDhBAQCARGSw3BR6HPy5Mk0YcIEURQUnDhxIk2bNk1c34V6elu2bBFZiwrJBdsLQEXAav/eYCmt9ZTSXDbq6SwG0/dFaPrOME1nUZi+PUyTdoXoPzvC1GtrkN7YGKTXeRrx0roAfcZ/N+JglBY6S2gvUouhMAoKaYikFQCYrB4ZIKQXRHgfw1BpGa1ngRhzKEKfsiC8xgLwt9UBjhpCNHR/hOYVlNBunxIAhfRF0gpAVVDKoX1B5Bht43B/iauEZuYX06Qj5Z5/g7eU9gXKyB1VUwCF9EVKCwCAyCDMHj7A/+JjFvHUAf+OxUW2fRFBKCikK1JeAGIBW9epoKCQZgKgoKBwIpQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikLYj+Pwk7SiFpR3rfAAAAAElFTkSuQmCC'
    decodedImage = base64.b64decode(enocdedImage)
    img = tk.PhotoImage(data=decodedImage)
    master.wm_iconphoto(True, img)

    # Variable List
    global ModelDropdown, SNText, StockText, MnfText, ColorText, CxText, POText, YouText, USB_Port_Text, Attenuation, TemperatureDropdown, AUTOST, AUTOST_PERIOD_TEXT, IP_Text, IP_Port_Text, COMM_DISABLE, NFG_Only, No_NFG, PWR_ON, Tune_DSA_EN
    TemperatureProfiles = ['None']
    COMM_DISABLE = tk.IntVar()
    NFG_Only = tk.IntVar()
    PWR_ON = tk.IntVar()
    AUTOST = tk.IntVar()
    Tune_DSA_EN = tk.IntVar()
    No_NFG = tk.IntVar()
    atten = []
    atten.append('Disabled')

    for num in range(64):
        atten.append(num / 2)

    for dirpath, dirnames, filenames in os.walk(ATE2_PATH + '\\_Temperature Profiles'):
        TemperatureProfiles.extend(filenames)

    ModelOptions = Settings_List_Options(settings_master_list)

    # Model Frame
    ModelFrame = tk.LabelFrame(master, text="Settings Menu")
    ModelFrame.grid(row=1, column=1, columnspan=3, sticky='W', padx=2, pady=2)
    tk.Label(ModelFrame, text="Model: ").grid(row=1, column=1, sticky='W')
    ModelDropdown = ttk.Combobox(ModelFrame, value=ModelOptions)  ##Makes the menu
    ModelDropdown.set('None')  ## Set Default Value
    ModelDropdown.grid(row=1, column=2, sticky='W', ipadx=115)  ## Positions the Menu
    tk.Checkbutton(ModelFrame,
                   text='NFG Testing ONLY',
                   variable=NFG_Only
                   ).grid(row=2, column=1, columnspan=2, sticky='W')
    tk.Checkbutton(ModelFrame,
                   text='DUT power remain ON after testing',
                   variable=PWR_ON
                   ).grid(row=3, column=1, columnspan=2, sticky='W')
    tk.Checkbutton(ModelFrame,
                   text='No NFG Testing',
                   variable=No_NFG
                   ).grid(row=4, column=1, columnspan=2, sticky='W')

    # Temperature Frame
    TemperatureFrame = tk.LabelFrame(master, text="Temperature Menu")
    TemperatureFrame.grid(row=2, column=1, columnspan=3, sticky='W', padx=2, pady=2)
    tk.Label(TemperatureFrame, text="Profile: ").grid(row=1, column=1, sticky='W')
    TemperatureDropdown = ttk.Combobox(TemperatureFrame, value=TemperatureProfiles)  ##Makes the menu
    TemperatureDropdown.set('None')  ## Set Default Value
    TemperatureDropdown.grid(row=1, column=2, sticky='W', ipadx=115, columnspan=4)  ## Positions the Menu
    tk.Label(TemperatureFrame, text='AUTOST:').grid(row=2, column=1, sticky='E')
    AUTOST_PERIOD_TEXT = tk.Entry(TemperatureFrame, width=2, textvariable=AUTOST)
    AUTOST.set(1)
    AUTOST_PERIOD_TEXT.grid(row=2, column=2, sticky='W', ipadx=2)
    tk.Label(TemperatureFrame, text='(minutes)').grid(row=2, column=2)  # ,sticky='E')

    # Communication Frame
    CommunicationFrame = tk.LabelFrame(master, text="Communication Menu")
    CommunicationFrame.grid(row=3, column=1, columnspan=3, sticky='W', padx=2, pady=2)
    tk.Label(CommunicationFrame, text='COM Port:').grid(row=2, column=1, sticky='E')
    USB_Port_Text = tk.Entry(CommunicationFrame)
    USB_Port_Text.grid(row=2, column=2, sticky='W', ipadx=2)
    tk.Label(CommunicationFrame, text='IP:').grid(row=2, column=3, sticky='E')
    IP_Text = tk.Entry(CommunicationFrame)
    IP_Text.grid(row=2, column=4, sticky='W', ipadx=2)
    tk.Label(CommunicationFrame, text='Port:').grid(row=3, column=3, sticky='E')
    IP_Port_Text = tk.Entry(CommunicationFrame)
    IP_Port_Text.grid(row=3, column=4, sticky='W', ipadx=2)
    tk.Label(CommunicationFrame, text="Attenuation:").grid(row=6, column=1, sticky='W')
    Attenuation = ttk.Combobox(CommunicationFrame, value=atten)
    Attenuation.set('0.5')
    Attenuation.grid(row=6, column=2, sticky='W', pady=5)
    tk.Label(CommunicationFrame,
             text='            Gain = Max Gain - Attenuation'
             ).grid(row=6, column=3, columnspan=2, sticky='E')
    tk.Checkbutton(CommunicationFrame,
                   text=F'Autotune DSA to target unit gain',
                   variable=Tune_DSA_EN
                   ).grid(row=8, column=1, sticky='W', columnspan=2)
    tk.Checkbutton(CommunicationFrame,
                   text='Disable Communication',
                   variable=COMM_DISABLE
                   ).grid(row=7, column=1, columnspan=4, sticky='W', pady=2)

    # Information Frame
    InfoFrame = tk.LabelFrame(master, text="Information Menu")
    InfoFrame.grid(row=4, column=1, columnspan=3, sticky='W', padx=2, pady=2)
    tk.Label(InfoFrame, text='Serial Number:').grid(row=1, column=1, sticky='E')
    SNText = tk.Entry(InfoFrame)
    SNText.grid(row=1, column=2, sticky='W')
    tk.Label(InfoFrame, text='Stock Number:').grid(row=1, column=3, sticky='E')
    StockText = tk.Entry(InfoFrame)
    StockText.grid(row=1, column=4, sticky='W')
    tk.Label(InfoFrame, text='Supplier S/N:').grid(row=2, column=1, sticky='E')
    MnfText = tk.Entry(InfoFrame)
    MnfText.grid(row=2, column=2, sticky='W')
    tk.Label(InfoFrame, text='Colour:').grid(row=2, column=3, sticky='E')
    ColorText = tk.Entry(InfoFrame)
    ColorText.grid(row=2, column=4, sticky='W')
    tk.Label(InfoFrame, text='Customer:').grid(row=3, column=1, sticky='E')
    CxText = tk.Entry(InfoFrame)
    CxText.grid(row=3, column=2, sticky='W')
    tk.Label(InfoFrame, text='PO Number:').grid(row=3, column=3, sticky='E')
    POText = tk.Entry(InfoFrame)
    POText.grid(row=3, column=4, sticky='W')
    tk.Label(InfoFrame, text='Tester:').grid(row=4, column=1, sticky='E')
    YouText = tk.Entry(InfoFrame)
    YouText.grid(row=4, column=2, sticky='W')

    width = 10
    tk.Label(master, text='Press CTRL+C to stop ATE during test.').grid(row=7, column=2)
    tk.Button(master, text='BEGIN', command=StartATE, width=width).grid(row=7, column=3, sticky='E', padx=2, pady=2)

    master.mainloop()


def StartATE():
    SN = str(SNText.get())
    tester = str(YouText.get())
    if SN == '':
        SN = 'No_Name'

    folder_path = Make_Folder(ATE2_PATH, SN)

    info = F"| Unit: {SN} | Tester: {tester} | Script: {os.path.basename(__file__)} |"

    global logger
    logger = setup_logger(F"{SN}", F"{folder_path}\\{SN}.log", '[%(levelname)s] %(message)s', True)
    ATE_logger = setup_logger('ATE2', 'ATE2.log', '%(asctime)s || %(message)s', False)

    ATE_logger.info(info)
    logger.info(info)
    logger.info(F"Setup logger: {folder_path}\\{SN}.log")

    try:
        main(folder_path)
    except Exception as e:
        #error = Catch_Exception(e)
        error = traceback.format_exc()
        logger.critical(error)
        StopATE()


def StopATE():
    if not PWR_ON.get():
        Pwr_off()
    if Resources['DC']:
        DC = dccontrol.Client(2, DC_VISA)
        logger.info(DC.query('$off,*'))
        DC.close()
    if Resources['SG']:
        SG.Output(False)
    if Resources['SA']:
        SA.write(':ABORt')
    if Resources['TChamber']:
        Thermo.STOP(TChamber)
    logger.info('Stopping ATE')
    logger.handlers.clear()
    sys.exit()


def Catch_Exception(exception):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    error = [exc_type, fname, exc_tb.tb_lineno, exception]

    return error


def Settings_List_Options(settings_list):
    options = []
    for i in range(len(settings_list)):
        options.append(settings_list[i][0]['Name'])
    return options


settings_master_list = DS_Gen.Load_All_Settings(settings_path)
Equipment_Init()
GUI(settings_master_list)

# Version Log:
# ------------------------------
# R1.0 -            -   Ported over code from LNB ATE V1.6.py
# R2.0 -            -   Rearranged NFG test within main() to account for non-remote NS inbetween tests
# R2.1 -            -   Added Image Filter for prettier screenshots. Orbital_Image_Filter_R3.py
# R2.2 - 20210902   -   Included new datasheet generator Revision 2.0
# R3.0 - 20211103   -   Remove Attenuation2 and 3.
#                       Added optional temperature profile testing.
#                       Updated to ATE2_Datasheet_Generator_R3.py
# R4.0 - 20220315   -   Revamp waiting time for temperature cycle to include $getst messages.
#                       Updated Communication Menu to WBKaLNB connection protocols with LNB_Communication_R1.py.
#                       Updated equipment connection reliability and recording
# R5.0 - 20220318   -   Added sub folders to temperature cycle testing to save datasheets
# R6.0 - 20220421   -   Transposed ATE2_Settings.csv, accounted for it in ATE2_Datasheet_Generator_R4.py.
#                       Added NFG only testing option.
#                       Added attenuation management when changing bands.
#                       Upgraded to LNB_Communication_R5.py
# R7.0 - 20220513   -   Minor upgrades to fall in line with the release of ATE1 R2.py.
#                       Increase voltage for temperature cycle in low temperatures.
#                       Check LO lock before NFG testing
# R7.1 - 20220606   -   Read voltage out of DMM, not PS. Save .csv for phase noise, gain, and S11/S22 testing.
# R7.2 - 20220614   -   Update PM VISA, phase noise remove RAW, LO lock delay for BDC, NFG scale for BDC
# R7.3 - 20220615   -   Voltage query to float, tidy data dict, get_LO_Lock delay timing
# R8.0 - 20220707   -   Oscilloscope addition for 22kHz tone band switching.
#                       Signal generator power limit.
#                       Temperature dict rename.
#                       Datasheet Gen R5.
#                       Remove shutil.
#                       Logging instead of print statements
#                       IMGREJ and Band Spur IF frequency change from 1G to unit dependant starting freq
# R9.0 - 20221104   -   Changed SG to Holzworth HSX9001A. Both CH1 and CH2 need to be controlled.
#                       Fixed bugs according to ATE1 R2.2: Added option to disable attenuation settings.
#                       Increased coms delay from 0.15 to 0.5 to avoid missing build string.
#                       Added $pll,* command for TriKa/DKa units. Accounted for DSA set =/= band set.
#                       Spur test ignore harmonic
# R9.1 - 20230530   -   Added changes from R9.1_TX5_Data_Collection_DSA.py written by Tian Hao Xu
#                       \\silvertip.local\data\Production does not like Image Filter function. Will filter image on
#                       local drive then use shutil to move to destination
# R9.2 - 20240417   -   Many undocumented changes... TBD
#                       Added typ/min to P1dB measurement
#                       Updated from HP3478A -> Keysight34465A for AMM readout.
#                       A -> mA


