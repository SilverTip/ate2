import xlsxwriter
import xlwings as xw
import json
import time
import sys
import os
import csv
import math

data_collection_location_path = "\\\silvertip.local\\data\\Production\\_To_be_REVIEWED\\In_Progress\\ATE_Data_Collection"

def JSONloading():
    path = input('Input directory containing units to add to list: ')
    subdirectories = [dI for dI in os.listdir(path) if os.path.isdir(os.path.join(path,dI))]
    name = input('Input model name of the unit to add to the list: ')
    docket_number = input('Please enter the docket number of the units: ')
    Create = 1
    for subdirectory in subdirectories:
        if docket_number in subdirectory:
            data_location = path + '\\' + subdirectory
            DataCollection(data_location,name, Create)

def JSONloadingATE(path, name, docket_number):
    subdirectories = [dI for dI in os.listdir(path) if os.path.isdir(os.path.join(path,dI))]
    Create = 1
    for subdirectory in subdirectories:
        if docket_number in subdirectory:
            data_location = path + '\\' + subdirectory
            DataCollection(data_location,name, Create)

def DataCollection(datapath, name, Create): 
    header = ['Date','Customer','S/N','Temperature','Max NF','Average NF','Average Gain','Max Gain','Min Gain','P1dB','OIP3','Image Rejection','S11','S22','DSA','Phase Noise(10Hz)', '100Hz', '1kHz','10kHz','100kHz','1MHz','10MHz','100MHz']
    collections = os.listdir(data_collection_location_path) 
    for collection in collections:
        if collection.endswith('.xlsx') and name in collection and '~' not in collection:
            print(collection)
            Append_Collection(data_collection_location_path, name, datapath)
            Create = 0   
    if Create == 1:
        Create_Collection(data_collection_location_path, name, datapath, header)       

def Create_Collection(folder_path, name, datapath, header):
    workbooklocation = folder_path + '\\' + name + '_Data_Collection.xlsx'   
    workbook = xlsxwriter.Workbook(workbooklocation)    

    files = os.listdir(datapath)
    for file in files: #Find all .json files in folder
        if file.endswith('.json'):
            if file.count("Settings"):
                continue
            datalocation = datapath + '\\' + file
            data, raw_data = Load_JSON(datalocation)
            worksheetname = 'Band ' + str(raw_data['Band'])+ ' Data Collection'
            worksheet= workbook.add_worksheet(worksheetname)
            bold = workbook.add_format({'bold': True})
            not_bold = workbook.add_format({'bold': False})
            worksheet.write_row('A1', header, bold)
            worksheet.write('A2','Averaging')
            ch = 'B'
            for i in range(0,23):
                cell = ch + '2'
                worksheet.write_formula(cell,F'=IFERROR(AVERAGE({ch}3:{ch}1000),"N/A")')
                ch = chr(ord(ch) + 1)
            #worksheet.write_dynamic_array_formula('B2:W2','=IFERROR(AVERAGE(B3:B1000),"N/A")')
            worksheet.write_row('A3', data, not_bold)

    workbook.close()

def Append_Collection(folder_path, name, datapath):
    files = os.listdir(datapath)
    workbookname = name + '_Data_Collection' + '.xlsx'
    worksheetpath = folder_path + '\\' + workbookname
    workbook = xw.Book(worksheetpath)
    for file in files: #Find all .json files in folder
        if file.endswith('.json'):
            if file.count("Settings"):
                continue
            datalocation = datapath + '\\' + file
            data, raw_data = Load_JSON(datalocation)
            worksheetname = 'Band ' + str(raw_data['Band'])+ ' Data Collection'
            worksheet = xw.sheets[worksheetname]
            row = xw.sheets[worksheetname].range('A' + str(xw.sheets[worksheetname].cells.last_cell.row)).end('up').row + 1
            SN_numbers = worksheet['B2:B'+str(row)].value
            # if data[1] in SN_numbers:
            #     ch = 'A'
            #     for i in range(2,int(row)):
            #         if data[1] == worksheet['B' + str(i)].value:
            #             last_col = worksheet.range("A1").end("right").column
            #             for col in range(1,last_col):
            #                 cell = ch + str(i)
            #                 if worksheet[cell].value == 'Empty' and data[col-1] != 'Empty':
            #                     worksheet[cell].value = data[col-1]
            #                 ch = chr(ord(ch) + 1)
            #else:
            worksheet['A'+str(row)].value=data

    workbook.save()
    time.sleep(1)
    workbook.app.quit()

def Load_JSON(file):
    print(file)
    with open(file, 'r') as f:
        data = json.load(f)
    f.close()
    needed_data = []
    needed_data.extend([data['Date'] + ' ' + data['Time'],data['Name'],data['Orbital_SN'],data['NFG']['max_NF'],data['NFG']['averageNF'],data['NFG']['averageGain'],data['NFG']['max_Gain'],data['NFG']['min_Gain']])
    print('hi')
    try:
        needed_data[3:3] = [data['Temperature']['Value']]
    except:
        needed_data[3:3] = ['Not Available']
    if data['P1dB']['P1dB']:
        needed_data.extend([data['P1dB']['P1dB'],data['P1dB']['OIP3'],data['image_rej']])
    else:
        needed_data.extend(['Empty','Empty','Empty'])
    if data['S11']:
        needed_data.extend([data['S11'],data['S22']])
    else:
        needed_data.extend(['Empty','Empty'])
    if data['NFG']['DSAhex']:
        needed_data.append(data['NFG']['DSAhex'])
    else:
        needed_data.extend(['Empty'])
    if data['phase']['10Hz']:
        needed_data.extend([data['phase']['10Hz'],data['phase']['100Hz'],data['phase']['1kHz'],data['phase']['10kHz'],data['phase']['100kHz'],data['phase']['1MHz']])
    else:
        needed_data.extend(['Empty','Empty','Empty','Empty','Empty','Empty','Empty','Empty'])
    if data['phase']['10MHz']:
        needed_data.append(data['phase']['10MHz'])
    if data['phase']['100MHz']:
        needed_data.append(data['phase']['100MHz'])
    print('Data Loaded')
    return needed_data, data

def Excelloading():
    path = input('Input directory containing units to add to list: ')
    name = input('Input model name of the unit to add to the list: ')
    docket_number = input('Please enter the docket number of the units: ')
    Create = 1
    files = os.listdir(path)
    for file in files: #Find all .json files in folder
        if file.endswith('.xlsx') and '~' not in file and docket_number in file:
            if file.count("Datasheet"):
                datasheetlocation =  path + '\\' + file
                DataCollection_Excel(datasheetlocation,name, Create)

def DataCollection_Excel(datapath, name, Create): 
    header = ['Date','Customer','S/N','Temperature','Max NF','Average NF','Average Gain','Max Gain','Min Gain','P1dB','OIP3','Image Rejection','S11','S22','DSA','Phase Noise(10Hz)', '100Hz', '1kHz','10kHz','100kHz','1MHz','10MHz','100MHz']
    collections = os.listdir(data_collection_location_path) 
    for collection in collections:
        if collection.endswith('.xlsx') and name in collection and '~' not in collection:
            Append_Collection_Excel(data_collection_location_path, name, datapath)
            Create = 0   
    if Create == 1:
        Create_Collection_Excel(data_collection_location_path, name, datapath, header)     

def Create_Collection_Excel(folder_path, name, datapath, header): 
    workbookname = name + '_Data_Collection' + '.xlsx'
    worksheetpath = folder_path + '\\' + workbookname
    workbook = xw.Book()
    datasheet = xw.Book(datapath)
    d = 1
    variables = ['P1dB','OIP3','Image Rejection','Input VSWR','Output VSWR','DSA']
    data = []
    for sheet in datasheet.sheets: #Append all sheets from .xlsx file to new file
        if 'Datasheet' in sheet.name:
            data.append(sheet['B4'].value)
            data.append(sheet['D2'].value)
            data.append(sheet['D3'].value)
            data.append('Not Available')
            IF = sheet['C8'].value
            IFstart = float(IF[:4].replace('-','').replace(' ',''))*1000000
            IFstop = float(IF[4:].replace('-','').replace(' ',''))*1000000
            found = 0
            for var in variables:
                lastrow = sheet.range('G' + str(sheet.cells.last_cell.row)).end('up').row + 1
                for row in range(7,lastrow):
                    cell = 'G' + str(row)
                    datacell = 'I' + str(row)
                    if var in sheet[cell].value and sheet[datacell].value is not None:
                        data.append(sheet[datacell].value)
                        found = 1 
                if found == 0: 
                    data.append('Empty')
                found = 0
            PN = sheet['N7:N14'].value
            if PN[0] == None:
                PN = ['Empty','Empty','Empty','Empty','Empty','Empty','Empty','Empty']
            data.extend(PN)
        elif 'Data' in sheet.name:
            lastrow = sheet.range('A' + str(sheet.cells.last_cell.row)).end('up').row
            frequency = sheet['A6:A'+str(lastrow)].value
            NF = sheet['B6:B'+str(lastrow)].value
            Gain = sheet['C6:C'+str(lastrow)].value
            x =0 
            InBandNF = []
            InBandGain = []
            for f in frequency:
                if int(IFstart) <= int(f) <= int(IFstop):
                    InBandNF.append(NF[x])
                    InBandGain.append(Gain[x])
                x += 1
            data[4:4] = [max(InBandNF),sum(InBandNF)/len(InBandNF),sum(InBandGain)/len(InBandGain),max(InBandGain),min(InBandGain)]
            d +=1
        if d%2 == 0:
            workbook.activate(steal_focus=True)
            worksheetname = 'Band ' + str(int(d/2))+ ' Data Collection'
            worksheet = workbook.sheets.add(worksheetname)
            worksheet['A1'].value=header
            worksheet['A2'].value = 'Averaging'
            worksheet.range('B2').formula = '=IFERROR(AVERAGE(B3:B1000),"N/A")'
            formula = worksheet.range("B2").formula
            worksheet.range("B2:W2").formula = formula
            worksheet['A3'].value=data
            data.clear()
            d +=1
    for sheet in workbook.sheets:
        if 'Sheet' in sheet.name: 
            sheet.delete()
    datasheet.close()
    workbook.save(worksheetpath)
    time.sleep(1)
    workbook.app.quit()

def Append_Collection_Excel(folder_path, name, datapath):
    workbookname = name + '_Data_Collection' + '.xlsx'
    worksheetpath = folder_path + '\\' + workbookname
    workbook = xw.Book(worksheetpath)
    
    datasheet = xw.Book(datapath)
    d = 1
    IFstart = 0
    IFend = 0
    variables = ['P1dB','OIP3','Image Rejection','Input VSWR','Output VSWR']
    data = []
    for sheet in datasheet.sheets: #Append all sheets from .xlsx file to new file
        if 'Datasheet' in sheet.name:
            data.append(sheet['B4'].value)
            data.append(sheet['D2'].value)
            data.append(sheet['D3'].value)
            data.append('Not Available')
            IF = sheet['C8'].value
            IFstart = float(IF[:4].replace('-','').replace(' ',''))*1000000
            IFstop = float(IF[4:].replace('-','').replace(' ',''))*1000000
            found = 0
            for var in variables:
                lastrow = sheet.range('G' + str(sheet.cells.last_cell.row)).end('up').row + 1
                for row in range(7,lastrow):
                    cell = 'G' + str(row)
                    datacell = 'I' + str(row)
                    if var in sheet[cell].value and sheet[datacell].value is not None:
                        data.append(sheet[datacell].value)
                        found = 1 
                if found == 0: 
                    data.append('Empty')
                found = 0
            if bool(sheet['H4'].value) == True:
                data.append(sheet['H4'].value)
            else:
                data.append('Empty')
            PN = sheet['N7:N14'].value
            if PN[0] == None:
                PN = ['Empty','Empty','Empty','Empty','Empty','Empty','Empty','Empty']
            data.extend(PN)
        elif 'Data' in sheet.name:
            lastrow = sheet.range('A' + str(sheet.cells.last_cell.row)).end('up').row
            frequency = sheet['A6:A'+str(lastrow)].value
            NF = sheet['B6:B'+str(lastrow)].value
            Gain = sheet['C6:C'+str(lastrow)].value
            x =0 
            InBandNF = []
            InBandGain = []
            for f in frequency:
                if int(IFstart) <= int(f) <= int(IFstop):
                    InBandNF.append(NF[x])
                    InBandGain.append(Gain[x])
                x += 1
            data[4:4] = [max(InBandNF),sum(InBandNF)/len(InBandNF),sum(InBandGain)/len(InBandGain),max(InBandGain),min(InBandGain)]
            d +=1
        if d%2 == 0:
            workbook.activate(steal_focus=True)
            worksheetname = 'Band ' + str(int(d/2))+ ' Data Collection'
            worksheet = workbook.sheets[worksheetname]
            row = workbook.sheets[worksheetname].range('A' + str(xw.sheets[worksheetname].cells.last_cell.row)).end('up').row + 1
            SN_numbers = worksheet['B2:B'+str(row)].value
            # if data[1] in SN_numbers:
            #     ch = 'A'
            #     for i in range(2,int(row)):
            #         if data[1] == worksheet['B' + str(i)].value:
            #             last_col = worksheet.range("A1").end("right").column
            #             for col in range(1,last_col):
            #                 cell = ch + str(i)
            #                 if worksheet[cell].value == 'Empty' and data[col-1] != 'Empty':
            #                     worksheet[cell].value = data[col-1]
            #                 ch = chr(ord(ch) + 1)
            # else:
            worksheet['A'+str(row)].value=data
            d +=1
            data.clear()
            datasheet.activate(steal_focus=True)
    datasheet.close()
    workbook.save()
    time.sleep(1)
    workbook.app.quit()

def main():
    loadingmethod = input('E for Excel and J for json:' )
    if loadingmethod == 'E':
        Excelloading()
    if loadingmethod == 'J':
        JSONloading()


if(__name__ == '__main__'):
	main()

