
import sys
import time
import math
import serial
import json
import os
import shutil
import socket

data = {'IP': '10.0.10.69', 'Socket_Port': 32019}

def Init_Socket(data):
    global sock
    server_address = (data['IP'],data['Socket_Port'])
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(server_address) 


def SocketReadLine(message):
    try:
        reply = sock.recv(1024)
        reply = reply.decode("utf-8")
        reply = reply.split(message)[-1]
    except Exception as e:
        print(e)
        reply = 'FAILED TO COMMUNICATE'

    return reply

def main():
    while(1):
        message = input('Message:')
        sock.sendall(bytes(message + '\r', 'ascii'))
        time.sleep(1)
        reply = SocketReadLine(message)

        print(reply)


Init_Socket(data)
main()


    
